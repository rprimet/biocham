:- module(units,
  [
    set_dimension/3,
    list_dimensions/0
  ]
).

:- use_module(doc).
:- use_module(library(clpfd)).


% store declared units
:- dynamic(k_unit/3).


set_dimension(P, U, V) :-
  biocham_command,
  doc('Declare dimension time^\\argument{U} and volume^\\argument{V}
    for parameter \\argument{P}'),
  type(P, parameter_name),
  type(U, number),
  type(V, number),
  retractall(k_unit(P, _, _)),
  assertz(k_unit(P, U, V)).


:- biocham_silent(clear_model).

list_dimensions :-
  biocham_command,
  doc('
    Checks the (time) dimensions of all parameters of the current model.
    \\begin{example}
    \\trace{
      biocham: MM(v, k) for A => B.
      biocham: parameter(k = 1, v = 1).
      biocham: list_dimensions.
    }
    \\end{example}
  '),
  findall((_ - P), item([kind: parameter, item: parameter(P=_)]), L),
  check_set_dim(L),
  with_current_ode_system(findall(E, ode(_, E), ExprList)),
  find_parameter_dim_rec(ExprList, L),
  !,
  keysort(L, LL),
  write_dimensions(LL).

list_dimensions :-
  print_message(error, no_satisfy),
  nl,
  nb_getval(current_dim_term, T),
  nb_getval(current_dim_expr, E),
  print_message(error, failed_dimension(T, E)).


check_set_dim([]).

check_set_dim([((U, V) - P) | L]) :-
  (
    k_unit(P, U, V),
    !
  ;
    true
  ),
  check_set_dim(L).


find_parameter_dim_rec([], _).

find_parameter_dim_rec([0 | EL], L) :-
  % no kinetics, i.e. constant species
  print_message(warning, constant_species),
  find_parameter_dim_rec(EL, L).

find_parameter_dim_rec([E | EL], L) :-
  debug(units, "Inferring for ~w with ~w", [E, L]),
  % all kinetics are Time^(-1) Volume^0
  nb_setval(current_dim_expr, E),
  nb_setval(current_dim_term, E),
  find_parameter_dim(E, (-1, 0), L),
  find_parameter_dim_rec(EL, L).


find_parameter_dim(A, Dim, L) :-
  debug(units, "checking ~w for ~w with ~w", [A, Dim, L]),
  fail.

find_parameter_dim(X + Y, Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(X - Y, Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(min(X,Y), Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(max(X,Y), Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(-Y, Dim, L)  :-
  !,
  find_parameter_dim(Y, Dim, L).

% FIXME? force dimensionless exponentiation
find_parameter_dim(X ^ Y, (DimT, DimV), L) :-
  !,
  find_parameter_dim(X, (DT, DV), L),
  find_parameter_dim(Y, 0, L),
  (
    (
      number(Y),
      V = Y,
      DT1 = DimT,
      DV1 = DimV,
      DT2 = DT,
      DV2 = DV
    ;
      parameter_value(Y, V),
      number(V),
      DT1 = DimT,
      DV1 = DimV,
      DT2 = DT,
      DV2 = DV
    ;
      Y = 1/Z,
      DT1 = DT,
      DV1 = DV,
      DT2 = DimT,
      DV2 = DimV,
      (
        number(Z),
        V = Z
      ;
        parameter_value(Z, V),
        number(V)
      )
    )
  ->
    (
      integer(V)
    ->
      DT1 #= DT2 * V,
      DV1 #= DV2 * V
    ;
      DT #= 0,
      DV #= 0,
      DimT #= 0,
      DimV #= 0
    )
  ).

% FIXME? correct? force Dim=0?
find_parameter_dim(log(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, log(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(exp(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, exp(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(sin(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, sin(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(cos(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, cos(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(N, (DimT, DimV), _) :-
  number(N),
  !,
  DimT #= 0,
  DimV #= 0.

find_parameter_dim('Time', (DimT, DimV), _) :-
  !,
  nb_setval(current_dim_term, 'Time'),
  DimT #= 1,
  DimV #= 0.

find_parameter_dim(X * Y, (DimT, DimV), L) :-
  !,
  find_parameter_dim(X, (DT1, DV1), L),
  find_parameter_dim(Y, (DT2, DV2), L),
  nb_setval(current_dim_term, X * Y),
  DimT #= DT1 + DT2,
  DimV #= DV1 + DV2.

find_parameter_dim(X / Y, (DimT, DimV), L) :-
  !,
  find_parameter_dim(X, (DT1, DV1), L),
  find_parameter_dim(Y, (DT2, DV2), L),
  nb_setval(current_dim_term, X / Y),
  DimT #= DT1 - DT2,
  DimV #= DV1 - DV2.

find_parameter_dim(if(then(_C, else(A, B))), Dim, L) :-
  !,
  find_parameter_dim(A, Dim, L),
  find_parameter_dim(B, Dim, L).

find_parameter_dim(P, (DimT, DimV), L) :-
  parameter_value(P, _),
  !,
  member(((DT, DV) - P), L),
  DimT #= DT,
  DimV #= DV.

find_parameter_dim([M], (DimT, DimV), _) :-
  !,
  nb_setval(current_dim_term, [M]),
  DimT #= 0,
  DimV #= -1.

find_parameter_dim(M, (DimT, DimV), _) :-
  enumerate_molecules(L),
  member(M, L),
  !,
  DimT #= 0,
  DimV #= -1.


write_dimensions([]).

write_dimensions([((DT, DV) - P) | L]) :-
  (
    number(DV),
    number(DV)
  ->
    format("~w has dimension time^(~w).volume^(~w)~n", [P, DT, DV])
  ;
    debug(units, "~w has unknown dimension: ~w", [P, (DT, DV)]),
    format("~w has unknown dimension~n", [P])
  ),
  write_dimensions(L).


prolog:message(constant_species) -->
  ['some species are constant in this model'-[]].

prolog:message(no_satisfy) -->
  ['Could not satisfy time units constraints on parameter dimensions.'-[]].

prolog:message(failed_dimension(T, E)) -->
  ['Failed on ~w in ~w~na kinetic expression which should be time^-1'-[T, E]].
