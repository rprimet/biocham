:- module(
  multistability,
  [
    % Commands
    multistability_graph/0,
    % Public API
    check_multistability_graph/0
    ]
  ).

:- use_module(doc).
:- use_module(counters).
:- use_module(invariants).

:- use_module(multistability_command).


:- devdoc('\\section{Commands}'). 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Permutation generation %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Generating all Transpositions
transposition(L, L).
transposition(L, Perm) :-
  transposition_aux(L, L, Perm).

transposition_aux(L, [H1 | T], Perm) :-
  transposition_aux(L, T, Perm)
;
  member(H2, T),
  check_swaps_for_transpo([H1, H2]),
  transpo_2_perm(L, H1, H2, Perm).

check_swaps_for_transpo([H1, H2]) :-
  forall(
    conflict_in_swap(_, Conflict),
    (
      member(H1, Conflict);member(H2, Conflict)
    )
  ).

perm_2_transpo(Swaps, [H1, H2]) :-
  member([H1, H2], Swaps),
  \+(H1==H2).

transpo_2_perm([], _, _, []).
transpo_2_perm([H1 | T], H1, H2, [H2 | PermT]) :-
  transpo_2_perm(T, H1, H2, PermT).
transpo_2_perm([H2 | T], H1, H2, [H1 | PermT]) :-
  transpo_2_perm(T, H1, H2, PermT).
transpo_2_perm([H | T], H1, H2, [H | PermT]) :-
  \+(H==H1;H==H2),
  transpo_2_perm(T, H1, H2, PermT).

%%%%% Generating all Permutations
permute([], []).
permute([H | T], P) :-
  permute(T, PT),
  select(H, P, PT).

check_swaps_conflicts(S2) :-
  forall(
    conflict_in_swap(S1, Conflict),
    (
      swap_difference(S1, S2, S21),
      findall(
        E,
        member([E, E], S21),
        Fixed_points
      ),
      \+(
        forall(
          member(Spe, Conflict),
          member(Spe, Fixed_points)
        )
      )
    )
  ).

%%%%% 
identity_swap([]) :- !.
identity_swap([[X, X] | T]) :-
  identity_swap(T), !.

swap_difference([], _, []).
swap_difference([[OS1, Origin] | S1_tail], S2, [[Origin, Target] | S21_tail]) :-
  member([OS1, Target], S2),
  swap_difference(S1_tail, S2, S21_tail).

assemble([], [], []).
assemble([S_head | S_tail], [P_head | P_tail], [[S_head, P_head] | Swaps_tail]) :-
  assemble(S_tail, P_tail, Swaps_tail).

get_swaps(Species, Swaps) :-
  get_option(test_permutations, TP),
  (
    TP == yes
  ->
    (
      permute(Species, Perm),
      assemble(Species, Perm, Swaps),
      (Perm=Species
      ->
        true
;
        check_swaps_conflicts(Swaps)
      )
    )
;
    (
      transposition(Species, Perm),
      assemble(Species, Perm, Swaps)
    )
  ).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% List operations %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Union of two ordered lists
list_union([], L, L) :- !.
list_union(L, [], L) :- !.
list_union([H1 | T1], [H2 | T2], L) :-
  (H1 > H2
  ->
    (
      list_union([H1 | T1], T2, TL),
      L = [H2 | TL]
    )
;
    (
      list_union(T1, [H2 | T2], TL),
      L = [H1 | TL]
    )
  ), !.

%%% Remove consecutive duplicates in an ordered list
remove_duplicates([], []).
remove_duplicates([H, H | T], L) :-
  remove_duplicates([H | T], L), !.
remove_duplicates([H | T], [H | L]) :-
  remove_duplicates(T, L).

%%% Fail if there are multiple elements in an ordered list
%%% except if it is Vertex passed as argument
remove_duplicates([], _, []).
remove_duplicates([Vertex, Vertex | T], Vertex, L) :-
  remove_duplicates([Vertex | T], Vertex, L), !.
remove_duplicates([H, H | _], _, _) :-
  !, fail.
remove_duplicates([H | T], Vertex, [H | L]) :-
  remove_duplicates(T, Vertex, L).

%%% Same with two Vertices
remove_duplicates([], _, _, []).
remove_duplicates([Vertex1, Vertex1 | T], Vertex1, Vertex2, L) :-
  remove_duplicates([Vertex1 | T], Vertex1, Vertex2, L), !.
remove_duplicates([Vertex2, Vertex2 | T], Vertex1, Vertex2, L) :-
  remove_duplicates([Vertex2 | T], Vertex1, Vertex2, L), !.
remove_duplicates([H, H | _], _, _, _) :-
  !, fail.
remove_duplicates([H | T], Vertex1, Vertex2, [H | L]) :-
  remove_duplicates(T, Vertex1, Vertex2, L).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Conservation laws %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Generating conservation laws
conserv_laws_for_multistability_graph :-
  retractall(conservLaws(_)),
  retractall(conservLawsParent(_)),
  with_output_to(atom(_), invariants:find_pinvar),
  findall(
    ConservNames,
    (
      invariants:base_mol(List),
      maplist(lemon:no_coeff, List, ConservNames)
    ),
    ConservList
  ),
  % write('Conserv laws are : '), write(ConservList), write('\n'),
  asserta(conservLaws([])),
  asserta(conservLawsParent(ConservList)).

%%% Conservation laws are adapted to the permutation
adapt_conserv_aux(_, [], []) :- !.
adapt_conserv_aux(Swaps, [H | T], [NewH | NewT]) :-
  member([H, NewHName], Swaps),
  correspond_vertex_name_id(NewHName, NewH),
  adapt_conserv_aux(Swaps, T, NewT), !.

adapt_conserv_laws(_, [], []) :- !.
adapt_conserv_laws(Swaps, [HList | TList], [NewHList | NewTList]) :-
  adapt_conserv_aux(Swaps, HList, NewHListUnsorted),
  sort(NewHListUnsorted, NewHList),
  adapt_conserv_laws(Swaps, TList, NewTList), !.
%%% 
remove_vertex_from_conserv_laws(_, [], []) :- !.
remove_vertex_from_conserv_laws(VertexName, [H | T], L) :-
  member(VertexName, H),
  remove_vertex_from_conserv_laws(VertexName, T, L), !.
remove_vertex_from_conserv_laws(VertexName, [H | T], [H | L]) :-
  remove_vertex_from_conserv_laws(VertexName, T, L), !.

remove_vertex_from_conserv_laws(VertexName) :-
  conservLawsParent(ConservList),
  retractall(conservLawsParent(_)),
  remove_vertex_from_conserv_laws(VertexName, ConservList, NewConservList),
  asserta(conservLawsParent(NewConservList)).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Gauss system for change of sign %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reset_gauss_system :-
  retractall(loop_system_complete(_)),
  retractall(loop_gauss_system(_)),
  asserta(loop_system_complete([])),
  asserta(loop_gauss_system([])).

%%% Add a loop to the gauss system
add_loop_to_system(Loop) :-
  loop_gauss_system(System),
  pass_loop_in_system(Loop, System, [Species, Sign], Pivots_Used),
  (
    Species = [_ | _]
  ->
    (
      loop_system_complete(System_complete),
      New_System = [[Species, Sign] | System],
      New_System_complete = [Loop | System_complete],
      retractall(loop_gauss_system(_)),
      retractall(loop_system_complete(_)),
      asserta(loop_gauss_system(New_System)),
      asserta(loop_system_complete(New_System_complete))
    )
  ;
    (
      Sign == +
    ->
      (
        get_option(test_transpositions, TT),
        get_option(test_permutations, TP),
        loop_system_complete(System_complete),
        generate_conflict(System, System_complete, Pivots_Used, Species_conflict),
        get_current_graph(GId),
        get_attribute(GId, swaps_used=Swaps),
        (
          TP == yes
        ->
          asserta(conflict_in_swap(Swaps, Species_conflict))
        ;
          (
            TT == yes
          ->
            (
              perm_2_transpo(Swaps, [H1, H2]),
              (
                (
                  member(H1, Species_conflict)
                ;
                  member(H2, Species_conflict)
                )
              ->
                (
                  true
                )
              ;
                (
                  asserta(conflict_in_swap([H1, H2], Species_conflict))
                )
              )
            )
          ;
            (
              true
            )
          )
        ),
        !,
        fail
      )
    ;
      true
    )
  ).

pass_loop_in_system(X, [], X, []).
pass_loop_in_system(Loop, [[System_Spe, Sign_Spe] | T], [Species, Sign], Pivots_Used) :-
  pass_loop_in_system(Loop, T, [Species_Aux, Sign_Aux], Pivots_Used_Old),
  (
    Species_Aux = [_ | _]
  ->
    (
      System_Spe = [Pivot | _],
      (
        member(Pivot, Species_Aux)
      ->
        (
          ord_symdiff(Species_Aux, System_Spe, Species),
          system_sign_rule(Sign_Aux, Sign_Spe, Sign),
          Pivots_Used = [Pivot | Pivots_Used_Old]
        )
      ;
        (
          Species = Species_Aux,
          Sign = Sign_Aux,
          Pivots_Used = Pivots_Used_Old
        )
      )
    )
  ;
    (
      Species = Species_Aux,
      Sign = Sign_Aux,
      Pivots_Used = Pivots_Used_Old
    )
  ).

%%% Solve the system
generate_change_sign_from_system([], X, X).
generate_change_sign_from_system([[Species, Sign] | T], Species_List, Species_Change_Sign) :-
  set_counter(count_species_common, 0),
  forall(
    (
      member(S, Species_List),
      member(S, Species)
    ),
    count(count_species_common, _)
  ),
  peek_count(count_species_common, Count),
  NSwaps is mod(Count, 2),
  (
    (
      Sign == +, NSwaps == 0
    ;
      Sign == -, NSwaps == 1
    )
  ->
    Species = [Pivot | _],
    generate_change_sign_from_system(T, [Pivot | Species_List], Species_Change_Sign)
  ;
    generate_change_sign_from_system(T, Species_List, Species_Change_Sign)
  ).

%%% Return all species involved in positive feedback loops that cannot be removed
%%% Used to remove some permutations
generate_conflict([], [], [], []) :- !.
generate_conflict([[[P | _], _] | TSystem], [[S, _] | TSystem_complete], [P | TPivots], Species_conflict) :-
  generate_conflict(TSystem, TSystem_complete, TPivots, Species_conflict_aux),
  conserv_names_to_id(S_Name_aux, S),
  list_to_ord_set(S_Name_aux, S_Name),
  ord_union(Species_conflict_aux, S_Name, Species_conflict), !.
generate_conflict([_ | TSystem], [_ | TSystem_complete], Pivots, Species_conflict) :-
  generate_conflict(TSystem, TSystem_complete, Pivots, Species_conflict), !.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Graph creation %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For merging arcs
sign_rule(X, X, +) :- !.
sign_rule(_, _, -).

% For solving system
system_sign_rule(X, X, -) :- !.
system_sign_rule(_, _, +).

conserv_names_to_id([], []).
conserv_names_to_id([N | TNames], [NId | TIds]) :-
  correspond_vertex_name_id(N, NId),
  conserv_names_to_id(TNames, TIds).

add_edge_to_multistability_graph(VertexAId, VertexBId, Species_Sorted, Reactions_Sorted, Sign) :-
  get_current_graph(MId),
  item([parent:MId, kind: vertex, id: VertexAId, item: VertexA]),
  item([parent:MId, kind: vertex, id: VertexBId, item: VertexB]),
  (
    item([parent:MId, kind: edge, item: ExistingEdge, id: EId]), ExistingEdge = (VertexA -> VertexB)
  -> 
    (
      get_attribute(EId, label = Label_list),
      get_attribute(EId, sign = OldSign),
      (
        OldSign == Sign
      ->
        (
          true
        )
      ;
        (
          set_attribute(EId, sign = '±')
        )
      ),
      Label = [VertexAId, VertexBId, Species_Sorted, Reactions_Sorted, Sign],
      New_Label_list = [Label | Label_list],
      set_attribute(EId, label = New_Label_list)
    )
  ;
    add_edge(MId, VertexAId, VertexBId, EdgeABId),
    set_attribute(EdgeABId, sign = Sign),
    set_attribute(EdgeABId, label = [[VertexAId, VertexBId, Species_Sorted, Reactions_Sorted, Sign]])
  ),
  get_attribute(VertexAId, out_deg = OutA),
  get_attribute(VertexBId, in_deg = InB),
  OutA1 is OutA+1,
  InB1 is InB+1,
  set_attribute(VertexAId, out_deg = OutA1),
  set_attribute(VertexBId, in_deg = InB1).



%%%%% Generate the influence graph with the reactions as labels
multistability_graph :-
  biocham_command,
  doc("Creates the influence graph of the current model as by \\command{influence_graph/1} but with the arcs labelled with the reactions they originate from. This is used for command \\command{check_multistability/0}."),
  option(force_graph, yesno, ForceMultistabilityGraph, 'Force the creation of the graph'),
  all_items([kind: graph], AllGraphs),
  (
    ForceMultistabilityGraph == no,
    member(multistability_graph_parent, AllGraphs)
  ->
    select_graph(multistability_graph_parent)
  ;
    delete_items([kind: graph, key: multistability_graph_parent]),
    new_graph,
    set_graph_name(multistability_graph_parent),
    get_current_graph(GraphId),
    multistability_graph(GraphId),
    select_graph(multistability_graph_parent)
  ).

:- initial(option(force_graph: no)).

multistability_graph(GraphId) :-
  lemon:number_reactions,
  retractall(reactions(_, _)),
  set_counter(reactions, 1),
  retractall(correspond_vertex_name_id(_, _)),
  \+ (
    (
      item([kind: reaction, item: Item]),
      reaction(Item, [
        reactants: Reactants,
        inhibitors: Inhibitors,
        products: Products
      ]),
      Origin = Item,
      substract_list(Reactants, Products, Difference),
      (
        member(_ * Input, Reactants),
        InputSign = +
      ;
        member(Input, Inhibitors),
        InputSign = -
      ),
      member(Coefficient * Output, Difference),
      (Coefficient > 0
      ->
        RuleSign = +
      ;
        RuleSign = -
      )
    ;
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegativeInputs,
        sign: RuleSign,
        output: Output
      ]),
      Origin = Item,
      (
        member(Input, PositiveInputs),
        InputSign = +
      ;
        member(Input, NegativeInputs),
        InputSign = -
      )
    ),
    \+ (
      lemon:normalize_reaction(Origin, Normalized, Negated),
      (
        reactions(Normalized, RId)
      ->
        (
          true
        )
      ;
        (
          count(reactions, RId),
          assertz(reactions(Normalized, RId)),
          assertz(reactions(Negated, RId))
        )
      ),
      sign_rule(RuleSign, InputSign, Sign),
      add_vertex(GraphId, Input, InputId),
      add_vertex(GraphId, Output, OutputId),
      (
        correspond_vertex_name_id(Input, InputId)
      ->
        (
          get_attribute(InputId, out_deg=OutDeg),
          NewOutDeg is OutDeg+1,
          set_attribute(InputId, out_deg=NewOutDeg)
        )
      ;
        (
          set_attribute(InputId, out_deg=1),
          set_attribute(InputId, in_deg=0),
          asserta(correspond_vertex_name_id(Input, InputId))
        )
      ),
      (
        correspond_vertex_name_id(Output, OutputId)
      ->
        (
          get_attribute(OutputId, in_deg=InDeg),
          NewInDeg is InDeg+1,
          set_attribute(OutputId, in_deg=NewInDeg)
        )
      ;
        (
          set_attribute(OutputId, in_deg=1),
          set_attribute(OutputId, out_deg=0),
          asserta(correspond_vertex_name_id(Output, OutputId))
        )
      ),
      add_edge(GraphId, InputId, OutputId, EdgeId),
      (
        get_attribute(EdgeId, sign = OldSign)
      ->
        (
          OldSign == Sign
        ->
          true
        ;
          set_attribute(EdgeId, sign = '±')
        )
      ;
        set_attribute(EdgeId, sign = Sign)
      ),
      (
        get_attribute(EdgeId, label = OldLabel)
      ->
        sort([InputId, OutputId], Species_sorted),
        set_attribute(EdgeId, label = [[InputId, OutputId, Species_sorted, [RId], Sign] | OldLabel])
      ;
        sort([InputId, OutputId], Species_sorted),
        set_attribute(EdgeId, label = [[InputId, OutputId, Species_sorted, [RId], Sign]])
      )
    )
  ),
  catch(conserv_laws_for_multistability_graph,
    _,
    (
      asserta(conservLaws([])),
      asserta(conservLawsParent([]))
    )
  ).

%%%%% Copying the graph is faster than generating
copy_multistability_graph(ParentId, Swaps) :-
  delete_items([kind: graph, key: multistability_graph]),
  new_graph,
  set_graph_name(multistability_graph),
  get_current_graph(GraphId),
  retractall(correspond_vertex_name_id(_, _)),
  forall(
    item([parent: ParentId, kind:vertex, item: VertexName, id: VId]),
    (
      member([VertexOrigin, VertexName], Swaps),
      find_item([parent: ParentId, kind: vertex, item: VertexOrigin, id: VOId]),
      add_vertex(GraphId, VertexName, VertexId),
      get_attribute(VId, out_deg=Out),
      get_attribute(VOId, in_deg=In),
      set_attribute(VertexId, out_deg=Out),
      set_attribute(VertexId, in_deg=In),
      asserta(correspond_vertex_name_id(VertexName, VertexId))
    )
  ),
  forall(
    item([parent: ParentId, kind:edge, item: Edge, id: EId]),
    (
      Edge = (VertexA->VertexSwap),
      member([VertexSwap, VertexB], Swaps),
      correspond_vertex_name_id(VertexA, VertexAId),
      correspond_vertex_name_id(VertexB, VertexBId),
      add_edge(GraphId, VertexAId, VertexBId, EdgeId),
      get_attribute(EId, sign=Sign),
      set_attribute(EdgeId, sign=Sign),
      sort([VertexAId, VertexBId], Species_sorted),
      get_attribute(EId, label=L),
      forall(
        member([_, _, _, R, S], L),
      (
        get_attribute(EdgeId, label=OldLabel)
    ->
      (
        set_attribute(EdgeId, label = [[VertexAId, VertexBId, Species_sorted, R, S] | OldLabel])
      )
    ;
      (
        set_attribute(EdgeId, label = [[VertexAId, VertexBId, Species_sorted, R, S]])
      )
    )
  )
)
  ),
  select_graph(multistability_graph),
  conservLawsParent(ConservList),
  retractall(conservLaws(_)),
  % write(Swaps), write(' '), write(ConservList), write('\n'),
  adapt_conserv_laws(Swaps, ConservList, NewConservList),
  asserta(conservLaws(NewConservList)),
  get_current_graph(MultiGraphId),
  set_attribute(MultiGraphId, swaps_used=Swaps).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Pre processing %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

remove_self_loops :-
  get_current_graph(MId),
  forall(
    (
      item([parent:MId, kind: edge, item: Edge, id: EdgeId]),
      Edge = (Vertex -> Vertex)
    ),
    (
      get_attribute(EdgeId, label=Label),
      length(Label, N),
      delete_edge([Edge]),
      add_vertex(MId, Vertex, VertexId),
      get_attribute(VertexId, in_deg=IN),
      get_attribute(VertexId, out_deg=OUT),
      IN1 is IN-N,
      OUT1 is OUT-N,
      set_attribute(VertexId, in_deg=IN1),
      set_attribute(VertexId, out_deg=OUT1)
    )
  ).


remove_zero_degree_nodes :-
  get_current_graph(MId),
  findall(VertexId,
    (
      item([parent:MId, kind: vertex, item: VertexName, id: VertexId]),
      get_attribute(VertexId, in_deg=IN),
      get_attribute(VertexId, out_deg=OUT),
      (IN == 0; OUT == 0),
      remove_vertex_from_conserv_laws(VertexName)
    ),
    VertexToRemove
  ),
  forall(
    member(VertexId, VertexToRemove),
    (
      remove_vertex(VertexId)
    )
  ),
  length(VertexToRemove, NVertex),
  (NVertex > 0
  ->
    (
      remove_zero_degree_nodes
    )
  ;
    (
      true
    )
  ).

pre_process_multistability_graph :-
  remove_zero_degree_nodes.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Graph reduction %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Generate and check all the arcs created by the removal of a node
create_associated_arcs(InArcs, OutArcs) :-
  forall(
    member([VertexAId, VertexMid, SpeciesIn_Sorted, ReactionsIn_Sorted, SignIn], InArcs),
    forall(
      member([_, VertexBId, SpeciesOut_Sorted, ReactionsOut_Sorted, SignOut], OutArcs),
      (
        list_union(ReactionsIn_Sorted, ReactionsOut_Sorted, New_Reactions_Sorted),
        remove_duplicates(New_Reactions_Sorted, Reactions_Aux),
        (
          New_Reactions_Sorted==Reactions_Aux
        ->
          (
            (
              VertexAId == VertexBId
            ->
              (
                % Self-Loop
                list_union(SpeciesIn_Sorted, SpeciesOut_Sorted, New_Species_Sorted_aux),
                (
                  remove_duplicates(New_Species_Sorted_aux, VertexAId, VertexMid, New_Species_Sorted)
                ->
                  (
                    length(New_Species_Sorted, SpeLength),
                    (
                      SpeLength > 1
                    ->
                      (
                        conservLaws(ConservList),
                        (
                          member(Conserv, ConservList),
                          ord_subtract(Conserv, New_Species_Sorted, [])
                        ->
                          true %Covers Conservation Law
                        ;
                          (
                            sign_rule(SignIn, SignOut, New_Sign),
                            add_loop_to_system([New_Species_Sorted, New_Sign])
                          )
                        )
                      )
                    ;
                      true  %Self-loop
                    )
                  )
                ;
                  true  %Same Species Twice
                )
              )
            ;
              (
                % Not Self-Loop
                list_union(SpeciesIn_Sorted, SpeciesOut_Sorted, New_Species_Sorted_aux),
                (
                  remove_duplicates(New_Species_Sorted_aux, VertexMid, New_Species_Sorted)
                ->
                  (
                    conservLaws(ConservList),
                    (
                      member(Conserv, ConservList),
                      ord_subtract(Conserv, New_Species_Sorted, [])
                    ->
                      true %Covers Conservation Law
                    ;
                      (
                        sign_rule(SignIn, SignOut, New_Sign),
                        add_edge_to_multistability_graph(VertexAId, VertexBId, New_Species_Sorted, New_Reactions_Sorted, New_Sign)
                      )
                    )
                  )
                ;
                  true  %Same Species Twice
                )
              )
            )
          )
        ;
          true  %Same Reaction Twice
        )
      )
    )
  ).


%%% Remove a vertex in the graph and create the new arcs
remove_vertex(VertexId) :-
  get_current_graph(MId),
  item([parent: MId, kind:vertex, item: VertexName, id: VertexId]),
  findall(
    Label,
    (
      item([parent: MId, kind: edge, item: InEdge, id: InEdgeId]),
      InEdge = (_ -> VertexName),
      get_attribute(InEdgeId, label = Label_list),
      member(Label, Label_list)
    ),
    InArcs
  ),
  findall(
    Label,
    (
      item([parent: MId, kind: edge, item: OutEdge, id: OutEdgeId]),
      OutEdge = (VertexName -> _),
      get_attribute(OutEdgeId, label = Label_list),
      member(Label, Label_list)
    ),
    OutArcs
  ),
  forall(
    member([VertexAId, _, _, _, _], InArcs),
    (
      get_attribute(VertexAId, out_deg = Out),
      Out1 is Out-1,
      set_attribute(VertexAId, out_deg = Out1)
    )
  ),
  forall(
    member([_, VertexBId, _, _, _], OutArcs),
    (
      get_attribute(VertexBId, in_deg = In),
      In1 is In-1,
      set_attribute(VertexBId, in_deg = In1)
    )
  ),
  create_associated_arcs(InArcs, OutArcs),
  delete_vertex([VertexName]).

%%% Determine vertex to remove (with lowest degree)
get_min_vertex_list([[VertexId, In, Out]], VertexId, Min) :-
  (In < Out)
->
  Min = In
;
  Min = Out.
get_min_vertex_list([[VId, In, Out] | T], VertexId, Min) :-
  get_min_vertex_list(T, VertexId_T, Min_T),
  (
    In < Out
  ->
    Min_loc = In
  ;
    Min_loc = Out
  ),
  (
    Min_loc < Min_T
  ->
    (
      Min = Min_loc,
      VertexId = VId
    )
  ;
    (
      Min = Min_T,
      VertexId = VertexId_T
    )
  ).

get_vertex_to_remove(VertexId) :-
  get_current_graph(MId),
  findall(
    [VId, In, Out],
    (
      item([parent:MId, kind: vertex, id: VId]),
      get_attribute(VId, in_deg = In),
      get_attribute(VId, out_deg = Out)
    ),
    V_list
  ),
  get_min_vertex_list(V_list, VertexId, _).


remove_best_vertex :-
  get_vertex_to_remove(VertexId),
  remove_vertex(VertexId).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Graph functions %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

get_number_of_nodes(N) :-
  get_current_graph(MId),
  set_counter(nodes, 0),
  forall(
    item([parent:MId, kind: vertex]),
    count(nodes, _)
  ),
  peek_count(nodes, N).

%%% List all species in the current graph
get_all_species(Species) :-
  get_current_graph(MId),
  findall(VertexName,
    item([parent: MId, kind: vertex, item: VertexName]),
    SpeciesUnsorted),
    sort(SpeciesUnsorted, Species).


check_acyclicity(0) :- !.

check_acyclicity(N) :-
  % write('Graph at state N = '),
  % write(N),
  % write('\n'),
  % list_graph_objects,
  remove_best_vertex,
  N1 is N-1,
  check_acyclicity(N1).

check_acyclicity :-
  select_graph(multistability_graph_parent),
  retractall(conflict_in_swap(_, _)),
  pre_process_multistability_graph,
  get_current_graph(ParentId),
  get_all_species(Species),
  get_swaps(Species, Swaps),
  get_option(test_permutations, TP),
  get_option(test_transpositions, TT),
  (
    (TP == no, TT == no)
  ->
    !
;
    true
  ),
  copy_multistability_graph(ParentId, Swaps),
  select_graph(multistability_graph),
  % list_graph_objects,
  remove_self_loops,
  reset_gauss_system,
  get_number_of_nodes(N),
  check_acyclicity(N).


check_multistability_graph :-
  multistability_graph,
  get_time(Start_time),
  (
    check_acyclicity
  ->
    (
      (
        debugging(multistability)
      ->
        get_time(End_time),
        Diff_time is End_time-Start_time
      ;
        true
      ),
      get_current_graph(GraphId),
      get_attribute(GraphId, swaps_used=Swaps),
      write('There is no multiple steady states with non-zero values, no positive circuit\n'),
      (
        identity_swap(Swaps)
      ->
        true
      ;
        (
          write('(using permutation of species: '),
          write(Swaps),
          write(')\n')
        )
      ),
      loop_gauss_system(System),
      generate_change_sign_from_system(System, [], Species_to_change_sign_id),
      (
        length(Species_to_change_sign_id, 0)
      ->
        debug(multistability, "Done in ~ws~n", [Diff_time])
      ;
        (
          conserv_names_to_id(Species_to_change_sign, Species_to_change_sign_id),
          write('(using change of sign for species : '),
          forall(
            member(S, Species_to_change_sign),
            (
              write(S),
              write(' ')
            )
          ),
          debug(multistability, "~nDone in ~ws", [Diff_time]),
          write(')\n')
        )
      )
    )
  ;
    (
      (
        debugging(multistability)
      ->
        get_time(End_time),
        Diff_time is End_time-Start_time
      ;
        true
      ),
      write('There may be non-degenerate multistationarity, positive circuit detected.'),
      debug(multistability, "~nDone in ~ws", [Diff_time]),
      write('\n')
    )
  ).
