:- module(
  ctl,
  [
    % grammar
    ctl/1,
    % command
    add_ctl/1,
    delete_ctl/1,
    delete_ctl/0,
    list_ctl/0,
    generate_ctl/1,
    generate_ctl/0,
    generate_ctl_not/0,
    cleanup_ctl/0,
    expand_ctl/1,
    % API
    in_ctl/1,
    normalize_query/3
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).
:- use_module(reaction_rules).

:- use_module(nusmv). % for ctl_truth

:- doc('The Computation Tree Logic CTL can be used to express qualitative properties of the behavior of a network in a given (set of) initial states, at the boolean level of abstraction \\cite{CCDFS04tcs}.
CTL extends propositional logic with modal operators to specify where (E: on some path, A: on all paths)
and when (X: next state, F: finally at some future state, G: globally on all states, U: until a second formula is true, R: release)
a formula is true. As in any logic, these modalities can be combined with logical connectives and imbricated,
with the only restriction that a temporal operator must be immediately preceded by a path quantifier, 
e.g. \\texttt{EF(AG(p))} which expresses the reachability of a stable state where protein \\texttt{p} will always remain present.
CTL is well suited to analyze attractors, their reachability and transient properties  \\cite{TFFT16bi}.
It is worth noting however that CTL reachability properties are purely factual and not necessarily causal.').

:- doc('The syntax of CTL formulas and some useful abbreviations are defined by the following grammar:').

:- grammar(ctl).

ctl('EX'(E)) :-
  ctl(E).

ctl('EF'(E)) :-
  ctl(E).

ctl('EG'(E)) :-
  ctl(E).

ctl('EU'(E, F)) :-
  ctl(E),
  ctl(F).

ctl('ER'(E, F)) :-
  ctl(E),
  ctl(F).

ctl('AX'(E)) :-
  ctl(E).

ctl('AF'(E)) :-
  ctl(E).

ctl('AG'(E)) :-
  ctl(E).

ctl('AU'(E, F)) :-
  ctl(E),
  ctl(F).

ctl('AR'(E, F)) :-
  ctl(E),
  ctl(F).

ctl(not(E)) :-
  ctl(E).

ctl(E /\ F) :-
  ctl(E),
  ctl(F).

ctl(E \/ F) :-
  ctl(E),
  ctl(F).

ctl(E -> F) :-
  ctl(E),
  ctl(F).

ctl(reachable(E)) :-
    ctl(E).
:- doc('\\emphright{reachable(f) is a shorthand for EF(f)}').

ctl(must_reach(E)) :-
    ctl(E).
:- doc('\\emphright{mustreach(f) is a shorthand for AF(f)}').

ctl(steady(E)) :-
  ctl(E).
:- doc('\\emphright{steady(f) is a shorthand for EG(f)}').

ctl(stable(E)) :-
  ctl(E).
:- doc('\\emphright{stable(f) is a shorthand for AG(f)}').

ctl(checkpoint(E, F)) :-
  ctl(E),
  ctl(F).
:- doc('\\emphright{checkpoint(f, g) is a shorthand for not EU(not f, g)). Note that such a factual property does not imply any causality relationship from f to g.}').

ctl(checkpoint2(E, F)) :-
  ctl(E),
  ctl(F).
%:- doc('\\emphright{checkpoint2(f, g) is a shorthand for EF(not f)/\\\\EF(g)/\\\\checkpoint(f,g)}'). % good point for EF(g) not sure for EF(not f)
:- doc('\\emphright{checkpoint2(f, g) is a shorthand for EF(g)/\\\\checkpoint(f,g)}'). 

%ctl(predecessor_checkpoint(E, F)) :-
%  ctl(E),
%  ctl(F).
%:- doc('\\emphright{predecessor_checkpoint(f, g) is a shorthand for AG(not g -> checkpoint(f, g)) which is true if the non trivial reachability of g, if any, is always preceded by a sequence of f}').

ctl(oscil2(E)) :-
  ctl(E).
:- doc('\\emphright{oscil2(f) is a shorthand for EU(not(f), f /\\\\ EU(f, not(f) /\\\\ EU(not(f), f))) which is true if f has at least two peaks on one path}').

ctl(oscil3(E)) :-
  ctl(E).
:- doc('\\emphright{oscil3(f) is a shorthand for EU(not(f), f /\\\\ EU(f, not(f) /\\\\ EU(not(f), f /\\\\ EU(f, not(f) /\\\\ EU(not(f), f))))) which is true if f has at least three peaks on one path}').

ctl(oscil(E)) :-
  ctl(E).
:- doc('\\emphright{oscil(f) is a shorthand for  oscil3(f) /\\\\ EG(EF(f) /\\\\ EF(not f)) which is a necessary (not sufficient) condition for infinite oscillations in CTL}').



ctl(current_spec).

ctl(E) :-
    object(E).





:- devdoc('\\section{Commands}').

:- doc('The qualitative behavior of a network can be specified by a set of CTL formulas using the following commands:').


list_ctl :-
  biocham_command,
  doc('Prints out all formulae from the current CTL specification.'),
  % model: current_model?
  list_items([kind: ctl_spec]).

expand_ctl(Formula) :-
  biocham_command,
  type(Formula, ctl),
  doc('shows the expansion in CTL of a formula with patterns.'),
  normalize_query(Formula,ExpandedFormula,_),
  format('~w\n', [ExpandedFormula]).

add_ctl(Formula) :-
  biocham_command,
  type(Formula, ctl),
  doc('Adds a CTL formula to the currently declared CTL specification.'),
  (in_ctl(Formula) -> true ; add_item([kind: ctl_spec, item: Formula])).


delete_ctl(Formula) :-
  biocham_command,
  type(Formula, ctl),
  doc('Removes a CTL formula to the currently declared CTL specification.'),
  find_item([id: Id, type: ctl_spec, item: Formula]),
  delete_item(Id).


in_ctl(Formula) :-
    devdoc('checks whether a formula belongs to the current CTL specification, syntactically.'),
    item([type: ctl_spec, item: Formula]).


delete_ctl :-
  biocham_command,
  doc('Removes all formulae from the current CTL specification.'),
  % model: current_model?
  delete_items([kind: ctl_spec]).

:- doc('The set of CTL formulas of some simple form that are true in the current set of initial states
can also be automatically generated as a specification of the network using the following commands.').

:- devdoc('\\begin{todo} generate_ctl options on:{molecules} between:{molecules}\\end{todo}').


generate_ctl(Formula):-
  biocham_command,
  type(Formula, ctl),
  doc('adds to the CTL specification all the CTL formulas that are true, not subsumed by another CTL formula in the specification, and that match the argument formula in which the names that are not molecules are treated as wildcards and replaced by distinct molecules of the network. This command is a variant with subsumption test of \\command{add_ctl} if all names match network molecule names, otherwise it enumerates all m^v instances (where m is the number of molecules and v the number of wildcards in the formula).'),
  normalize_query(Formula,F,Leaves),
  enumerate_molecules(Molecules),
  subtract(Leaves,Molecules,L),
  length(L,N),
  length(Wildcards,N),
  substitute(L,Wildcards,F,S),
  substitute(L,Wildcards,Formula,Spec), % for pretty spec
  \+ (
      enumerate_instances(Wildcards,Molecules),
      \+ cleanup_ctl(S),
      \+ cleanup_ctl(Spec),
      ctl_truth(S, true),
      print(Spec),nl,
      \+ add_ctl(Spec)
  ).

    
generate_ctl:-
  biocham_command,
  doc('adds to the CTL specification all reachable stable, reachable steady, reachable, checkpoint2, and oscil formulas on all molecules (using this order of enumeration for performing subsumption checks).'),
  generate_ctl(reachable(stable(wildcard))),
  generate_ctl(reachable(steady(wildcard))),
  generate_ctl(reachable(wildcard)),
  generate_ctl(checkpoint2(wildcard,dummy)),
  generate_ctl(oscil(wildcard)),
  generate_ctl(oscil3(wildcard)),
  generate_ctl(oscil2(wildcard)). 



generate_ctl_not:-
  biocham_command,
  doc('adds to the CTL specification all reachable stable , reachable stable not, reachable steady, reachable steady not, reachable, reachable not, checkpoint2, and oscil formulas on all molecules.'),
  generate_ctl(reachable(stable(wildcard))),
  generate_ctl(reachable(stable(not(wildcard)))),
  generate_ctl(reachable(steady(wildcard))),
  generate_ctl(reachable(steady(not(wildcard)))),
  generate_ctl(reachable(wildcard)),
  generate_ctl(reachable(not(wildcard))),
  generate_ctl(checkpoint2(wildcard,dummy)),
  generate_ctl(checkpoint2(wildcard,not(dummy))), 
  generate_ctl(checkpoint2(not(wildcard),dummy)),
  generate_ctl(checkpoint2(not(wildcard),not(dummy))), 
  generate_ctl(oscil(wildcard)),
  generate_ctl(oscil3(wildcard)),
  generate_ctl(oscil2(wildcard)). 


cleanup_ctl:-
    biocham_command,
    doc('cleans up the CTL specification by removing redundant formulae such as reachable(steady(A)) if reachable(stable(A)) is true, etc.'),
    devdoc('This command will be generalized when the CTL abbreviations will be functions.'),
    \+ (
      item([kind: ctl_spec, id: Id, item: Formula]),
      cleanup_ctl(Formula),
      \+ delete_item(Id)
    ),
    list_ctl.

cleanup_ctl(reachable(steady(A))):-
    !,
    in_ctl(reachable(stable(A))).

cleanup_ctl(reachable(A)):-
    !,
    (in_ctl(reachable(steady(A)));
     in_ctl(reachable(stable(A)));
     in_ctl(oscil(A));in_ctl(oscil2(A));in_ctl(oscil3(A));
     (A=not(B), (in_ctl(oscil(B));in_ctl(oscil2(B));in_ctl(oscil3(B))))
    ),
    !.

cleanup_ctl(oscil2(A)):-
    !,
    in_ctl(oscil(A));in_ctl(oscil3(A)).

cleanup_ctl(oscil3(A)):-
    !,
    in_ctl(oscil(A)).




:- doc('\\begin{example}').
:- biocham_silent(clear_model).
:- biocham(a=>b).
:- biocham(b=>c).
:- biocham(c=>d).
:- biocham(present(b)).
:- biocham(make_absent_not_present).
:- biocham(generate_ctl(checkpoint2(x,d))).
:- biocham(generate_ctl_not).
:- doc('\\end{example}').




:- devdoc('\\section{API predicates}').


:-devdoc('\\section{Internal predicates}').

enumerate_instances([V|Variables],Molecules):-
    select(V,Molecules,Molecules2),
    enumerate_instances(Variables,Molecules2).

enumerate_instances([],_).

:- devdoc('Internal predicates.').


normalize_query(Query, NQuery, SLeaves) :-
  normalize_query(Query, NQuery, [], Leaves),
  sort(Leaves, SLeaves).


normalize_query(reachable(Q), QQ, L1, L2) :-
  !,
  normalize_query('EF'(Q), QQ, L1, L2).

normalize_query(must_reach(Q), QQ, L1, L2) :-
  !,
  normalize_query('AF'(Q), QQ, L1, L2).

normalize_query(steady(Q), QQ, L1, L2) :-
  !,
  normalize_query('EG'(Q), QQ, L1, L2).

normalize_query(stable(Q), QQ, L1, L2) :-
  !,
  normalize_query('AG'(Q), QQ, L1, L2).

normalize_query(oscil(Q), QQ, L1, L2) :-
  !,
  normalize_query(oscil3(Q) /\ 'EG'('EF'(Q) /\ 'EF'(not(Q))), QQ, L1, L2).

normalize_query(oscil2(Q), QQ, L1, L2) :-
  !,
  normalize_query('EU'(not(Q), Q /\ 'EU'(Q, not(Q) /\ 'EU'(not(Q), Q))), QQ, L1, L2).

normalize_query(oscil3(Q), QQ, L1, L2) :-
  !,
  normalize_query('EU'(not(Q), Q /\ 'EU'(Q, not(Q) /\ 'EU'(not(Q), Q /\ 'EU'(Q, not(Q) /\ 'EU'(not(Q), Q))))), QQ, L1, L2).

normalize_query(checkpoint(A, B), QQ, L1, L2) :-
  !,
  normalize_query(not('EU'(not(A), B)), QQ, L1, L2).

normalize_query(checkpoint2(A, B), QQ, L1, L2) :-
  !,
  normalize_query('EF'(B) /\ not('EU'(not(A), B)), QQ, L1, L2).

%normalize_query(predecessor_checkpoint(A, B), QQ, L1, L2) :-
%  !,
%  normalize_query('AG'(not(B) -> checkpoint(A,B)), QQ, L1, L2).

normalize_query(Q, Q, L, [Q | L]) :-
  atomic(Q),
  !.

normalize_query(Q, QQ, L1, L2) :-
  Q =.. [Functor | Args],
  % maplist(normalize_query, Args, QArgs),
  foldl(normalize_query, Args, QArgs, L1, L2),
  QQ =.. [Functor | QArgs].
