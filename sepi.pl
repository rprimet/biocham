:- module(
  sepi,
  [
    delete_molecule/1,
    merge_molecules/2,
    delete_reaction/1,
    merge_reactions/2,
    sepi_reduces/2,
    siso_reduces/2,
    minor_reduces/2
  ]
).


:- doc('CURRENTLY NOT INCLUDED. This section describes commands to reduce a reaction model by deleting species, deleting reactions, merging species and merging reactions. We refer to \\cite{GSF10bi} for the use of these operations for detecting model reductions in model repositories such as BioModels. We refer to \\cite{GFS14dam} for the theory of these graphical operations which are equivalent to subgraph epimorphisms (SEPI). We also provide a model reduction command which automatically finds a model reduction preserving the CTL specification of the behavior.').

:- devcom('\\begin{todo}These commands should be generalized, 
first, to deletions and merge of molecules and reactions,

second, to influence networks and hybrid reaction-influence networks (great SEPI unification), 

and third, to hybrid specifications of behaviors with attractors, CTL, FOLTL, traces.\\end{todo}').
:- devcom(
	          'todo with options sepi_reduce_model Deletes and/or merges molecules and/or reactions (aka subgraph epimorphism, SEPI reduction) as long as the specification of the behavior given by a CTL formula passed as
    argument remains satisfied in the current initial state.
').

%	       delete_molecules(Query, MoleculesRemoved),
%	       format('molecules removed ~p\n',[MoleculesRemoved]),
%	       merge_molecules(Query, MoleculesMerged),
%	       format('removed ~p\n',[MoleculesMerged]),
%	       delete_reactions(Query, ReactionsRemoved),
%	       format('removed ~p\n',[ReactionsRemoved]),
%	       merge_reactions(Query, ReactionsMerged),
%	       format('removed ~p\n',[ReactionsMerged])


delete_molecule(M):-
  biocham_command,
  doc('Deletes a molecule and its aliases from the current model, by replacing them by a parameter set to the initial value of the concentration.'),
  get_initial_concentration(M, C),
  \+ (
    item([kind: initial_state, id: Id, key:M]),
    \+ (
      delete_item(Id)
    )
  ),
  set_parameter(M, C),
  \+ (
    item([kind: reaction, id: Id, item: Reaction]),
    reaction(Reaction, [
      reactants: Reactants,
      inhibitors: Inhibitors,
      products: Products
    ]),
    (member(_ * M, Reactants) ; member(_ * M, Inhibitors) ; member(_ * M, Products)),
    \+ (
      delete_item(Id),
      rewrite_molecule_in_reaction(M, Reaction, SimplifiedReaction),
      add_item([kind: reaction, item: SimplifiedReaction])
    )
  ).


merge_molecules(M,N):-
  biocham_command,
  type(M, object),
  type(N, object),
  doc('
    merges two molecules by creating an alias making the second molecule canonical, and with initial concentration set to the sum of the initial concentrations of the merged molecules.
  '),
  devcom('should be the semantics of alias which should be synonymous'),
  get_initial_concentration(M, CM),
  get_initial_concentration(N, CN),
  alias(M=N),
  canonical(N),
  C is CM+CN,
  present(N,C).

delete_reaction(R).

merge_reactions(R,S).

sepi_reduces(A,B).

siso_reduces(A,B).

minor_reduces(A,B).

