FROM jupyter/minimal-notebook
LABEL Author="Sylvain Soliman <Sylvain.Soliman@inria.fr>"
LABEL Maintainer="Biocham team <biocham@inria.fr>"

ARG tag=latest

USER root

ENV LD_LIBRARY_PATH /usr/local/lib/
ENV PYTHONPATH /home/$NB_USER/biocham/
ENV PATH /home/$NB_USER/biocham:$PATH
RUN cd /home/$NB_USER/ && \
	wget https://lifeware.gitlabpages.inria.fr/biocham/biocham_${tag}.zip && \
	unzip biocham_${tag}.zip && rm biocham_${tag}.zip
RUN cd biocham && ./install.sh && apt-get autoremove -y && \
	apt-get clean -y && rm -rf /var/lib/apt/lists/* && \
	conda uninstall -y --force terminado && \
	fix-permissions /home/$NB_USER/.local && \
	fix-permissions /home/$NB_USER/biocham

USER $NB_USER
WORKDIR /home/$NB_USER/biocham/library/examples

CMD ["start.sh", "biocham", "--notebook"]
