#!/bin/bash

export TOKEN=$( head -c 30 /dev/urandom | xxd -p )
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --expose 8000 --name=proxy jupyter/configurable-http-proxy --default-target http://127.0.0.1:9999
# orchestrate.py options for culling (i.e. destroying) containers:
# --cull-max (14400, i.e. 4h) --cull-timeout (3600, i.e. 1h if idle)
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --name=tmpnb -v /var/run/docker.sock:/docker.sock jupyter/tmpnb python orchestrate.py --image='lifeware/biocham:latest' --pool-size=20 --command='start.sh biocham --notebook --NotebookApp.base_url=/{base_path} --NotebookApp.token={token} --ip=0.0.0.0 --port {port}'

# second server for lifeware.inria.fr/biocham4/online
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --expose 8080 --expose 8081 --name=secondproxy jupyter/configurable-http-proxy --port 8080 --default-target http://127.0.0.1:9990
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN -e CONFIGPROXY_ENDPOINT=http://127.0.0.1:8081 --name=secondtmpnb -v /var/run/docker.sock:/docker.sock jupyter/tmpnb python orchestrate.py --image='registry.gitlab.inria.fr/lifeware/biocham:v4.1.29' --pool-size=30 --port=9990 --admin-port=9991 --command='start.sh biocham --notebook --NotebookApp.base_url=/{base_path} --NotebookApp.token={token} --ip=0.0.0.0 --port {port}'
