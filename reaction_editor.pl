:- module(
  reaction_editor,
  [
    % Commands
    add_reaction/1,
    list_reactions/0,
    list_rules/0,
    delete_reaction/1,
    delete_reaction_named/1,
    delete_reaction_prefixed/1,
    merge_reactions/2,
    simplify_all_reactions/0,
    % Public API
    find_reaction_prefixed/2,
    is_reaction_model/0,
    check_reaction_model/0,
    list_model_reactions/0,
    reaction/2,
    solution_to_list/2,
    list_to_solution/2,
    compute_ode_for_reactions/0
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(reaction_rules).
:- use_module(objects).


:- devdoc('\\section{Commands}').

:- devcom('\\begin{todo}For several reasons, we should automatically introduce kinetic parameters k_1, k_2, etc. for the default mass action law kinetics of reactions given without kinetic expression. The same could go for initial_state concentrations which BTW should be definable by parameters or functions, introducing concentration parameters  c_1, c_2, etc. for present molecules. The pros are an encouragement to use parameters, parameter search, sensitivity and robustness analyses. The cons are an irrelevant encouragement for logical models. \\end{todo}').

add_reaction(Reaction) :-
  biocham_command,
  type(Reaction, reaction),
  doc('
    adds reaction rules to the current set of reactions.
    This command is implicit: reaction rules are directly added when reading them.
  '),
  \+ (
    reaction_named(Reaction, Name, Kinetics, Reactants, Inhibitors, Products),
    \+ (
      add_reaction(Name, Kinetics, Reactants, Inhibitors, Products, false)
    )
  ).

:- doc('\\begin{remark}In Biocham v4, a reaction network is represented by a multiset of reactions. Reactions can thus be added in multiple copies in which case their reaction rates are summed.\\end{remark}').



delete_reaction(Reaction):-
  biocham_command,
  type(Reaction, reaction),
  doc('
    removes one reaction rule from the current set of reactions.
  '),
  delete_item([kind: reaction, item: Reaction]).

delete_reaction_named(Name):-
  biocham_command,
  type(Name, name),
  doc('
    removes  reaction rules by their name from the current set of reactions.
  '),
  Item=(Name--_),
\+ (
    item([kind:reaction, id: Id, item:Item]),
\+ (
    delete_item(Id)
)
).

delete_reaction_prefixed(Prefix):-
  biocham_command,
  type(Prefix, name),
  doc('
    removes the reaction rules that match a name prefix.
  '),
  Item=(Name--_),
\+ (
    item([kind:reaction, id: Id, item:Item]),
    atom_concat(Prefix,_,Name),
\+ (
    delete_item(Id)
)
).


find_reaction_prefixed(Prefix,Name):-
    biocham_command,
    type(Prefix, name),
    type(Name, name),
    devdoc('
    Name is the name of a reaction prefixed by Prefix.
    '),
    Item=(Name--_),
    item([kind:reaction, id: _Id, item:Item]),
    atom_concat(Prefix,_,Name).


merge_reactions(Reaction1, Reaction2):-
  biocham_command,
  type(Reaction1, reaction),
  type(Reaction2, reaction),
  doc('
    merges two reaction rules by removing them and replacing them by a new reaction with reactants the sum of reactants, with products the sum of the products and with the sum of the kinetics.
  '),
  reaction(Reaction1, Kinetics1, Reactants1, Inhibitors1, Products1),
  reaction(Reaction2, Kinetics2, Reactants2, Inhibitors2, Products2),
  delete_reaction(Reaction1),
  delete_reaction(Reaction2),
  append(Reactants1,Reactants2,Reactants),
  append(Inhibitors1,Inhibitors2,Inhibitors),
  append(Products1,Products2,Products),
  add_reaction(Kinetics1+Kinetics2,Reactants,Inhibitors,Products,false).


%simplify_all_reactions :-
%  biocham_command,
%  doc('
%    replaces each reaction by a simplified form, by grouping common molecules,
%    identifying catalysts, and by using the canonical molecule for aliases.
%  '),
%  \+ (
%    item([kind: reaction, id: Id, item: Reaction]),
%    \+ (
%      delete_item(Id),
%      simplify_reaction(Reaction, SimplifiedReaction),
%      add_item([kind: reaction, item: SimplifiedReaction])
%    )
%  ).


list_reactions :-
  biocham_command,
  doc('lists the current set of reaction rules.'),
  list_items([kind: reaction]).


list_rules :-
  biocham_command,
  doc('lists the current set of reaction and influence rules.'),
  list_items([kind: [reaction, influence]]).


:- devdoc('\\section{Public API}').


is_reaction_model :-
   devdoc('
     succeeds if the current model is a reaction model
     (i.e., does not contain any influence rules).
   '),
   \+ item([kind: influence]).


check_reaction_model :-
  (
    is_reaction_model
  ->
    true
  ;
    throw(error(not_a_reaction_model, check_reaction_model))
  ).


list_model_reactions :-
  devdoc('
    lists all the reaction rules in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  \+ (
    item([no_inheritance, kind: reaction, item: Reaction]),
    \+ (
      format('~w.\n', [Reaction])
    )
  ).


prolog:error_message(not_a_reaction_model) -->
  ['Not a reaction model'].


add_reaction(Kinetics, Left, Inhibors, Right, Reversible) :-
    add_reaction('', Kinetics, Left, Inhibors, Right, Reversible).

add_reaction(Name, Kinetics, Left, Inhibors, Right, Reversible) :-
    check_identifier_kind(Name, rule_name),
  \+ (
    (
      member(_ * Object, Left)
    ;
      member(Object, Inhibors)
    ;
      member(_ * Object, Right)
    ),
    \+ (
      check_identifier_kind(Object, object)
    )
  ),
  make_reaction(Name, Kinetics, Left, Inhibors, Right, Reversible, Reaction),
  add_item([kind: reaction, item: Reaction]).


simplify_all_reactions :-
%  biocham_command,
%  doc('
%    replaces each reaction by a simplified form, by grouping common molecules,
%    identifying catalysts, and by using the canonical molecule for aliases.
%  '),
  \+ (
    item([kind: reaction, id: Id, item: Reaction]),
    \+ (
      delete_item(Id),
      simplify_reaction(Reaction, SimplifiedReaction),
      add_item([kind: reaction, item: SimplifiedReaction])
    )
  ).

reaction_named(
  Item, Name, Kinetics, Reactants, Inhibitors, Products, Reversible
) :-
    (
        Item=(Name -- _)
     ->
         true
     ;
     Name=''
    ),
    reaction(Item, Kinetics, Reactants, Inhibitors, Products, Reversible).
    

reaction_named(Item, Name, Kinetics, Reactants, Inhibitors, Products) :-
    (
        Item=(Name -- _)
     ->
         true
     ;
     Name=''
    ),
    reaction(Item, Kinetics, Reactants, Inhibitors, Products).
    



reaction(ItemNamed, Kinetics, Reactants, Inhibitors, Products, Reversible) :-
    (
        ItemNamed=(_ -- Item)
     ->
         true
     ;
     Item=ItemNamed
    ),
    
  (
    Item = (Kinetics for Body)
  ->
    true
  ;
    Kinetics = 'MA'(1),
    Body = Item
  ),
  (
    Body = (Left =[ Catalyst ]=> Right)
  ->
    Reversible = false
  ;
    Body = (Left <=[ Catalyst ]=> Right)
  ->
    Reversible = true
  ;
    Body = (Left => Right)
  ->
    Reversible = false,
    Catalyst = '_'
  ;
    Body = (Left <=> Right)
  ->
    Reversible = true,
    Catalyst = '_'
  ),
  (
    Left = LeftPositive / LeftNegative
  ->
    true
  ;
    LeftPositive = Left,
    LeftNegative = '_'
  ),
  solution_to_canonical_list(LeftPositive, LeftMolecules),
  enumeration_to_canonical_list(LeftNegative, Inhibitors),
  solution_to_canonical_list(Right, RightMolecules),
  solution_to_canonical_list(Catalyst, CatalystMolecules),
  append(CatalystMolecules, LeftMolecules, AllReactants),
  append(CatalystMolecules, RightMolecules, AllProducts),
  maplist(
    simplify_solution,
    [AllReactants, AllProducts],
    [Reactants, Products]
  ).

reaction(Item, Kinetics, Reactants, Inhibitors, Products) :-
  reaction(
    Item, PairKinetics, LeftMolecules, Inhibitors, RightMolecules, Reversible
  ),
  (
    Reversible = true
  ->
    pair_kinetics(PairKinetics, ForwardKinetics, BackwardKinetics),
    (
      Kinetics = ForwardKinetics,
      Reactants = LeftMolecules,
      Products = RightMolecules
    ;
      Kinetics = BackwardKinetics,
      Reactants = RightMolecules,
      Products = LeftMolecules
    )
  ;
    Kinetics = PairKinetics,
    Reactants = LeftMolecules,
    Products = RightMolecules
  ).


reaction(Reaction, Components) :-
  devdoc('
builds or decomposes a reaction rule.
\\argument{Reaction} is a reaction term.
\\argument{Components} is a record-style list with items of the form
\\begin{itemize}
\\item \\texttt{name: Name} where \\texttt{Name} is an atom,
\\item \\texttt{kinetics: Kinetics} where \\texttt{Kinetics} is an arithmetic
expression,
\\item \\texttt{reactants: Reactants},
\\item \\texttt{products: Products} where \\texttt{Reactants} and
\\texttt{Products} are lists of items that can either be objects or terms of the
form \\texttt{Coefficient * Object},
\\item \\texttt{inhibitors: Inhibitors} where \\texttt{Inhibitors} is a list of
objects.
\\end{itemize}
  '),
  (
    var(Reaction)
  ->
    field_default(name, '', Components, Name),
    field_default(kinetics, 'MA'(1), Components, Kinetics),
    field_default(reactants, [], Components, Reactants),
    field_default(inhibitors, [], Components, Inhibitors),
    field_default(products, [], Components, Products),
    maplist(put_coefficient, Reactants, ReactantsWithCoefficients),
    maplist(put_coefficient, Products, ProductsWithCoefficients),
    make_reaction(
      Name, Kinetics, ReactantsWithCoefficients, Inhibitors,
      ProductsWithCoefficients, false, Reaction
    )
  ;
    reaction_named(Reaction, Name, Kinetics, Reactants, Inhibitors, Products),
    unify_records(Components, [
      name: Name,
      kinetics: Kinetics,
      reactants: Reactants,
      inhibitors: Inhibitors,
      products: Products
    ])
  ).


put_coefficient(Coefficient * Object, Coefficient * Object) :-
  !.

put_coefficient(Object, 1 * Object).


pair_kinetics(PairKinetics, ForwardKinetics, BackwardKinetics) :-
  (
    PairKinetics = (ForwardKinetics, BackwardKinetics)
  ->
    true
  ;
    ForwardKinetics = PairKinetics,
    BackwardKinetics = PairKinetics
  ).


solution_to_canonical_list(Solution, List) :-
  solution_to_list(Solution, NonCanonicalList),
  maplist(coefficient_canonical, NonCanonicalList, List).


enumeration_to_canonical_list(Enumeration, List) :-
  list_enumeration(NonCanonicalList, Enumeration),
  maplist(canonical, NonCanonicalList, List).


solution_to_list('_', []) :-
  !.

solution_to_list(A + B, Solution) :-
  !,
  solution_to_list(A, SolutionA),
  solution_to_list(B, SolutionB),
  append(SolutionA, SolutionB, Solution).

solution_to_list(Coefficient * Object, Solution) :-
  !,
  Solution = [Coefficient * Object].

solution_to_list(Object, [1 * Object]).


list_to_solution([], '_') :-
  !.

list_to_solution(List, Solution) :-
  reverse(List, [Head | Tail]),
  list_to_solution(Tail, Head, Solution).

list_to_solution([], CoefficientObject, Object) :-
  coefficient_object(CoefficientObject, Object).

list_to_solution([Head | Tail], CoefficientObject, Solution) :-
  coefficient_object(CoefficientObject, Object),
  Solution = SolutionTail + Object,
  list_to_solution(Tail, Head, SolutionTail).


coefficient_object(1 * Object, Object) :-
  !.

coefficient_object(1.0 * Object, Object) :-
  !.

coefficient_object(CoefficientObject, CoefficientObject).


simplify_reaction(Reaction, SimplifiedReaction) :-
  reaction_named(
    Reaction, Name, Kinetics, Reactants, Inhibitors, Products, Reversible
  ),
  make_reaction(
    Name, Kinetics, Reactants, Inhibitors, Products, Reversible,
    SimplifiedReaction
  ).


simplify_kinetics(Kinetics, KineticsSimplified) :-
  (
    Kinetics = (LeftKinetics, RightKinetics)
  ->
    simplify(LeftKinetics, LeftKineticsSimplified),
    simplify(RightKinetics, RightKineticsSimplified),
    (
      LeftKineticsSimplified = RightKineticsSimplified
    ->
      KineticsSimplified = LeftKineticsSimplified
    ;
      KineticsSimplified = (LeftKineticsSimplified, RightKineticsSimplified)
    )
  ;
    simplify(Kinetics, KineticsSimplified)
  ).


simplify_solution(Solution, SimplifiedSolution) :-
  factorize_solution(Solution, SimplifiedSolution).


coefficient_canonical(Coefficient * Object, Coefficient * Canonical) :-
  canonical(Object, Canonical).


factorize_solution([], []).

factorize_solution([Coefficient * Object | Tail], SimplifiedSolution) :-
  collect_object(Tail, Object, TailCoefficient, Others),
  % simplify(Coefficient + TailCoefficient, SimplifiedCoefficient),
  SimplifiedCoefficient is Coefficient + TailCoefficient,
  (
    SimplifiedCoefficient = 0
  ->
    simplify_solution(Others, SimplifiedSolution)
  ;
    SimplifiedSolution = [SimplifiedCoefficient * Object | SimplifiedTail],
    simplify_solution(Others, SimplifiedTail)
  ).


collect_object([], _Object, 0, []).

collect_object([Coefficient * Object | Tail], Object, Sum, Others) :-
  !,
  Sum = Coefficient + TailCoefficient,
  collect_object(Tail, Object, TailCoefficient, Others).

collect_object([CoefficientObject | Tail], Object, Sum, Others) :-
  Others = [CoefficientObject | OthersTail],
  collect_object(Tail, Object, Sum, OthersTail).


simplify_catalyst([], Right, [], [], Right).

simplify_catalyst([LeftCoeff*Head | Tail], Right, NewLeft, Catalyst, NewRight) :-
  (
    select(RightCoeff*Head, Right, OthersRight)
  ->
    Coeff is min(LeftCoeff, RightCoeff),
    Catalyst = [Coeff*Head | CatalystTail],
    (
      Coeff < LeftCoeff
    ->
      Remain is LeftCoeff - Coeff,
      NewLeft = [Remain*Head | RemainLeft],
      NewRight = RemainRight
    ;
      Coeff < RightCoeff
    ->
      Remain is RightCoeff - Coeff,
      NewLeft = RemainLeft,
      NewRight = [Remain*Head | RemainRight]
    ;
      NewLeft = RemainLeft,
      NewRight = RemainRight
    ),
    simplify_catalyst(Tail, OthersRight, RemainLeft, CatalystTail, RemainRight)
  ;
    NewLeft = [LeftCoeff*Head | LeftTail],
    simplify_catalyst(Tail, Right, LeftTail, Catalyst, NewRight)
  ).


make_reaction(
  Name, Kinetics, LeftMolecules, Inhibitors, RightMolecules, Reversible,
  Reaction
) :-
  simplify_kinetics(Kinetics, KineticsSimplified),
  simplify_solution(LeftMolecules, LeftSimplified),
  simplify_solution(RightMolecules, RightSimplified),
  simplify_catalyst(LeftSimplified, RightSimplified, Left, Catalyst, Right),
  (
    append(Left, Catalyst, Reactants),
    kinetics(Reactants, Inhibitors, 'MA'(1), KineticsSimplified)
  ->
    KineticsSimplifiedBis = 'MA'(1)
  ;
    KineticsSimplifiedBis = KineticsSimplified
  ),
  make_reaction(
    Name, KineticsSimplifiedBis, Left, Inhibitors, Catalyst, Right,
    Reversible, Reaction
  ).



make_reaction(
  Name, Kinetics, Left, Inhibitors, Catalyst, Right, Reversible, ReactionNamed
) :-
    (
        Name=''
     ->
         ReactionNamed=Reaction
     ;
     ReactionNamed=(Name -- Reaction)
    ),
  (
    Kinetics = 'MA'(1)
  ->
    Reaction = Body
  ;
    Reaction = (Kinetics for Body)
  ),
  list_to_solution(Left, LeftSolution),
  list_enumeration(Inhibitors, InhibitorsEnumeration),
  list_to_solution(Catalyst, CatalystSolution),
  list_to_solution(Right, RightSolution),
  (
    InhibitorsEnumeration = '_'
  ->
    Reactants = LeftSolution
  ;
    Reactants = LeftSolution / InhibitorsEnumeration
  ),
  (
    Reversible = true
  ->
    (
      CatalystSolution = '_'
    ->
      Body = (Reactants <=> RightSolution)
    ;
      Body = (Reactants <=[ CatalystSolution ]=> RightSolution)
    )
  ;
    (
      CatalystSolution = '_'
    ->
      Body = (Reactants => RightSolution)
    ;
      Body = (Reactants =[ CatalystSolution ]=> RightSolution)
    )
  ).


compute_ode_for_reactions :-
  forall(
    item([kind: reaction, item: Item]),
    (
      get_stoichiometry_and_kinetics(Item, SimplifiedSolution, KineticsExpression),
      add_molecules(SimplifiedSolution, KineticsExpression)
    )
  ).


add_molecules(Molecules, Kinetics) :-
  \+ (
    member(Molecule, Molecules),
    \+ (
      add_molecule(Molecule, Kinetics)
    )
  ).


add_molecule(Coefficient * Molecule, Kinetics) :-
  ode_add_expression_to_molecule(Coefficient * Kinetics, Molecule).


negate_coefficient(C*X, D*X) :-
  (
    number(C)
  ->
    D is -C
  ;
    D = -C
  ).


get_stoichiometry_and_kinetics(Item, SimplifiedSolution, KineticsExpression) :-
  reaction(Item, Kinetics, Reactants, Inhibitors, Products),
  kinetics(Reactants, Inhibitors, Kinetics, KineticsExpression),
  maplist(negate_coefficient, Reactants, NegatedReactants),
  append(NegatedReactants, Products, StoichiometricSolution),
  simplify_solution(StoichiometricSolution, SimplifiedSolution).
