:- module(
	   transfer_function,
	   [
	       % Grammars
	       transfer_polynomial/1,
	       transfer_function/1,
	       
	       op(200, xfy, ^),
	       op(500, yfx, +),
	       op(500, yfx, -),
	       op(400, yfx, *),
	       
	       % Commands
	       compile_transfer_function/4,
	       compile_transfer_function/3,
	       
	       enable_p_m_mode/0,
	       disable_p_m_mode/0,
	       which_p_m_mode/0,

	       set_p_m_rate/1,
	       set_kinetics_rate/1
	   ]).

:- dynamic
       fresh_index/1,
   plus_m_mode/0,
   plus_m_rate/1,
   kinetics_rate/1.


:- devdoc('\\section{Grammars}').


:- grammar(transfer_polynomial).

transfer_polynomial(s).

transfer_polynomial(N) :-
    number(N).

transfer_polynomial(P ^ N) :-
    transfer_polynomial(P),
    nat(N).

transfer_polynomial(P + Q) :-
    transfer_polynomial(P),
    transfer_polynomial(Q).

transfer_polynomial(P - Q) :-
    transfer_polynomial(P),
    transfer_polynomial(Q).

transfer_polynomial(P * Q) :-
    transfer_polynomial(P),
    transfer_polynomial(Q).


:- grammar(transfer_function).

transfer_function(N / D) :-
    transfer_polynomial(N),
    transfer_polynomial(D).

transfer_get_numerator(N / D, N) :-
    transfer_polynomial(N),
    transfer_polynomial(D).

transfer_get_denominator(N / D, D) :-
    transfer_polynomial(N),
    transfer_polynomial(D).

transfer_function_degree(N / D, Deg) :-
    polynomial_degree(N, DegN),
    polynomial_degree(D, DegD),
    Deg is DegN - DegD.


transfer_strictly_proper(F) :-
    transfer_function_degree(F, D),
    D < 0.

% this predicate indicates whether the values
% are stored in one species per variable or
% in two species, using the convention val(X) = [x+] - [x-].

enable_p_m_mode :-
    biocham_command,
    doc('Each variable corresponds to two species: one for the negative part and one for the positive part.'),
    ( \+(plus_m_mode) -> assert(plus_m_mode) ; true ).

disable_p_m_mode :-
    biocham_command,
    doc('Each variable corresponds to one species.'),
    ( plus_m_mode -> retract(plus_m_mode) ; true ).

which_p_m_mode :-
    biocham_command,
    doc('Displays which mode is being used.'),
    ( plus_m_mode -> write('enabled') ; write('disabled') ), nl.

plus_m_rate(1000).

set_p_m_rate(Rate) :-
    biocham_command,
    type(Rate, number),
    doc('Set the annihilation rate between two complementary species.'),
    retract(plus_m_rate(_)),
    assert(plus_m_rate(Rate)).

kinetics_rate(1000).

set_kinetics_rate(Rate) :-
    biocham_command,
    type(Rate, number),
    doc('Set the final summator rate'),
    retract(kinetics_rate(_)),
    assert(kinetics_rate(Rate)).

% some handy shortcuts

small(N) :-
    N < 0.0001, N > -0.0001.

nat(N) :-
    integer(N),
    N >= 0.

% global counter for anonymous species
fresh_index(0).

get_index(N) :-
    retract(fresh_index(N)),
    M is N + 1,
    assert(fresh_index(M)).

output_counter(0).

get_output_counter(N) :-
    retract(output_counter(N)),
    M is N + 1,
    assert(output_counter(M)).

reset_output_counter :-
    retract(output_counter(_)),
    assert(output_counter(0)).

add_normal(A * s ^ N, 0, A * s ^ N) :-
    number(A), nat(N), !.

add_normal(A * s ^ N, B * s ^ M, Q) :-
    number(A), number(B), nat(N), nat(M),
    (
	N > M
     ->
	 Q = A * s ^ N + (B * s ^ M)
     ;
     N =:= M
     ->
	 C is A + B,
	 (
	     C =:= 0 -> Q = 0
	  ;
	  Q = C * s ^ N
	 )
     ;
     Q = B * s ^ M + (A * s ^ N)
    ), !.

add_normal(A * s ^ N, B * s ^ M + P, Q) :-
    number(A), number(B), nat(N), nat(M),
    (
	N > M
     ->
	 Q = A * s ^ N + (B * s ^ M + P)
     ;
     N =:= M
     ->
	 C is A + B,
	 (
	     C =:= 0 -> Q = P
	  ;
	  Q = C * s ^ N + P
	 )
     ;
     add_normal(A * s ^ N, P, R),
     (
	 R = 0 -> Q = B * s ^ M ; Q = B * s ^ M + R
     )
    ), !.


mult_normal(A * s ^ N, 0, 0) :-
    number(A), nat(N), !.

mult_normal(A * s ^ N, B * s ^ M, C * s ^ O) :-
    number(A), number(B), nat(N), nat(M),
    C is A * B,
    O is N + M, !.

mult_normal(A * s ^ N, B * s ^ M + P, C * s ^ O + Q) :-
    number(A), number(B), nat(N), nat(M),
    C is A * B,
    O is N + M,
    mult_normal(A * s ^ N, P, Q), !.


polynomial_normal(s, 1 * s ^ 1) :- !.

polynomial_normal(0, 0) :- !.

polynomial_normal(A, A * s ^ 0) :-
    number(A), !.

polynomial_normal(s ^ N, 1 * s ^ N) :-
    nat(N), !.

polynomial_normal(A * s ^ N, A * s ^ N) :-
    number(A),
    nat(N), !.

polynomial_normal(P ^ N, Q) :-
    nat(N),
    (
	N > 0
     ->
	 M is N - 1,
	 polynomial_normal(P ^ M, R),
	 polynomial_normal(P * R, Q)
     ;
     N =:= 0
     ->
	 Q = 1 * s ^ 0
    ), !.

polynomial_normal(P1 + P2, Q) :-
    polynomial_normal(P1, Q1),
    polynomial_normal(P2, Q2),
    (
	Q1 = 0
     ->
	 Q = Q2
     ;
     Q1 = A * s ^ N,
     number(A), N >= 0
     ->
	 add_normal(A * s ^ N, Q2, Q)
     ;
     Q1 = A * s ^ N + R,
     number(A), N >= 0
     ->
	 add_normal(A * s ^ N, Q2, S),
	 polynomial_normal(R + S, Q)
    ), !.


polynomial_normal(P1 - P2, Q) :-
    polynomial_normal(P1 + (-1) * P2, Q), !.


polynomial_normal(P1 * P2, Q) :-
    polynomial_normal(P1, Q1),
    polynomial_normal(P2, Q2),
    (
	Q1 = 0
     ->
	 Q = 0
     ;
     Q1 = A * s ^ N
     ->
	 mult_normal(A * s ^ N, Q2, Q)
     ;
     Q1 = A * s ^ N + R
     ->
	 mult_normal(A * s ^ N, Q2, S),
	 polynomial_normal(R * Q2, T),
	 polynomial_normal(S + T, Q)
    ), !.


polynomial_degree(P, -1) :-
    polynomial_normal(P, 0), !.

polynomial_degree(P, N) :-
    polynomial_normal(P, _ * s ^ N + _), !.

polynomial_degree(P, N) :-
    polynomial_normal(P, _ * s ^ N), !.


polynomial_normal_list(0, []).

polynomial_normal_list(A * s ^ N, [[N,A]]).

polynomial_normal_list(A * s ^ N + P, [[N,A]|T]) :-
	polynomial_normal_list(P, T).


polynomial_flat([], []) :- !.

polynomial_flat([[0,A]], [A]) :- !.

polynomial_flat([[N,A]|T], [A|FL]) :-
    Nprime is N - 1,
    polynomial_flat_list(T, Nprime, FL).

polynomial_flat_list([], 0, [0]) :- !.

polynomial_flat_list([], N, [0|FL]) :-
    Nprime is N - 1,
    polynomial_flat_list([], Nprime, FL).

polynomial_flat_list([[0,A]], 0, [A]) :- !.

polynomial_flat_list([[N,A]|T], N, [A|FL]) :-
    Nprime is N - 1,
    polynomial_flat_list(T, Nprime, FL), !.

polynomial_flat_list([[N,A]|T], M, [0|FL]) :-
    Mprime is M - 1,
    polynomial_flat_list([[N,A]|T], Mprime, FL).



% converts a polynomial expression into a coefficient list.
polynomial_list(P, FL) :-
    polynomial_normal(P, Q),
    polynomial_normal_list(Q, L), !,
    polynomial_flat(L, FLinv),
    reverse(FLinv, FL).


% biochemically implements the transfer function A/(s+Alpha)^N, N > 0
% by a series of degre-(1, 0) blocks.
transfer_series_1_0(N, A, Alpha, U, Y) :-
    (
	N = 1
     ->
	 transfer_block_1_0(Alpha, A, U, Y)
     ;
     N > 1
     ->
	 Nprime is N - 1,

	 get_index(NewIndex),
	 atom_concat(U, NewIndex, IntermediarySpecies),
     
	 transfer_series_1_0(Nprime, 1, Alpha, U, IntermediarySpecies),
	 transfer_block_1_0(Alpha, 1, IntermediarySpecies, Y)
     ;
     fail
    ).

% biochemically implements the transfer function A/(s2+Betas+Gamma)^N, N > 0
% by a series of degree-(2, 0) blocks.
transfer_series_2_0(N, A, Beta, Gamma, U, Y, X1) :-
    (
	N = 1
     ->
	 get_index(NewIndex_X1),
	 atom_concat(X1, NewIndex_X1, IntermediarySpecies_X1),
	 
	 transfer_block_2_0(Beta, A, Gamma, 1, U, Y, IntermediarySpecies_X1)
     ;
     N > 1
     ->
	 Nprime is N - 1,

	 get_index(NewIndex_U),
	 atom_concat(U, NewIndex_U, IntermediarySpecies_U),

	 get_index(NewIndex_X1),
	 atom_concat(X1, NewIndex_X1, IntermediarySpecies_X1),
     
	 transfer_series_2_0(Nprime, 1, Beta, Gamma, U, IntermediarySpecies_U, X1),
	 transfer_block_2_0(Beta, A, Gamma, 1, IntermediarySpecies_U, Y, IntermediarySpecies_X1)
     ;
     fail
    ).

% biochemically implements the tranfer function As/(s2+Betas+Gamma)^N, N > 0
% by a series of degree-(2, 1) blocks.
transfer_series_2_1(N, A, Beta, Gamma, U, Y, X2) :-
    (
	N = 1
     ->
	 get_index(NewIndex_X2),
	 atom_concat(X2, NewIndex_X2, IntermediarySpecies_X2),
	 
	 transfer_block_2_1(Beta, A, Gamma, 1, U, Y, IntermediarySpecies_X2)
     ;
     N > 1
     ->
	 Nprime is N - 1,

	 get_index(NewIndex_U),
	 atom_concat(U, NewIndex_U, IntermediarySpecies_U),

	 get_index(NewIndex_X2),
	 atom_concat(X2, NewIndex_X2, IntermediarySpecies_X2),
     
	 transfer_series_2_1(Nprime, 1, Beta, Gamma, U, IntermediarySpecies_U, X2),
	 transfer_block_2_1(Beta, A, Gamma, 1, IntermediarySpecies_U, Y, IntermediarySpecies_X2)
     ;
     fail
    ).
	

transfer_block_1_0(K0, K1, U, Y) :-
    ( plus_m_mode %()
     ->
	 atom_concat(U, '_p', U_p), atom_concat(U, '_m', U_m),
	 atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),

	 % functional rules
	 ( K0 > 0
	  ->
	      add_reaction(K0 * [Y_p] for Y_p => _), add_reaction(K0 * [Y_m] for Y_m => _)
	  ;
	  K0 < 0
	  ->
	      add_reaction(-K0 * [Y_p] for Y_p => _), add_reaction(-K0 * [Y_m] for Y_m => _)
	  ;
	  true
	 ),
	 ( K1 > 0
	  ->
	      add_reaction(K1 * [U_p] for _ =[U_p]=> Y_p), add_reaction(K1 * [U_m] for _ =[U_m]=> Y_m)
	  ;
	  K1 < 0
	  ->
	      add_reaction(-K1 * [U_p] for _ =[U_p]=> Y_m), add_reaction(-K1 * [U_m] for _ =[U_m]=> Y_p)
	  ;
	  true
	 ),

	 plus_m_rate(K),
	 
	 % annihilation rules
	 add_reaction(K * [U_p] * [U_m] for U_p + U_m => _),
	 add_reaction(K * [Y_p] * [Y_m] for Y_p + Y_m => _)
     ;
     add_reaction(K1 * [U] for _ =[U]=> Y),
     add_reaction(K0 * [Y] for Y => _)
    ).

transfer_block_2_0(K0, K1, K2, K3, U, Y, X1) :-
    ( plus_m_mode %()
     ->
	 atom_concat(U, '_p', U_p), atom_concat(U, '_m', U_m),
	 atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
	 atom_concat(X1, '_p', X1_p), atom_concat(X1, '_m', X1_m),

	 % functional rules
	 ( K0 > 0
	  ->
	      add_reaction(K0 * [X1_p] for X1_p => _), add_reaction(K0 * [X1_m] for X1_m => _)
	  ;
	  K0 < 0
	  ->
	      add_reaction(-K0 * [X1_p] for X1_p => _), add_reaction(-K0 * [X1_m] for X1_m => _)
	  ;
	  true
	 ),
	 ( K1 > 0
	  ->
	      add_reaction(K1 * [U_p] for _ =[U_p]=> X1_p), add_reaction(K1 * [U_m] for _ =[U_m]=> X1_m)
	  ;
	  K1 < 0
	  ->
	      add_reaction(-K1 * [U_p] for _ =[U_p]=> X1_m), add_reaction(-K1 * [U_m] for _ =[U_m]=> X1_p)
	  ;
	  true
	 ),
	 ( K2 > 0
	  ->
	      add_reaction(K2 * [Y_p] for _ =[Y_p]=> X1_m), add_reaction(K2 * [Y_m] for _ =[Y_m]=> X1_p)
	  ;
	  K2 < 0
	  ->
	      add_reaction(-K2 * [Y_p] for _ =[Y_p]=> X1_m), add_reaction(-K2 * [Y_m] for _ =[Y_m]=> X1_p)
	  ;
	  true
	 ),
	 ( K3 > 0
	  ->
	      add_reaction(K3 * [X1_p] for _ =[X1_p]=> Y_p), add_reaction(K3 * [X1_m] for _ =[X1_m]=> Y_m)
	  ;
	  K3 < 0
	  ->
	      add_reaction(-K3 * [X1_p] for _ =[X1_p]=> Y_m), add_reaction(-K3 * [X1_m] for _ =[X1_m]=> Y_p)
	  ;
	  true
	 ),

	 plus_m_rate(K),

	 % annihilation rules
	 add_reaction(K * [U_p] * [U_m] for U_p + U_m => _),
	 add_reaction(K * [Y_p] * [Y_m] for Y_p + Y_m => _),
	 add_reaction(K * [X1_p] * [X1_m] for X1_p + X1_m => _)
	 
     ;
     add_reaction(K1 * [U] for _=[U]=> X1),
     add_reaction(-K2 * [Y] for _=[Y]=> X1),
     add_reaction(K3 * [X1] for _=[X1]=> Y),
     add_reaction(K0 * [X1] for X1 => _)
    ).

transfer_block_2_1(K0, K1, K2, K3, U, Y, X2) :-
    transfer_block_2_0(K0, K1, K2, K3, U, X2, Y).


% calculates the partial fraction decomposition of a given transfer function.
get_partial_fraction(N / D, AL, QL, ML) :-
    transfer_function(N / D),
    transfer_strictly_proper(N / D),
    polynomial_list(N, NL),
    polynomial_list(D, DL), 
    partial_fraction(NL, DL, AL, QL, ML).


% converts a partial fraction decomposition into blocks.
translate_list_to_blocks([], [], [], _, _).

translate_list_to_blocks([ALH|ALT], [QLH|QLT], [MLH|MLT], U, Y) :-
    translate_to_blocks(ALH, QLH, MLH, U, Y),
    translate_list_to_blocks(ALT, QLT, MLT, U, Y).

% converts a local group of fractions into blocks
translate_to_blocks(AL, Q, M, U, Y) :-
    % first-order block
    (length(Q, 2)
     ->
	 Q = [Alpha|_],
	 list_to_1(AL, Alpha, M, U, Y)
     ;
     % second-order block
     length(Q, 3)
     ->
	 Q = [Gamma, Beta|_],
	 list_to_2(AL, Beta, Gamma, M, U, Y)
     ;
     fail ).


% converts a local first-order group of fractions into first-order blocks.
list_to_1([], _, 0, _, _).

list_to_1([AH|AT], Alpha, M, U, Y) :-
    kinetics_rate(K),
    ( AH = [A]
     ->
	 ( \+(small(A))
	  ->
	      get_index(N),
	      atom_concat(Y, '_', Y_underscore), atom_concat(Y_underscore, N, Y_N),
	      transfer_series_1_0(M, A, Alpha, U, Y_N),
	      ( plus_m_mode %()
	       ->
		   atom_concat(Y_N, '_p', Y_N_p), atom_concat(Y_N, '_m', Y_N_m),
		   atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
		   add_reaction(K * [Y_N_p] for _=[Y_N_p]=> Y_p),
		   add_reaction(K * [Y_N_m] for _=[Y_N_m]=> Y_m)
	       ; 
	       add_reaction(K * [Y_N] for _=[Y_N]=> Y) )
	  ;
	  true )
     ;
     AH = []
     ->
	 true
     ;
     fail ),
    Mprime is M - 1,
    list_to_1(AT, Alpha, Mprime, U, Y).


% converts a local first-order group of fractions into second-order blocks
% and automatically allot names for the intermediary species.
list_to_2([], _, _, 0, _, _).

list_to_2([BH|BT], Beta, Gamma, M, U, Y) :-
    kinetics_rate(K),
    ( BH = [B0, B1]
     ->
	 ( \+(small(B0)) ->
	       X1 = 'x1_',
	       get_index(N1),
	       atom_concat(Y, '_', Y_underscore1), atom_concat(Y_underscore1, N1, Y_N1),
	       transfer_series_2_0(M, B0, Beta, Gamma, U, Y_N1, X1),
	       ( plus_m_mode %()
		->
		    atom_concat(Y_N1, '_p', Y_N1_p), atom_concat(Y_N1, '_m', Y_N1_m),
		    atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
		    add_reaction(K * [Y_N1_p] for _=[Y_N1_p]=> Y_p),
		    add_reaction(K * [Y_N1_m] for _=[Y_N1_m]=> Y_m)
		; 
		add_reaction(K * [Y_N1] for _=[Y_N1]=> Y) )
	  ; true ),
	 ( \+ (small(B1)) ->
	       X2 = 'x2_',
	       get_index(N2),
	       atom_concat(Y, '_', Y_underscore2), atom_concat(Y_underscore2, N2, Y_N2),
	       transfer_series_2_1(M, B1, Beta, Gamma, U, Y_N2, X2),
	       ( plus_m_mode %()
		->
		    atom_concat(Y_N2, '_p', Y_N2_p), atom_concat(Y_N2, '_m', Y_N2_m),
		    atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
		    add_reaction(K * [Y_N2_p] for _=[Y_N2_p]=> Y_p),
		    add_reaction(K * [Y_N2_m] for _=[Y_N2_m]=> Y_m)
		; 
		add_reaction(K * [Y_N2] for _=[Y_N2]=> Y) )
	  ; true )
     ;
     BH = [B0]
     ->
	 ( \+(small(B0)) ->
	       X1 = 'x1_',
	       get_index(N1),
	       atom_concat(Y, '_', Y_underscore1), atom_concat(Y_underscore1, N1, Y_N1),
	       transfer_series_2_0(M, B0, Beta, Gamma, U, Y_N1, X1),
	       ( plus_m_mode %()
		->
		    atom_concat(Y_N1, '_p', Y_N1_p), atom_concat(Y_N1, '_m', Y_N1_m),
		    atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
		    add_reaction(K * [Y_N1_p] for _=[Y_N1_p]=> Y_p),
		    add_reaction(K * [Y_N1_m] for _=[Y_N1_m]=> Y_m)
		; 
		add_reaction(K * [Y_N1] for _=[Y_N1]=> Y) )
	  ; true )
     ;
     BH = []
     ->
	 true
     ;
     fail ),
    Mprime is M - 1,
    list_to_2(BT, Beta, Gamma, Mprime, U, Y).

% /!\ transfer_function type not recognized

compile_transfer_function(F, U, Y) :-
    biocham_command, %()
    % type(F, transfer_function),
    type(U, object),
    type(Y, object),
    doc('compile a transfer function in variable s into a chemical filter.'),

    kinetics_rate(K),
    ( transfer_strictly_proper(F)
     -> 
	 get_partial_fraction(F, AL, QL, ML),
	 translate_list_to_blocks(AL, QL, ML, U, Y),
	 ( plus_m_mode %()
    ->
	       atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
	       add_reaction(K * [Y_p] for Y_p=>_),
	       add_reaction(K * [Y_m] for Y_m=>_)
	  ;
	  add_reaction(K * [Y] for Y=>_) )
     ;
     write('Error: unproper transfer function, compilation aborted.'), nl
    ).
  
    
compile_transfer_function(NL, DL,  U, Y) :-
    biocham_command, %()
    type(U, object),
    type(Y, object),
    doc('compile a transfer function into a chemical filter.'),

    kinetics_rate(K),
    ( transfer_list_strictly_proper(NL, DL)
     ->
	 partial_fraction(NL, DL, AL, QL, ML),
	 translate_list_to_blocks(AL, QL, ML, U, Y),
	 ( plus_m_mode %()
    ->
	       atom_concat(Y, '_p', Y_p), atom_concat(Y, '_m', Y_m),
	       add_reaction(K * [Y_p] for Y_p=>_),
	       add_reaction(K * [Y_m] for Y_m=>_)
	  ;
	  add_reaction(K * [Y] for Y=>_) )
     ;
     write('Error: unproper transfer function, compilation aborted.'), nl
    ).

:- doc('\\begin{example}
      ').
:- biocham_silent(clear_model).
:- biocham(set_kinetics_rate(1000)).
:- biocham(compile_transfer_function([22, 51, 47, 19, 3], [30, 66, 67, 36, 10, 1], a, b)).
:- biocham(present(a)).
:- biocham(numerical_simulation(method:msbdf)).
:- biocham(plot).
:- doc('
      \\end{example}
      ').


list_degree([], -1).

list_degree([_|T], Deg) :-
    list_degree(T, DegT),
    Deg is DegT + 1.

transfer_list_degree(N, D, Deg) :-
    list_degree(N, DegN),
    list_degree(D, DegD),
    Deg is DegN - DegD.

transfer_list_strictly_proper(N, D) :-
    transfer_list_degree(N, D, Deg),
    Deg < 0.

debug_pol_list(P) :-
    polynomial_list(P, L),
    nl,
    display_list(L), nl.

display_list([]).

display_list([H|T]) :-
    write(H), write(' '),
    display_list(T).
