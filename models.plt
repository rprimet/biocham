:- use_module(library(plunit)).

:- begin_tests(models).


test('new_model', [Reactions == []]) :-
  new_model,
  single_model(Id0),
  add_reaction(a => b),
  new_model,
  single_model(Id1),
  all_items([kind: reaction], Reactions),
  delete_item(Id0),
  delete_item(Id1).

test('new_model inherits from initial') :-
  new_model,
  single_model(Id),
  once((
    models:inherits_from(Id, InitialId),
    get_model_name(InitialId, 'initial')
  )).

test('load', [Name == mapk]) :-
  command(load(library:examples/mapk/mapk)),
  single_model(Id),
  get_model_name(Id, Name),
  delete_item(Id).

test(
  'list_model',
  [Output == 'MA(k)for a=>b.\nparameter(\n  k = 1\n).\n']
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  with_output_to(
    atom(Output),
    list_model
  ).


:- end_tests(models).
