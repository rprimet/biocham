:- module(
  odefunction,
  [
    add_function/1,
    compile_program/1,
    op( 800, fx, para ),
    op( 800, fx, pre  ),
    op( 800, yf, post )
  ]
).

% compilation flow{{{
:- devdoc( "\\section{Compilation Flow}" ).

:- devdoc( "Either use 'compile_program' to compile the program from file, or use the 'add_function' directly.
            'compile_program' transforms the program to the list passed to 'add_function' and call it, so it is just like a syntax sugar." ).

:- devdoc( "The remaining flow is described below:" ).

:- devdoc( "\\begin{itemize}" ).
:- devdoc( "\\item 'expression_to_ODE' parses the expression and call the corresponding xxx_to_PIVP." ).
:- devdoc( "\\item 'xxx_to_PIVP' uses the OdeSystem to store the corresponding PIVP." ).
:- devdoc( "\\item 'import_reactions_from_ode_system' compiles the overall PIVP into chemical reactions." ).
:- devdoc( "\\end{itemize}" ).
% end compilation flow
% }}}
% program syntax{{{
%:- doc( "\\section{Program Syntax}" ).

%:- doc( "\\begin{itemize}" ).
%:- doc( "\\item compile_program" ).

%:- doc( "This function views each prolog term as a statement.
%         This means a statement end with ',' or '.'." ).
:- doc("In this simple program syntax each statement must be terminated by ',' or '.'." ).

:- devdoc( "Internally, it uses prolog 'read' to parse the file." ).

:- grammar( statement ).

statement( Statement ) :-
  assignment( Statement ).

statement( Statement ) :-
  control_flow_statement( Statement ).

:- grammar( assignment ).

assignment( Variable = Expression ) :-
  variable( Variable ),
  expression( Expression ).

:- grammar( variable ).

variable( Variable ) :-
  name( Variable ).

:- grammar( expression ).

expression( Expression ) :-
  arithmetic_expression( Expression ).

expression( Expression ) :-
  condition( Expression ).

:- grammar_doc( "The support of arithmetic_expression is not complete, but most of them are supported." ).

:- grammar( control_flow_statement ).

control_flow_statement( if( Condition ) ) :-
  condition( Condition ).

control_flow_statement( while( Condition ) ) :-
  condition( Condition ).

control_flow_statement( 'else' ).
control_flow_statement( 'endif' ).
control_flow_statement( 'endwhile' ).

:- doc( "Statements terminated by ',' will execute parallelly with next statement." ).

%:- doc( "\\item add_function" ).

%:- doc( "This function only accepts input list with each element of this form: <lhs> = <rhs>." ).

:- devdoc( "It is the backend of 'compile_program'." ).

:- grammar( lhs ).

lhs( Variable ) :-
  variable( Variable ).

lhs( Keyword ) :-
  control_flow_keyword( Keyword ).

:- grammar( rhs ).

rhs( para Expression ) :-
  expression( Expression ).

rhs( pre Expression ) :-
  expression( Expression ).

rhs( pre Expression post ) :-
  expression( Expression ).

rhs( Expression post ) :-
  expression( Expression ).

rhs( Expression ) :-
  expression( Expression ).

:- grammar( control_flow_keyword ).

control_flow_keyword( 'if_tag'    ).
control_flow_keyword( 'while'     ).

:- grammar_doc( "When using these two keywords, the rhs serves as the condition." ).

control_flow_keyword( 'else_tag'  ).
control_flow_keyword( 'endif'     ).
control_flow_keyword( 'endwhile'  ).

:- grammar_doc( "When using these three keywords, the rhs does not matter." ).

:- devdoc( "The reason of using 'if_tag' and 'else_tag' rather than 'if' and 'else' is that 'if' and 'else' are operators in biocham, which will cause parsing error." ).

:- doc( "The operators 'pre' and 'post' are used to describe the procedural structure of a program,
         so if not specified, the list element will execute parallelly, which makes it possible to describe sequential and parallel part of a program." ).

:- doc( "To mix the sequential and parallel computation, statements which executed parallelly need to add 'para' before the expression, except the last statement." ).

:- doc( "Currently it is user's responsibility to ensure the execution of last statement finishs after other parallelly executed statements." ).

:- devcom( "Currently, only the 'variable = ( expression post )' syntax handles the initialization of program.
            Theoretically, 'if_tag' and 'while' keyrword should also handle the problem,
            since these two keywords can appear in the first statement." ).

%:- doc( "\\end{itemize}" ).
% end program syntax
% }}}
% interface{{{
:- devdoc('\\section{Commands}').

add_function(FunctionList) :-% {{{
  biocham_command(*),
  type(FunctionList, '*'(term = term)),
  devdoc( "Compile the statement into chemical reactions." ),
  doc('
    adds reactions to compute the result of a function of the current variables in the concentration of a new variable, at steady state.
    This command only accepts input list with each element of this form: <lhs> = <rhs>.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(present(x,1)),
  biocham(present(y,3)),
  biocham(add_function(z=x+y)),
  biocham(list_ode),
  biocham(list_model),
  biocham(numerical_simulation(time: 10)),
  biocham(plot),
  doc('
    \\end{example}
  '),
  new_ode_system(OdeSystem),
  get_option(fast_rate, Fast), % for import_reactions it is better to use a kinetic parameter instead of a number 
  set_parameter(fast, Fast),
  create_ode(FunctionList, OdeSystem),
  import_reactions_from_ode_system(OdeSystem),
  delete_item(OdeSystem).
% }}}
compile_program( InputFile ) :-% {{{
  % compile_program( +InputFile )
  biocham_command,
  type( InputFile, input_file ),
  devdoc( "Transform the terms in a file into internal list format of 'add_function'." ),
  devdoc( "Only generate sequential program." ),

  doc( "Compile the high-level language program from file.
        \\begin{example}" ),
        doc( "We can write the program '\\texttt{z = 2. x = z + 1. y = x - z.}' in file 'compile_program_example.bc' and compile it in reactions. This is equivalent to '\\texttt{add_function( z = ( 2 post ), x = ( pre z + 1 post ), y = ( pre x - z )).}' and can be compared with the parallel version of the program, '\\texttt{z = 2, x = z + 1, y = x - z.}', without pre and post conditions."),
  biocham_silent(
    prolog( 'open( \'compile_program_example.bc\', write, ExampleFile ),
            writeln( ExampleFile, \'z = 2.\' ),
            writeln( ExampleFile, \'x = z + 1.\' ),
            writeln( ExampleFile, \'y = x - z.\' ),
            close( ExampleFile )' )
    ),

  biocham_silent( clear_model ),
  biocham( compile_program( 'compile_program_example.bc' ) ),
  biocham( numerical_simulation( time: 80, method: msbdf ) ),
  biocham( plot( show:{ x_p, x_n, y_p, y_n, z_p, z_n } ) ),
  biocham( clear_model ),
  biocham_silent(
    prolog( 'open( \'compile_program_parallel_example.bc\', write, ExampleFile ),
            writeln( ExampleFile, \'z = 2,\' ),
            writeln( ExampleFile, \'x = z + 1,\' ),
            writeln( ExampleFile, \'y = x - z.\' ),
            close( ExampleFile )' )
    ),

  biocham( compile_program( 'compile_program_parallel_example.bc' ) ),
  biocham( numerical_simulation( time: 20 ) ),
  biocham( plot( show:{ x_p, x_n, y_p, y_n, z_p, z_n } ) ),
  doc( "Here is another example shows the compilation result of euclid division: "),
  doc( "'\\texttt{
        a = 8.
        b = 3.
        while( a >= b ).
        a = a - b.
        q = q + 1.
        endwhile.
        r = a.
        }'" ),
  biocham_silent(
    prolog( 'open( \'compile_program_example_division.bc\', write, ExampleFile ),
            writeln( ExampleFile, \'a = 8.\' ),
            writeln( ExampleFile, \'b = 3.\' ),
            writeln( ExampleFile, \'while( a >= b ).\' ),
            writeln( ExampleFile, \'a = a - b.\' ),
            writeln( ExampleFile, \'q = q + 1.\' ),
            writeln( ExampleFile, \'endwhile.\' ),
            writeln( ExampleFile, \'r = a.\' ),
            close( ExampleFile )' )
    ),

  biocham_silent( clear_model ),
  biocham( compile_program( 'compile_program_example_division.bc' ) ),
  biocham( numerical_simulation( time: 400, method: msbdf ) ),
  biocham( plot( show:{ a_p, a_n, b_p, b_n, q_p, q_n, r_p, r_n } ) ),
  doc( "and the parallel version." ),
  doc( "'\\texttt{
        a = 8,
        b = 3.
        while( a >= b ).
        t1 = a - b,
        t2 = q + 1.
        a = t1,
        q = t2.
        endwhile.
        r = a.
        }'" ),
  biocham_silent(
    prolog( 'open( \'compile_program_example_division_parallel.bc\', write, ExampleFile ),
            writeln( ExampleFile, \'a = 8,\' ),
            writeln( ExampleFile, \'b = 3.\' ),
            writeln( ExampleFile, \'while( a >= b ).\' ),
            writeln( ExampleFile, \'t1 = a - b,\' ),
            writeln( ExampleFile, \'t2 = q + 1.\' ),
            writeln( ExampleFile, \'a = t1,\' ),
            writeln( ExampleFile, \'q = t2.\' ),
            writeln( ExampleFile, \'endwhile.\' ),
            writeln( ExampleFile, \'r = a.\' ),
            close( ExampleFile )' )
    ),

  biocham_silent( clear_model ),
  biocham( compile_program( 'compile_program_example_division_parallel.bc' ) ),
  biocham( numerical_simulation( time: 400, method: msbdf ) ),
  biocham( plot( show:{ a_p, a_n, b_p, b_n, q_p, q_n, r_p, r_n } ) ),
  doc( "
        \\end{example}
        " ),

  access_file( InputFile, read ),
  open( InputFile, read, File ),
  compile( File, [] ),
  close( File ).
% }}}
:- devdoc( "\\section{Command Backend}" ).

create_ode( FunctionList, OdeSystem ) :-% {{{
  type( FunctionList, list          ),
  type( OdeSystem,    'ode system'  ),
  devdoc( "Compile the statements into PIVP." ),
  fail.

% pre process of PIVP compilation
create_ode(_, OdeSystem) :-
  constant( counter, CounterName ),
  (
    nb_current( CounterName, CounterInit )
  ->
    true
  ;
    CounterInit = 0
  ),
  set_counter( CounterName, CounterInit ),
  constant( true,   True  ),
  constant( false,  False ),
  add_constant( True  ),
  add_constant( False ),
  new_variable( OdeSystem, [ Pre, Post ] ),
  set_condition( precondition,  Pre   ),
  set_condition( postcondition, Post  ),
  fail.

% initialize variables with initial value ( variable initialize with 'present' command )
create_ode(_, OdeSystem) :-
  enumerate_molecules(L), % useless if item backtracks on Object
  member(Object, L),
  get_initial_concentration(Object, Concentration),
  set_ode_initial_value(OdeSystem, Object, Concentration),
  fail.

% compile the statement into PIVP
create_ode(FunctionList, OdeSystem) :-
  member(Variable = Value, FunctionList),
  (
    is_variable_in_expression( Value, Variable )
  ->
    % deal with statement with variable in the expression
    new_variable( OdeSystem, T ),
    (
      Value = ( pre _ post )
    ->
      ExpressionT = Value,
      Expression  = ( pre T post )
    ;
      Value = ( pre ValueT )
    ->
      ExpressionT = ( pre ValueT post ),
      Expression  = ( pre T )
    ;
      ExpressionT = Value,
      Expression  = ( pre T post )
    ),
    expression_to_ODE( OdeSystem, ExpressionT, T )
  ;
    Expression = Value
  ),
  expression_to_ODE( OdeSystem, Expression, Variable ),
  fail.

% post process of PIVP compilation
create_ode(_, OdeSystem) :-
  add_fast_annihilation( OdeSystem ),
  remove_variable( _ ),
  fail.

create_ode(_,_).
% }}}
/* compile{{{
 */
compile( In, FunctionList ) :-
  % compile( +InStream, +FunctionList )
  type( In,           stream          ),
  type( FunctionList, { term = term } ),
  devdoc( "Parse the input file of 'compile_program' and transform it into 'add_function' format." ),

  is_stream( In ),
  read( In, Term ),
  compile( Term, FunctionList, NewFunctionList ),
  (
    Term == end_of_file
  ->
    add_function( NewFunctionList )
  ;
    compile( In, NewFunctionList )
  ).

compile( end_of_file, FunctionList, NewFunctionList ) :-
  % compile( end_of_file, +FunctionList, -NewFuctionList )
  !,
  (
    last( FunctionList, Variable = ( Term post ) )
  ->
    % remove post in the last statement
    append( FunctionListPre, [ Variable = ( Term post ) ], FunctionList   ),
    append( FunctionListPre, [ Variable = ( Term ) ],      FunctionListT  )
  ;
    FunctionListT = FunctionList
  ),
  (
    member( _ = ( _ post ), FunctionListT )
  ->
    NewFunctionList = FunctionListT
  ;
    remove_para( FunctionListT, NewFunctionList )
  ).

compile( if( Condition ), FunctionList, NewFunctionList ) :-
  % compile( if( +Condition ), +FunctionList, -NewFunctionList )
  condition( Condition ),
  !,
  (
    FunctionList == []
  ->
    Function = [ if_tag = ( Condition ) ]
  ;
    Function = [ if_tag = ( pre Condition ) ]
  ),
  append( FunctionList, Function, NewFunctionList ).

compile( else, FunctionList, NewFunctionList ) :-
  % compile( else, +FunctionList, -NewFunctionList )
  !,
  append( FunctionList, [ else_tag = 0 ], NewFunctionList ).

compile( while( Condition ), FunctionList, NewFunctionList ) :-
  % compile( while( +Condition ), +FunctionList, -NewFunctionList )
  condition( Condition ),
  !,
  append( FunctionList, [ while = ( Condition ) ], NewFunctionList ).

compile( Variable = Value, FunctionList, NewFunctionList ) :-
  % compile( +Variable = +Expression, +FunctionList, -NewFunctionList )
  arithmetic_expression( Value ),
  !,
  (
    FunctionList == []
  ->
    Options = [ first(true) ]
  ;
    Options = []
  ),
  compile( Variable = Value, FunctionList, NewFunctionList, Options ).

compile( Keyword, FunctionList, NewFunctionList ) :-
  % compile( +Keyword, +FunctionList, -NewFunctionList )
  keyword( Keyword ),
  !,
  append( FunctionList, [ Keyword = 0 ], NewFunctionList ).

compile( A ',' B, FunctionList, NewFunctionList ) :-
  % compile( +Statement ',' +Statement, +FunctionList, -NewFunctionList )
  !,
  (
    FunctionList == []
  ->
    Options = [ first(true) ]
  ;
    Options = []
  ),
  compile( A ',' B, FunctionList, NewFunctionList, Options ).

% this is only for documentation
compile( Statement, FunctionList, NewFunctionList ) :-
  type( Statement,        term            ),
  type( FunctionList,     { term = term } ),
  type( NewFunctionList,  { term = term } ),
  devdoc( "Compile the statement \\emph{Statement}." ),
  devdoc( "add \\emph{Statement} to \\emph{FunctionList}, producing \\emph{NewFnuctionList}." ),
  fail.

compile( A ',' B, FunctionList, NewFunctionList, Options ) :-
  % compile( +Statement ',' +Statement, +FunctionList, -NewFunctionList, +Options )
  % Options: parallel(+Bool), first(+Bool)
  !,
  compile( A, FunctionList,   FunctionListT,    [ parallel(true) ]  ),
  compile( B, FunctionListT,  NewFunctionList,  Options             ).

compile( Variable = Value, FunctionList, NewFunctionList, Options ) :-
  % compile( +Variable = +Expression, +FunctionList, -NewFuntionList, +Options )
  % Options: parallel(+Bool), first(+Bool)
  arithmetic_expression( Value ),
  !,
  (
    option( parallel(true), Options )
  ->
    Function = [ Variable = ( para Value ) ]
  ;
    (
      option( first(true), Options )
    ->
      Function = [ Variable = ( Value post ) ] % first statement
    ;
      Function = [ Variable = ( pre Value post ) ]
    )
  ),
  append( FunctionList, Function, NewFunctionList ).

% this is only for documentation
compile( Statement, FunctionList, NewFunctionList, Options ) :-
  type( Statement,        term            ),
  type( FunctionList,     { term = term } ),
  type( NewFunctionList,  { term = term } ),
  type( Options,          { option      } ),
  devdoc( "Compile the statement \\emph{Statement}." ),
  devdoc( "add \\emph{Statement} to \\emph{FunctionList}, producing \\emph{NewFnuctionList}." ),
  devdoc( "options: first(Boolean), parallel(Boolean)." ),
  fail.

% }}}
remove_para( FunctionList, ResultFunctionList ) :-% {{{
  % remove_para( +FunctionList, -ResultFunctionList )
  length( FunctionList,       Length ),
  length( ResultFunctionList, Length ),
  remove_para( Length, FunctionList, ResultFunctionList ).

remove_para( Index, FunctionList, ResultFunctionList ) :-
  % remove_para( +Index, +FunctionList, -ResultFunctionList )
  nth1( Index, FunctionList,        Variable = ( para Term )  ),
  nth1( Index, ResultFunctionList,  Variable = ( Term )       ),
  !,
  (
    Index == 1
  ->
    true
  ;
    NextIndex is Index - 1,
    remove_para( NextIndex, FunctionList, ResultFunctionList )
  ).

remove_para( Index, FunctionList, ResultFunctionList ) :-
  % remove_para( +Index, +FunctionList, -ResultFunctionList )
  nth1( Index, FunctionList,        Elem ),
  nth1( Index, ResultFunctionList,  Elem ),
  !,
  (
    Index == 1
  ->
    true
  ;
    NextIndex is Index - 1,
    remove_para( NextIndex, FunctionList, ResultFunctionList )
  ).% }}}
% end interface
% }}}
% helper functions{{{
:- devdoc( "\\section{Helper Functions}" ).
:- devdoc( "\\begin{itemize}" ).

% init variable{{{
:- devdoc( "\\item init variable" ).
:- devdoc( "Initialize the variable with differential symantics." ).

init_variable( OdeSystem, VariableName, Value ) :-
  % init_variable( +OdeSystem, +VariableName, +Value )
  type( OdeSystem,    "ode system"  ),
  type( VariableName, name          ),
  type( Value,        number        ),
  devdoc( "Initialize variable with name \\emph{VariableName} to \\emph{Value} in the \\emph{OdeSystem}." ),

  (
    Value >= 0
  ->
    init_variable( OdeSystem, VariableName, Value, 0 )
  ;
    ValueN is -Value,
    init_variable( OdeSystem, VariableName, 0, ValueN )
  ).

init_variable( OdeSystem, VariableName, PInit, NInit ) :-
  % init_variable( +OdeSystem, +VariableName, +PInit, +NInit )
  type( OdeSystem,    "ode system"  ),
  type( VariableName, name          ),
  type( PInit,        number        ),
  type( NInit,        number        ),
  devdoc( "Initialize variable with name \\emph{VariableName} in the \\emph{OdeSystem}." ),
  devdoc( "The positive part is initialized to \\emph{PInit}, and the negative part is initialized to \\emph{NInit}." ),

  variable( VariableName, [ P, N ] ),

  set_ode_initial_value( OdeSystem, P, PInit ),
  set_ode_initial_value( OdeSystem, N, NInit ).
% }}}
% get ode initial value{{{
:- devdoc( "\\item get ode initial value" ).

get_ode_initial_value( _, [ ConstantP, ConstantN ], Value ) :-
  % get_ode_initial_value( ?OdeSystem, +Variable, -Value )
  number( ConstantP ),
  number( ConstantN ),
  Value = [ ConstantP, ConstantN ].

get_ode_initial_value( OdeSystem, [ P, N ], [ PInit, NInit ] ) :-
  % get_ode_initial_value( +OdeSystem, +Variable, -ValueDiff )
  variable( _, [ P, N ] ),
  item( [ parent: OdeSystem, kind: initial_concentration, item: init( P = PInit ) ] ),
  item( [ parent: OdeSystem, kind: initial_concentration, item: init( N = NInit ) ] ).

% this is for documentation
get_ode_initial_value( OdeSystem, Variable, Value ) :-
  type( OdeSystem, "ode system"             ),
  type( Variable,   "differential semantic" ),
  type( Value,      "differential semantic" ),
  devdoc( "Get the initial value of variable \\emph{Variable} in the ode system \\emph{OdeSystem}, and store in \\emph{Value}." ),
  fail.
% }}}
% add ode{{{
:- devdoc( "\\item add ode" ).
/*  add ode
*
*   add the ode for a vairable into the ode system.
*/
add_ode( OdeSystem, [ P, N ], DP, DN ) :-
  % add_ode( +OdeSystem, +Variable, +DP, +DN )
  add_ode( OdeSystem, d( P ) / dt  = DP ),
  add_ode( OdeSystem, d( N ) / dt  = DN ).

% this is for documentation
add_ode( OdeSystem, Variable, DP, DN ) :-
  type( OdeSystem,  "ode system" ),
  type( Variable,   "differential semantic" ),
  type( DP,         "ode formula" ),
  type( DN,         "ode formula" ),
  devdoc( "Add ode formula to variable \\emph{Variable} in the ode system \\emph{OdeSystem}." ),
  devdoc( "\\emph{DP} is added to the positive part, and \\emph{DN} is added to the negative part." ),
  fail.
% }}}
% get ode{{{
:- devdoc( "\\item get ode" ).
/* get ode
 *
 * get the ode for a variable.
 */
get_ode( OdeSystem, [ P, N ], DP, DN ) :-
  % get_ode( +OdeSystem, +Variable, -DP, -DN )
  findall(
    DPT,
    item( [ parent: OdeSystem, kind: ode, key: P, item: ( d( P ) / dt = DPT ) ] ),
    DP
  ),
  findall(
    DNT,
    item( [ parent: OdeSystem, kind: ode, key: N, item: ( d( N ) / dt = DNT ) ] ),
    DN
  ).
% }}}
% variable( ?VariableName, ?VariableDifferential ){{{
:- devdoc( "\\item variable" ).
:- devdoc( "A variable with name x uses differential semantic, and is stored as [ x_p, x_n ].
            variable( VariableName, VariableDifferential ) function can be used to get to variable information from the database.
            Either using variable name to get the differential semantic, or using differential semantic to get the variable name.
            Here are some functions to handle the variable database." ).

:- dynamic variable/2.

remove_variable( VariableName ) :-
  % remove_variable( +VariableName )
  type( VariableName, name ),
  devdoc( "Remove variable with name \\emph{VariableName} from the database" ),

  retractall( variable( VariableName, _ ) ).

add_variable( VariableName, VariableDiff ) :-
  % add_variable( +VariableName, +VariableDiff )
  type( VariableName, name                    ),
  type( VariableDiff, "differential semantic" ),
  devdoc( "Add variable with name \\emph{VariableName} and differential semantic \\emph{VariableDiff} to the database." ),
  devdoc( "It is recommended to use add_variable( VariableName ) instead." ),

  assertz( variable( VariableName, VariableDiff ) ).

add_variable( VariableName ) :-
  % add_variable( +VariableName )
  type( VariableName, name ),
  devdoc( "Add variable with name \\emph{VariableName} to the database." ),
  devdoc( "It will implicitly add differential semantic [ \\emph{VariableName}_p, \\emph{VariableName}_n ]." ),

  (
    nonvar( VariableName )
  ->
    true
  ;
    throw( error( type_error( nonvar, VariableName ) ) )
  ),
  (
    variable( VariableName, _ )
  ->
    true
  ;
    atom_concat( VariableName, '_p', P ),
    atom_concat( VariableName, '_n', N ),
    add_variable( VariableName, [ P, N ] )
  ).

add_constant( Constant ) :-
  % add_constant( +Constant:int )
  type( Constant, number ),
  devdoc( "Add constant \\emph{Constant} to the variable database." ),

  (
    number( Constant )
  ->
    true
  ;
    throw( error( type_error( number, Constant ) ) )
  ),
  (
    variable( Constant, _ )
  ->
    true
  ;
    (
      Constant >= 0
    ->
      add_variable( Constant, [ Constant, 0 ] )
    ;
      ConstantN is -Constant,
      add_variable( Constant, [ 0, ConstantN ] )
    )
  ).
% }}}
%  get_variable_name( ?Variable ){{{
:- devdoc( "\\item get variable name" ).

get_variable_name( VariableName ) :-
  % get_variable_name( +Variable )
  type( VariableName, name ),
  devdoc( "Generate temperory variable name if \\emph{VariableName} is not instantiate, do nothing otherwise." ),

  nonvar( VariableName ), !.

get_variable_name( VariableName ) :-
  % get_variable_name( --Variable )
  var( VariableName ),
  constant( counter, CounterName ),

  peek_count( CounterName, Counter ),
  constant( tempPrefix, TPrefix ),
  atom_concat( TPrefix, Counter, VariableName ),
  count( CounterName, Counter ).
% }}}
%  new variable{{{
:- devdoc( "\\item new variable" ).
:- devdoc( "Generate and initialize variable." ).

new_variable( Name ) :-
  % new_variable( ?Name )
  type( Name, name ),
  devdoc( "Generate variable with name \\emph{Name} without initial value." ),
  \+ list( Name ),
  get_variable_name( Name ),
  add_variable( Name ).

new_variable( [ Name | NameList ] ) :-
  % new_variable( ?NameList )
  \+ list( Name ),
  list( NameList ),
  new_variable( Name ),
  (
    NameList == []
  ->
    true
  ;
    new_variable( NameList )
  ).

new_variable( OdeSystem, Name ) :-
  % new_variable( +OdeSystem, ?Name )
  type( OdeSystem,  "ode system"  ),
  type( Name,       name          ),
  devdoc( "Generate variable with name \\emph{Name} and set initial value to 0 in the ode system \\emph{OdeSystem}." ),
  \+ list( Name ),

  new_variable( OdeSystem, Name, 0, 0 ).

new_variable( OdeSystem, [ Name | NameList ] ) :-
  % new_variable( +OdeSystem, ?NameList )
  \+ list( Name ),
  list( NameList ),
  new_variable( OdeSystem, Name ),
  (
    NameList == []
  ->
    true
  ;
    new_variable( OdeSystem, NameList )
  ).

new_variable( OdeSystem, Name, Value ) :-
  % new_variable( +OdeSystem, ?Name, +Value:int )
  type( OdeSystem,  "ode system"  ),
  type( Name,       name          ),
  type( Value,      number        ),
  devdoc( "Generate variable with name \\emph{Name} and set initial value to \\emph{Value} in the ode system \\emph{OdeSystem}." ),

  new_variable( Name ),
  init_variable( OdeSystem, Name, Value ).

new_variable( OdeSystem, Name, PInit, NInit ) :-
  % new_variable( +OdeSystem, ?Name, +PInit:int, +NInit:int )
  type( OdeSystem,  "ode system"  ),
  type( Name,       name          ),
  type( PInit,      number        ),
  type( NInit,      number        ),
  devdoc( "Generate variable with name \\emph{Name} and set initial value in the ode system \\emph{OdeSystem}." ),
  devdoc( "The positive part is initialized to \\emph{PInit}, and the negative part is initialized to \\emph{NInit}." ),

  new_variable( Name ),
  init_variable( OdeSystem, Name, PInit, NInit ).
% }}}
% condition{{{
:- devdoc( "\\item condition" ).
:- devdoc( "The fact of each condition should exist only one.
            Here are some functions to handle the control flow condition global variables.
            Supported conditions is recorded in the \\emph{control_flow_condition} predicate." ).

set_condition( Condition, Name ) :-
  % set_condition( +Condition, +Name )
  type( Condition,  control_flow_condition  ),
  type( Name,       name                    ),
  devdoc( "Set the condition \\emph{Condition} to be the variable with name \\emph{Name}." ),

  control_flow_condition( Condition ),
  nb_setval( Condition, Name ).

get_condition( Condition, Name ) :-
  % get_condition( +Condition, --Name )
  type( Condition,  control_flow_condition  ),
  type( Name,       name                    ),
  devdoc( "Get the condition \\emph{Condition} variable with name \\emph{Name} that is set previously." ),
  devdoc( "If there is no such condition being set, a constant \\emph{conNull} is returned." ),

  control_flow_condition( Condition ),
  (
    nb_current( Condition, Name )
  ->
    true
  ;
    constant( conNull, Name )
  ).

clear_condition( Condition, Name ) :-
  % clear_condition( +Condition, --Name )
  type( Condition,  control_flow_condition  ),
  type( Name,       name                    ),
  devdoc( "This is the same as 'get_condition', but it also delete the global condition \\emph{Condition}." ),

  get_condition( Condition, Name ),
  (
    constant( conNull, Name )
  ->
    true
  ;
    nb_delete( Condition )
  ).
% }}}
% nnary_expression_to_ODE( +OdeSystem, +ExpressionList, -VariableENameList, ?VariableZName ){{{
:- devdoc( "\\item xxx expression to ODE" ).
:- devdoc( "Here are some function to recursively parse and check the expression." ).

nnary_expression_to_ODE( OdeSystem, [], [], Z ) :-
  new_variable( OdeSystem, Z ).

nnary_expression_to_ODE( OdeSystem, [ Expression | ExpressionList ], [ E | EList ], Z ) :-
  expression_to_ODE( OdeSystem, Expression, E ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ).

% this is only for documentation
nnary_expression_to_ODE( OdeSystem, ExpressionList, VariableENameList, VariableZName ) :-
  type( OdeSystem,          "ode system"    ),
  type( ExpressionList,     { expression  } ),
  type( VariableENameList,  { name        } ),
  type( VariableZName,      name            ),
  devdoc( "Generate temperary variable with name \\emph{VariableENameList} in the ode system \\emph{OdeSystem} to record the value of \\emph{ExpressionList}." ),
  devdoc( "Generate temperary variable with name \\emph{VariableZName} if necessary." ),
  devdoc( "The length of \\emph{VariableENameList} should be the same as \\emph{ExpressionList}." ),
  devdoc( "This function is used to recursively parse the expression to be assigned to \\emph{Z}." ),
  fail.
% }}}
% nnary_expression_to_ODE( +OdeSystem, +ExpressionList, -VariableENameList, ?VariableZName, -EvalValueList ){{{
nnary_expression_to_ODE( OdeSystem, [], [], Z, [] ) :-
  new_variable( OdeSystem, Z ).

nnary_expression_to_ODE( OdeSystem, [ Expression | ExpressionList ], [ E | EList ], Z, [ EvalValue | EvalValueList ] ) :-
  expression_to_ODE( OdeSystem, Expression, E, EvalValue ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z, EvalValueList ).

% this is only for documentation
nnary_expression_to_ODE( OdeSystem, ExpressionList, VariableENameList, VariableZName, EvalValueList ) :-
  type( OdeSystem,          "ode system,"   ),
  type( ExpressionList,     { expression  } ),
  type( VariableENameList,  { name        } ),
  type( VariableZName,      name            ),
  type( EvalValueList,      { number }      ),
  devdoc( "This version of nnary_expression_to_ODE evaluate the expression at compiling time and store in \\emph{EvalValueList}." ),
  devdoc( "The length of \\emph{EvalValueList} should be the same as \\emph{ExpressionList}." ),
  fail.
% }}}
unary_expression_to_ODE( OdeSystem, Expression0, E0, Z, EvalValue0 ) :-% {{{
  % unary_expression_to_ODE( +OdeSystem, +Expression0, -VariableE0Name, ?VariableZName, -EvalValue0:int )
  type( OdeSystem,    "ode system"  ),
  type( Expression0,  expression    ),
  type( E0,           name          ),
  type( Z,            name          ),
  type( EvalValue0,   number        ),
  devdoc( "This is a version of nnary_expression_to_ODE that is convenient for unary case and evaluate the expression." ),

  nnary_expression_to_ODE( OdeSystem, [ Expression0 ], [ E0 ], Z, [ EvalValue0 ] ).
% }}}
unary_expression_to_ODE( OdeSystem, Expression0, E0, Z ) :-% {{{
  % unary_expression_to_ODE( +OdeSystem, +Expression0, -VariableE0Name, ?VariableZName )
  type( OdeSystem,    "ode system"  ),
  type( Expression0,  expression    ),
  type( E0,           name          ),
  type( Z,            name          ),
  devdoc( "This is a version of nnary_expression_to_ODE that is convenient for unary case." ),

  nnary_expression_to_ODE( OdeSystem, [ Expression0 ], [ E0 ], Z ).
% }}}
binary_expression_to_ODE( OdeSystem, Expression0, Expression1, E0, E1, Z, EvalValue0, EvalValue1 ) :-% {{{
  % binary_expression_to_ODE( +OdeSystem, +Expression0, +Expression1, -VariableE0Name, -VariableE1Name, ?VariableZName, -EvalValue0:int ,-EvalValue1:int )
  type( OdeSystem,    "ode system"  ),
  type( Expression0,  expression    ),
  type( Expression1,  expression    ),
  type( E0,           name          ),
  type( E1,           name          ),
  type( Z,            name          ),
  type( EvalValue0,   number        ),
  type( EvalValue1,   number        ),
  devdoc( "This is a version of nnary_expression_to_ODE that is convenient for binary case and evaluate the expression." ),

  nnary_expression_to_ODE( OdeSystem, [ Expression0, Expression1 ], [ E0, E1 ], Z, [ EvalValue0, EvalValue1 ] ).
% }}}
binary_expression_to_ODE( OdeSystem, Expression0, Expression1, E0, E1, Z ) :-% {{{
  % binary_expression_to_ODE( +OdeSystem, +Expression0, +Expression1, -VariableE0Name, -VariableE1Name, ?VariableZName )
  type( OdeSystem,    "ode system"  ),
  type( Expression0,  expression    ),
  type( Expression1,  expression    ),
  type( E0,           name          ),
  type( E1,           name          ),
  type( Z,            name          ),
  devdoc( "This is a version of nnary_expression_to_ODE that is convenient for binary case." ),

  nnary_expression_to_ODE( OdeSystem, [ Expression0, Expression1 ], [ E0, E1 ], Z ).
% }}}
trinary_expression_to_ODE( OdeSystem, Expression0, Expression1, Expression2, E0, E1, E2, Z, EvalValue0, EvalValue1, EvalValue2 ) :-% {{{
  % trinary_expression_to_ODE( +OdeSystem, +Expression0, +Expression1, +Expression2, -VariableE0Name, -VariableE1Name, -VariableE2Name, ?VariableZName, -Evalvalue0, -Evalvalue1, -Evalvalue2 )
  type( OdeSystem,    "ode system"  ),
  type( Expression0,  expression    ),
  type( Expression1,  expression    ),
  type( Expression2,  expression    ),
  type( E0,           name          ),
  type( E1,           name          ),
  type( E2,           name          ),
  type( Z,            name          ),
  type( EvalValue0,   number        ),
  type( EvalValue1,   number        ),
  type( EvalValue2,   number        ),
  devdoc( "This is a version of nnary_expression_to_ODE that is convenient for trinary case and evaluate the expression." ),

  nnary_expression_to_ODE( OdeSystem, [ Expression0, Expression1, Expression2 ], [ E0, E1, E2 ], Z, [ EvalValue0, EvalValue1, EvalValue2 ] ).
% }}}
trinary_expression_to_ODE( OdeSystem, Expression0, Expression1, Expression2, E0, E1, E2, Z ) :-% {{{
  % trinary_expression_to_ODE( +OdeSystem, +Expression0, +Expression1, +Expression2, -VariableE0Name, -VariableE1Name, -VariableE2Name, ?VariableZName )
  type( OdeSystem,    "ode system"  ),
  type( Expression0,  expression    ),
  type( Expression1,  expression    ),
  type( Expression2,  expression    ),
  type( E0,           name          ),
  type( E1,           name          ),
  type( E2,           name          ),
  type( Z,            name          ),
  devdoc( "This is a version of nnary_expression_to_ODE that is convenient for trinary case." ),

  nnary_expression_to_ODE( OdeSystem, [ Expression0, Expression1, Expression2 ], [ E0, E1, E2 ], Z ).
% }}}
% arithmetic_expression_to_ODE( +OdeSystem, +ExpressionList, -VariableENameList, ?VariableZName ){{{
arithmetic_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ) :-
  list( ExpressionList ),
  list( EList ),
  !,

  type( OdeSystem,      "ode system"              ),
  type( ExpressionList, { arithmetic_expression } ),
  type( EList,          { name }                  ),
  type( Z,              name                      ),
  devdoc( "'nnary_expression_to_ODE' with the test of arithmetic_expression." ),

  forall( member( Expression, ExpressionList ), arithmetic_expression( Expression ) ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ).

arithmetic_expression_to_ODE( OdeSystem, Expression, E, Z ) :-
  type( OdeSystem, "ode system"           ),
  type( Expression, arithmetic_expression ),
  type( E,          name                  ),
  type( Z,          name                  ),
  devdoc( "'arithmetic_expression_to_ODE' for unary expression." ),

  !,
  arithmetic_expression_to_ODE( OdeSystem, [ Expression ], [ E ], Z ).

arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ) :-
  type( OdeSystem,    "ode system"          ),
  type( ExpressionX,  arithmetic_expression ),
  type( ExpressionY,  arithmetic_expression ),
  type( X,            name                  ),
  type( Y,            name                  ),
  type( Z,            name                  ),
  devdoc( "'arithmetic_expression_to_ODE' for binary expression." ),

  arithmetic_expression_to_ODE( OdeSystem, [ ExpressionX, ExpressionY ], [ X, Y ], Z ).
% }}}
% arithmetic_expression_to_ODE( +OdeSystem, +ExpressionList, -VariableENameList, ?VariableZName, -Evalvaluelist ){{{
arithmetic_expression_to_ODE( OdeSystem, ExpressionList, EList, Z, EvalValueList ) :-
  list( ExpressionList ),
  list( EList ),
  list( EvalValueList ),
  !,

  type( OdeSystem,      "ode system"              ),
  type( ExpressionList, { arithmetic_expression } ),
  type( EList,          { name }                  ),
  type( Z,              name                      ),
  type( EvalValueList,  { number }                ),
  devdoc( "'arithmetic_expression_to_ODE' with compiling time evaluation value." ),

  forall( member( Expression, ExpressionList ), arithmetic_expression( Expression ) ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z, EvalValueList ).

arithmetic_expression_to_ODE( OdeSystem, Expression, E, Z, EvalValue ) :-
  type( OdeSystem, "ode system"           ),
  type( Expression, arithmetic_expression ),
  type( E,          name                  ),
  type( Z,          name                  ),
  type( EvalValue,  number                ),
  devdoc( "'arithmetic_expression_to_ODE' for unary expression and with compiling time evaluation value." ),

  !,
  arithmetic_expression_to_ODE( OdeSystem, [ Expression ], [ E ], Z, [ EvalValue ] ).

arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ) :-
  type( OdeSystem,    "ode system"          ),
  type( ExpressionX,  arithmetic_expression ),
  type( ExpressionY,  arithmetic_expression ),
  type( X,            name                  ),
  type( Y,            name                  ),
  type( Z,            name                  ),
  devdoc( "'arithmetic_expression_to_ODE' for binary expression and with compiling time evaluation value." ),

  arithmetic_expression_to_ODE( OdeSystem, [ ExpressionX, ExpressionY ], [ X, Y ], Z, [ EvalValueX, EvalValueY ] ).
% }}}
%  condition_expression_to_ODE( +OdeSystem, +ExpressionList, -VariableENameList, ?VariableZName ){{{
condition_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ) :-
  list( ExpressionList ),
  list( EList ),
  !,

  type( OdeSystem,      "ode system"  ),
  type( ExpressionList, { condition } ),
  type( EList,          { name }      ),
  type( Z,              name          ),
  devdoc( "'nnary_expression_to_ODE' with the test of condition." ),

  forall( member( Expression, ExpressionList ), condition( Expression ) ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ).

condition_expression_to_ODE( OdeSystem, Expression, E, Z ) :-
  type( OdeSystem, "ode system" ),
  type( Expression, condition   ),
  type( E,          name        ),
  type( Z,          name        ),
  devdoc( "'condition_expression_to_ODE' for unary expression." ),

  !,
  condition_expression_to_ODE( OdeSystem, [ Expression ], [ E ], Z ).

condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ) :-
  type( OdeSystem,    "ode system"  ),
  type( ExpressionX,  condition     ),
  type( ExpressionY,  condition     ),
  type( X,            name          ),
  type( Y,            name          ),
  type( Z,            name          ),
  devdoc( "'condition_expression_to_ODE' for binary expression." ),

  condition_expression_to_ODE( OdeSystem, [ ExpressionX, ExpressionY ], [ X, Y ], Z ).
% }}}
% condition_expression_to_ODE( +OdeSystem, +ExpressionList, -VariableENameList, ?VariableZName, -EvalValueList ){{{
condition_expression_to_ODE( OdeSystem, ExpressionList, EList, Z, EvalValueList ) :-
  list( ExpressionList ),
  list( EList ),
  list( EvalValueList ),
  !,

  type( OdeSystem,      "ode system"  ),
  type( ExpressionList, { condition } ),
  type( EList,          { name }      ),
  type( Z,              name          ),
  type( EvalValueList,  { number }    ),
  devdoc( "'condition_expression_to_ODE' with compiling time evaluation value." ),

  forall( member( Expression, ExpressionList ), condition( Expression ) ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z, EvalValueList ).

condition_expression_to_ODE( OdeSystem, Expression, E, Z, EvalValue ) :-
  type( OdeSystem, "ode system" ),
  type( Expression, condition   ),
  type( E,          name        ),
  type( Z,          name        ),
  type( EvalValue,  number      ),
  devdoc( "'condition_expression_to_ODE' for unary expression and with compiling time evaluation value." ),

  !,
  condition_expression_to_ODE( OdeSystem, [ Expression ], [ E ], Z, [ EvalValue ] ).

condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ) :-
  type( OdeSystem,    "ode system"  ),
  type( ExpressionX,  condition     ),
  type( ExpressionY,  condition     ),
  type( X,            name          ),
  type( Y,            name          ),
  type( Z,            name          ),
  devdoc( "'arithmetic_expression_to_ODE' for binary expression and with compiling time evaluation value." ),

  condition_expression_to_ODE( OdeSystem, [ ExpressionX, ExpressionY ], [ X, Y ], Z, [ EvalValueX, EvalValueY ] ).
% }}}
% add fast annihilation{{{
:- devdoc( "\\item add fast annihilation" ).
:- devdoc( "Generate fast annihilation ode for a variable.
            The 'fast' rate constant is initialized with the fast_rate option." ).
:- devcom( "The 'fast' rate parameter shoulod rather depend on the ODE." ).

add_fast_annihilation( OdeSystem ) :-
  % add_fast_annihilation( +OdeSystem )
  type( OdeSystem, "ode system" ),
  devdoc( "Generate all the fast annihilation ode for variable database and add to the ode system \\emph{OdeSystem}." ),

  forall(
    get_variable( Variable ),
    add_fast_annihilation( OdeSystem, Variable )
  ).

add_fast_annihilation( OdeSystem, Variable ) :-
  % add_fast_annihilation( +OdeSystem, +Variable )
  type( OdeSystem,  "ode system"            ),
  type( Variable,   "differential semantic" ),
  devdoc( "Generate fast annihilation ode for variable \\emph{Variable} and add to the ode system \\emph{OdeSystem}." ),

  get_ode( OdeSystem, Variable, DP, DN ),
  (
    DP == [] ; DN == [] % add fast annihilation only if both derivatives of diffential semantic are not 0
  ->
    true
  ;
  %  constant( fast, Fast ),
  % better to use the fast parameter
  add_fast_annihilation( OdeSystem, Variable, fast )
  ).

add_fast_annihilation( OdeSystem, [ P, N ], Fast ) :-
  % add_fast_annihilation( +OdeSystem, +Variable, +Fast:int )
  add_ode( OdeSystem, [ P, N ], -Fast * P * N, -Fast * P * N ).

% this is only for documentation
add_fast_annihilation( OdeSystem, Variable, Fast ) :-
  type( OdeSystem,  "ode system"            ),
  type( Variable,   "differential semantic" ),
  type( Fast,       number                  ),
  devdoc( "Generate fast annihilation ode for variable \\emph{Variable} with fast coefficient \\emph{Fast} and add to the ode system \\emph{OdeSystem}." ),
  fail.
% }}}
get_variable( Variable ) :-% {{{
  % get_variable( --Variable )
  type( Variable, "differential semantic" ),
  devdoc( "Get a non constant variable from the database." ),
  devdoc( "This is a helper function for 'add_fast_annihilation'." ),

  variable( VariableName, Variable ),
  \+ number( VariableName ).
% }}}
% is_variable_in_expression( +Expression, +VariableName ){{{
:- devdoc( "\\item is variable in expression" ).

is_variable_in_expression( Expression, Variable ) :-
  type( Expression, expression  ),
  type( Variable,   name        ),
  devdoc( "Test if a variable with name \\emph{VariableName} is in the \\emph{Expression}." ),

  compound( Expression ),
  atomic( Variable ),

  Expression =.. TermList,
  (
    member( Variable, TermList )
  ->
    true
  ;
    member( Term, TermList ),
    is_variable_in_expression( Term, Variable )
  ).
% }}}
:- devdoc( "\\end{itemize}" ).
% end helper functions
% }}}
% synthesis from expression to pivp{{{
:- devdoc( "\\section{From Expression to PIVP}" ).
:- devdoc( "\\begin{itemize}" ).

% expression parser{{{
:- devdoc( "\\item Expression Parser " ).

:- discontiguous expression_to_ODE/4.
:- discontiguous expression_to_ODE/3.
% constant{{{
expression_to_ODE( _, Constant, Constant, Constant ) :-% {{{
  % expression_to_ODE( ?OdeSystem, +Constant, -Constant, -Constant )
  number( Constant ),
  !,
  add_constant( Constant ).
% }}}
expression_to_ODE( _, Constant, Constant ) :-% {{{
  % expression_to_ODE( ?OdeSystem, +Constant, -Constant )
  number( Constant ),
  !,
  add_constant( Constant ).
% }}}
expression_to_ODE( _, BooleanConstant, Constant, Constant ) :-% {{{
  % expression_to_ODE( ?OdeSystem, +BooleanConstant, -Constant, -Constant )
  constant( BooleanConstant, Constant ),
  !,
  add_constant( Constant ).
% }}}
expression_to_ODE( _, BooleanConstant, Constant ) :-% {{{
  % expression_to_ODE( ?OdeSystem, +BooleanConstant, -Constant )
  constant( BooleanConstant, Constant ),
  !,
  add_constant( Constant ).
% }}}
% end constant
% }}}
% branch{{{
/* if_tag, else_tag, end_if{{{
 *
 *        statements before branch
 *                   |
 *                   | pre
 *                   |
 *              if statement
 *                   |
 *          +--------+--------+
 *          |                 |
 *          | then_pre        | else_pre
 *          |                 |
 *  then statements     else stataments
 *          |                 |
 *          | then_post       | else_post
 *          |                 |
 *          +--------+--------+
 *                   |
 *                   | then_post || else_post
 *                   |
 *         statements after branch
 *
 *  global variables:
 *
 *    - branch_else_precondition
 *      - 'else_pre'
 *    - branch_else_precondition_post
 *      - the 'pre_post' of the first statement in else branch
 *    - branch_then_postcondition
 *      - 'then_post'
 *    - branch_then_precondition_post
 *      - the 'pre_post' of the first statement after the branch
 */% }}}
:- devdoc( '\\begin{itemize}' ).
:- devdoc( '\\item Global Variables In Branch Structure' ).
:- devdoc( '\\begin{itemize}' ).
:- devdoc( '\\item branch_else_precondition' ).
:- devdoc( '\\item branch_else_precondition_post' ).
:- devdoc( '\\item branch_else_precondition_post_next' ).
:- devdoc( '\\item branch_then_precondition_post' ).
:- devdoc( '\\item branch_then_precondition_post_next' ).
:- devdoc( '\\item branch_then_postcondition' ).
:- devdoc( '\\item branch_then_post_precondition_post' ).
:- devdoc( '\\end{itemize}' ).
:- devdoc( '\\end{itemize}' ).

expression_to_ODE( OdeSystem, Expression post, Z ) :- % if_tag{{{
  % expression_to_ODE( +OdeSystem, +Expression, if_tag )
  Z == if_tag,
  !,
  expression_to_ODE( OdeSystem, Expression, if_tag ).

expression_to_ODE( OdeSystem, Expression, Z ) :- % if_tag
  % expression_to_ODE( +OdeSystem, +Expression, if_tag )
  Z == if_tag,
  !,
  (
    Expression = ( pre Condition )
  ->
    get_condition( precondition,      Pre     ),
    get_condition( precondition_post, PrePost )
  ;
    Condition = Expression,
    constant( conNull, Pre )
  ),
  condition( Condition ),

  new_variable( OdeSystem,
                [ C, ThenPrePostNext, ElsePrePostNext, ThenPre, ElsePre, ThenPrePost, ElsePrePost ] ),

  expression_to_ODE( OdeSystem, Condition, C ),
  (
    constant( conNull, Pre )
  ->
    branch_to_PIVP( OdeSystem, if_tag, C, ThenPre, ElsePre )
  ;
    branch_to_PIVP( OdeSystem, if_tag, C, Pre, PrePost, ThenPrePostNext, ElsePrePostNext, ThenPre, ElsePre )
  ),

  set_condition( branch_then_precondition_post_next,  ThenPrePostNext ),
  set_condition( branch_then_precondition_post,       ThenPrePost     ),
  set_condition( branch_else_precondition_post_next,  ElsePrePostNext ),
  set_condition( branch_else_precondition,            ElsePre         ),
  set_condition( branch_else_precondition_post,       ElsePrePost     ),
  set_condition( precondition,                        ThenPre         ),
  set_condition( precondition_post,                   ThenPrePost     ).
% }}}
expression_to_ODE( _, _, Z ) :- % else_tag{{{
  % expression_to_ODE( +OdeSystem, +Expression, else_tag )
  Z == else_tag,
  !,
  clear_condition(  branch_else_precondition,       ElsePre     ),
  get_condition(    branch_else_precondition_post,  PrePost     ),
  get_condition(    precondition,                   ThenPost    ),
  get_condition(    precondition_post,              PrePostNext ),

  set_condition( branch_then_postcondition,           ThenPost    ),
  set_condition( branch_then_post_precondition_post,  PrePostNext ),
  set_condition( precondition,                        ElsePre     ),
  set_condition( precondition_post,                   PrePost     ).
% }}}
expression_to_ODE( OdeSystem, _, Z ) :- % endif{{{
  % expression_to_ODE( +OdeSystem, +Expression, endif )
  Z == endif,
  !,
  clear_condition( branch_then_postcondition, ThenPostT ),
  (
    constant( conNull, ThenPostT )
  ->
    clear_condition(  branch_else_precondition,       ElsePost        ),
    get_condition(    branch_else_precondition_post,  ElsePostPrePost ),
    get_condition(    precondition,                   ThenPost        ),
    get_condition(    precondition_post,              ThenPostPrePost )
  ;
    ThenPost = ThenPostT,
    clear_condition(  branch_then_post_precondition_post, ThenPostPrePost ),
    get_condition(    precondition,                       ElsePost        ),
    get_condition(    precondition_post,                  ElsePostPrePost )
  ),
  clear_condition( branch_then_precondition_post_next, ThenPrePostNext ),
  clear_condition( branch_then_precondition_post,      ThenPrePost     ),
  clear_condition( branch_else_precondition_post_next, ElsePrePostNext ),
  clear_condition( branch_else_precondition_post,      ElsePrePost     ),
  new_variable( OdeSystem, [ Post, PrePostNext ] ),

  branch_to_PIVP( OdeSystem, endif, ThenPost, ThenPostPrePost, ThenPrePost, ThenPrePostNext, ElsePost, ElsePostPrePost, ElsePrePost, ElsePrePostNext, Post, PrePostNext ),
  set_condition( precondition,      Post        ),
  set_condition( precondition_post, PrePostNext ).
% }}}
expression_to_ODE( OdeSystem, if Condition then ExpressionT else ExpressionE, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  condition( Condition ),
  arithmetic_expression( ExpressionT ),
  arithmetic_expression( ExpressionE ),
  !,
  trinary_expression_to_ODE( OdeSystem, Condition, ExpressionT, ExpressionE, C, T, E, Z ),
  branch_to_PIVP( OdeSystem, C, T, E, Z ).
% }}}
% end branch
% }}}
% loop{{{
/* while, end_while{{{
 *
 *   statements before loop
 *              |
 *              | pre
 *              |
 *  +--- while statement
 *  |           |
 *  |           +--------------+
 *  |           |              |
 *  |           | in_loop_pre  |
 *  |           |              |
 *  |   loop statements        | no_loop_pre
 *  |           |              |
 *  |           | loop_post    |
 *  |           |              |
 *  +-----------+              |
 *                             |
 *              +--------------+
 *              |
 *              |
 *    statements after loop
 *
 *  global variables:
 *
 *    - loop_postcondition
 *      - 'loop_post'
 *    - loop_postcondition_post
 *      - the next 'pre_post' of the last statement in loop
 *    - loop_noloop_precondition
 *      - 'no_loop_pre'
 *    - loop_noloop_precondition_post
 *      - the 'pre_post' of the first statement after the loop
 */% }}}
:- devdoc( '\\begin{itemize}' ).
:- devdoc( '\\item Global Variables In Loop Structure' ).
:- devdoc( '\\begin{itemize}' ).
:- devdoc( '\\item loop_postcondition' ).
:- devdoc( '\\item loop_post_precondition_post' ).
:- devdoc( '\\item loop_noloop_precondition' ).
:- devdoc( '\\item loop_noloop_precondition_post' ).
:- devdoc( '\\end{itemize}' ).
:- devdoc( '\\end{itemize}' ).

expression_to_ODE( OdeSystem, Expression post, Z ) :- % while{{{
  % expression_to_ODE( +OdeSystem, +Expression, while )
  Z == while,
  !,
  expression_to_ODE( OdeSystem, Expression, while ).

expression_to_ODE( OdeSystem, pre Expression, Z ) :- % while
  % expression_to_ODE( +OdeSystem, +Expression, while )
  Z == while,
  !,
  expression_to_ODE( OdeSystem, Expression, while ).

expression_to_ODE( OdeSystem, Condition, Z ) :- % while
  % expression_to_ODE( +OdeSystem, +Expression, while )
  Z == while,
  !,
  condition( Condition ),

  expression_to_ODE( OdeSystem, Condition, C ),
  (
    get_condition( precondition, LoopPre )
  ->
    get_condition( precondition_post, LoopPrePost )
  ;
    constant( true, LoopPre ),
    add_constant( LoopPre ),
    new_variable( OdeSystem, LoopPrePost )
  ),

  new_variable( OdeSystem,
                [ LoopPost, InLoopPre, InLoopPrePost, NoLoopPre, NoLoopPrePost ] ),

  set_condition( loop_postcondition,            LoopPost      ),
  set_condition( loop_post_precondition_post,   LoopPrePost   ),
  set_condition( loop_noloop_precondition,      NoLoopPre     ),
  set_condition( loop_noloop_precondition_post, NoLoopPrePost ),
  loop_to_PIVP( OdeSystem, while, C, LoopPre, LoopPrePost, InLoopPre, InLoopPrePost, NoLoopPre, NoLoopPrePost, LoopPost ),
  set_condition( precondition,      InLoopPre     ),
  set_condition( precondition_post, InLoopPrePost ).
% }}}
expression_to_ODE( OdeSystem, _, Z ) :- % endwhile{{{
  % expression_to_ODE( +OdeSystem, +Expression, endwhile )
  Z == endwhile,
  !,
  clear_condition(  loop_noloop_precondition,       NoLoopPre             ),
  clear_condition(  loop_noloop_precondition_post,  NoLoopPrePost         ),
  clear_condition(  loop_postcondition,             LoopPost              ),
  clear_condition(  loop_post_precondition_post,    LoopPostPrePostNext   ),
  get_condition(    precondition,                   LoopPostT             ),
  get_condition(    precondition_post,              LoopPostPrePostNextT  ),
  loop_to_PIVP( OdeSystem, endwhile, LoopPostT, LoopPostPrePostNextT, LoopPost, LoopPostPrePostNext ),
  set_condition( precondition,      NoLoopPre     ),
  set_condition( precondition_post, NoLoopPrePost ).
% }}}
% end loop
% }}}
% procedural{{{
expression_to_ODE( OdeSystem, pre Expression post, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  new_variable( OdeSystem, [ PostNext, PrePostNext ] ),

  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  get_condition( precondition,      Pre     ),
  get_condition( precondition_post, PrePost ),
  get_condition( postcondition,     Post    ),
  inter_to_PIVP( OdeSystem, E, Z, Pre, PrePost, PrePostNext, Post ),
  set_condition( precondition,      Post        ),
  set_condition( precondition_post, PrePostNext ),
  set_condition( postcondition,     PostNext    ).
% }}}
expression_to_ODE( OdeSystem, pre Expression, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  clear_condition( precondition_post, PrePost ),
  clear_condition( precondition,      Pre     ),
  clear_condition( postcondition,     _       ),
  pre_to_PIVP( OdeSystem, E, Z, Pre, PrePost ).
% }}}
expression_to_ODE( OdeSystem, Expression post, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  new_variable( OdeSystem, [ PostNext, PrePostNext ] ),

  unary_expression_to_ODE( OdeSystem, Expression, E, Z, EvalValue ),
  get_condition( precondition,  Pre   ),
  get_condition( postcondition, Post  ),
  post_to_PIVP( OdeSystem, E, Z, EvalValue, Pre, PrePostNext, Post ),
  set_condition( precondition,      Post        ),
  set_condition( precondition_post, PrePostNext ),
  set_condition( postcondition,     PostNext    ).
% }}}
expression_to_ODE( OdeSystem, para Expression, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  get_condition( precondition,  Pre   ),
  get_condition( postcondition, Post  ),
  parallel_to_PIVP( OdeSystem, E, Z, Pre, Post ).
% }}}
% end procedural
% }}}
% conditional operations{{{
expression_to_ODE( OdeSystem, ExpressionX = ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  equal_to_PIVP(  OdeSystem, X, Y, Z ),
  (
    EvalValueX == EvalValueY
  ->
    constant( true, EvalValue )
  ;
    constant( false, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX = ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  equal_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX < ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  lessthan_to_PIVP( OdeSystem, X, Y, Z ),
  (
    EvalValueX < EvalValueY
  ->
    constant( true, EvalValue )
  ;
    constant( false, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX < ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  lessthan_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX <= ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  lessequal_to_PIVP(  OdeSystem, X, Y, Z ),
  (
    EvalValueX =< EvalValueY
  ->
    constant( true, EvalValue )
  ;
    constant( false, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX <= ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  lessequal_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX > ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  greaterthan_to_PIVP(  OdeSystem, X, Y, Z ),
  (
    EvalValueX > EvalValueY
  ->
    constant( true, EvalValue )
  ;
    constant( false, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX > ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  greaterthan_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX >= ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  greaterequal_to_PIVP( OdeSystem, X, Y, Z ),
  (
    EvalValueX >= EvalValueY
  ->
    constant( true, EvalValue )
  ;
    constant( false, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX >= ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  greaterequal_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX or ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  or_to_PIVP(   OdeSystem, X, Y, Z ),
  (
    constant( false, EvalValueX ),
    constant( false, EvalValueY )
  ->
    constant( false, EvalValue )
  ;
    constant( true, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX or ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  or_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX and ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  and_to_PIVP( OdeSystem, X, Y, Z ),
  (
    constant( true, EvalValueX ),
    constant( true, EvalValueY )
  ->
    constant( true, EvalValue )
  ;
    constant( false, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX and ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  and_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, not Expression , Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  condition_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  not_to_PIVP( OdeSystem, T, Z ),
  (
    constant( true, EvalValueT )
  ->
    constant( false, EvalValue )
  ;
    constant( true, EvalValue )
  ).
% }}}
expression_to_ODE( OdeSystem, not Expression , Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  condition_expression_to_ODE( OdeSystem, Expression, T, Z ),
  not_to_PIVP( OdeSystem, T, Z ).
% }}}
% end conditional operations
% }}}
% arithmetic operations{{{
expression_to_ODE( OdeSystem, ExpressionX + ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -VariableStableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  addition_to_PIVP( OdeSystem, X, Y, Z ),
  EvalValue is EvalValueX + EvalValueY.
% }}}
expression_to_ODE( OdeSystem, ExpressionX + ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  addition_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX - ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -Evalvalue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  substraction_to_PIVP( OdeSystem, X, Y, Z ),
  EvalValue is EvalValueX - EvalValueY.
% }}}
expression_to_ODE( OdeSystem, ExpressionX - ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  substraction_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX * ExpressionY, Z, EvalValue ) :- %{{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -Evalvalue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  multiplication_to_PIVP( OdeSystem, X, Y, Z ),
  EvalValue is EvalValueX * EvalValueY.
% }}}
expression_to_ODE( OdeSystem, ExpressionX * ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  multiplication_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX / ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -Evalvalue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  division_to_PIVP( OdeSystem, X, Y, Z ),
  EvalValue is EvalValueX / EvalValueY.
% }}}
expression_to_ODE( OdeSystem, ExpressionX / ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  division_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, ExpressionX ^ ExpressionY, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  power_to_PIVP(  OdeSystem, X, Y, Z ),
  EvalValue is EvalValueX ^ EvalValueY.
% }}}
expression_to_ODE( OdeSystem, ExpressionX ^ ExpressionY, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  power_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, Operator + Expression, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  operator( Operator ),
  arithmetic_expression( Expression ),
  !,
  expression_to_ODE( OdeSystem, + Expression, Z, EvalValue ).
% }}}
expression_to_ODE( OdeSystem, Operator + Expression, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  operator( Operator ),
  arithmetic_expression( Expression ),
  !,
  expression_to_ODE( OdeSystem, + Expression, Z ).
% }}}
expression_to_ODE( OdeSystem, + Expression, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValue ),
  positive_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, + Expression, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  positive_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, Operator - Expression, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  operator( Operator ),
  arithmetic_expression( Expression ),
  !,
  expression_to_ODE( OdeSystem, - Expression, Z, EvalValue ).
% }}}
expression_to_ODE( OdeSystem, Operator - Expression, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  operator( Operator ),
  arithmetic_expression( Expression ),
  !,
  expression_to_ODE( OdeSystem, - Expression, Z ).
% }}}
expression_to_ODE( OdeSystem, - Expression, Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  negative_to_PIVP( OdeSystem, T, Z ),
  EvalValue is -EvalValueT.
% }}}
expression_to_ODE( OdeSystem, - Expression, Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  negative_to_PIVP( OdeSystem, T, Z ).
% }}}
% functions{{{
expression_to_ODE( OdeSystem, abs( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, E, Z, EvalValueT ),
  abs_to_PIVP( OdeSystem, E, Z ),
  EvalValue is abs( EvalValueT ).
% }}}
expression_to_ODE( OdeSystem, abs( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, E, Z ),
  abs_to_PIVP( OdeSystem, E, Z ).
% }}}
expression_to_ODE( OdeSystem, min( ExpressionX ',' ExpressionY ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  min_to_PIVP( OdeSystem, X, Y, Z ),
  EvalValue is min( EvalValueX, EvalValueY ).
% }}}
expression_to_ODE( OdeSystem, min( ExpressionX ',' ExpressionY ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  min_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, max( ExpressionX ',' ExpressionY ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z, EvalValueX, EvalValueY ),
  max_to_PIVP( OdeSystem, X, Y, Z ),
  EvalValue is max( EvalValueX, EvalValueY ).
% }}}
expression_to_ODE( OdeSystem, max( ExpressionX ',' ExpressionY ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  max_to_PIVP( OdeSystem, X, Y, Z ).
% }}}
expression_to_ODE( OdeSystem, log( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  log_to_PIVP( OdeSystem, T, Z ),
  EvalValue is log( EvalValueT ).
% }}}
expression_to_ODE( OdeSystem, log( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  log_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, exp( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  exp_to_PIVP( OdeSystem, T, Z ),
  EvalValue is exp( EvalValueT ).
% }}}
expression_to_ODE( OdeSystem, exp( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  exp_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, cos( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  cos_to_PIVP( OdeSystem, T, Z ),
  EvalValue is cos( EvalValueT ).
% }}}
expression_to_ODE( OdeSystem, cos( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  cos_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, sin( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  sin_to_PIVP( OdeSystem, T, Z ),
  EvalValue is sin( EvalValueT ).
% }}}
expression_to_ODE( OdeSystem, sin( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  sin_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, hill( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  constant( true, True ),
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  hill_to_PIVP( OdeSystem, T, Z, _ ),
  EvalValue is True * EvalValueT ^ 5 / ( 0.1 + EvalValueT ^ 5 ).
% }}}
expression_to_ODE( OdeSystem, hill( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  hill_to_PIVP( OdeSystem, T, Z, _ ).
% }}}
expression_to_ODE( OdeSystem, pos_tanh( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  constant( true, True ),
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  pos_tanh_to_PIVP( OdeSystem, T, Z ),
  EvalValue is True * tanh( 6 * ( EvalValueT - 0.8 ) ).
% }}}
expression_to_ODE( OdeSystem, pos_tanh( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  pos_tanh_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, neg_tanh( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  constant( true ,True ),
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  neg_tanh_to_PIVP( OdeSystem, T, Z ),
  EvalValue is True * tanh( -6 * ( EvalValueT - 0.8 ) ).
% }}}
expression_to_ODE( OdeSystem, neg_tanh( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  neg_tanh_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, pos_step( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  constant( true, True ),
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  pos_step_to_PIVP( OdeSystem, T, Z ),
  EvalValue is True * ( 1 + tanh( 6 * ( EvalValueT - 0.8 ) ) ).
% }}}
expression_to_ODE( OdeSystem, pos_step( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  pos_step_to_PIVP( OdeSystem, T, Z ).
% }}}
expression_to_ODE( OdeSystem, neg_step( Expression ), Z, EvalValue ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName, -EvalValue )
  !,
  constant( true ,True ),
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z, EvalValueT ),
  neg_step_to_PIVP( OdeSystem, T, Z ),
  EvalValue is True * ( 1 + tanh( -6 * ( EvalValueT - 0.8 ) ) ).
% }}}
expression_to_ODE( OdeSystem, neg_step( Expression ), Z ) :-% {{{
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  neg_step_to_PIVP( OdeSystem, T, Z ).
% }}}
% end functions}}}
% end arithmetic operations
% }}}
% switch{{{
expression_to_ODE( OdeSystem, switch_y( ExpressionSX, ExpressionSY ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionSX, ExpressionSY, SX, SY, Z ),
  switch_to_PIVP( OdeSystem, SX, SY, _, Z ).

expression_to_ODE( OdeSystem, switch_x( ExpressionSX, ExpressionSY ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionSX, ExpressionSY, SX, SY, Z ),
  switch_to_PIVP( OdeSystem, SX, SY, Z, _ ).
% end switch
% }}}
% variable{{{
expression_to_ODE( OdeSystem, X, X, EvalValue ) :-
  % expression_to_ODE( +OdeSystem, +VariableName, -VariableName, -EvalValue )
  !,
  add_variable( X ),
  argument_to_PIVP( OdeSystem, X ),
  item( [ parent: OdeSystem, kind: initial_concentration, item: init( X = EvalValue ) ] ).

expression_to_ODE( OdeSystem, X, X ) :-
  % expression_to_ODE( +OdeSystem, +VariableName, -VariableName )
  !,
  add_variable( X ),
  argument_to_PIVP( OdeSystem, X ).

expression_to_ODE( OdeSystem, X, Z, EvalValue ) :-
  % expression_to_ODE( +OdeSystem, +VariableXName, -VariableZName, -EvalValue:int )
  !,
  unary_expression_to_ODE( OdeSystem, X, T, Z, EvalValue ),
  assign_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, X, Z ) :-
  % expression_to_ODE( +OdeSystem, +VariableXName, -VariableZName )
  !,
  unary_expression_to_ODE( OdeSystem, X, T, Z ),
  assign_to_PIVP( OdeSystem, T, Z ).

% end variable
% }}}
% this is only for documentation{{{
expression_to_ODE( OdeSystem, Expression, VariableName ) :-
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  type( OdeSystem,    "ode system"  ),
  type( Expression,   expression    ),
  type( VariableName, name          ),
  devdoc( "Compile the expression into ODE with differential semantic so that it can then be compiled into chemical reactions." ),
  devdoc( "The ode will be stored in the \\emph{OdeSystem}." ),
  devdoc( "The meaning of the variable with name \\emph{VariableName} will be the result value of the \\emph{Expression}." ),
  devdoc( "More details of implementation are described in the comments of each expression." ),
  fail.
% }}}
% this is only for documentation{{{
expression_to_ODE( OdeSystem, Expression, VariableName, EvalValue ) :-
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  type( OdeSystem,    "ode system"  ),
  type( Expression,   expression    ),
  type( VariableName, name          ),
  type( EvalValue,    number        ),
  devdoc( "This version of 'expression_to_ODE' evaluates the expression \\emph{Expression} at compiling time." ),
  devdoc( "The evaluated value is \\emph{EvalValue}." ),
  devdoc( "This value is used for the test of the initialization of program." ),
  fail.% }}}
% end expression parser
% }}}
% unit synthesizer{{{
:- devdoc( "\\item Compilation Unit" ).
:- devdoc( "Basically, the compilation units have the form xxx_to_PIVP( OdeSystem, VariableEName, ..., VariableName ).
            These functions compile a specific expression unit into PIVP and store it in the \\emph{OdeSystem}.
            An expression may consist of many subexpressions, these subexpressions are \\emph{VarialbeEName}s.
            The meaning of the variable with name \\emph{VariableName} will be the result of the expression unit.
            The internal of many 'xxx_to_PIVP's separate into two part.
            'xxx_to_ODE' deals with the ode formula, and 'init_xxx' deals with the initial value." ).

:- devdoc( "There is a version of xxx_to_PIVP generate a variable \\emph{Stable} to indicate whether the expression is stable.
            The stable test is to check whether the derivative of the ODE is 0.
            Currently, only the equivalence operation is used to test the initialization of the program." ).
:- devcom( "It remains to be checked whether the stable signal is useful." ).
:- devcom( "Currently, when the value of variables gets large or the program gets complex( ex. nested while loop ), the simulation of gsl will fail. Simulation with method 'msbdf' seems to solve the problem." ).

:- devdoc( "Only some special compilation units are described below.
            More details of impelmentation are described in the comments of each compilation unit." ).
:- devdoc( "\\begin{itemize}" ).

/*  z = x :{{{
*
*     z_p - z_n = ( x_p - x_n )
*
*     dz_p = x_p - z_p
*     dz_n = x_n - z_n
*/
assign_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % assign_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  assign_to_PIVP( OdeSystem, X, Z ),
  equal_to_PIVP(  OdeSystem, X, Z, Stable ).

assign_to_PIVP( OdeSystem, X, Z ) :-
  % assign_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  variable( Z,  ZDiff       ),
  variable( X,  [ XP, XN ]  ),

  assign_to_ODE( OdeSystem, ZDiff, XP, XN ).

assign_to_ODE( OdeSystem, [ P, N ], PValue, NValue ) :-
  % assign_to_ODE( +OdeSystem, +Variable, +PValue, +NValue )
  add_ode( OdeSystem, [ P, N ], PValue - P, NValue - N ).
% }}}
% arithmetic operations{{{
/*  z = +x :{{{
*
*     z = x
*/
positive_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % positive_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  assign_to_PIVP( OdeSystem, X, Z, Stable ).

positive_to_PIVP( OdeSystem, X, Z ) :-
  % positive_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  assign_to_PIVP( OdeSystem, X, Z ).
% }}}
/*  z = -x :{{{
*
*     z_p - z_n = -( x_p - x_n )
*               = x_n - x_p
*
*     dz_p = x_n - z_p
*     dz_n = x_p - z_n
*/
negative_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % negative_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, Error ),

  variable( Z,      [ ZP, ZN ]  ),
  variable( X,      [ XP, XN ]  ),
  variable( Error,  ErrorDiff   ),

  negative_to_PIVP(           OdeSystem, X, Z ),
  assign_to_ODE(              OdeSystem, ErrorDiff, ZP + XP, ZN + XN ),
  neg_number_to_bool_to_PIVP( OdeSystem, Error, Stable, bidir ).

negative_to_PIVP( OdeSystem, X, Z ) :-
  % negative_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  variable( Z,  ZDiff       ),
  variable( X,  [ XP, XN ]  ),

  assign_to_ODE( OdeSystem, ZDiff, XN, XP ).
% }}}
/*  z = x ^ y :{{{
*
*     z = exp( y * log( x ) )
*/
power_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % power_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, [ T0, T1 ] ),

  log_to_PIVP(            OdeSystem, X,   T0          ),
  multiplication_to_PIVP( OdeSystem, Y,   T0, T1      ),
  exp_to_PIVP(            OdeSystem, T1,  Z,  Stable  ).

power_to_PIVP( OdeSystem, X, Y, Z ) :-
  % power_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, [ T0, T1 ] ),

  log_to_PIVP(            OdeSystem, X,   T0      ),
  multiplication_to_PIVP( OdeSystem, Y,   T0, T1  ),
  exp_to_PIVP(            OdeSystem, T1,  Z       ).
% }}}
/*  z = x * y :{{{
*
*     z_p - z_n = ( x_p - x_n ) * ( y_p - y_n )
*               = ( x_p * y_p + x_n * y_n ) - ( x_p * y_n + x_n * y_p )
*
*     dz_p = x_p * y_n + x_n * y_n - z_p
*     dz_n = x_p * y_n + x_n * y_p - z_n
*/
multiplication_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % multiplication_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName, VariableStableName )
  new_variable( OdeSystem, Error ),

  variable( Z,      [ ZP, ZN ]  ),
  variable( X,      [ XP, XN ]  ),
  variable( Y,      [ YP, YN ]  ),
  variable( Error,  ErrorDiff   ),

  multiplication_to_PIVP(     OdeSystem, X, Y, Z ),
  assign_to_ODE(              OdeSystem, ErrorDiff, XP * YP + XN * YN + ZN, XP * YN + XN * YP + ZP ),
  neg_number_to_bool_to_PIVP( OdeSystem, Error, Stable, bidir ).

multiplication_to_PIVP( OdeSystem, X, Y, Z ) :-
  % multiplication_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  variable( Z,  ZDiff       ),
  variable( X,  [ XP, XN ]  ),
  variable( Y,  [ YP, YN ]  ),

  assign_to_ODE( OdeSystem, ZDiff, XP * YP + XN * YN, XP * YN + XN * YP ).
% }}}
/*  z = constant / x :{{{
*
*   Note: currently do not support negative x.
*
*     dz      = -constant * temp0
*     dtemp0  = -2 * constant * z * temp0
*
*     =>
*
*     dg      = x - g
*     dz      = -constant * temp0 * dg
*     dtemp0  = -2 * constant * z * temp0 * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = -constant * ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = constant * (  temp0_p * dg_n + temp0_n * dg_p ) -
*                               constant * (  temp0_p * dg_p + temp0_n * dg_n )
*     d( temp0_p - temp0_n  ) = -2 * constant * ( z_p - z_n ) * ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = 2 * constant * (  z_p * ( temp0_p * dg_n + temp0_n * dg_p ) +
*                                                 z_n * ( temp0_p * dg_p + temp0_n * dg_n ) ) -
*                               2 * constant * (  z_p * ( temp0_p * dg_p + temp0_n * dg_n ) +
*                                                 z_n * ( temp0_p * dg_n + temp0_n * dg_p ) )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = constant * (  temp0_p * dg_n + temp0_n * dg_p )
*     dz_n      = constant * (  temp0_p * dg_p + temp0_n * dg_n )
*     dtemp0_p  = 2 * constant * (  z_p * ( temp0_p * dg_n + temp0_n * dg_p ) +
*                                   z_n * ( temp0_p * dg_p + temp0_n * dg_n ) )
*     dtemp0_n  = 2 * constant * (  z_p * ( temp0_p * dg_p + temp0_n * dg_n ) +
*                                   z_n * ( temp0_p * dg_n + temp0_n * dg_p ) )
*
*     g init      = x + 1 = 1
*     z init      = 1 / ( x + 1 ) = 1
*     temp0 init  = 1 / ( x + 1 ) ^ 2 = 1
*/
reciprocal_to_PIVP( OdeSystem, Constant, X, Z, Stable ) :-
  % reciprocal_to_PIVP( +OdeSystem, +Constant, +VariableXName, +VariableZName, +VariableStableName )
  init_reciprocal( OdeSystem, Z, T0, G ),

  variable( X,  XDiff   ),
  variable( G,  GDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),

  reciprocal_to_ODE(  OdeSystem, Constant,  GDiff,  XDiff, T0Diff, ZDiff ),
  equal_to_PIVP(      OdeSystem, X, G, Stable ).

reciprocal_to_PIVP( OdeSystem, Constant, X, Z ) :-
  % reciprocal_to_PIVP( +OdeSystem, +Constant, +VariableXName, +VariableZName )
  init_reciprocal( OdeSystem, Z, T0, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( G,  GDiff   ),

  reciprocal_to_ODE( OdeSystem, Constant, GDiff, XDiff, T0Diff, ZDiff ).

init_reciprocal( OdeSystem, Z, T0, G ) :-
  % init_reciprocal( +OdeSystem, +VariableZName, -VariableT0Name, -VariableGName )
  init_variable(  OdeSystem, Z,   1 ),
  new_variable(   OdeSystem, T0,  1 ),
  new_variable(   OdeSystem, G,   1 ).

reciprocal_to_ODE( OdeSystem, Constant, [ GP, GN ], [ XP, XN ], [ T0P, T0N ], [ ZP, ZN ] ) :-
  % reciprocal_to_ode( +OdeSystem, +Constant, +VariableG, +VariableX, +VariableT0, +VariableZ )
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ], DGP, DGN ),
  add_ode(  OdeSystem, [ ZP, ZN ],
            Constant * T0P * DGN + Constant * T0N * DGP,
            Constant * T0P * DGP + Constant * T0N * DGN ),
  add_ode(  OdeSystem, [ T0P, T0N ],
            2 * Constant * ( ZP * ( T0P * DGN + T0N * DGP ) + T0N * ( T0P * DGP + T0N * DGN ) ),
            2 * Constant * ( ZP * ( T0P * DGP + T0N * DGN ) + T0N * ( T0P * DGN + T0N * DGP ) ) ).
% }}}
/*  z = x / y :{{{
*
*   Note: currently do not support negative y ( because constant / x do no support negative x ).
*
*     z = 1 / y * x
*/
division_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % division_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  reciprocal_to_PIVP(     OdeSystem, 1, Y, T ),
  multiplication_to_PIVP( OdeSystem, X, T, Z, Stable ).

division_to_PIVP( OdeSystem, X, Y, Z ) :-
  % division_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, T ),

  reciprocal_to_PIVP(     OdeSystem, 1, Y, T ),
  multiplication_to_PIVP( OdeSystem, X, T, Z ).
% }}}
/*  z = x + y :{{{
*
*     z_p - z_n = ( x_p - x_n ) + ( y_p - y_n )
*               = ( x_p + y_p ) - ( x_n + y_n )
*
*     dz_p = x_p + y_p - z_p
*     dz_n = x_n + y_n - z_n
*/
addition_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % addition_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, Error ),

  variable( Z,      [ ZP, ZN ]  ),
  variable( X,      [ XP, XN ]  ),
  variable( Y,      [ YP, YN ]  ),
  variable( Error,  ErrorDiff   ),

  addition_to_PIVP(           OdeSystem, X, Y, Z ),
  assign_to_ODE(              OdeSystem, ErrorDiff, XP + YP + ZN, XN + YN + ZP ),
  neg_number_to_bool_to_PIVP( OdeSystem, Error, Stable, bidir ).

addition_to_PIVP( OdeSystem, X, Y, Z ) :-
  % addition_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName )
  variable( Z,   ZDiff      ),
  variable( X,  [ XP, XN ]  ),
  variable( Y,  [ YP, YN ]  ),

  assign_to_ODE( OdeSystem, ZDiff, XP + YP, XN + YN ).
% }}}
/*  z = x - y :{{{
*
*     z_p - z_n = ( x_p - x_n ) - ( y_p - y_n )
*               = ( x_p + y_n ) - ( x_n + y_p )
*
*     dz_p = x_p + y_n - z_p
*     dz_n = x_n + y_p - z_n
*/
substraction_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % substraction_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, Error ),

  variable( Z,      [ ZP, ZN ]  ),
  variable( X,      [ XP, XN ]  ),
  variable( Y,      [ YP, YN ]  ),
  variable( Error,  ErrorDiff   ),

  substraction_to_PIVP(       OdeSystem, X, Y, Z ),
  assign_to_ODE(              OdeSystem, ErrorDiff, XP + YN + ZN, XN + YP + ZP ),
  neg_number_to_bool_to_PIVP( OdeSystem, Error, Stable, bidir ).

substraction_to_PIVP( OdeSystem, X, Y, Z ) :-
  % substraction_to_PIVP( +OdeSystem, +VairableXName, +VariableYName, +VariableZName )
  variable( Z,  ZDiff       ),
  variable( X,  [ XP, XN ]  ),
  variable( Y,  [ YP, YN ]  ),

  assign_to_ODE( OdeSystem, ZDiff, XP + YN, XN + YP ).
% }}}
% arithmetic operations
% }}}
/*  x = x_p - x_n :{{{
*
*     x >= 0  : x_p = x, x_n = 0
*     x < 0   : x_p = 0, x_n = -x
*/
argument_to_PIVP( OdeSystem, X ) :-
  % argument_to_PIVP( +OdeSystem, +VariableXName )
  get_initial_concentration( X, Concentration ),
  init_variable( OdeSystem, X, Concentration ).
% }}}
% functions{{{
/*  z = abs( x ):{{{
*
*     dz = x_p + x_n - z
*
*     =>
*
*     dz_p  = x_p + x_n - z_p
*     dz_n  = 0 - z_n
*/
abs_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % abs_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, Error ),

  variable( X,      [ XP, XN ]  ),
  variable( Z,      [ ZP, ZN ]  ),
  variable( Error,  ErrorDiff   ),

  abs_to_PIVP( OdeSystem, X, Z ),
  assign_to_ODE(              OdeSystem, ErrorDiff, XP + XN + ZN, ZP ),
  neg_number_to_bool_to_PIVP( OdeSystem, Error,     Stable, bidir ).

abs_to_PIVP( OdeSystem, X, Z ) :-
  % abs_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  variable( X, XDiff ),
  variable( Z, ZDiff ),

  abs_to_ODE( OdeSystem, XDiff, ZDiff ).

abs_to_ODE( OdeSystem, [ XP, XN ], [ ZP, _ ] ) :-
  % abs_to_ODE( +OdeSystem, +VariableX, +VariableZ )
  add_ode( OdeSystem, d( ZP ) / dt = XP + XN - ZP ).
% }}}
/*  z = min( x, y ):{{{
*
*     z = ( x > y ) ? y : x
*/
min_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % min_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  greaterthan_to_PIVP(  OdeSystem, X, Y, T, Stable  ),
  branch_to_PIVP(       OdeSystem, T, Y, X, Z       ).

min_to_PIVP( OdeSystem, X, Y, Z ) :-
  % min_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, T ),

  greaterthan_to_PIVP(  OdeSystem, X, Y, T ),
  branch_to_PIVP(       OdeSystem, T, Y, X, Z ).
% }}}
/*  z = max( x, y ):{{{
*
*     z = ( x > y ) ? x : y
*/
max_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % max_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  greaterthan_to_PIVP(  OdeSystem, X, Y, T, Stable  ),
  branch_to_PIVP(       OdeSystem, T, X, Y, Z       ).

max_to_PIVP( OdeSystem, X, Y, Z ) :-
  % max_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, T ),

  greaterthan_to_PIVP(  OdeSystem, X, Y, T ),
  branch_to_PIVP(       OdeSystem, T, X, Y, Z ).
% }}}
/*  z = log( x ):{{{
*
*     dz      = temp0
*     dtemp0  = -temp1
*     dtemp1  = -2 * temp0 * temp1
*
*     =>
*
*     dg      = x - g
*     dz      = temp0 * dg
*     dtemp0  = - temp1 * dg
*     dtemp1  = -2 * temp0 * temp1 * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = ( temp0_p * dg_p + temp0_n * dg_n ) -
*                               ( temp0_p * dg_n + temp0_n * dg_p )
*     d( temp0_p - temp0_n  ) = -( ( temp1_p - temp1_n ) * ( dg_p - dg_n ) )
*                             = ( temp1_p * dg_n + temp1_n * dg_p ) -
*                               ( temp1_p * dg_p + temp1_n * dg_n )
*     d( temp1_p - temp1_n  ) = -2 * ( ( temp0_p - temp0_n ) * ( temp1_p - temp1_n ) * ( dg_p - dg_n ) )
*                             = 2 * ( temp0_p * ( temp1_p * dg_n + temp1_n * dg_p ) +
*                                     temp0_n * ( temp1_p * dg_p + temp1_n * dg_n ) ) -
*                               2 * ( temp0_p * ( temp1_p * dg_p + temp1_n * dg_n ) +
*                                     temp0_n * ( temp1_p * dg_n + temp1_n * dg_p ) )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = temp0_p * dg_p + temp0_n * dg_n
*     dz_n      = temp0_p * dg_n + temp0_n * dg_p
*     dtemp0_p  = temp1_p * dg_n + temp1_n * dg_p
*     dtemp0_n  = temp1_p * dg_p + temp1_n * dg_n
*     dtemp1_p  = 2 * ( temp0_p * ( temp1_p * dg_n + temp1_n * dg_p ) +
*                       temp0_n * ( temp1_p * dg_p + temp1_n * dg_n ) )
*     dtemp1_n  = 2 * ( temp0_p * ( temp1_p * dg_p + temp1_n * dg_n ) +
*                       temp0_n * ( temp1_p * dg_n + temp1_n * dg_p ) )
*
*     g init      = x + 1 = 1
*     z init      = log( x + 1 ) = 0
*     temp0 init  = 1 / ( x + 1 ) = 1
*     temp1 init  = 1 / ( x + 1 ) ^ 2 = 1
*/
log_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % log_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  init_log( OdeSystem, Z, T0, T1, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( T1, T1Diff  ),
  variable( G,  GDiff   ),

  log_to_ODE(     OdeSystem, GDiff, XDiff, T0Diff, T1Diff, ZDiff ),
  equal_to_PIVP(  OdeSystem, X, G, Stable ).

log_to_PIVP( OdeSystem, X, Z ) :-
  % log_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  init_log( OdeSystem, Z, T0, T1, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( T1, T1Diff  ),
  variable( G,  GDiff   ),

  log_to_ODE( OdeSystem, GDiff, XDiff, T0Diff, T1Diff, ZDiff ).

init_log( OdeSystem, Z, T0, T1, G ) :-
  % init_log( +OdeSystem, +VairableZName, -VariableT0Name, -VariableT1Name, -VariableGName )
  init_variable(  OdeSystem, Z,   0 ),
  new_variable(   OdeSystem, T0,  1 ),
  new_variable(   OdeSystem, T1,  1 ),
  new_variable(   OdeSystem, G,   1 ).

log_to_ODE( OdeSystem, [ GP, GN ], [ XP, XN ], [ T0P, T0N ], [ T1P, T1N ], [ ZP, ZN ] ) :-
  % log_to_ODE( +OdeSystem, +VariableG, +VariableX, +VariableT0, +VariableT1, +VariableZ )
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ],    DGP,                    DGN                   ),
  add_ode(  OdeSystem, [ ZP, ZN ],    T0P * DGP + T0N * DGN,  T0P * DGN + T0N * DGP ),
  add_ode(  OdeSystem, [ T0P, T0N ],  T1P * DGN + T1N * DGP,  T1P * DGP + T1N * DGN ),
  add_ode(  OdeSystem, [ T1P, T1N ],
            2 * ( T0P * ( T1P * DGN + T1N * DGP ) + T0N * ( T1P * DGP + T1N * DGN ) ),
            2 * ( T0P * ( T1P * DGP + T1N * DGN ) + T0N * ( T1P * DGN + T1N * DGP ) ) ).
% }}}
/*  z = exp( x ):{{{
*
*     dz = z
*
*     =>
*
*     dg  = x - g
*     dz  = z * dg
*
*     d( g_p - g_n  ) = ( x_p - x_n ) - ( g_p - g_n )
*                     = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n  ) = ( z_p - z_n ) * ( dg_p - dg_n )
*                     = ( z_p * dg_p + z_n * dg_n ) -
*                       ( z_p * dg_n + z_n * dg_p )
*
*     dg_p  = x_p + g_n
*     dg_n  = x_n + g_p
*     dz_p  = z_p * dg_p + z_n * dg_n
*     dz_n  = z_p * dg_n + z_n * dg_p
*
*     g init  = x = 0
*     z init  = exp( x ) = 1
*/
exp_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % exp_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  init_exp( OdeSystem, Z, G ),

  variable( X, XDiff ),
  variable( Z, ZDiff ),
  variable( G, GDiff ),

  exp_to_ODE(     OdeSystem, GDiff, XDiff, ZDiff ),
  equal_to_PIVP(  OdeSystem, X, G, Stable ).

exp_to_PIVP( OdeSystem, X, Z ) :-
  % exp_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  init_exp( OdeSystem, Z, G ),

  variable( X, XDiff ),
  variable( Z, ZDiff ),
  variable( G, GDiff ),

  exp_to_ODE( OdeSystem, GDiff, XDiff, ZDiff ).

init_exp( OdeSystem, Z, G ) :-
  % init_exp( +OdeSystem, +VairableZName, -VariableGName )
  init_variable(  OdeSystem, Z, 1 ),
  new_variable(   OdeSystem, G, 0 ).

exp_to_ODE( OdeSystem, [ GP, GN ], [ XP, XN ], [ ZP, ZN ] ) :-
  % exp_to_ODE( +OdeSystem, +VariableG, +VariableX, +VariableZ )
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ], DGP,                 DGN                 ),
  add_ode(  OdeSystem, [ ZP, ZN ], ZP * DGP + ZN * DGN, ZP * DGN + ZN * DGP ).
% }}}
/*  z = cos( x ):{{{
*
*     dz      = temp0
*     dtemp0  = -z
*
*     =>
*
*     dg      = x - g
*     dz      = temp0 * dg
*     dtemp0  = -z * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = ( temp0_p * dg_p + temp0_n * dg_n ) -
*                               ( temp0_p * dg_n + temp0_n * dg_p )
*     d( temp0_p - temp0_n  ) = -( ( z_p - z_n ) * ( dg_p - dg_n ) )
*                             = ( z_p * dg_n + z_n * dg_p ) -
*                               ( z_p * dg_p + z_n * dg_n )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = temp0_p * dg_p + temp0_n * dg_n
*     dz_n      = temp0_p * dg_n + temp0_n * dg_p
*     dtemp0_p  = z_p * dg_n + z_n * dg_p
*     dtemp0_n  = z_p * dg_p + z_n * dg_n
*
*     g init      = x = 0
*     z init      = cos( x ) = 1
*     temp0 init  = -sin( x ) = 0
*/
cos_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % cos_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, Stable )
  init_cos( OdeSystem, Z, T0, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( G,  GDiff   ),

  cos_to_ODE(     OdeSystem, GDiff, XDiff, T0Diff, ZDiff ),
  equal_to_PIVP(  OdeSystem, X, G, Stable ).

cos_to_PIVP( OdeSystem, X, Z ) :-
  % cos_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  init_cos( OdeSystem, Z, T0, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( G,  GDiff   ),

  cos_to_ODE( OdeSystem, GDiff, XDiff, T0Diff, ZDiff ).

init_cos( OdeSystem, Z, T0, G ) :-
  % init_cos( +OdeSystem, +VairableZName, -VariableT0Name, -VariableGName )
  init_variable(  OdeSystem, Z,   1 ),
  new_variable(   OdeSystem, T0,  0 ),
  new_variable(   OdeSystem, G,   0 ).

cos_to_ODE( OdeSystem, [ GP, GN ], [ XP, XN ], [ T0P, T0N ], [ ZP, ZN ] ) :-
  % cos_to_ODE( +OdeSystem, +VariableG, +VariableX, +VariableT0, +VariableZ )
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP,  GN  ], DGP,                   DGN                   ),
  add_ode(  OdeSystem, [ ZP,  ZN  ], T0P * DGP + T0N * DGN, T0P * DGN + T0N * DGP ),
  add_ode(  OdeSystem, [ T0P, T0N ], ZP * DGN + ZN * DGP,   ZP * DGP + ZN * DGN   ).
% }}}
/*  z = sin( x ):{{{
*
*     dz      = temp0
*     dtemp0  = -z
*
*     =>
*
*     dg      = x - g
*     dz      = temp0 * dg
*     dtemp0  = -z * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = ( temp0_p * dg_p + temp0_n * dg_n ) -
*                               ( temp0_p * dg_n + temp0_n * dg_p )
*     d( temp0_p - temp0_n  ) = -( ( z_p - z_n ) * ( dg_p - dg_n ) )
*                             = ( z_p * dg_n + z_n * dg_p ) -
*                               ( z_p * dg_p + z_n * dg_n )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = temp0_p * dg_p + temp0_n * dg_n
*     dz_n      = temp0_p * dg_n + temp0_n * dg_p
*     dtemp0_p  = z_p * dg_n + z_n * dg_p
*     dtemp0_n  = z_p * dg_p + z_n * dg_n
*
*     g init      = x = 0
*     z init      = sin( x ) = 0
*     temp0 init  = cos( x ) = 1
*/
sin_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % sin_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  init_sin( OdeSystem, Z, T0, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( G,  GDiff   ),

  cos_to_ODE(     OdeSystem, GDiff, XDiff, T0Diff, ZDiff ),
  equal_to_PIVP(  OdeSystem, X, G, Stable ).

sin_to_PIVP( OdeSystem, X, Z ) :-
  % sin_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  init_sin( OdeSystem, Z, T0, G ),

  variable( X,  XDiff   ),
  variable( Z,  ZDiff   ),
  variable( T0, T0Diff  ),
  variable( G,  GDiff   ),

  cos_to_ODE( OdeSystem, GDiff, XDiff, T0Diff, ZDiff ).

init_sin( OdeSystem, Z, T0, G ) :-
  % init_sin( +OdeSystem, +VairableZName, -VariableT0Name, -VariableGName )
  init_variable(  OdeSystem, Z,   0 ),
  new_variable(   OdeSystem, T0,  1 ),
  new_variable(   OdeSystem, G,   0 ).
% }}}
/*  z = pos_sigmoid( x ):{{{
*
*     z = step( x )
*
*   TODO: check if this unit can be implemented by bio switch.
*/
:- devdoc( "\\item pos_sigmoid_to_PIVP" ).
:- devdoc( "This is an interface of sigmoid function used when viewing the input as Boolean value.
            The output is true when input is larger than a theshold value, otherwise it is false.
            Ideally, the theshold should be close to constant 'true'" ).

pos_sigmoid_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % pos_sigmoid_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),
  constant( true, True ),
  Shift is True / 2,
  add_constant( Shift ),

  pos_tanh_to_PIVP( OdeSystem, X, T ),
  addition_to_PIVP( OdeSystem, T, Shift, Z, Stable ).

pos_sigmoid_to_PIVP( OdeSystem, X, Z ) :-
  % pos_sigmoid_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  constant( true, True ),
  Threshold is True / 2,

  zero_order_to_PIVP( OdeSystem, Threshold, X, _, Z ).
% }}}
/*  z = neg_sigmoid( x ):{{{
*
*     z = step( -x )
*
*   TODO: check if this unit can be implemented by bio switch.
*/
:- devdoc( "\\item neg_sigmoid_to_PIVP" ).
:- devdoc( "This is an interface of sigmoid function used when viewing the input as Boolean value.
            The output is true when input is smaller than a theshold value, otherwise it is false.
            Ideally, the theshold should be close to constant 'true'" ).

neg_sigmoid_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % neg_sigmoid_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),
  constant( true, True ),
  Shift is True / 2,
  add_constant( Shift ),

  neg_tanh_to_PIVP( OdeSystem, X, T ),
  addition_to_PIVP( OdeSystem, T, Shift, Z, Stable ).

neg_sigmoid_to_PIVP( OdeSystem, X, Z ) :-
  % neg_sigmoid_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  constant( true, True ),
  Threshold is True / 2,

  zero_order_to_PIVP( OdeSystem, Threshold, X, Z, _, true ).
% }}}
/*  z = pos_number_to_bool( x ):{{{
*
*     z = step( x )
*
*   TODO: check if this unit can be implemented by bio switch.
*/
:- devdoc( "\\item pos_number_to_bool_to_PIVP" ).
:- devdoc( "This is an interface of sigmoid function used when viewing the input as real number.
            The output is true when input is greater than a theshold, otherwise it is false.
            Ideally, the theshold should be close to 0." ).

pos_number_to_bool_to_PIVP( OdeSystem, X, Z ) :-
  % pos_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName )
  Threshold is 0.2,

  zero_order_to_PIVP( OdeSystem, Threshold, X, _, Z ).

pos_number_to_bool_to_PIVP( OdeSystem, X, Z, bidir ) :-
  % pos_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName, bidir )
  !,
  new_variable( OdeSystem, T ),

  abs_to_PIVP(                OdeSystem, X, T ),
  pos_number_to_bool_to_PIVP( OdeSystem, T, Z ).

pos_number_to_bool_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % pos_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName, +VariableStableName )
  !,
  new_variable( OdeSystem, T ),
  constant( true, True ),
  K     is  True / 2,
  C     =   6,
  Shift =   0.5,
  add_constant( K ),

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, T ),
  addition_to_PIVP(     OdeSystem, T, K, Z, Stable ).

pos_number_to_bool_to_PIVP( OdeSystem, X, Z, Stable, bidir ) :-
  % pos_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName, +VariableStableName, bidir )
  new_variable( OdeSystem, T ),

  abs_to_PIVP(                OdeSystem, X, T ),
  pos_number_to_bool_to_PIVP( OdeSystem, T, Z, Stable ).
% }}}
/*  z = neg_number_to_bool( x ):{{{
*
*     z = step( -x )
*
*   TODO: check if this unit can be implemented by bio switch.
*/
:- devdoc( "\\item neg_number_to_bool_to_PIVP" ).
:- devdoc( "This is an interface of sigmoid function used when viewing the input as real number.
            The output is true when input is smaller than a theshold, otherwise it is false.
            Ideally, the theshold should be close to 0." ).

neg_number_to_bool_to_PIVP( OdeSystem, X, Z ) :-
  % neg_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName )
  Threshold is 0.2,

  zero_order_to_PIVP( OdeSystem, Threshold, X, Z, _, true ).

neg_number_to_bool_to_PIVP( OdeSystem, X, Z, bidir ) :-
  % neg_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName, bidir )
  !,
  new_variable( OdeSystem, T ),

  abs_to_PIVP(                OdeSystem, X, T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z ).

neg_number_to_bool_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % neg_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName, +VariableStableName )
  !,
  new_variable( OdeSystem, T ),
  constant( true, True ),
  K     is  True / 2,
  C     =   -6,
  Shift =   0.5,
  add_constant( K ),

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, T ),
  addition_to_PIVP(     OdeSystem, T, K, Z, Stable ).

neg_number_to_bool_to_PIVP( OdeSystem, X, Z, Stable, bidir ) :-
  % neg_number_to_bool_to_PIVP( +OdeSytem, +VariableXName, +VariableZName, +VariableStableName, bidir )
  new_variable( OdeSystem, T ),

  abs_to_PIVP(                OdeSystem, X, T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z, Stable ).
% }}}
/*  hilln( x ):{{{
*
*     h   = k * x ^ n / ( c + x ^ n )
*     nh  = k * 1 / ( c + x ^ n )
*
*     dh  = n * x ^ ( n - 1 ) * nh ^ 2 / ( c * k )
*     dnh = -n * x ^ ( n - 1 ) * nh ^ 2 / ( c * k )
*         = -dh
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg  = x - g
*     dh  = n * g ^ ( n - 1 ) * nh ^ 2 * ( x - g ) / ( c * k )
*     dnh = -dh
*
*     g init  = x = 0
*     h init  = 0
*     nh init = k
*
*     k = true_value
*     c = 0.1
*     n = 5
*/
hill_to_PIVP( OdeSystem, X, H, NH, Stable ) :- % differential symantic
  % hill_to_PIVP( +OdeSystem, +VariableXName, ?VariableHName, ?VariableNHName, +VariableStalbeName )
  variable( X,  [ XP,   _ ] ),
  !,
  (
    var( H )
  ->
    HP = H
  ;
    variable( H,  [ HP,   _ ] )
  ),
  (
    var( NH )
  ->
    NHP = NH
  ;
    variable( NH, [ NHP,  _ ] )
  ),

  hill_to_PIVP( OdeSystem, XP, HP, NHP, Stable ).

hill_to_PIVP( OdeSystem, X, H, NH, Stable ) :- % non differential symantic
  % hill_to_PIVP( +OdeSystem, +VariableXName, ?VariableHName, ?VariableNHName, +VariableStableName )
  !,
  constant( true, Scale ),
  init_hill( OdeSystem, Scale, H, NH, G ),
  hill_to_ODE( OdeSystem, Scale, G, X, H, NH ),

  new_variable( OdeSystem, G ),
  new_variable( OdeSystem, X ),
  equal_to_PIVP( OdeSystem, X, G, Stable ).

hill_to_PIVP( OdeSystem, X, H, NH ) :- % differential symantic
  % hill_to_PIVP( +OdeSystem, +VariableXName, ?VariableHName, ?VariableNHName )
  variable( X,  [ XP,   _ ] ),
  !,
  (
    var( H )
  ->
    HP = H
  ;
    variable( H,  [ HP,   _ ] )
  ),
  (
    var( NH )
  ->
    NHP = NH
  ;
    variable( NH, [ NHP,  _ ] )
  ),

  hill_to_PIVP( OdeSystem, XP, HP, NHP ).

hill_to_PIVP( OdeSystem, X, H, NH ) :- % non differential symantic
  % hill_to_PIVP( +OdeSystem, +VariableXName, ?VariableHName, ?VariableNHName )
  !,
  constant( true, Scale ),
  init_hill( OdeSystem, Scale, H, NH, G ),
  hill_to_ODE( OdeSystem, Scale, G, X, H, NH ).

init_hill( OdeSystem, Scale, H, NH, G ) :-
  % init_hill( +OdeSystem, +Scale, ?VariableHName, ?VariableNHName, -VariableGName )
  get_variable_name( H ),   set_ode_initial_value( OdeSystem, H,  0     ),
  get_variable_name( NH ),  set_ode_initial_value( OdeSystem, NH, Scale ),
  get_variable_name( G ),   set_ode_initial_value( OdeSystem, G,  0     ).

hill_to_ODE( OdeSystem, K, G, X, H, NH ) :-
  % hill_to_ODE( +OdeSystem, +K, +VariableGName, +VariableXName, +VariableHName, +VariableNHName )
  C = 0.1,
  N = 5,

  add_ode( OdeSystem, d( G ) / dt   = X - G ),
  add_ode( OdeSystem, d( H ) / dt   = N / ( C * K ) * ( G ^ ( N - 1 ) * NH ^ 2 * ( X - G ) ) ),
  add_ode( OdeSystem, d( NH ) / dt  = N / ( C * K ) * ( G ^ ( N - 1 ) * NH ^ 2 * ( G - X ) ) ).
% }}}
/*  k * tanh( c * ( x - shift ) ):{{{
*
*     k > 0
*
*     tanh   = k * tanh( c * x )
*     dtanh  = c / k * ( k ^ 2 - tanh ^ 2 )
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg    = x - shift - g
*     dtanh = c / k * ( k ^ 2 - tanh ^ 2 ) * dg
*
*     d( g_p - g_n )        = ( x_p - x_n ) - ( shift_p - shift_n ) - ( g_p - g_n )
*                           = ( x_p + g_n + shift_n ) - ( x_n + g_p + shift_p )
*     d( tanh_p - tanh_n )  = c / k * ( k ^ 2 - ( tanh_p - tanh_n ) ^ 2 / k ) * ( dg_p - dg_n )
*                           = c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n ) -
*                             c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p )
*
*     dtanh  = -c / k * ( k ^ 2 - tanh ^ 2 )
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg    = x - shift - g
*     dtanh = c / k * ( k ^ 2 - tanh ^ 2 ) * dg
*
*     d( g_p - g_n )        = ( x_p - x_n ) - ( shift_p - shift_n ) - ( g_p - g_n )
*                           = ( x_p + g_n + shift_n ) - ( x_n + g_p + shift_p )
*     d( tanh_p - tanh_n )  = c / k * ( k ^ 2 - ( tanh_p - tanh_n ) ^ 2 / k ) * ( dg_p - dg_n )
*                           = c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p ) -
*                             c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n )
*
*     g init    = 0
*     tanh init = k * tanh( c * 0 ) = 0
*
*     shift = 0.8
*     k     = true_value / 2
*     c     = 6
*/
general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh, Stable ) :-
  % general_tanh_to_PIVP( +OdeSystem, +K:unsigned_int, +C:int, +Shift:int, +VariableXName, +VariableTanhName, +VariableStableName )
  add_constant( Shift ),
  init_general_tanh( OdeSystem, Tanh, G ),

  variable( Shift,  ShiftDiff ),
  variable( X,      XDiff     ),
  variable( G,      GDiff     ),
  variable( Tanh,   TanhDiff  ),

  general_tanh_to_ODE( OdeSystem, ShiftDiff, GDiff, XDiff, DGDiff ),
  (
    C >= 0
  ->
    pos_tanh_to_ODE( OdeSystem, K, C, DGDiff, TanhDiff )
  ;
    CT is -C,
    neg_tanh_to_ODE( OdeSystem, K, CT, DGDiff, TanhDiff )
  ),
  equal_to_PIVP( OdeSystem, X, G, Stable ).

general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh ) :-
  % general_tanh_to_PIVP( +OdeSystem, +K:unsigned_int, +C:int, +Shift:int, +VariableXName, +VariableTanhName )
  add_constant( Shift ),
  init_general_tanh( OdeSystem, Tanh, G ),

  variable( Shift,  ShiftDiff ),
  variable( X,      XDiff     ),
  variable( G,      GDiff     ),
  variable( Tanh,   TanhDiff  ),

  general_tanh_to_ODE( OdeSystem, ShiftDiff, GDiff, XDiff, DGDiff ),
  (
    C >= 0
  ->
    pos_tanh_to_ODE( OdeSystem, K, C, DGDiff, TanhDiff )
  ;
    CT is -C,
    neg_tanh_to_ODE( OdeSystem, K, CT, DGDiff, TanhDiff )
  ).

init_general_tanh( OdeSystem, Tanh, G ) :-
  % init_general_tanh( +OdeSystem, +VariableTanhName, -VariableGName )
  new_variable( OdeSystem, Tanh,  0 ),
  new_variable( OdeSystem, G,     0 ).

general_tanh_to_ODE( OdeSystem, [ ShiftP, ShiftN ], [ GP, GN ], [ XP, XN ], [ DGP, DGN ] ) :-
  % general_tanh_to_ODE( +OdeSystem, +Shift, +GDiff, +XDiff, -DGDiff )
  DGP = XP + GN + ShiftN,
  DGN = XN + GP + ShiftP,

  add_ode(  OdeSystem, [ GP, GN ], DGP, DGN ).

pos_tanh_to_PIVP( OdeSystem, X, Tanh, Stable ) :-
  % pos_tanh_to_PIVP( +OdeSystem, +VariableXName, +VariableTanhName, +VariableStableName )
  constant( true, True ),
  K     = True / 2,
  C     = 6,
  Shift = 0.8,

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh, Stable ).

pos_tanh_to_PIVP( OdeSystem, X, Tanh ) :-
  % pos_tanh_to_PIVP( +OdeSystem, +VariableXName, +VariableTanhName )
  constant( true, True ),
  K     = True / 2,
  C     = 6,
  Shift = 0.8,

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh ).

pos_tanh_to_ODE( OdeSystem, K, C, [ DGP, DGN ], [ TanhP, TanhN ] ) :-
  % pos_tanh_to_ODE( +OdeSystem, +K:unsigned_int, +C:unsigned_int, +DGDiff, +TanhDiff )
  add_ode(  OdeSystem, [ TanhP, TanhN ],
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGP + ( TanhP ^ 2 + TanhN ^ 2 ) * DGN ),
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGN + ( TanhP ^ 2 + TanhN ^ 2 ) * DGP ) ).

neg_tanh_to_PIVP( OdeSystem, X, Tanh, Stable ) :-
  % neg_tanh_to_PIVP( +OdeSystem, +VariableXName, +VariableTanhName, +VariableStableName )
  constant( true, True ),
  K     = True / 2,
  C     = -6,
  Shift = 0.8,

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh, Stable ).

neg_tanh_to_PIVP( OdeSystem, X, Tanh ) :-
  % neg_tanh_to_PIVP( +OdeSystem, +VariableXName, +VariableTanhName )
  constant( true, True ),
  K     = True / 2,
  C     = -6,
  Shift = 0.8,

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh ).

neg_tanh_to_ODE( OdeSystem, K, C, [ DGP, DGN ], [ TanhP, TanhN ] ) :-
  % neg_tanh_to_ODE( +OdeSystem, +K:unsigned_int, +C:unsigned_int, +DGDiff, +TanhDiff )
  add_ode(  OdeSystem, [ TanhP, TanhN ],
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGN + ( TanhP ^ 2 + TanhN ^ 2 ) * DGP ),
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGP + ( TanhP ^ 2 + TanhN ^ 2 ) * DGN ) ).
% }}}
/*  step( x ):{{{
*
*     step = k / 2 * ( 1 + tanh( c * ( x - shift ) ) )
*
*     dstep = k * c / 2 * ( 1 - tanh ^ 2 )
*     dtanh = c * ( 1 - tanh ^ 2 )
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg    = x - shift - g
*     dstep = k * c / 2 * ( 1 - tanh ^ 2 ) * ( x - shift - g )
*     dtanh = c * ( 1 - tanh ^ 2 ) * ( x - shift - g )
*
*     d( g_p - g_n )        = ( x_p - x_n ) - ( shift_p - shift_n ) - ( g_p - g_n )
*                           = ( x_p + shift_n + g_n ) - ( x_n + shift_p + g_p )
*     d( step_p - step_n )  = k * c / 2 * ( 1 + 2 * tanh_p * tanh_n - tanh_p ^ 2 - tanh_n ^ 2 ) * ( dg_p - dg_n )
*                           = k * c / 2 * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n ) -
*                             k * c / 2 * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p )
*     d( tanh_p - tanh_n )  = c * ( 1 + 2 * tanh_p * tanh_n - tanh_p ^ 2 - tanh_n ^ 2 ) * ( dg_p - dg_n )
*                           = c * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n ) -
*                             c * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p )
*
*     g init    = x = 0
*     step init = k / 2 * ( 1 + tanh( c * -shift ) )
*     tanh init = tanh( c * -shift )
*
*     shift = 0.8
*     k     = true_value
*     c     = 6
*/
general_step_to_PIVP( OdeSystem, K, C, Shift, X, Step, Stable ) :- % differential symantic
  !,
  init_general_step(    OdeSystem, K, C, Shift, Step, Tanh, G ),
  general_step_to_ODE(  OdeSystem, K, C, X,     G,    Tanh, Step ),

  new_variable( OdeSystem, X ),
  new_variable( OdeSystem, G ),
  equal_to_PIVP( OdeSystem, X, G, Stable ).

general_step_to_PIVP( OdeSystem, K, C, Shift, X, Step ) :- % differential symantic
  !,
  add_constant( Shift ),
  init_general_step( OdeSystem, K, Step, Tanh, G ),

  variable( Shift,  ShiftDiff ),
  variable( X,      XDiff     ),
  variable( G,      GDiff     ),
  variable( Tanh,   TanhDiff  ),
  variable( Step,   StepDiff  ),

  general_step_to_ODE( OdeSystem, K, C, ShiftDiff, XDiff, GDiff, TanhDiff, StepDiff ).

init_general_step( OdeSystem, K, Step, Tanh, G ) :-
  % init_pos_step( +OdeSystem, +K, ?VariableStepName, ?VariableTanhName, ?VariableGName )
  StepInit is K / 2,

  new_variable( OdeSystem, Step,  StepInit  ),
  new_variable( OdeSystem, Tanh,  0         ),
  new_variable( OdeSystem, G,     0         ).

general_step_to_ODE( OdeSystem, K, C, [ ShiftP, ShiftN ], [ XP, XN ], [ GP, GN ], [ TanhP, TanhN ], [ StepP, StepN ] ) :-
  % pos_step_to_ODE( +OdeSystem, +K, +C, +ShiftDiff, +XDiff, +GDiff, +TanhDiff, +StepDiff )
  DGP         = XP + ShiftN + GN,
  DGN         = XN + ShiftP + GP,
  TanhTermP   = 1 + 2 * TanhP * TanhN,
  TanhTermN   = TanhP ^ 2 + TanhN ^ 2,
  DTanhTermP  = TanhTermP * DGP + TanhTermN * DGN,
  DTanhTermN  = TanhTermP * DGN + TanhTermN * DGP,
  (
    C >= 0
  ->
    DTanhP = C * DTanhTermP,
    DTanhN = C * DTanhTermN
  ;
    CN is -C,
    DTanhP = CN * DTanhTermN,
    DTanhN = CN * DTanhTermP
  ),
  (
    K >= 0
  ->
    DStepP = K / 2 * DTanhP,
    DStepN = K / 2 * DTanhN
  ;
    KN is -K,
    DStepP = KN / 2 * DTanhN,
    DStepN = KN / 2 * DTanhP
  ),

  add_ode( OdeSystem, [ GP,     GN    ], DGP,     DGN     ),
  add_ode( OdeSystem, [ TanhP,  TanhN ], DTanhP,  DTanhN  ),
  add_ode( OdeSystem, [ StepP,  StepN ], DStepP,  DStepN  ).

pos_step_to_PIVP( OdeSystem, X, Step, Stable ) :-
  % pos_step_to_PIVP( +OdeSystem, +VariableXName, +VariableStepName, +VariableStableName )
  C     = 6,
  Shift = 0.8,
  constant( true, Scale ),

  general_step_to_PIVP( OdeSystem, Scale, C, Shift, X, Step, Stable ).

pos_step_to_PIVP( OdeSystem, X, Step ) :-
  % pos_step_to_PIVP( +OdeSystem, +VariableXName, +VariableStepName )
  C     = 6,
  Shift = 0.8,
  constant( true, Scale ),

  general_step_to_PIVP( OdeSystem, Scale, C, Shift, X, Step ).

neg_step_to_PIVP( OdeSystem, X, Step, Stable ) :-
  % neg_step_to_PIVP( +OdeSystem, +VariableXName, +VariableStepName, +VariableStableName )
  C     = -6,
  Shift = 0.8,
  constant( true, Scale ),

  general_step_to_PIVP( OdeSystem, Scale, C, Shift, X, Step, Stable ).

neg_step_to_PIVP( OdeSystem, X, Step ) :-
  % neg_step_to_PIVP( +OdeSystem, +VariableXName, +VariableStepName )
  C     = -6,
  Shift = 0.8,
  constant( true, Scale ),

  general_step_to_PIVP( OdeSystem, Scale, C, Shift, X, Step ).
% }}}
% end functions
% }}}
% conditional operations{{{
/*  z = x == y :{{{
*
*     z = neg_number_to_bool_bidir( x - y )
*/
equal_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % equal_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, X, Y, T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z, Stable, bidir ).

equal_to_PIVP( OdeSystem, X, Y, Z ) :-
  % equal_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, X, Y,  T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z, bidir ).
% }}}
/*  z = x < y :{{{
*
*     z = pos_number_to_bool( y - x )
*/
lessthan_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % lessthan_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, Y, X, T      ),
  pos_number_to_bool_to_PIVP( OdeSystem, T, Z, Stable ).

lessthan_to_PIVP( OdeSystem, X, Y, Z ) :-
  % lessthan_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, T  ),

  substraction_to_PIVP(       OdeSystem, Y, X, T ),
  pos_number_to_bool_to_PIVP( OdeSystem, T, Z ).
% }}}
/*  z = x <= y :{{{
*
*     z = neg_number_to_bool( x - y )
*/
lessequal_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % lessequal_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, X, Y, T      ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z, Stable ).

lessequal_to_PIVP( OdeSystem, X, Y, Z ) :-
  % lessequal_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, X, Y, T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z ).
% }}}
/*  z = x > y :{{{
*
*     z = y < x
*/
greaterthan_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % greaterthan_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  lessthan_to_PIVP( OdeSystem, Y, X, Z, Stable ).

greaterthan_to_PIVP( OdeSystem, X, Y, Z ) :-
  % greaterthan_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  lessthan_to_PIVP( OdeSystem, Y, X, Z ).
% }}}
/*  z = x >= y :{{{
*
*     z = y <= x
*/
greaterequal_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % greaterequal_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  lessequal_to_PIVP( OdeSystem, Y, X, Z, Stable ).

greaterequal_to_PIVP( OdeSystem, X, Y, Z ) :-
  % greaterequal_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  lessequal_to_PIVP( OdeSystem, Y, X, Z ).
% }}}
/*  z = x or y :{{{
*
*     x and y should be the result of condition
*
*     z = pos_sigmoid( x + y )
*/
or_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % or_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  addition_to_PIVP(     OdeSystem, X, Y, T      ),
  pos_sigmoid_to_PIVP(  OdeSystem, T, Z, Stable ).

or_to_PIVP( OdeSystem, X, Y, Z ) :-
  % or_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  constant( true, True ),
  Threshold is True / 4,
  new_variable( OdeSystem, T ),

  zero_order_to_PIVP( OdeSystem, Threshold, X, T, Z ),
  zero_order_to_PIVP( OdeSystem, Threshold, Y, T, Z ).
% }}}
/*  z = x and y :{{{
*
*     x and y should be the result of condition
*
*     z = pos_sigmoid( x * y )
*/
and_to_PIVP( OdeSystem, X, Y, Z, Stable ) :-
  % and_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName, +VariableStableName )
  new_variable( OdeSystem, T ),

  multiplication_to_PIVP( OdeSystem, X, Y, T      ),
  pos_sigmoid_to_PIVP(    OdeSystem, T, Z, Stable ).

and_to_PIVP( OdeSystem, X, Y, Z ) :-
  % and_to_PIVP( +OdeSystem, +VariableXName, +VariableYName, +VariableZName )
  constant( true, True ),
  Threshold is True / 2,
  new_variable( OdeSystem, [ T1, T2 ] ),

  zero_order_to_PIVP( OdeSystem, Threshold, X, T1, T2 ),
  zero_order_to_PIVP( OdeSystem, Threshold, Y, T2, Z, false, true, false ).
% }}}
/*  z = not x :{{{
*
*     x should be the result of a condition
*
*     z = neg_sigmoid( x )
*/
not_to_PIVP( OdeSystem, X, Z, Stable ) :-
  % not_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariableStableName )
  neg_sigmoid_to_PIVP( OdeSystem, X, Z, Stable ).

not_to_PIVP( OdeSystem, X, Z ) :-
  % not_to_PIVP( +OdeSystem, +VariableXName, +VariableZName )
  neg_sigmoid_to_PIVP( OdeSystem, X, Z ).
% }}}
% end conditional operations
% }}}
% switch{{{
/*
*   this implement the "Approximately Majority" bio switch.
*
*       sx
*       |
*   +--+--+
*   |  |  |
*   |<-- <--
*   x   b   y
*    --> -->|
*     |  |  |
*     +--+--+
*       |
*       sy
*
*   sx: switch to x
*   sy: switch to y
*
*   the switch can be viewed as two states.
*
*   - 'x' state: the switch is truned off
*   - 'y' state: the switch is turned on.
*
*   intially, the state of the switch is 'x'.
*   the value of state will change the threshold of the input to switch the state.
*   the larger the state value, the larger threshold, which means larger noise tolerance.
*/
:- devdoc( "\\item switch_to_PIVP" ).
:- devdoc( "The compilation of 'approxmate majority' bio switch.
            The internal implementation directly add the reactions, not PIVP." ).

switch_to_PIVP( OdeSystem, SX, SY, X, Y ) :- % differential semantic
  % switch_to_reaction( +OdeSystem, +VariableSXName, +VariableSYName, +VariableXName, +VariableYName )
  variable( SX, [ SXP, _ ] ),
  variable( SY, [ SYP, _ ] ),
  !,
  (
    var( X )
  ->
    get_variable_name( XP )
  ;
    variable( X, [ XP, _ ] )
  ),
  (
    var( Y )
  ->
    get_variable_name( YP )
  ;
    variable( Y, [ YP, _ ] )
  ),

  switch_to_PIVP( OdeSystem, SXP, SYP, XP, YP ).

switch_to_PIVP( OdeSystem, SX, SY, X, Y ) :- % non differential semantic
  % switch_to_reaction( +OdeSystem, +VariableSXName, +VariableSYName, +VariableXName, +VariableYName )
  !,
  init_switch( OdeSystem, X, Y, B ),

  switch_core_to_reaction( X, Y, B ),

  (
    constant( switchNull, SY )
  ->
    true
  ;
    switch_control_to_reaction( SY, X, Y, B )
  ),
  (
    constant( switchNull, SX )
  ->
    true
  ;
    switch_control_to_reaction( SX, Y, X, B )
  ).

init_switch( OdeSystem, X, Y, B ) :-
  % init_switch( +OdeSystem, +VariableXName, +VariableYName, +VariableBName )
  constant( true,   True  ),
  constant( false,  False ),

  get_variable_name( B ),
  set_ode_initial_value( OdeSystem, X, True   ),
  set_ode_initial_value( OdeSystem, Y, False  ),
  set_ode_initial_value( OdeSystem, B, 0      ).

switch_core_to_reaction( X, Y, B ) :-
  % switch_core_to_reaction( +VariableXName, +VariableYName, +VairableBName )
  switch_control_to_reaction( Y, X, Y, B ),
  switch_control_to_reaction( X, Y, X, B ).

switch_control_to_reaction( SY, X, Y, B ) :-
  % switch_control_to_reaction( +VariableSYName, +VariableXName, +VariableYName, +VairableBName )
  add_reaction( X =[ SY ]=> B ),
  add_reaction( B =[ SY ]=> Y ).
% end switch
% }}}
% zero-order ultrasensitivity{{{
/*
 *  This implement the zero order ultrasensitivity and
 *  using Goldbeter-Koshland kinetics.
 *  The mechenism is similar to the CMOS logic gate.
 *
 *     sx
 *     |
 *   <--
 *  x   y
 *   -->
 *   |
 *   sy
 *
 *   sx: switch to x
 *   sy: switch to y
 *
 *  Goldbeter-Koshland kinetics:
 *
 *    z   = x + y
 *    v1  = k_cat_sy * sy
 *    v2  = k_cat_sx * sx
 *    J1  = Km_sy / z
 *    J2  = Km_sx / z
 *    B   = v2 - v1 + J1 * v2 + J2 * v1
 *
 *    y = 2 * v1 * J2 / ( B + sqrt( B ^ 2 - 4 * ( v2 - v1 ) * v1 * J2 ) )
 *
 *  v2 control the threshold of the sigmoid function
 *  J1, J2 control the quality of sigmoid function( smaller J => steeper sigmoid )
 *
 *  current parameter:
 *
 *  z   = true
 *  v1  = sy
 *  v2  = threshold
 *  J1  = 0.001
 *  J2  = 0.001
 */
zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y ) :- % differential semantic
  % zero_order_to_PIVP( +OdeSystem, +Thrshold:int, +VariableSYName, +VariableXName, +VarialbeYName )
  variable( SY, [ SYP, _ ] ),
  !,
  (
    var( Y )
  ->
    get_variable_name( YP )
  ;
    variable( Y, [ YP, _ ] )
  ),
  (
    var( X )
  ->
    get_variable_name( XP )
  ;
    variable( X, [ XP, _ ] )
  ),

  zero_order_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, true, true, false ).

zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y, Inverse ) :- % differential semantic
  % zero_order_to_PIVP( +OdeSystem, +Thrshold:int, +VariableSYName, +VariableXName, +VarialbeYName, +Inverse:bool )
  variable( SY, [ SYP, _ ] ),
  !,
  (
    var( Y )
  ->
    get_variable_name( YP )
  ;
    variable( Y, [ YP, _ ] )
  ),
  (
    var( X )
  ->
    get_variable_name( XP )
  ;
    variable( X, [ XP, _ ] )
  ),

  zero_order_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, true, true, Inverse ).

zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y, InitX, InitY, Inverse ) :- % differential semantic
  % zero_order_to_PIVP( +OdeSystem, +Thrshold:int, +VariableSYName, +VariableXName, +VarialbeYName, +Inverse:bool )
  variable( SY, [ SYP, _ ] ),
  !,
  (
    var( Y )
  ->
    get_variable_name( YP )
  ;
    variable( Y, [ YP, _ ] )
  ),
  (
    var( X )
  ->
    get_variable_name( XP )
  ;
    variable( X, [ XP, _ ] )
  ),

  zero_order_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, InitX, InitY, Inverse ).

zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y, InitX, InitY, Inverse ) :- % non differential semantic
  % zero_order_to_PIVP( +OdeSystem, +Thrshold:int, +VariableSYName, +VariableXName, +VarialbeYName, +Inverse:bool )
  !,
  init_zero_order( OdeSystem, Threshold, X, Y, SX, KcatSX, KcatSY, KmSX, KmSY, InitX, InitY, Inverse ),

  gk_to_reaction( KcatSX, KmSX, KcatSY, KmSY, SX, SY, X, Y ).

init_zero_order( OdeSystem, Threshold, X, Y, SX, KcatSX, KcatSY, KmSX, KmSY, InitX, InitY, Inverse ) :-
  % init_zero_order( +OdeSystem, +Threshold:int, +VariableXName, +VariableYName, -VariableSXName, -KcatSX:real, -KcatSY:real, -KmSX:real, -KmSY:real, +Inverse:bool )
  constant( true,   True  ),
  constant( false,  False ),
  KcatSX  is 1,
  KcatSY  is 1,
  KmSX    is 0.001 * True,
  KmSY    is 0.001 * True,
  SXInit  is Threshold / KcatSX,

  get_variable_name( SX ),
  (
    Inverse == true
  ->
    XInit = False,
    YInit = True
  ;
    XInit = True,
    YInit = False
  ),
  (
    InitY == true
  ->
    set_ode_initial_value( OdeSystem, Y, YInit )
  ;
    true
  ),
  (
    InitX == true
  ->
    set_ode_initial_value( OdeSystem, X, XInit )
  ;
    true
  ),
  set_ode_initial_value( OdeSystem, SX, SXInit ).

gk_to_reaction( KcatSX, KmSX, KcatSY, KmSY, SX, SY, X, Y ) :-
  % gk_to_reaction( +KcatSX:real, +KmSX:real, +KcatSY:real, +KmSY:real, +VariableSXName, +VariableSYName, +VariableXName, +VariableYName )
  mm_to_reaction( KcatSX, KmSX, SX, Y, X ),
  mm_to_reaction( KcatSY, KmSY, SY, X, Y ).

mm_to_reaction( Kcat, Km, SY, X, Y ) :-
  % mm_to_reaction( +Kcat:real, +Km:real, +VariableSYName, +VariableXName, +VariableYName )
  MM = ( Kcat * [SY] * [X] / ( Km + [X] ) ),
  add_reaction( MM for X =[ SY ]=> Y ).
%}}}
% control flow{{{
:- discontiguous branch_to_PIVP/5.
/*  z = pre x post:{{{
*
*     the evaluation of 'z' and 'eval_post' only happen between 'pre_post' and 'post_pre'.
*     the postcondition success only if the evaluation stop.
*
*     z         = ( pre_post ) ? x : z
*     eval_post = ( z == x )
*
*     there are two bio switches:
*
*     1. pre_post
*
*       - turn on state: pre_post
*         - turn on by 'pre'
*       - turn off state: pre_post_not
*         - turn off by 'post_pre'
*
*     2. post_pre
*
*       - turn on state: post_pre
*         - turn on by 'pre_post' && 'eval_post'
*       - turn off state: post_pre_not
*         - turn off by 'pre_post_next'
*
*     post = post_pre && pre_post_not
*
*     the 'pre_post_next' is the 'pre_post' of next statement.
*/
:- devdoc( "\\item inter_to_PIVP" ).
:- devdoc( "The compiltion of 'rhs' being 'pre <expression> post'.
            This is a core compilation unit for sequentiality." ).

inter_to_PIVP( OdeSystem, X, Z, Pre, PrePost, PrePostNext, Post ) :-
  % inter_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +VariablePreName, VariablePrePostNextName, +VairablePostName )
  new_variable( OdeSystem, [ PrePostNot, PostPre ] ),

  inter_pre_post_to_PIVP(   OdeSystem, Pre,         PostPre,  PrePost,  PrePostNot ),
  branch_to_PIVP(           OdeSystem, PrePost,     X,        Z,        Z ),
  inter_post_pre_to_PIVP(   OdeSystem, X,           Z,        PrePost,  PrePostNext, PostPre ),
  inter_post_to_PIVP(       OdeSystem, PrePostNot,  PostPre,  Post ).

inter_pre_post_to_PIVP( OdeSystem, Pre, PostPre, PrePost, PrePostNot ) :-
  % inter_pre_to_PIVP( +OdeSystem, +VariablePreName, +VariablePostPreName, +VariablePrePostName, +VariablePrePostNotName )
  switch_to_PIVP( OdeSystem, PostPre, Pre, PrePostNot, PrePost ).

inter_post_pre_to_PIVP( OdeSystem, X, Z, PrePost, PrePostNext, PostPre ) :-
  % inter_post_to_PIVP( +OdeSystem, +VariableXName, VariableZName, +VariablePreName, +VariablePrePostName, +VariablePrePostNextName, +VariablePostPreName )
  new_variable( OdeSystem, [ Equal, EvalPost, PostPreNot ] ),

  equal_to_PIVP(  OdeSystem, X,           Z,        Equal     ),
  and_to_PIVP(    OdeSystem, Equal,       PrePost,  EvalPost  ),
  switch_to_PIVP( OdeSystem, PrePostNext, EvalPost, PostPreNot, PostPre ).

inter_post_to_PIVP( OdeSystem, PrePostNot, PostPre, Post ) :-
  % inter_post_to_pivp( +OdeSystem, +VariablePrePostNotName, +VariablePostPreName, +VariablePostName )
  and_to_PIVP( OdeSystem, PrePostNot, PostPre, Post ).
% }}}
/*  z = pre x :{{{
*
*     z = ( pre ) ? x : z
*/
:- devdoc( "\\item pre_to_PIVP" ).
:- devdoc( "The compiltion of 'rhs' being 'pre <expression>'.
            This is a core compilation unit for sequentiality." ).

pre_to_PIVP( OdeSystem, X, Z, Pre, PrePost ) :-
  % pre_to_PIVP( +OdeSystem, +VariableEName, +VariableZName, +VariablePreName, +VariablePrePostName )
  constant( switchNull, Dummy ),
  new_variable( OdeSystem, PrePostNot ),

  switch_to_PIVP( OdeSystem, Dummy, Pre, PrePostNot, PrePost ),
  branch_to_PIVP( OdeSystem, PrePost, X, Z, Z ).
% }}}
/*  z = x post :{{{
*
*     the first statement should wait for the initialization of program whose initial value may not be the initial value of PIVP.
*     since the initialzation of program should stuck at true after the program execute,
*     to turn off the 'pre_post' switch, we need to scale the turn off signal.
*
*     z         = ( pre_post ) ? x : z,
*     eval_post = ( z == x )
*
*     there are three bio switches:
*
*     1. pre_post
*
*       - turn on state: pre_post
*         - turn on by 'pre'
*       - turn off state: pre_post_not
*         - turn off by the scaled 'program_start'
*
*     2. post_pre
*
*       - turn on state: post_pre
*         - turn on by 'pre_post' && 'eval_post'
*       - turn off state: post_pre_not
*         - turn off by 'pre_post_next'
*
*     3. program_start
*
*       - turn on state: program_start
*         - turn on by 'eval_post'
*       - turn off state: program_start_not
*
*     post  = post_pre && pre_post_not
*     pre   = stable( z == x )
*
*     the 'pre_post_next' is the 'pre_post' of next statement.
*
*   TODO:
*
*     - seperate the initialzation of program with the statement implimentation.
*     - the initialization of the program seems not robust.
*/
:- devdoc( "\\item post_to_PIVP" ).
:- devdoc( "The compiltion of 'rhs' being '<expression> post'.
            This is a core compilation unit for sequentiality.
            This compilation unit also deals with the initialization of program." ).

post_to_PIVP( OdeSystem, X, Z, EvalValue, Pre, PrePostNext, Post ) :-
  % post_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +EvalValue:int, +VariablePrePostNextName, +VariablePostName )
  new_variable( OdeSystem, [ PrePost, PrePostNot, PostPre ] ),

  post_pre_post_to_PIVP(  OdeSystem, Pre,         PostPre,  PrePost,    PrePostNot ),
  branch_to_PIVP(         OdeSystem, PrePost,     X,        Z,          Z ),
  post_post_pre_to_PIVP(  OdeSystem, X,           Z,        EvalValue,  PrePost, PrePostNext, PostPre, Pre ),
  post_post_to_PIVP(      OdeSystem, PrePostNot,  PostPre,  Post ).

post_pre_post_to_PIVP( OdeSystem, Pre, PostPre, PrePost, PrePostNot ) :-
  % post_pre_post_to_PIVP( +OdeSystem, +VariablePreName, +VariablePostPreName, +VariablePrePostName, +VariablePrePostNotName )
  switch_to_PIVP( OdeSystem, PostPre, Pre, PrePostNot, PrePost ).

post_post_pre_to_PIVP( OdeSystem, X, Z, EvalValue, PrePost, PrePostNext, PostPre, Pre ) :-
  % post_post_pre_to_PIVP( +OdeSystem, +VariableXName, +VariableZName, +EvalValue:int, +VariablePrePostName, +VariablePrePostNextName, +VariablePostPreName, +VariablePreName )
  constant( switchNull, SwitchNull ),
  new_variable( OdeSystem,
                [ Equal, EqualT, XStable, EqualStable, Stable,
                  PostPreNot, ProgramStart, ProgramStartNot ] ),
  add_constant( EvalValue ),

  equal_to_PIVP(  OdeSystem, X,       Z,      EqualT  ),
  and_to_PIVP(    OdeSystem, PrePost, EqualT, Equal   ),

  equal_to_PIVP(  OdeSystem, X,           EvalValue,        XStable ),
  not_to_PIVP(    OdeSystem, Equal,       EqualStable ),
  and_to_PIVP(    OdeSystem, EqualStable, XStable,          Stable  ),
  and_to_PIVP(    OdeSystem, Stable,      ProgramStartNot,  Pre     ),

  switch_to_PIVP( OdeSystem, PrePostNext, Equal,    PostPreNot,       PostPre       ),
  switch_to_PIVP( OdeSystem, SwitchNull,  PrePost,  ProgramStartNot,  ProgramStart  ).

post_post_to_PIVP( OdeSystem, PrePostNot, PostPre, Post ) :-
  % post_post_to_PIVP( +OdeSystem, +VariablePrePostNotName, +VariablePostPreName, +VariablePostName )
  and_to_PIVP( OdeSystem, PrePostNot, PostPre, Post ).

:- devdoc( "\\item branch_to_PIVP" ).
:- devdoc( "The compilation unit for all branch related unit.
            The first argument after \\emph{OdeSystem} will indicate which control flow keyword it is related.
            A special version without control flow keyword is for the simple branch 'if then else'." ).
% }}}
/*  z = para x :{{{
*
*     z = ( pre ) ? x : z
*/
:- devdoc( "\\item parallel_to_PIVP" ).
:- devdoc( "The compiltion of 'rhs' being 'para <expression>'.
            This is a core compilation unit for parallel computation." ).

parallel_to_PIVP( OdeSystem, X, Z, Pre, Post ) :-
  % parallel_to_PIVP( +OdeSystem, +VariableEName, +VariableZName, +VariablePreName, +VariablePostName )
  new_variable( OdeSystem, [ PrePost, PrePostNot ] ),

  switch_to_PIVP( OdeSystem, Post, Pre, PrePostNot, PrePost ),
  branch_to_PIVP( OdeSystem, PrePost, X, Z, Z ).
% }}}
/*  if_tag = condition :{{{
*/
branch_to_PIVP( OdeSystem, if_tag, C, Pre, PrePost, ThenPrePostNext, ElsePrePostNext, ThenPre, ElsePre ) :-
  % branch_to_PIVP( +OdeSystem, if_tag, +VariableCName, +VariablePreName, +VairiablePrePostName, +VairableThenPrePostNextName, +VariablePreElsePostNextName, +VariableThenPreName, +VariableElsePreName )
  !,
  new_variable( OdeSystem, [ NC, PrePostNext ] ),

  not_to_PIVP(        OdeSystem, C, NC ),
  condition_to_PIVP(  OdeSystem, C, NC, Pre, PrePost, PrePostNext, ThenPre, ElsePre ),

  or_to_PIVP( OdeSystem, ThenPrePostNext, ElsePrePostNext, PrePostNext ).

branch_to_PIVP( OdeSystem, if_tag, C, ThenPre, ElsePre ) :-
  % branch_to_pivp( +OdeSystem, if_tag, +VariableCName, +VariableThenPreName, +VariableElsePreName )
  !,
  new_variable( OdeSystem, NC ),

  not_to_PIVP(        OdeSystem, C,   NC      ),
  condition_to_PIVP(  OdeSystem, C,   ThenPre ),
  condition_to_PIVP(  OdeSystem, NC,  ElsePre ).
% }}}
/*  endif = _ :{{{
*/
branch_to_PIVP( OdeSystem, endif, ThenPost, ThenPostPrePostNext, ThenPrePost, ThenPrePostNext, ElsePost, ElsePostPrePostNext, ElsePrePost, ElsePrePostNext, Post, PrePostNext ) :-
  % loop_to_PIVP( +OdeSystem, endif, +VariableThenPostName, +VariablePrePostNextName, +VariableElsePostName, +VariableElsePrePostNextName, +VariablePostName, +VariablePrePostNextName )
  !,
  or_to_PIVP(     OdeSystem, ThenPost,    ElsePost,     Post            ),
  or_to_PIVP(     OdeSystem, ThenPrePost, PrePostNext,  ThenPrePostNext ),
  or_to_PIVP(     OdeSystem, ElsePrePost, PrePostNext,  ElsePrePostNext ),
  assign_to_PIVP( OdeSystem, PrePostNext, ThenPostPrePostNext ),
  assign_to_PIVP( OdeSystem, PrePostNext, ElsePostPrePostNext ).

:- devdoc( "\\item loop_to_PIVP" ).
:- devdoc( "The compilation unit for all loop related unit.
            The first argument after \\emph{OdeSystem} will indicate which control flow keyword it is related." ).
% }}}
/*  while = condition :{{{
*
*     loop_pre_t = loop_post || loop_pre
*
*     'loop_pre_t' is the real precondition of the while loop.
*/
loop_to_PIVP( OdeSystem, while, C, LoopPre, LoopPrePost, InLoopPre, InLoopPrePost, NoLoopPre, NoLoopPrePost, LoopPost ) :-
  % loop_to_PIVP( +OdeSystem, while, +VariableCName, +VariableLoopPreName, +VariableLoopPrePostName, +VariableInLoopPreName, +VariableInLoopPrePostName, +VariableNoLoopPreName, +VariableNoLoopPrePostName, +VairableLoopPostName )
  new_variable( OdeSystem, [ NC, LoopPreT, LoopPrePostNext ] ),

  loop_pre_to_PIVP( OdeSystem, LoopPre, LoopPost, LoopPreT ),

  not_to_PIVP(        OdeSystem, C, NC ),
  condition_to_PIVP(  OdeSystem, C, NC, LoopPreT, LoopPrePost, LoopPrePostNext, InLoopPre, NoLoopPre ),
 
  or_to_PIVP( OdeSystem, InLoopPrePost, NoLoopPrePost, LoopPrePostNext ).

loop_pre_to_PIVP( OdeSystem, LoopPre, LoopPost, LoopPreT ) :-
  % loop_pre_to_PIVP( +OdeSystem, +VariableLoopPreName, +VariableLoopPostName, +VariableLoopPreTName )
  or_to_PIVP( OdeSystem, LoopPost, LoopPre, LoopPreT ).
% }}}
/*  endwhile = _ :{{{
*/
loop_to_PIVP( OdeSystem, endwhile, LoopPostT, LoopPostPrePostNextT, LoopPost, LoopPostPrePostNext ) :-
  % loop_to_PIVP( +OdeSystem, endwhile, +VariableLoopPostTName, +VariableLoopPostPrePostNextTName, +VariableNoLoopPostName, +VariableLoopPostPrePostNextName )
  assign_to_PIVP( OdeSystem, LoopPostT,           LoopPost              ),
  assign_to_PIVP( OdeSystem, LoopPostPrePostNext, LoopPostPrePostNextT  ).
% }}}
/*  z = if c then t else e:{{{
*     = ( c ) ? t : e
*/
branch_to_PIVP( OdeSystem, C, T, E, Z ) :-
  % branch_to_PIVP( +OdeSystem, +VariableCName, +VariableTName, +VariableEName, +VariableZName )
  !,
  variable( C, [ CP, _ ] ),
  variable( T, TDiff ),
  variable( E, EDiff ),
  variable( Z, ZDiff ),

  branch_to_ODE( OdeSystem, CP, TDiff, EDiff, ZDiff ).

branch_to_ODE( OdeSystem, C, [ TP, TN ], [ EP, EN ], [ ZP, ZN ] ) :-
  % branch_to_ODE( +OdeSystem, +VariableCP, +VariableT, +VariableE, +VariableZ )
  constant( true, True ),

  add_ode(  OdeSystem, [ ZP, ZN ],
            ( TP - ZP ) * C ^ 2 / True ^ 2 + ( EP - ZP ) * ( True - C ) * ( True - C ) / True ^ 2,
            ( TN - ZN ) * C ^ 2 / True ^ 2 + ( EN - ZN ) * ( True - C ) * ( True - C ) / True ^ 2 ).
% }}}
% helper functions{{{
:- devdoc( "\\item condition_to_PIVP" ).
:- devdoc( "This is a helper function for the true and false condition postcondition of branch and loop." ).

condition_to_PIVP( OdeSystem, C, NC, Pre, PrePost, PrePostNext, CPost, NCPost ) :-% {{{
  % condition_to_PIVP( +OdeSystem, +VariableCName, +VariableNCName, +VariablePreName, +VariablePrePostName, +VariablePrePostNextNAme, +VariableCPostName, +VariableNCPostName )
  new_variable( OdeSystem, [ Condition, ConditionNot, ConditionPost ] ),

  inter_to_PIVP(    OdeSystem, NC,  ConditionNot,  Pre, PrePost,  PrePostNext,  ConditionPost ),
  parallel_to_PIVP( OdeSystem, C,   Condition,     Pre,                         ConditionPost ),

  and_to_PIVP( OdeSystem, Condition,    ConditionPost, CPost  ),
  and_to_PIVP( OdeSystem, ConditionNot, ConditionPost, NCPost ).
% }}}
condition_to_PIVP( OdeSystem, C, Post ) :-% {{{
  % condition_to_PIVP( +OdeSystem, +VariableCName, +VariablePostName )
  new_variable( OdeSystem, [ Condition, ConditionPost ] ),

  post_to_PIVP( OdeSystem, C,         Condition,      ConditionPost ),
  and_to_PIVP(  OdeSystem, Condition, ConditionPost,  Post ).
% }}}
% end helper functions}}}
% end control flow
% }}}
:- devdoc( "\\end{itemize}" ).
% end unit synthesizer
% }}}
% database{{{
:- devdoc( "\\item Database" ).

% constants{{{
constant( true,       1.5     ).
constant( false,      0       ).
constant( tempPrefix, temp    ).
constant( conNull,    null    ).
constant( counter,    counter ).
constant( switchNull, Value   ) :-
  constant( false, Value ).

% this is only for documentation
constant( Name, Value ) :-
  type( Name,   name    ),
  type( Value,  atomic  ),
  devdoc( "The constant value \\emph{Value} is associated to name \\emph{Name}." ),
  devdoc( "Below is a list of constants." ),
  devdoc( "\\begin{itemize}" ),
  devdoc( "\\item true: the Boolean true." ),
  devdoc( "\\item false: the Boolean false." ),
  devdoc( "\\item tempPrefix: the prefix of the temperary variable name." ),
  devdoc( "\\item conNull: indicate there is no condition set to the global control flow condition." ),
  devdoc( "\\item counter: The counter name used to count the number of temperary variables." ),
  devdoc( "\\item switchNull: indicate there is no turn on or off signal of a switch." ),
  devdoc( "\\end{itemize}" ),
  fail.
% end constants
% }}}
% operators{{{
operator(+).
operator(-).
operator(*).
operator(/).
operator(^).
operator('(').
operator(')').
operator('<').
operator('<=').
operator('<').
operator('>').
operator('>=').
operator('=').
operator('or').
operator('and').
operator('not').
operator('pre').
operator('post').
operator('if').
operator('then').
operator('else').
operator('if_tag').
operator('else_tag').
operator('endif').
operator('while').
operator('endwhile').

% this is only for documentation
operator( Operator ) :-
  type( Operator, atom ),
  devdoc( "The operators used in the syntax of 'add_function'." ),
  devdoc( "This is for the parsing of the sign operation + and - in 'expression_to_ODE'." ),
  fail.
% end operators
% }}}
% keywords{{{
keyword('endif').
keyword('endwhile').

% this is only for documentation
keyword( Keyword ) :-
  type( Keyword, atom ),
  devdoc( "Some keywords used in the syntax of 'compile_program'." ),
  devdoc( "This is for the parsing of these keywords in 'compile'" ),
  fail.
% end keywords}}}
% conditions{{{
control_flow_condition(precondition).
control_flow_condition(precondition_post).
control_flow_condition(postcondition).
control_flow_condition(branch_then_precondition_post_next).
control_flow_condition(branch_then_precondition_post).
control_flow_condition(branch_else_precondition_post_next).
control_flow_condition(branch_else_precondition_post).
control_flow_condition(branch_else_precondition).
control_flow_condition(branch_then_postcondition).
control_flow_condition(branch_then_post_precondition_post).
control_flow_condition(loop_postcondition).
control_flow_condition(loop_post_precondition_post).
control_flow_condition(loop_noloop_precondition).
control_flow_condition(loop_noloop_precondition_post).

% this is only for documentation
control_flow_condition( Condition ) :-
  type( Condition, atom ),
  devdoc( "This is for handling global variables of precondition, postcondition, ..., etc." ),
  fail.
% end conditions}}}
% end database
% }}}
:- devdoc( "\\end{itemize}" ).
% end synthesis from expression to pivp}}}
% vim: foldmethod=marker foldmarker={{{,}}}
