* Overview
** Main features
- doc.pl
- biocham.pl
** Biocham files, notebooks and call options
- commandline.pl
- util.pl
- counters.pl
** Interpreter top-level
- toplevel.pl
- about.pl
- namespace.pl
* Part I: Biochemical Networks 
- part1.pl
* Biochemical Objects
** Syntax
- filename.pl
- objects.pl
** Molecules and initial state
- molecules.pl
- initial_state.pl
** Parameters
- parameters.pl
** Functions
- functions.pl
** Aliases
- aliases.pl
* Reaction Networks
#** Syntax of reactions and transport rules
- reaction_rules.pl
- kinetics.pl
- types.pl
** Reaction editor
- reaction_editor.pl
** Reaction graph
- reaction_graphs.pl
** Graph visualization and editing
- graph_editor.pl
- graphviz.pl
* Influence Networks
#** Syntax of influence systems
- influence_rules.pl
** Influence editor
- influence_editor.pl
** Influence graph
- influence_graphs.pl
- lemon.pl
- multistability.pl
* Events
- events.pl
* Importing and Exporting BIOCHAM Models
** Biocham files
- models.pl
** SBML and SBML-qual files
- sbml_files.pl
- qual_files.pl
** Ordinary Differential Equations
- ode.pl
#** Graphics files
#** Other files
* Part II: Qualitative Analysis and Synthesis
* Static Analyses
** Dimensional analysis
- units.pl
** Conservation laws and invariants
- conservation_laws.pl
- invariants.pl
** Detecting model reductions
- sepi_graphs.pl
** Tropical algebra equilibrations
- tropical.pl
** Multistability analysis
- multistability_command.pl
- oscilations.pl
* Boolean Dynamics, Verification and Synthesis
** Boolean attractors
- influence_properties.pl
** Computation Tree Logic (CTL) formulae
- ctl.pl
- nusmv.pl
** Model reduction from CTL specification
- reduce.pl
** Model revision from CTL specification
- revision.pl
** PAC Learning influence models from Boolean traces
- pac_learning.pl
* Part III: Quantitative Analysis and Synthesis
* Numerical Simulations
** ODE and stochastic simulations
- arithmetic_rules.pl
- formal_derivation.pl
- c_compiler.pl
- gsl.pl
- numerical_simulation.pl
- markov.pl
** Hybrid simulations
- na.pl
** Plotting and exporting the result of simulations
- plot.pl
** Variations and bifurcations
- variations.pl
* Verification of Temporal Behaviors and Parameter Synthesis
** Numerical data time series
- tables.pl
** Temporal logic FO-LTL(Rlin) formulae
- foltl.pl
** Parameter sensitivity, robustness and parameter optimization w.r.t. FO-LTL(Rlin) properties
- search.pl
* Synthesis of Reaction Networks
** Synthesis from mathematical expressions and simple programs
- odefunction.pl
** Synthesis from GPAC circuits
- wgpac.pl
** Synthesis from polynomial differential equations
- gpac.pl
- lazy_negation.pl
- lazy_negation_gpac.pl
** Synthesis from transfer functions
- transfer_function.pl
#** Synthesis from programs
#- analog_digital.pl
#* Part IV: Wrap-Up
#* Biocham Model Analyzer
   #- analysis.pl
* Index
- index
#* Bibliography
#- biocham.bib
