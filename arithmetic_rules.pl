:- module(
  arithmetic_rules,
  [
    simplify/2,
    distribute/2,
    additive_normal_form/2,
    always_negative/1,
    always_positive/1,
    normalize_number/2
  ]).


:- dynamic(canonical/3).


simplify(In, Out) :-
  (
    simplify_aux(In, Out0)
  ->
    Out = Out0
  ;
    throw(error(simplify_failure))
  ).


simplify_aux(In, Out) :-
  additive_block(In, Blocks),
  !,
  simplify_blocks(additive_block, additive_index, Blocks, ReducedSubBlocks),
  maplist(rebuild_additive_coef, ReducedSubBlocks, CoefSubBlocks),
  rebuild_additive_blocks(CoefSubBlocks, Out).

simplify_aux(In, Out) :-
  multiplicative_block(In, Blocks),
  !,
  simplify_blocks(multiplicative_block, multiplicative_index, Blocks, ReducedSubBlocks),
  map_blocks(rebuild_multiplicative_coef, ReducedSubBlocks, CoefSubBlocks),
  compute_product(CoefSubBlocks, CoefSubBlocksComputed),
  rebuild_multiplicative_blocks(CoefSubBlocksComputed, OutCoef),
  (
    extract_coefficient(OutCoef, Coef, SubOut),
    Coef < 1
  ->
    (
      Coef < 0
    ->
      CoefAbs is - Coef,
      Out = - OutAbs
    ;
      CoefAbs = Coef,
      Out = OutAbs
    ),
    (
      CoefAbs = 1
    ->
      OutAbs = SubOut
    ;
      insert_coef(SubOut, CoefAbs, OutAbs)
    )
  ;
    Out = OutCoef
  ).

simplify_aux(log(exp(Expr)), Out) :-
  !,
  simplify_aux(Expr, Out).

simplify_aux(exp(log(Expr)), Out) :-
  !,
  simplify_aux(Expr, Out).

simplify_aux(exp(N * log(Expr)), Out) :-
  number(N),
  !,
  simplify_aux(Expr ^ N, Out).

simplify_aux(log(A) + log(B), Out) :-
  !,
  simplify_aux(log(A * B), Out).

simplify_aux(exp(A) * exp(B), Out) :-
  !,
  simplify_aux(exp(A + B), Out).

simplify_aux(Expr ^ 0.5, Out) :-
  !,
  simplify_aux(sqrt(Expr), Out).

simplify_aux(Expr ^ 1, Out) :-
  !,
  simplify_aux(Expr, Out).

simplify_aux(In, Out) :-
  term_morphism(simplify_aux, In, Middle),
  (
    rewrite_eval(Middle, Out)
  ->
    true
  ;
    Out = Middle
  ).


simplify_blocks(Block, Index, Blocks, ReducedSubBlocks) :-
  gather_loop(Block, Blocks, SubBlocks),
  maplist(Index, SubBlocks, IndexedSubBlocks),
  sort(4, @=<, IndexedSubBlocks, SortedSubBlocks),
  with_clean(
    [arithmetic_rules:canonical/3],
    (
      gather_indexed(SortedSubBlocks),
      reduce_blocks(IndexedSubBlocks, ReducedSubBlocks)
    )
  ).


gather_loop(Block, Blocks, SubBlocks) :-
  gather_blocks(Block, Blocks, Blocks0),
  map_blocks(simplify_aux, Blocks0, Blocks1),
  (
    Blocks0 = Blocks1
  ->
    SubBlocks = Blocks0
  ;
    gather_loop(Block, Blocks1, SubBlocks)
  ).


compute_product([], []).

compute_product([B | T], Out) :-
  block_number(B, N),
  !,
  compute_product_with_others(T, PT, Others),
  P is PT * N,
  (
    P = 1
  ->
    Out = Others
  ;
    normalize_number(P, PNorm),
    Out = [+ PNorm | Others]
  ).

compute_product([H | TIn], [H | TOut]) :-
  compute_product(TIn, TOut).


block_number(B, N) :-
  sign(B, Sign, C),
  (
    number(C),
    V = C
  ;
    C = M ^ E,
    number(M),
    V is M ^ E
  ),
  (
    Sign = 1
  ->
    N = V
  ;
    N is 1 / V
  ).


compute_product_with_others([], 1, []).

compute_product_with_others([B | T], P, Others) :-
  block_number(B, N),
  !,
  compute_product_with_others(T, PT, Others),
  P is PT * N.

compute_product_with_others([H | TIn], P, [H | TOut]) :-
  compute_product_with_others(TIn, P, TOut).


reduce_blocks([], []).

reduce_blocks([index(_, _, _, _, Canonical) | TailIn], Out) :-
  retract(canonical(Canonical, Sum, Others)),
  !,
  assert(canonical(Canonical, 0, Others)),
  reduce_block(Sum, Others, Out, TailOut),
  reduce_blocks(TailIn, TailOut).

reduce_blocks([index(Expr, Sign, Coef, _, _) | TailIn], Out) :-
  (
    Coef = 0
  ->
    reduce_blocks(TailIn, Out)
  ;
    reduce_block(Sign, Expr, Out, TailOut),
    reduce_blocks(TailIn, TailOut)
  ).


reduce_block(Coef, Expr, Out, TailOut) :-
  (
    Coef > 0
  ->
    Out = [+ (Coef : Expr) | TailOut]
  ;
    Coef < 0
  ->
    CoefOpp is -Coef,
    Out = [- (CoefOpp : Expr) | TailOut]
  ;
    Out = TailOut
  ).


gather_indexed([]).

gather_indexed([index(_, _, Coef1, Others, Canonical), index(_, _, Coef2, _, Canonical) | Tail]) :-
  !,
  gather_indexed_same(Canonical, Tail, TailSum, TailOthers),
  Sum is Coef1 + Coef2 + TailSum,
  assertz(canonical(Canonical, Sum, Others)),
  gather_indexed(TailOthers).

gather_indexed([_ | Tail]) :-
  gather_indexed(Tail).


gather_indexed_same(Canonical, [index(_, _, Coef, _, Canonical) | Tail], Sum, Others) :-
  !,
  gather_indexed_same(Canonical, Tail, TailSum, Others),
  Sum is Coef + TailSum.

gather_indexed_same(_Canonical, Others, 0, Others).


additive_index(H, index(Expr, Sign, Coef, Others, Canonical)) :-
  sign(H, Sign, Expr),
  (
    extract_coefficient(Expr, SubCoef, Others)
  ->
    true
  ;
    number(Expr)
  ->
    SubCoef = Expr,
    Others = 1
  ;
    SubCoef = 1,
    Others = Expr
  ),
  Coef is Sign * SubCoef,
  canonical_expression(Others, Canonical).


extract_coefficient(C, C, 1) :-
  number(C),
  !.

extract_coefficient(- CA, COpp, A) :-
  extract_coefficient(CA, C, A),
  !,
  COpp is - C.

extract_coefficient(+ CA, C, A) :-
  extract_coefficient(CA, C, A),
  !.

extract_coefficient(A * B, A, B) :-
  number(A),
  !.

extract_coefficient(A / B, A, 1 / B) :-
  number(A),
  !.

extract_coefficient(A * B, B, A) :-
  number(B),
  !.

extract_coefficient(A / B, BInv, A) :-
  number(B),
  !,
  BInv is 1 / B.

extract_coefficient(CA * B, C, A * B) :-
  extract_coefficient(CA, C, A),
  !.

extract_coefficient(A * CB, C, A * B) :-
  extract_coefficient(CB, C, B).


extract_powers(sqrt(Expr), Power, Sub) :-
  !,
  extract_powers(Expr, SubPower, Sub),
  Power is 0.5 * SubPower.

extract_powers(Expr ^ Pow, Power, Sub) :-
  number(Pow),
  !,
  extract_powers(Expr, SubPower, Sub),
  Power is Pow * SubPower.

extract_powers(Expr, 1, Expr).


multiplicative_index(H, index(Expr, Sign, Coef, Others, Canonical)) :-
  sign(H, Sign, Expr),
  extract_powers(Expr, SubCoef, Others),
  Coef is Sign * SubCoef,
  canonical_expression(Others, Canonical).


canonical_expression(In, Out) :-
  additive_block(In, Blocks),
  !,
  gather_blocks(additive_block, Blocks, SubBlocks),
  map_blocks(arithmetic_rules:canonical_expression, SubBlocks, CanonicalSubBlocks),
  sort(CanonicalSubBlocks, SortedSubBlocks),
  rebuild_additive_blocks(SortedSubBlocks, Out).

canonical_expression(In, Out) :-
  multiplicative_block(In, Blocks),
  !,
  gather_blocks(multiplicative_block, Blocks, SubBlocks),
  map_blocks(arithmetic_rules:canonical_expression, SubBlocks, CanonicalSubBlocks),
  sort(CanonicalSubBlocks, SortedSubBlocks),
  rebuild_multiplicative_blocks(SortedSubBlocks, Out).

canonical_expression(In, Out) :-
  term_morphism(canonical_expression, In, Out).


rebuild_additive_coef(SignedBlock, Value) :-
  sign(SignedBlock, Sign, Block),
  (
    Block = (Coef : Expr)
  ->
    SignedCoef is Sign * Coef,
    (
      SignedCoef = 1
    ->
      Value = + Expr
    ;
      Expr = 1
    ->
      Value = + SignedCoef
    ;
      (
        extract_coefficient(Expr, OtherCoef, SubExpr)
      ->
        FullCoef is SignedCoef * OtherCoef
      ;
        FullCoef = SignedCoef,
        SubExpr = Expr
      ),
      (
        FullCoef < 0
      ->
        NewSign = -1,
        NewCoef is - FullCoef
      ;
        NewSign = 1,
        NewCoef = FullCoef
      ),
      (
        NewCoef = 1
      ->
        UnsignedValue = SubExpr
      ;
        insert_coef(SubExpr, NewCoef, UnsignedValue)
      ),
      sign(Value, NewSign, UnsignedValue)
    )
  ;
    Value = + Block
  ).


insert_coef(A * B, Coef, Result * B) :-
  !,
  insert_coef(A, Coef, Result).

insert_coef(A, Coef, Result) :-
  (
    Coef > 0,
    Coef < 1,
    CoefInv is 1 / Coef,
    0.0 is float_fractional_part(CoefInv)
  ->
    normalize_number(CoefInv, CoefInvNorm),
    Result = A / CoefInvNorm
  ;
    normalize_number(Coef, CoefNorm),
    Result = CoefNorm * A
  ).


rebuild_multiplicative_coef(Block, Value) :-
  (
    Block = (Coef : Expr)
  ->
    (
      Coef = 1
    ->
      Value = Expr
    ;
      Expr = 1
    ->
      Value = 1
    ;
      Coef = 0.5
    ->
      Value = sqrt(Expr)
    ;
      Value = Expr ^ Coef
    )
  ;
    Value = Block
  ).


rebuild_additive_blocks([], 0).

rebuild_additive_blocks([+H | T], Out) :-
  !,
  rebuild_additive_blocks(H, T, Out).

rebuild_additive_blocks([-H | T], Out) :-
  select(+A, T, Others),
  !,
  rebuild_additive_blocks(A, [-H | Others], Out).

rebuild_additive_blocks([-H | T], Out) :-
  rebuild_additive_blocks(-H, T, Out).


rebuild_additive_blocks(A, [], A) :-
  !.

rebuild_additive_blocks(A, [+H | T], Out) :-
  !,
  rebuild_additive_blocks(A + H, T, Out).

rebuild_additive_blocks(A, [-H | T], Out) :-
  rebuild_additive_blocks(A - H, T, Out).


rebuild_multiplicative_blocks([], 1).

rebuild_multiplicative_blocks([+H | T], Out) :-
  !,
  rebuild_multiplicative_blocks(H, T, Out).

rebuild_multiplicative_blocks([-H | T], Out) :-
  select(+A, T, Others),
  !,
  rebuild_multiplicative_blocks(A, [-H | Others], Out).

rebuild_multiplicative_blocks([-H | T], Out) :-
  rebuild_multiplicative_blocks(1 / H, T, Out).


rebuild_multiplicative_blocks(A, [], A) :-
  !.

rebuild_multiplicative_blocks(A, [+H | T], Out) :-
  !,
  rebuild_multiplicative_blocks(A * H, T, Out).

rebuild_multiplicative_blocks(A, [-H | T], Out) :-
  rebuild_multiplicative_blocks(A / H, T, Out).


gather_blocks(P, Blocks, SubBlocks) :-
  map_blocks(maybe(P), Blocks, BlocksOfBlocks),
  flatten_blocks(BlocksOfBlocks, SubBlocks).


maybe(P, A, B) :-
  call(P, A, SubBlocks),
  !,
  gather_blocks(P, SubBlocks, B).

maybe(_P, A, [+A]).


map_blocks(P, Blocks, SubBlocks) :-
  maplist(map_block(P), Blocks, SubBlocks).


map_block(P, +A, +B) :-
  !,
  call(P, A, B).

map_block(P, -A, -B) :-
  call(P, A, B).


flatten_blocks([], []).

flatten_blocks([H | TailIn], Out) :-
  (
    H = +L
  ->
    true
  ;
    H = -LOpp
  ->
    maplist(opp, LOpp, L)
  ),
  append(L, TailOut, Out),
  flatten_blocks(TailIn, TailOut).


opp(+ A, - A).

opp(- A, + A).


sign(+ A, 1, A).

sign(- A, -1, A).


additive_block(+ A, [+A]).

additive_block(- A, [-A]).

additive_block(A + B, [+A, +B]).

additive_block(A - B, [+A, -B]).

multiplicative_block(+ A, [+A]).

multiplicative_block(- A, [+(-1), +A]).

multiplicative_block(A * B, [+A, +B]).

multiplicative_block(A / B, [+A, -B]).


rewrite_simplify(A + 0, A).

rewrite_simplify(0 + A, A).

rewrite_simplify(A - 0, A).

rewrite_simplify(0 - A, A).

rewrite_simplify(- - A, A).

rewrite_simplify(A + (- B), A - B).

rewrite_simplify(A - (- B), A + B).

rewrite_simplify(- (A + B), - A - B).

rewrite_simplify(- (A - B), - A + B).

rewrite_simplify(A + (B + C), A + B + C).

rewrite_simplify(A + (B - C), A + B - C).

rewrite_simplify(A - (B + C), A - B - C).

rewrite_simplify(A - (B - C), A - B + C).

rewrite_simplify(_A * 0, 0).

rewrite_simplify(0 * _A, 0).

rewrite_simplify(A * (B * C), A * B * C).

rewrite_simplify(0 / _A, 0).

rewrite_simplify(A / A, 1).


distribute(In, Out) :-
  rewrite(rewrite_distribute, In, Out).


rewrite_distribute((A + B) * C, A * C + B * C).

rewrite_distribute(A * (B + C), A * B + A * C).

rewrite_distribute((A - B) * C, A * C - B * C).

rewrite_distribute(A * (B - C), A * B - A * C).

rewrite_distribute((A + B) / C, A / C + B / C).

rewrite_distribute((A - B) / C, A / C - B / C).

rewrite_distribute(A - (B + C), A - B - C).

rewrite_distribute(- (A + B), - A - B).


additive_normal_form(In, Out) :-
  rewrite(rewrite_additive_normal_form, In, Out).


rewrite_additive_normal_form(A, B) :-
  rewrite_eval(A, B).

rewrite_additive_normal_form(A, B) :-
  rewrite_distribute(A, B).

rewrite_additive_normal_form(A * B, B * A) :-
  number(B).

rewrite_additive_normal_form((A * B) * C, A * (B * C)) :-
  number(A).

rewrite_additive_normal_form(A * (B * C), D * C) :-
  number(A),
  number(B),
  D is A*B.

rewrite_additive_normal_form(A * (B * C), B * (A * C)) :-
  number(B).

rewrite_additive_normal_form((A * B) / C, A * (B / C)) :-
  number(A).

rewrite_additive_normal_form(- A, (-1) * A).

rewrite_additive_normal_form(A - B, A + (-1) * B).


rewrite_eval(In, Out) :-
  arithmetic_operation(In),
  In =.. [_ | Args],
  maplist(number, Args),
  !,
  Out is In.


arithmetic_operation(_ + _).

arithmetic_operation(_ - _).

arithmetic_operation(_ * _).

arithmetic_operation(_ / _).

arithmetic_operation(- _).

arithmetic_operation(_ ^ _).

always_negative(A) :-
  rewrite(rewrite_simplify, A, B),
  B \= A,
  !,
  always_negative(B).

always_negative(A) :-
  number(A),
  !,
  A =< 0.

always_negative(- A) :-
  always_positive(A).

always_negative(A + B) :-
  always_negative(A),
  always_negative(B).

always_negative(A - B) :-
  always_negative(A),
  always_positive(B).

always_negative(A * B) :-
  always_negative(A),
  always_positive(B).

always_negative(0 * _) :-
  !.

always_negative(A * B) :-
  always_positive(A),
  always_negative(B).

always_negative(A / B) :-
  always_negative(A),
  always_positive(B).

always_negative(A / B) :-
  always_positive(A),
  always_negative(B).


always_positive(A) :-
  rewrite(rewrite_simplify, A, B),
  B \= A,
  !,
  always_positive(B).

always_positive(A) :-
  number(A),
  !,
  A >= 0.

always_positive(A):- % assumed for concentrations, parameters 
    atom(A),
    !.

always_positive(- A) :-
  always_negative(A).

always_positive(_A ^ B) :-
  number(B),
  0 is B mod 2.

always_positive(A + B) :-
  always_positive(A),
  always_positive(B).

always_positive(A - B) :-
  always_positive(A),
  always_negative(B).

always_positive(0 * _) :-
  !.

always_positive(A * B) :-
  always_negative(A),
  always_negative(B).

always_positive(A * B) :-
  always_positive(A),
  always_positive(B).

always_positive(A / B) :-
  always_positive(A),
  always_positive(B).

always_positive(A / B) :-
  always_negative(A),
  always_negative(B).


normalize_number(N, Norm) :-
  (
    F is float_fractional_part(N),
    (
      F = 0
    ;
      F = 0.0
    )
  ->
    Norm is truncate(N)
  ;
    Norm = N
  ).
