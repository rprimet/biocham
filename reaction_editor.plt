:- use_module(library(plunit)).


:- begin_tests(reaction_editor).


test(
  'compound',
  [Reactions == [2 * a + 2 * b => 2 * 'a-b']]
) :-
  clear_model,
  command(add_reaction(2 * a + 2 * b => 2 * a-b)),
  all_items([kind: reaction], Reactions).

test(
  'catalyst',
  [Reactions == [b =[ b + a + c ]=> '_', '_' =[ b + a + c ]=> b]]
) :-
  clear_model,
  add_reaction(a + b + c <=[ b ]=> a + c),
  simplify_all_reactions,
  all_items([kind: reaction], Reactions).

test(
  'reversible reaction',
  [true(Reactions == [1 for a => b, 2 for b => a])]
) :-
  clear_model,
  command((1, 2) for a <=> b),
  all_items([kind: reaction], Reactions).

test(
'michaelis menten reduction',
  [true(Reactions == [(2*'MA'(1) for 'S'=['E'+'C']=>'P')])]
) :-

  command(load('library:examples/michaelis-menten/mm.bc')),
  command(delete_reaction('C'=>'E'+'S')),
  command(merge_reactions('E'+'S'=>'C','C'=>'E'+'P')),
%  command(merge_molecules('E','C')),
  all_items([kind: reaction], Reactions).

test(
'michaelis menten reduction C',
  [true(Reactions == [(2*'MA'(1) for 'S'=['E'+'C']=>'P')])]
) :-

  command(load('library:examples/michaelis-menten/mm.bc')),
  command(merge_reactions('E'+'S'=>'C','C'=>'E'+'P')),
  command(delete_reaction('C'=>'E'+'S')),
  all_items([kind: reaction], Reactions).

:- end_tests(reaction_editor).
