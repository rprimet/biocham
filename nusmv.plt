:- use_module(library(plunit)).
% not necessary, except for separate compilation
:- use_module(reaction_rules).
:- use_module(influence_rules).

:- begin_tests(nusmv).

test('export_reac', [
  setup(set_a_dummy_reaction_model),
  cleanup((clear_model, delete_file('unittest.smv')))
]) :-
  export_nusmv('unittest').

test(
  'check_ctl_reac',
  [
    setup(set_a_dummy_reaction_model),
    cleanup(clear_model),
    true(Result == 'false')
  ]
) :-
  nusmv:check_ctl_impl('AG'(c -> b), all, no, negative, Result).


test('export_infl', [
  setup(set_a_dummy_influence_model),
  cleanup((clear_model, delete_file('unittest.smv')))
]) :-
  export_nusmv('unittest').

test(
  'check_ctl_infl',
  [
    setup(set_a_dummy_influence_model),
    cleanup(clear_model),
    true(Result == 'false')
  ]
) :-
  nusmv:check_ctl_impl('AG'(not(c) \/ b), all, no, negative, Result).


test(
  'Lotka-Voltera',
  [
    forall(member(File, [
      'library:examples/lotka_volterra/LVr.bc',
      'library:examples/lotka_volterra/LVi.bc'
    ])),
    setup(load(File)),
    cleanup(clear_model)
  ]
) :-
  assertion(nusmv:check_ctl_impl('EG'('R') /\ 'P', all, no, negative, true)),
  assertion(nusmv:check_ctl_impl('EF'('AG'('R' /\ not('P'))), all, no,
  negative, true)),
  assertion(nusmv:check_ctl_impl('EF'('AG'(not('R') /\ not('P'))), all, no,
  negative, true)),
  assertion(nusmv:check_ctl_impl('EF'('AG'(not('R') /\ 'P')), all, no,
  negative, false)).


test(
  'MAPK',
  [
    condition(flag(slow_test, true, true)),
    true(Result == 'true')
  ]
) :-
  command(clear_model),
  command(load('library:examples/mapk/mapk.bc')),
  nusmv:check_ctl_impl(
    reachable('MAPK~{p1,p2}') /\
    checkpoint('MEK~{p1}', 'MAPK~{p1,p2}') /\
    'EF'('MEK~{p1}-MEKPH' /\ 'EF'('MAPK~{p1,p2}')) /\
    'EU'(not('MEK~{p1}-MEKPH'), 'MAPK~{p1,p2}'),
    all,
    no,
    negative,
    Result
  ).


set_a_dummy_reaction_model :-
  clear_model,
  add_reaction(a => b),
  add_reaction(a + b => c),
  command(present(a)),
  command(absent(c)).


set_a_dummy_influence_model :-
  clear_model,
  add_influence(a -> b),
  add_influence((a, b) -< c),
  command(present(a)),
  command(present(c)).

:- end_tests(nusmv).
