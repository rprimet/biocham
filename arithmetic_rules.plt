:- use_module(library(plunit)).

:- begin_tests(arithmetic_rules).

test('distribute', [true(Out == a * a + a * b + c)]) :-
  distribute(a * (a + b) + c, Out).

:- end_tests(arithmetic_rules).
