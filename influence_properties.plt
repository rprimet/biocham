:- use_module(library(plunit)).

:- begin_tests(tscc).

test(
  'list_bernot_tscc',
  [
    setup(load('library:examples/circadian_cycle/bernot_comet.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(Output), list_tscc_candidates),
  sub_atom(Output, _, _, _, '[G-1,L-1,PC-1] terminal'),
  !.

test(
  'list_kaufman_tscc',
  [
    setup(load('library:examples/p53/kaufman.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(Output), list_tscc_candidates),
  sub_atom(Output, Before, _, After, '[C-1,D-1,N-1,P-0] terminal'),
  !,
  \+ sub_atom(Before, _, _, _, '] terminal'),
  \+ sub_atom(After, _, _, _, '] terminal').


:- end_tests(tscc).
