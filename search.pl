:- module(
  search,
  [
    search_parameters/3,
    search_parameters/2,
    robustness/3
  ]
).


% for linting purposes
:- use_module(objects).
:- use_module(biocham).

:- doc('The continuous satisfaction degree of an FO-LTL(Rlin) in a given trace with respect to the objective values for the free variables can be used to compute parameter sensitivity indices and robustness measures with respect to parameter perturbation according to normal distributions, and to search parameter values for satisfying an FO-LTL(Rlin) formula \\cite{RBFS11tcs} or even maximizing the margins and the robustness \\cite{FS18cmsb}.').

:- initial('option(robustness_samples: 100)').
:- initial('option(robustness_relative_error: 0.05)').
:- initial('option(robustness_coeff_var: 0.1)').


robustness(Formula, Parameters, Objective) :-
  biocham_command,
  type(Formula, foltl),
  type(Parameters, [parameter_name]),
  type(Objective, [variable_objective]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    robustness_coeff_var,
    number,
    Variation,
    'coefficient of variation of the normal law'
  ),
  option(
    robustness_samples,
    integer,
    Samples,
    'number of samples used for averaging'
  ),
  option(
    robustness_relative_error,
    number,
    RelativeError,
    'relative sampling error used to stop estimation of the robustness'
  ),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  doc('
    computes the robustness degree as defined in \\cite{RBFS11tcs}, with
    respect to formula \\argument{Formula}, for the list of parameters
    \\argument{Parameters} and with list of objectives for the free variables
    of \\argument{Formula} given in \\argument{Objective}.'),
  doc('
    The robustness is the average satisfaction degree (truncated to one) estimated by sampling of a normal perturbation law with
    a coefficient of variation (stddev/mean) given by the corresponding option,
    and centered around the current parameter values.
\\clearmodel
\\begin{example}
\\trace{
biocham: k for a => b. present(a,10). parameter(k=0.7).
biocham: robustness(G(Time < T => a > b), [k], [T -> 5]).
}
\\end{example}
  '),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      robustness_aux(Parameters, [(Formula, [], Objective)], Samples,
        RelativeError, Variation)
    )
  ).



:- initial('option(cmaes_init_center: no)').
:- initial('option(cmaes_log_normal: no)').
:- initial('option(cmaes_stop_fitness: 0.0001)').


search_parameters(Formula, Parameters, Objective) :-
  biocham_command,
  type(Formula, foltl),
  type(Parameters, [parameter_between_interval]),
  type(Objective, [variable_objective]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    cmaes_init_center, yesno, _CmaesInit,
    'initialize parameter values at the center of their domain instead of
    their current value'
  ),
  option(
    cmaes_log_normal, yesno, _Log,
    'search on the log of the parameters (better if very different orders of
    magnitude)'
  ),
  option(
    cmaes_stop_fitness, number, _Fitness,
    'stop when distance to the objective is less than this value (use 0 by default and 
    negative values in [-1,0] to maximize margins)'
  ),
  doc('
    tries to satisfy a FO-LTL(Rlin) constraint by varying the parameters listed
    in \\argument{Parameters}.
    This search command uses the stochastic optimization procedure CMA-ES
    (covariance matrix adaptation evolution strategy)
    with the continuous satisfaction degree (truncated to 1 or not according to stop option) of the given FO-LTL(Rlin) property as
    fitness function.
\\clearmodel
\\begin{example}
\\trace{
biocham: k for a => b. present(a,10).
biocham: search_parameters(G(Time < T => a > b), [0 <= k <= 2], [T -> 5]).
biocham: search_parameters(G(Time < T => a > b), [0 <= k <= 2], [T -> 5]).
}
\\end{example}
  '),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      search_parameters_aux(Parameters, [(Formula, [], Objective)], Result)
    )
  ),
  set_parameters(Result),
  find_parameter_ids(Parameters, ParameterIds),
  list_ids(ParameterIds).


search_parameters(Parameters, Conditions) :-
  biocham_command,
  type(Parameters, [parameter_between_interval]),
  % type(Conditions, [(foltl, *(parameter_name=number), [variable_objective])]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    cmaes_init_center, yesno, _CmaesInit,
    'initialize parameter values at the center of their domain instead of
    their current value'
  ),
  option(
    cmaes_log_normal, yesno, _Log,
    'search on the log of the parameters (better if very different orders of
    magnitude)'
  ),
  option(
    cmaes_stop_fitness, number, _Fitness,
    'stop when distance to the objective is less than this value (use
    negative to enforce robustness)'
  ),
  doc('similar to \\command{search_parameters/3} but uses the list of
    \\argument{Conditions} as triples with an FOLTL formula, a list of parameters instantiations (e.g. describing a mutant), and an objective for the
    variables of the formula.
\\clearmodel
\\begin{example}'),
  biocham_silent('option(time: 20)'),
  biocham('ka for _ => a'),
  biocham('kb for _ => b'),
  biocham('parameter(ka=1, kb=1)'),
  biocham('search_parameters([0 <= ka <= 3], [(G(a-b<x/\\b-a<x), [], [x -> 10]), (G(a-b<y/\\b-a<y), [kb=2], [y -> 10])])'),
  doc('\\end{example}'),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      search_parameters_aux(Parameters, Conditions, Result)
    )
  ),
  set_parameters(Result),
  find_parameter_ids(Parameters, ParameterIds),
  list_ids(ParameterIds).


% store free_variables for each formula
:- dynamic(free_variable_index/3).


search_parameters_aux(Parameters, Conditions, Result) :-
  retractall(free_variable_index(_, _, _)),
  GslCFilename = 'ode.inc',
  DomainCFilename = 'check.inc',
  SearchParametersCFilename = 'search_parameters.inc',
  statistics(walltime, _),
  ExecutableFilename = 'search',
  ensure_parameters(Parameters),
  prepare_numerical_simulation_options(Options),
  with_output_to_file(GslCFilename, write_gsl(Options)),
  with_clean(
    [foltl:column/2],
    (
      populate_field_columns(Options),
      with_output_to_file(DomainCFilename,
        forall(
          nth0(Index, Conditions, (Formula, _, _)), 
          (
            expand_formula(Formula, ExpandedFormula),
            atom_number(AIndex, Index),
            generate_domain_cpp(ExpandedFormula, AIndex),
            forall(
              foltl:free_variable_index(X, Y),
              assertz(free_variable_index(Index, X, Y))
            )
          )
        )
      )
    )
  ),
  findall(
    Objective,
    member((_, _, Objective), Conditions),
    Objectives
  ),
  % use our indexed free_var_index/3
  retractall(foltl:free_variable_index(_, _)),
  with_output_to_file(
    SearchParametersCFilename,
    (
      generate_random_seed,
      generate_conditions(Conditions),
      generate_search_parameters(Parameters),
      generate_objective(Objectives)
    )
  ),
  compile_search_cpp_program(ExecutableFilename),
  call_subprocess(
    ExecutableFilename, [], [stdout(pipe(ResultStream))]
  ),
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  format('Time: ~p s\n', [Time]),
  read_stop_reason(ResultStream, StopReason),
  format('Stopping reason: ~w~n', [StopReason]),
  read_parameter_values(Parameters, ResultStream, Result),
  read_satisfaction_degree(ResultStream, Degree),
  format('Best satisfaction degree: ~p~n', [Degree]),
  (
    have_to_delete_temporary_files
  ->
    delete_file(GslCFilename),
    delete_file(DomainCFilename),
    delete_file(SearchParametersCFilename),
    delete_file(ExecutableFilename)
  ;
    true
  ).


robustness_aux(ParameterList, Conditions, Samples, RelativeError, Variation) :-
  retractall(free_variable_index(_, _, _)),
  GslCFilename = 'ode.inc',
  DomainCFilename = 'check.inc',
  SearchParametersCFilename = 'search_parameters.inc',
  ExecutableFilename = 'robustness',
  statistics(walltime, _),
  findall(0 <= X <= 0, member(X, ParameterList), Parameters),
  ensure_parameters(Parameters),
  prepare_numerical_simulation_options(Options),
  with_output_to_file(GslCFilename, write_gsl(Options)),
  with_clean(
    [foltl:column/2],
    (
      populate_field_columns(Options),
      with_output_to_file(DomainCFilename,
        forall(
          nth0(Index, Conditions, (Formula, _, _)), 
          (
            expand_formula(Formula, ExpandedFormula),
            atom_number(AIndex, Index),
            generate_domain_cpp(ExpandedFormula, AIndex),
            forall(
              foltl:free_variable_index(X, Y),
              assertz(free_variable_index(Index, X, Y))
            )
          )
        )
      )
    )
  ),
  findall(
    Objective,
    member((_, _, Objective), Conditions),
    Objectives
  ),
  % use our indexed free_var_index/3
  retractall(foltl:free_variable_index(_, _)),
  with_output_to_file(
    SearchParametersCFilename,
    (
      generate_random_seed,
      generate_sample_number_and_var(Samples, RelativeError, Variation),
      generate_conditions(Conditions),
      generate_search_parameters(Parameters),
      generate_objective(Objectives)
    )
  ),
  compile_robustness_cpp_program(ExecutableFilename),
  call_subprocess(
    ExecutableFilename, [], [stdout(pipe(ResultStream))]
  ),
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  format('Time: ~p s\n', [Time]),
  read_satisfaction_degree(ResultStream, Degree),
  format('Robustness degree: ~p~n', [Degree]),
  (
    have_to_delete_temporary_files
  ->
    delete_file(GslCFilename),
    delete_file(DomainCFilename),
    delete_file(SearchParametersCFilename),
    delete_file(ExecutableFilename)
  ;
    true
  ).


generate_random_seed :-
  (
    toplevel:random_seed(Seed)
  ->
    NewSeed is Seed + 1
  ;
    NewSeed = 0
  ),
  format('#define SEED ~d~n', [NewSeed]).


generate_sample_number_and_var(Samples, RelativeError, Variation) :-
  format('#define SAMPLES ~d~n', [Samples]),
  format('#define RELATIVE_ROBUSTNESS_ERROR ~p~n', [RelativeError]),
  format('#define COEFF_VAR ~p~n', [Variation]).


generate_conditions(Conditions) :-
  length(Conditions, N),
  format('#define CONDITIONS ~d\n', [N]),
  MaxCond is N - 1,
  write('static Domain (*compute_condition_domain[CONDITIONS]) (const Table &) = {\n'),
  forall(
    between(0, MaxCond, I),
    format('    compute_domain~d,~n', [I])
  ),
  write('};\n'),
  forall(
    nth0(Index, Conditions, (_, Mutant, _)),
    setup_condition(Index, Mutant)
  ),
  write('static void (*setup_condition[CONDITIONS]) (gsl_solver*) = {\n'),
  forall(
    between(0, MaxCond, I),
    format('    setup_condition~d,~n', [I])
  ),
  write('};\n').


setup_condition(Index, Mutant) :-
  format('static void setup_condition~d (gsl_solver *solver) {~n', [Index]),
  maplist(numerical_simulation:convert_parameter, Mutant, IndexedMutant),
  forall(
    member(ParamIndex = Value, IndexedMutant),
    % Value should be a number
    format('    solver->p[~d] = ~w;~n', [ParamIndex, Value])
  ),
  write('};\n').


generate_search_parameters(Parameters) :-
  get_option(cmaes_init_center, Center),
  length(Parameters, ParameterCount),
  format('#define DIMENSION ~d\n', [ParameterCount]),
  get_option(cmaes_stop_fitness, Fitness),
  format('#define STOPFIT ~p\n', [Fitness]),
  get_option(cmaes_log_normal, Log),
  (
    Log == yes
  ->
    format('#define LOGNORMAL true\n')
  ;
    format('#define LOGNORMAL false\n')
  ),
  write('\c
static const struct search_parameter search_parameters[DIMENSION] = {\n'),
  \+ (
    member(Min <= Parameter <= Max, Parameters),
    \+ (
      numerical_simulation:parameter_index(Parameter, ParameterIndex),
      (
        Center == yes
      ->
        Value is (Min + Max)/2
      ;
        parameter_value(Parameter, Value)
      ),
      (
        Log == yes
      ->
        LMin is log(Min),
        LMax is log(Max),
        LVal is log(Value),
        format('    {~f, ~d, ~f, ~f},\n', [LMin, ParameterIndex, LMax, LVal])
      ;
        format('    {~f, ~d, ~f, ~f},\n', [Min, ParameterIndex, Max, Value])
      )
    )
  ),
  write('};\n').


compile_search_cpp_program(ExecutableFilename) :-
  gsl_compile_options(GslCompileOptions),
  ppl_compile_options(PplCompileOptions),
  openmp_compile_options(OmpCompileOptions),
  flatten(
    [GslCompileOptions, PplCompileOptions, OmpCompileOptions],
    AllCompileOptions
  ),
  compile_cpp_program(
    ['search_parameters.cc', 'gsl_solver.o', 'cmaes.o'],
    AllCompileOptions,
    ExecutableFilename
  ).


compile_robustness_cpp_program(ExecutableFilename) :-
  gsl_compile_options(GslCompileOptions),
  ppl_compile_options(PplCompileOptions),
  openmp_compile_options(OmpCompileOptions),
  flatten(
    [GslCompileOptions, PplCompileOptions, OmpCompileOptions],
    AllCompileOptions
  ),
  compile_cpp_program(
    ['robustness.cc', 'gsl_solver.o'],
    AllCompileOptions,
    ExecutableFilename
  ).


% FIXME ugly hack to try and guess if we have openmp support
openmp_compile_options(OmpCompileOptions) :-
  current_prolog_flag(arch, Arch),
  (
    sub_atom(Arch, _, 6, _, darwin)
  ->
    (
      getenv('CXX', CXX),
      sub_atom(CXX, _, 1, _, '-')
    ->
      OmpCompileOptions = ['-fopenmp']
    ;
      OmpCompileOptions = []
    )
  ;
    OmpCompileOptions = ['-fopenmp']
  ).


ensure_parameters(Parameters) :-
  forall(
    member(Min <= Parameter <= _Max, Parameters),
    (
      parameter_value(Parameter, _)
    ->
      true
    ;
      set_parameter(Parameter, Min)
    )
  ).


find_parameter_ids([], []).

find_parameter_ids([_Min <= Parameter <= _Max | Tail], [Id | IdTail]) :-
  find_item([key: Parameter, id: Id]),
  find_parameter_ids(Tail, IdTail).


populate_field_columns(Options) :-
  memberchk(fields: Fields, Options),
  \+ (
    nth0(Index, Fields, Field: _),
    \+ (
      assertz(foltl:column(Field, Index))
    )
  ).



read_stop_reason(Stream, Reason) :-
  read_line_to_string(Stream, Reason).


read_parameter_values([], _ResultStream, []).

read_parameter_values(
  [_Min <= Parameter <= _Max | ParameterTail], ResultStream,
  [Parameter = Value | ResultTail]
) :-
  read_line_to_codes(ResultStream, ValueCode),
  number_codes(Value, ValueCode),
  read_parameter_values(ParameterTail, ResultStream, ResultTail).


read_satisfaction_degree(Stream, Degree) :-
  read_line_to_codes(Stream, Codes),
  number_codes(Degree, Codes).
