#include <stdio.h>
#include <SWI-Prolog.h>
#include <graphviz_swiprolog.h>
#include <sbml_swiprolog.h>
#include <roots.h>
#include "modules/glucose/glucose_swiprolog.h"

int
main(int argc, char **argv)
{
    install_graphviz_swiprolog();
    install_sbml_swiprolog();
    install_roots();
    install_glucose_swiprolog();

#ifdef READLINE
    PL_initialise_hook(install_readline);
#endif

    if (!PL_initialise(argc, argv)) {
        PL_halt(1);
    }

    /* PL_install_readline(); */

    PL_halt(PL_toplevel() ? 0 : 1);
}
