:- module(
  formal_derivation,
  [
    derivate/3,
    jacobian/2
  ]).

:- use_module(library(error)).

derivate(Expression, Variable, Result) :-
  derivate_raw(Expression, Variable, UnsimplifiedResult),
  simplify(UnsimplifiedResult, Result).

derivate_raw(p(_ParameterIndex), _Variable, 0) :-
  !.

derivate_raw(Value, _Variable, 0) :-
  number(Value),
  !.

derivate_raw(- A, Variable, - Aprime) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(A + B, Variable, Aprime + Bprime) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

derivate_raw(A - B, Variable, Aprime - Bprime) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

derivate_raw(A * B, Variable, A * Bprime + Aprime * B) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

derivate_raw(A / B, Variable, (Aprime * B - A * Bprime) / (B ^ 2)) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

derivate_raw(A ^ B, Variable, Result) :-
  !,
  derivate_raw(exp(B * log(A)), Variable, Result).

derivate_raw(sqrt(A), Variable, Result) :-
  !,
  derivate_raw(A ^ 0.5, Variable, Result).

derivate_raw(log(A), Variable, Aprime / A) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(exp(A), Variable, Aprime * exp(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(sin(A), Variable, Aprime * cos(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(cos(A), Variable, - Aprime * sin(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(Variable0, Variable1, Result) :-
  !,
  (
    Variable0 = Variable1
  ->
    Result = 1
  ;
    Result = 0
  ).


jacobian(Equations, Jacobian) :-
  length(Equations, VariableCount),
  findall(
    Line,
    (
      member(Equation, Equations),
      jacobian_line(Equation, VariableCount, Line)
    ),
    Jacobian).


jacobian_line(Equation, VariableCount, Line) :-
  VariableMax is VariableCount - 1,
  findall(
    Derivative,
    (
      between(0, VariableMax, VariableIndex),
      derivate(Equation, [VariableIndex], Derivative)
    ),
    Line).
