:- module(
  c_compiler,
  [
    % Public API
    compile_c_program/3,
    compile_cpp_program/3,
    gsl_compile_options/1,
    ppl_compile_options/1
  ]
).


% Public API


compile_c_program(SourceFiles, Options, ExecutableFilename) :-
  (
    getenv('CC', CC)
  ->
    true
  ;
    CC = 'gcc'
  ),
  compile_program(CC, SourceFiles, Options, ExecutableFilename).


compile_cpp_program(SourceFiles, Options, ExecutableFilename) :-
  (
    getenv('CXX', CXX)
  ->
    true
  ;
    CXX = 'g++'
  ),
  compile_program(
    CXX, SourceFiles,
    ['--std=c++11', '-Dtypeof(x)=decltype(x)' | Options],
    ExecutableFilename
  ).


%% TODO 
%%  - robust parsing (w.r.t. spaces for instance)
%%  - error handling (unknown library)
pkg_compile_options(PkgName, Options) :-
  format(string(Command), "pkg-config --libs --cflags ~w", [PkgName]),
  open(pipe(Command), read, Fd),
  read_string(Fd, "", "\n", _, OptionString),
  split_string(OptionString, " ", "", OptionsT),
  delete(OptionsT, "", Options).


gsl_compile_options(Options) :-
  pkg_compile_options('gsl', Options).

ppl_compile_options(['-lppl_c', '-lppl', '-lgmpxx', '-lgmp']).


% Private predicates

callp(P,E,O) :-
  is_absolute_file_name(P),
  !,
  call_subprocess(P,E,O).

callp(P,E,O) :-
  call_subprocess(path(P),E,O).

compile_program(Compiler, SourceFiles, Options, ExecutableFilename) :-
  library_path(LibraryPath),
  findall(
    AbsoluteSourceFile,
    (
      member(SourceFile, SourceFiles),
      absolute_file_name(
          SourceFile, AbsoluteSourceFile, [relative_to(LibraryPath)]
      )
    ),
    AbsoluteSourceFiles
  ),
  append(AbsoluteSourceFiles, Options, AllOptions),
  (
    debugging('c_compiler')
  ->
    AllOptions0 = ['-g' | AllOptions]
  ;
    AllOptions0 = AllOptions
  ),
  callp(
    Compiler, ['-o', ExecutableFilename, '-I.' | AllOptions0], []
  ).
