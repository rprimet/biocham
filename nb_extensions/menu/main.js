import { html_menu } from "../../../biocham_kernel/cmd_tree";
import $ from 'jquery';

let options = {
    sibling: undefined, // if undefined, set by cfg.sibling_selector
    menus : [],
    hooks: {
        pre_config: undefined,
        post_config: undefined,
    }
};


// default parameters
let cfg = {
    insert_as_new_cell: true,
    insert_before_sibling: false,
    include_submenu: {}, // default set after this definition
    sibling_selector: '#help_menu',
    top_level_submenu_goes_right: true,
};


function config_loaded_callback () {
    if (options['pre_config_hook'] !== undefined) {
        options['pre_config_hook']();
    }

    options.menus = [
        {
            'name' : 'Commands',
            'sub_menu_direction' : cfg.top_level_submenu_goes_right ? 'right' : 'left',
            'children' : html_menu,
        },
    ];

    // select correct sibling
    if (options.sibling === undefined) {
        options.sibling = $(cfg.sibling_selector).parent();
        if (options.sibling.length < 1) {
            options.sibling = $("#help_menu").parent();
        }
    }
}

function insert_snippet_code (snippet_code) {
    //TODO: insert in last cell
    if (cfg.insert_as_new_cell) {
        const new_cell = Jupyter.notebook.insert_cell_below('code');
        new_cell.set_text(snippet_code);
        new_cell.focus_cell();
    }
    else {
        const selected_cell = Jupyter.notebook.get_selected_cell();
        Jupyter.notebook.edit_mode();
        selected_cell.code_mirror.replaceSelection(snippet_code, 'around');
    }
}

function callback_insert_snippet (evt) {
    // this (or event.currentTarget, see below) always refers to the DOM
    // element the listener was attached to - see
    // http://stackoverflow.com/questions/12077859
    if (document.getElementById('gui_panel').style.display === 'none') {
        insert_snippet_code($(evt.currentTarget).data('snippet-code'));
    }
}

function build_menu_element (menu_item_spec, direction) {
    // Create the menu item html element
    let element = $('<li/>').attr('id', 'biocham-commands-menu');

    let a = $('<a/>')
        .attr('href', '#')
        .html(menu_item_spec.name)
        .appendTo(element);

    if (menu_item_spec.hasOwnProperty('snippet')) {
        let snippet = menu_item_spec.snippet;
        if (typeof snippet === 'string' || snippet instanceof String) {
            snippet = [snippet];
        }
        a.attr({
            'title' : "", // Do not remove this, even though it's empty!
            'data-snippet-code' : snippet.join('\n'),
            'container': menu_item_spec.container
        })
            .on('click', callback_insert_snippet)
            .addClass('snippet');
    }

    if (menu_item_spec.hasOwnProperty('children') && menu_item_spec.children.length > 0) {

        element
            .addClass('dropdown-submenu')
            .toggleClass('dropdown-submenu-left', direction === 'right');
        let sub_element = $('<ul class="dropdown-menu"/>')
            .toggleClass('dropdown-menu-compact', menu_item_spec.overlay === true) // For space-saving menus
            .appendTo(element);

        let new_direction = (menu_item_spec['sub_menu_direction'] === 'left') ? 'left' : 'right';
        for (let j=0; j<menu_item_spec.children.length; ++j) {
            let sub_menu_item_spec = build_menu_element(menu_item_spec.children[j], new_direction);
            if(sub_menu_item_spec !== null) {
                sub_menu_item_spec.appendTo(sub_element);
            }
        }
    }

    return element;
}

function menu_setup (menu_item_specs, sibling, insert_before_sibling) {
    for (let i=0; i<menu_item_specs.length; ++i) {
        let menu_item_spec;
        if (insert_before_sibling) {
            menu_item_spec = menu_item_specs[i];
        } else {
            menu_item_spec = menu_item_specs[menu_item_specs.length-1-i];
        }
        let direction = (menu_item_spec['menu_direction'] === 'right') ? 'right' : 'left';
        let menu_element = build_menu_element(menu_item_spec, direction);
        // We need special properties if this item is in the navbar
        if ($(sibling).parent().is('ul.nav.navbar-nav')) {
            menu_element
                .addClass('dropdown')
                .removeClass('dropdown-submenu dropdown-submenu-left');
            menu_element.children('a')
                .addClass('dropdown-toggle')
                .attr({
                    'data-toggle' : 'dropdown',
                    'aria-expanded' : 'false'
                });
        }

        // Insert the menu element into DOM
        menu_element[insert_before_sibling ? 'insertBefore': 'insertAfter'](sibling);

        // Make sure MathJax will typeset this menu
        window.MathJax.Hub.Queue(["Typeset", window.MathJax.Hub, menu_element[0]]);
    }
}

export default function loadMenu () {
    // Arrange the menus as given by the configuration
    Jupyter.notebook.config.loaded.then(
        config_loaded_callback
    ).then(function () {
        // Parse and insert the menu items
        menu_setup(options.menus, options.sibling, cfg.insert_before_sibling);
    });
}
