import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { modelSectionsWithCruds } from 'Utils/sections';
import { createComm } from "Utils/comms";
import Section from 'Components/Section';
import BTable from 'Components/widgets/Table';
import { dashingLowercase } from 'Utils/utils';
import { workflows } from 'Utils/sections';

const styles = theme => ({
    section: {
        marginTop: '10px',
        marginBottom: '10px',
        boxShadow: '0px 1px 5px 3px rgba(0, 0, 0, 0.2), \
0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)'
    },
    title: {
        paddingTop: '10px',
        marginLeft: '6px',
    },
    select: {
        margin: '10px',
    }
});

class Model extends Component {
    // modelSectionsWithCruds is actually the model from the config file
    // where each section is populated with crud props (read, create and
    // del commands)
    static defaultProps = {
        modelSections: modelSectionsWithCruds,
        workflows: workflows
    }

    state = {
        selected: 'Differential'
    }

    handleChange = (e) => {
        this.setState({selected: e.target.value});
    }
    
    generateOutputs = (section) => {
        const { handleKernelResponse, kernelTarget,
                executionCount, autoNumSim } = this.props;
        const { display } = section;
        let listCommands = [];

        display.crud.read && listCommands.push(display.crud);
        if (display.lists) {
            display.lists.map(cmd => listCommands.push(cmd));
        }
        
        return (
            listCommands.map((table, index) => {
                return (
                    <BTable key={index}
                            content={table}
                            kernelTarget={kernelTarget}
                            executionCount={executionCount}
                            autoNumSim={autoNumSim}
                            handleKernelResponse={handleKernelResponse}/>
                );
            })
        );
    }

    generateWorkflows = () => {
        const { classes, handleKernelResponse, options } = this.props;
        const { selected } = this.state;
        const content = workflows.find(w => w.name === selected).commands;
       
        return (
            <Paper className={classes.section}
                   id="workflows">
              <Typography variant="h4"
                          className={classes.title}>
                Workflows
              </Typography>
              <select className={classes.select}
                      name="workflow-list"
                      value={selected}
                      onChange={this.handleChange}>
                {workflows.map(w => {
                    return (
                        <option key={w.name.toLowerCase()}
                                value={w.name}>{dashingLowercase(w.name)}</option>
                    );
                })}
              </select>
              <Section content={content}
                       options={options}
                       handleKernelResponse={handleKernelResponse}
                       main/>
            </Paper>
        );
    }

    render () {
        const { classes, modelSections,
                handleKernelResponse, toggleWorkflows, options } = this.props;
        const sectionsLen = modelSections.length;

        return (
            <div>
              {toggleWorkflows && this.generateWorkflows()}
              {modelSections.map((section, index) => {
                  return (
                      <Paper key={`model-section-${index}`}
                             id={dashingLowercase(section.name)}
                             className={classes.section}>
                        <Typography variant="h4"
                                    className={classes.title}>
                          {section.name}
                        </Typography>
                        {this.generateOutputs(section)}
                        <Section content={section.children}
                                 key={index}
                                 options={options}
                                 handleKernelResponse={handleKernelResponse}/>
                      </Paper>
                  );
              })}
            </div>
        );
    }
}

export default withStyles(styles)(Model);
