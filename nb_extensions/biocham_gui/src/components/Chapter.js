import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Section from 'Components/Section';

const styles = theme => ({
    root: {
    },
});

class Chapter extends Component {
    
    render() {
        const { classes, content, options,
                handleKernelResponse, appendResults } = this.props;
    
        return (
            <div>
              {content.map((section, index) => {
                  return (
                      section.children.length > 0 &&
                          <Section key={index}
                                   content={section.children}
                                   title={section.name}
                                   options={options}
                                   handleKernelResponse={handleKernelResponse}
                                   appendResults={appendResults} main/>
                  );
              })
              }
            </div>
        );
    }
}

export default withStyles(styles)(Chapter);
