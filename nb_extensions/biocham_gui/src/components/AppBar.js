import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';

const drawerWidth = '160px';

const styles = theme => ({
    root: {
        top: 'auto !important',
        height: '40px',
        zIndex: 0,
        backgroundColor: '#fff',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        boxShadow: 'none',
        borderBottom: '1px solid #e7e7e7'
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth})`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    appBar: {
        minHeight: 'inherit',
        display: 'flex',
        flex: '1 1 auto',
        alignItems: 'flex-start'
    },
    input: {
        color: 'black',
        alignSelf: 'center'
    },
    checkbox: {
        paddingLeft: '12px',
    },
    hide: {
        display: 'none'
    },
    button: {
        float: 'right',
        textTransform: 'capitalize',
        margin: theme.spacing.unit
    }
});


class BAppBar extends Component {

    state = {
        value: '',
        numsim: true,
        workflows: true
    }

    componentDidUpdate(prevProps, prevState) {
        const { numsim, workflows } = this.state;
        if (prevState.numsim !== numsim) {
            this.props.handleState('numsim', numsim);
        } else if (prevState.workflows !== workflows) {
            this.props.handleState('workflows', workflows);
        }
    }
    
    handleChange = (e) => {
        this.setState({value: e.target.value});
    }
    
    handleDrawerOpen = () => {
        this.props.handleDrawerOpen();
    }

    handleCheck = (e) => {
        this.setState({[e.target.name]: e.target.checked});
    }

    toggleHeader = () => {
        const header = document.getElementById('header');

        if (header.style.display === 'none') {
            header.style.display = 'block';
        } else {
            header.style.display = 'none';
        }
    }
    
    render() {
        const { classes, open } = this.props;
        const { value, numsim, workflows } = this.state;
        
        return (
            <AppBar className={classNames(classes.root, {
                [classes.appBarShift]: open,
            })}>
              <Toolbar className={classes.appBar}>
                <IconButton className={open ? classes.hide: ''}
                            aria-label="Open menu"
                            onClick={this.handleDrawerOpen}>
                  <MenuIcon/>
                </IconButton>
                <FormControlLabel
                  className={classes.checkbox}
                  control={
                      <Checkbox checked={numsim}
                                name="numsim"
                                onChange={this.handleCheck}
                                value="numsim"/>
                  }
                  label="Automatic numerical_simulation"/>
                <FormControlLabel
                  className={classes.checkbox}
                  control={
                      <Checkbox checked={workflows}
                                name="workflows"
                                onChange={this.handleCheck}
                                value="toggleWorkflows"/>
                  }
                  label="Show workflows"/>
              </Toolbar>
            </AppBar>
        );
    }
}

export default withStyles(styles)(BAppBar);
