import React, { Component, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
    inputlist: {
        display: 'flex',
        flexDirection: 'column'
    },
    input: {
        display: 'flex',
        flexDirection: 'row'
    }
});

function Settings(props) {

    const { classes, options, handleChange } = props;
    const [numSim, setNumSim] = useState(true);

    useEffect(() => {
        if (props.numSim !== numSim) {
            handleChange('numSim', numSim);
        }
    });
    
    return (
        <div>
          <FormControlLabel control={
              <Checkbox checked={numSim}
                        name="numsim"
                        onChange={() => setNumSim(!numSim)}
                        value="numsim"/>
          }
                            label="Automatic numerical simulation"/>

          <div className={classes.inputlist}>
            {options.map(option => {
                return (
                    <div key={option.name}
                         className={classes.input}>
                      <div>{option.name}</div><input defaultValue={option.value}/>
                    </div>
                );
            })}
          </div>
        </div>
    );
    
}

export default withStyles(styles)(Settings);
