import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Close';

import { TabContainer } from './OutputTabs';
import Plot from './widgets/Plot';
import LatexDiv from './widgets/LatexDiv';
import { dashingLowercase } from 'Utils/utils';

const appbarHeight = '30px';

const styles = ({
    root: {
        overflowY: 'auto',
        position: 'absolute',
        display: 'flex',
        justifyContent: 'stretch',
        width: '100%',
        height: '100%',
        flexDirection: 'column'
    },
    img: {
        margin: '20px'
    },
    header: {
        zIndex: 1,
        height: appbarHeight,
        minHeight: appbarHeight
    },
    tab: {
        display: 'flex',
        minHeight: appbarHeight,
        height: appbarHeight,
        minWidth: '20px',
        justifyContent: 'flex-start'
    },
    tabContainer: {
        flex: 1,
        position: 'relative'
    },
    hidden: {
        display: 'none'
    },
    icon: {
        height: '20px',
        width: '20px',
        padding: 0
    },
    label: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        textTransform: 'capitalize',
        height: appbarHeight,
        zIndex: 100
    }
});

class GraphicalTab extends Component {
    state = {
        activeTab: 0
    }

    componentDidUpdate(prevProps, prevState) {
        const { outputs } = this.props;
        
        if (prevProps.outputs.length < outputs.length) {
            this.setState({ activeTab: outputs.length - 1 });
        }
        else if (prevProps.outputs.length > outputs.length) {
            if (prevState.activeTab >= outputs.length) {
                const tab = outputs.length > 0 ? outputs.length-1: 0;
                this.setState({activeTab: tab});
            }
            else {
                const tab = prevState.activeTab === 0 ? 0: prevState.activeTab - 1;
                this.setState({activeTab: prevState.activeTab});
            }
        }
    }
    
    handleChange = (event, value) => {
        this.setState({ activeTab: value });
    }

    handleDelete = (value) => {
        this.props.handleDelete(value);
    }
    
    generateOutput = (obj) => {
        const { classes } = this.props;
        if (!obj) return null;
        if (obj.type === 'plot') {
            return (
                <Plot plot={obj}/>
            );
        }
        else if (obj.type === 'image') {
            if (obj.image_type === 'text/latex') {
                return (
                    <LatexDiv latex={obj}/>
                );
            } else {
                const source = `data:${obj.image_type};base64, ${obj.image_data}`;
                return (
                    <div>
                      <img src={source}
                           className={classes.img}/>
                    </div>
                );
            }
        }
        return null;
    }

    render() {
        const { classes, outputs, current } = this.props;
        const { activeTab } = this.state;
        const hasOutputs = outputs.length > 0;
             
        return (
            hasOutputs ?
            (
                <div className={classes.root}>
                  <AppBar position="static" color="default" className={classes.header}>
                    <Tabs className={classes.header}
                          value={activeTab}
                          onChange={this.handleChange}
                          textcolor="primary"
                          indicatorColor="primary"
                          fullWidth>
                      {outputs.map((tab, index) =>
                                   <Tab key={`tab-${index}`}
                                        component='div'
                                        className={classes.tab}
                                        disableRipple
                                        icon={<div className={classes.label}>
                                                <div>{tab.type}</div>
                                                <IconButton size='small'
                                                            className={classes.icon}
                                                            onClick={() => this.handleDelete(index)}>                                                  <Close className={classes.icon}/>
                                                </IconButton>
                                              </div>}/>)}
                    </Tabs>
                  </AppBar>
                  <TabContainer className={classes.tabContainer}>
                    {this.generateOutput(outputs[activeTab])}
                  </TabContainer>
                </div>) :
            (<div></div>)
        );
    }
}

export default withStyles(styles)(GraphicalTab);
