import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import { dashingLowercase } from 'Utils/utils';


const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    menu: {
        marginRight: '12px'
    },
    item: {
        paddingLeft: '10px',
        paddingRight: '0'
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
        paddingTop: '6px',
        paddingBottom: '6px',
        fontSize: '12px'
    },
    drawer: {
        height: '100%',
        width: '160px',
        flexShrink: 0,
    },
    drawerPaper: {
        width: '160px',
        zIndex: 99,
        position: 'relative',
        transform: 'none !important'
    },
    drawerHeader: {
        display: 'flex',
        minHeight: '39px',
        height: '39px',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
});

class NavMenu extends Component {

    state = {
        open: {
            model: true
        }
    }

    componentDidMount() {
        const { sections } = this.props;
        const lowerDashTitles = sections.map(chapter => dashingLowercase(chapter.name));
        lowerDashTitles.map(title => {
            this.setState(prevState => ({
                open: {...prevState.open, [title]: false}
            }));
        });
    }

    handleDrawerClose = () => {
        this.props.handleDrawerClose();
    }
    
    handleClick = (id) => {
        const keys = Object.keys(this.state.open);
        this.props.handleSelect(id);
        this.setState(prevState => ({
            open: {...prevState.open, [id]: !this.state.open[id]}
        }));
        keys.map(k => {
            if (this.state.open[k] && k !== id)
            this.setState(prevState => ({
                open: {...prevState.open, [k]: false}
            }));
        });
    }

    scrollIntoComponent = (id) => {
        const elem = document.getElementById(id);
        elem.scrollIntoView();
    }

    generateSectionItems = (section) => {
        /**
         * Here we generate only the items
         */
        const { classes } = this.props;

        return section.map((section, index) => {
            return (
                <ListItem button
                          className={classes.nested}
                          key={`section-menu-${index}`}
                          onClick={() => this.scrollIntoComponent(dashingLowercase(section.name))}>
                  <ListItemText primary={section.name}
                                disableTypography/>
                </ListItem>
            );
        });
    }

    generateMenu = () => {
        const { classes, sections } = this.props;
        const { open } = this.state;
        
        return sections.map((chapter, index) => {
            const title = dashingLowercase(chapter.name);
            return (
                <div key={`chapter-menu-${index}`}>
                  <ListItem id={title}
                            button
                            className={classes.item}
                            onClick={() => this.handleClick(title)} >
                    <ListItemText primary={chapter.name}
                                  disableTypography/>
                    {open[title] ? <ExpandLess/> : <ExpandMore/>}
                  </ListItem>
                  <Collapse in={open[title]}
                            unmountOnExit>
                    <List component="div"
                          disablePadding>
                      {this.generateSectionItems(chapter.children)}
                    </List>
                  </Collapse>
                </div>
            );
        });
    }

    render() {
        const { classes, modelSections, menuOpen, workflows } = this.props;
        const { open } = this.state;

        return (
            <Drawer className={classes.drawer}
                    variant="persistent"
                    anchor="left"
                    open={menuOpen}
                    classes={{
                        paper: classes.drawerPaper,
                    }}>
              <List component="nav"
                    disablePadding>
                <ListItem id="model"
                          button
                          className={classes.item}
                          onClick={() => this.handleClick('model')}>
                  <ListItemText primary="Model"
                                disableTypography/>
                  {open.model ? <ExpandLess/> : <ExpandMore/>}
                </ListItem>
                <Collapse in={open.model}
                          unmountOnExit>
                  <List component="div"
                        disablePadding>
                    {workflows &&
                     <ListItem button
                               className={classes.nested}
                               onClick={() => this.scrollIntoComponent(dashingLowercase('workflows'))}>
                       <ListItemText primary={'Workflows'}
                                     disableTypography/>
                     </ListItem>
                    }
                    {this.generateSectionItems(modelSections)}
                  </List>
                </Collapse>
                {this.generateMenu()}
                <ListItem id="settings"
                          button
                          className={classes.item}
                          onClick={() => this.handleClick('settings')}>
                  <ListItemText primary="Settings"
                                disableTypography/>
                </ListItem>
              </List>
            </Drawer>
        );
    }
}

export default withStyles(styles)(NavMenu);
