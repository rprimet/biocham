import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import { addToNotebook } from 'Utils/comms';
import { listCommands } from 'Utils/sections';

const styles = theme => ({
    root: {
        display: 'flex',
        position: 'absolute',
        whiteSpace: 'pre-line',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        width: '100%',
        height: '100%',
        padding: '10px',
        overflowY: 'auto'
    },
    outputs: {
        width: '100%'
    },
    input: {
        paddingTop: '10px'
    }
});

class Console extends Component {

    state = {
        value: '',
        cmdHistory: [],
        current: 0
    }

    componentDidUpdate(prevProps) {
        if (prevProps.output !== this.props.output) {
            const elem = document.getElementById('bc-console');
            elem.scrollTop = elem.scrollHeight;
        }
    }

    handleChange = (e) => {
        this.setState({value: e.target.value});
    }
    
    handleKeyUp = (e) => {
        const { handleKernelResponse } = this.props;
        const { value } = this.state;
        
        if (e.keyCode === 13) {
            // if enter
            const code = value;
            addToNotebook(code);
            this.setState({value: ''});
        }
    }
    
    render() {
        const { classes, output } = this.props;
        const { value } = this.state;

        return (
            <div className={classes.root}
                 id="bc-console">
              <div className={classes.outputs}>
                {output}
              </div>
              <InputBase className={classes.input}
                         value={value}
                         type="text"
                         onChange={this.handleChange}
                         onKeyUp={this.handleKeyUp}
                         fullWidth
                         autoFocus
                         startAdornment={
                             <InputAdornment position="start">
                               {'>'}
                             </InputAdornment>
                         }
              />
            </div>
        );
    }
}

export default withStyles(styles)(Console);
