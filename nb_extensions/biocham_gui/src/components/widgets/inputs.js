import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Slider from '@material-ui/lab/Slider';
import Tooltip from '@material-ui/core/Tooltip';
import Autocomplete from 'react-autocomplete';

import { createComm,
         handleResult,
         addToNotebook } from 'Utils/comms';
import { format } from 'Utils/formats';

/**
 * Inputs css
 */
const styles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: '6px'
    },
    input: {
        minWidth: '50px',
        textAlign: 'center'
    },
});

function Title(props) {
    if (props.doc) {
        return (
            <Tooltip title={props.doc}
                     placement="top-start">
              <div>
                {props.name}:
              </div></Tooltip>
        );
    }
    return (<div>{props.name}:</div>);
}

class TextInput extends Component {
    state = {
        value: ''
    }

    componentDidMount() {
        const { content, name } = this.props;
        this.setState({ value: content.value });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if (prevState.value !== this.state.value) {
            this.props.handleChange(this.props.name,
                                    this.state.value,
                                    this.props.type);
        }
    }

    handleChange = (e) => {
        this.setState({value: e.target.value});
    }

    render () {
        const { classes, content, name } = this.props;
        const { value } = this.state;

        return (
            <div className={classes.root}>
              <Title doc={content.doc}
                     name={name}/>
              <input className={classes.input}
                     value={value}
                     type="text"
                     onChange={(e) => this.handleChange(e)}/>
            </div>
        );
    }
}

class NumberInput extends Component {
    state = {
        value: ''
    }

    componentDidMount() {
        const { content } = this.props;
        this.setState({ value: content.value });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if (prevState.value !== this.state.value) {
            this.props.handleChange(this.props.name,
                                    this.state.value,
                                    this.props.type);
        }
    }

    handleChange = (e) => {
        this.setState({value: e.target.value});
    }

    render () {
        const { classes, content, name } = this.props;
        const { value } = this.state;
        
        return (
            <div className={classes.root}>
              <Title doc={content.doc}
                     name={name}/>
              <input className={classes.input}
                     value={value}
                     type="number"
                     onChange={(e) => this.handleChange(e)}/>
            </div>
        );
    }
}

class CheckboxInput extends Component {
    state = {
        checkboxChecked: false,
    }

    componentDidMount() {
        const { content } = this.props;
        content.value === 'yes' && this.setState({ checkboxChecked: true });
    }
    
    componentDidUpdate(prevProps, prevState) {
        const { name, handleChange, type } = this.props;
        
        if (prevState.checkboxChecked !== this.state.checkboxChecked) {
            // biocham syntax for yes/no
            const checked = this.state.checkboxChecked ? 'yes' : 'no';
            this.props.handleChange(name, checked, type);
        }
    }

    handleChange = (e) => {
        this.setState({checkboxChecked: e.target.checked});
    }

    render() {
        const { name, content } = this.props;
        
        return(
            <FormControlLabel
              control={
                  <Checkbox
                    checked={this.state.checkboxChecked}
                    onChange={this.handleChange}
                    value="checkboxChecked"/>
              }
              label={
                  <Title doc={content.doc}
                         name={name}/>}
            />
        );
    }
}


/**
*/
class FileInput extends Component {

    state = {
        file: ''
    }

    componentDidUpdate(prevProps, prevState) {
        const { file } = this.state;
        if (prevState.file !== file && file != null) {
            this.handleFile(file);
            this.props.handleChange(this.props.name,
                                    file.name,
                                    this.props.type);
        } 
    }

    handleChange = (e) => {
        this.setState({file: e});
    }

    /**
     * This
     */
    handleFile = (file) => {
        const { kernelTarget } = this.props;
        const reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = (evt) => {
            const comm = createComm(kernelTarget);
            comm.send({file: file.name, content: evt.target.result});
            comm.on_msg((msg) => {
                if (!msg.content.data.saved) {
                    alert(`File didn't load`);
                }
            });
        };
        reader.onerror = () => {
            alert(`There was an error while loading the file : 
            ${reader.error}`);
        };
    }

    render() {
        const { classes, content, name } = this.props;
        
        return (
            <div className={classes.root}>
              <Title doc={content.doc}
                     name={name}/>
              <input type="file"
                     id={this.props.id}
                     onChange={(e) => this.handleChange(e.target.files[0])}/>
            </div>
        );
    }
}


/**
 * Slider/input css 
 */
const sliderStyles = (theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
        justifyContent: 'space-between'
    },
    dotInput: {
        textAlign: 'center',
        marginLeft: '4px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        minWidth: '72px'
    },
    slider: {
        marginLeft: '6px',
        marginRight: '12px',
        flex: '1 1 148px'
    }
});


/**
 * A widget which renders a slider with a text input next to it.
 * The widget only stores its value at all times, all its' other 
 * properties are stored in the Table component (Table.js). TODO
 * write explanation why
 */
class SliderInput extends Component {
    /**
     * Slider state 
     */
    state = {
        value: 0
    }

    /**
     * Set state value with props on component mount.
     */
    componentDidMount() {
        const { value } = this.props;
        this.setState({ value });
    }

    /**
     * Controlled changes in value 
     */
    componentDidUpdate(prevProps) {
        const { value } = this.props;
        if (prevProps.value !== value) {
            this.setState({ value });
        }
    }
    
    /**
     * Handle text input value changes.
     */
    handleInputChange = (event) => {
        this.setState({value: event.target.value});
    };


    /**
     * Handle slider value changes. The value is rounded to
     * a computed precision passed through props to avoid 
     * floating point problems.
     */
    handleSlider = (event, value) => {
        const { precision } = this.props;
        const rounded = this.round(value, precision);
        this.setState({value: rounded });
    }

    /**
     * Cast value as a number, round it to a precision if needed.
     */
    round = (value, precision) => {
        return precision > 0 ?
            parseFloat(value).toFixed(precision) : parseInt(value);
    }

    /**
     * If event is either key pressed Ret or mouse up, add change to 
     * notebook.
     */
    updateChange = (e) => {
        const { kernelTarget, crud, name, autoNumSim } = this.props;
        const { value } = this.state;
        
        // TODO deuglify this cause it sucks
        if (e.keyCode === 13 || (e.type === 'mouseup' && !isNaN(value))) {
            if (crud.create && crud.commandSep && !isNaN(value)) {
                const create = crud.create.name;
                const replot = 'numerical_simulation. plot.';
                // avoid linebreak with template literals with \ at the end
                const code = `${create}(${name}${crud.commandSep}${value}). \
${autoNumSim? replot: ''}`;
                addToNotebook(code);
            }
        }
    }

    /**
     * Render component.
     */
    render() {
        const { classes, min, max, step, precision  } = this.props;
        const { value } = this.state;
        
        return (
            <div className={classes.root}>
              <Slider className={classes.slider}
                      value={value}
                      onChange={this.handleSlider}
                      onDragEnd={this.updateChange}
                      min={ min<max ? min : max }
                      max={max>min ? max : min}
                      step={step}
                      disabled={isNaN(value)}/>
              <input className={classes.dotInput}
                     value={value}
                     onChange={this.handleInputChange}
                     onKeyUp={this.updateChange}/>
            </div>
        );
    }
}


/**
 * Input with list of completions component. 
 */
class AutocompleteInput extends Component {
    static defaultProps = {
        menuStyle: {
            fontFamily: 'monospace',
            fontSize: '110%',
            borderRadius: '3px',
            boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
            background: 'rgba(255, 255, 255, 0.9)',
            padding: '2px 0',
            position: 'fixed',
            overflow: 'auto',
            zIndex: 110,
            lineHeight: '1em',
            maxHeight: '10em'
        }
    }
    
    state = {
        completions: [],
        value: ''
    }

    componentDidMount() {
        const { content } = this.props;
        this.setState({ value: content.value });
    }
    
    componentDidUpdate(prevProps, prevState) {
        const { name, type } = this.props;
        const { value } = this.state;
        if (prevState.value !== value) {
            this.props.handleChange(name, value, type);
        }
    }

    onFocus = () => {
        const { kernelTarget, completionCommand } = this.props;
        const comm = createComm('from_gui');
        comm.send({code: `${completionCommand}.`, silent: true});
        comm.on_msg(msg => {
            const completions = format({str: msg.content.data.output,
                                        type: completionCommand,
                                        display: true});
            this.setState({ completions });
        });
    }

    onChange = (e) => {
        this.setState({ value: e.target.value });
    }

    onSelect = (value) => {
        this.setState({ value });
    }
    
    render() {
        const { classes, content, menuStyle, name } = this.props;
        const { completions, value } = this.state;
        
        return (
            <div className={classes.root}>
              <Title doc={content.doc}
                     name={name}/>
              <Autocomplete
                menuStyle={menuStyle}
                getItemValue={(item) => item}
                items={completions}
                renderItem={(item, isHighlighted) =>
                            <div style={{background: isHighlighted?
                                         'lightgray': 'white'}}>
                              {item}
                            </div>
                           }
                shouldItemRender={(item, value) =>
                                  item.toLowerCase()
                                  .indexOf(value.toLowerCase()) > -1}
                value={value}
                inputProps={{onFocus: this.onFocus}}
                onChange={this.onChange}
                onSelect={(val) => this.onSelect(val)}
              />
            </div>
        );
    }
}

/**
 * Add styles to components.
 */
TextInput = withStyles(styles)(TextInput);
NumberInput = withStyles(styles)(NumberInput);
FileInput = withStyles(styles)(FileInput);
AutocompleteInput = withStyles(styles)(AutocompleteInput);
SliderInput = withStyles(sliderStyles)(SliderInput);

/**
 * Classes exports.
 */
export { TextInput,
         NumberInput,
         FileInput,
         CheckboxInput,
         SliderInput,
         AutocompleteInput
       };

