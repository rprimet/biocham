import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import { createComm } from 'Utils/comms';
import { dashingLowercase } from 'Utils/utils';
import { formatArgs } from 'Utils/formats';
import { addToNotebook } from 'Utils/comms';
import { BIOCHAM_TYPE_WIDGETS } from 'Config/config';
import { TextInput } from './inputs';


const inputWrapperStyles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column'
    }
});

function CommandInputs(props) {
    const { classes, content, handleChange, type } = props;
    return (
        <div className={classes.root}>
          { Object.keys(content).map((arg, index) => {
              const widget = BIOCHAM_TYPE_WIDGETS[content[arg].type];
              const isFunction = typeof widget === 'function';
              const TypedInput = widget ? isFunction ?
                    widget: widget.type: TextInput;
              const completionCommand = widget && !isFunction ? widget.complete: '';
            
              return (
                  <TypedInput key={`input-${arg}-${index}`}
                              name={arg}
                              content={content[arg]}
                              handleChange={handleChange}
                              type={type}
                              completionCommand={completionCommand}/>
              );
          })}
        </div>
    );
}

CommandInputs = withStyles(inputWrapperStyles)(CommandInputs);


const styles = theme => ({
    root: {
        paddingTop: '10px',
        paddingBottom: '10px',
        paddingLeft: '15px',
        paddingRight: '15px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        fontFamily: 'monospace'
    },
    title: {
        fontWeight: 'bold',
        marginRight: '30px',
    },
    mainWrapper: {
        display: 'flex',
        flexFlow: 'row wrap',
        width: '100%',
        flex: '1 1 0',
        paddingRight: '20px'
    },
    argWrapper: {
        display: 'flex',
        flexFlow: 'row wrap',
        flex: '1 1 0',
        justifyContent: 'space-evenly',
    },
    args: {
        flex : '1 1 50%',
        paddingRight: '15px'
    },
    span: {
        fontWeight: 350,
        color: 'grey'
    },
    button: {
        fontSize: '11px',
        backgroundColor: 'lightgrey',
    }
});


class Command extends Component {

    state = {
        options: {},
        args: {}
    }

    componentDidMount() {
        const { command, options } = this.props;
        command.arguments.map(arg => {
            this.setState(prevState => ({
                args: {
                    ...prevState.args,
                    [arg.name]: {
                        value: '',
                        type: arg.type
                    }
                }
            }));
        });

        options.length > 0 && this.fillOptions();
    }

    componentDidUpdate(prevProps) {
        const { command, options } = this.props;
        if (prevProps.options !== options) {
            this.fillOptions();
        }
    }
    
    fillOptions = () => {
        const { command, options } = this.props;
        command.options.map(opt => {
            this.setState(prevState => ({
                options: {
                    ...prevState.options,
                    [opt.name]: {
                        value: options.find(o => o.name === opt.name).value,
                        type: opt.type,
                        doc: opt.doc
                    }
                }
            }));
        });
    }
    
    handleKernelResponse = (e) => {
        const { command } = this.props;
        const { args, options } = this.state;
        const code = `${command.name}${formatArgs(args, options)}.`;
        addToNotebook(code);
    }

    handleChange = (argName, value, type) => {
        this.setState(prevState => ({
            [type]: {
                ...prevState[type],
                [argName]: {
                    ...prevState[type][argName],
                    value: value
                }            
            }
        }));
    }

    render () {
        const { classes, command } = this.props;
        const { options, args } = this.state;
        const { name, doc } = command;

        const hasArgs = Object.keys(args).length > 0;
        const hasOptions = Object.keys(options).length > 0;
        
        return (
            <div className={classes.root}
                 id={name}>
              <div className={classes.mainWrapper}>
                <Tooltip title={doc}
                         placement="top-start">
                  <div className={classes.title}>
                    {name}
                  </div>
                </Tooltip>
                <div className={classes.argWrapper}>
                  { hasArgs &&
                    <div className={classes.args}>
                      <div className={classes.span}>Args</div>
                      <CommandInputs content={args}
                                     handleChange={this.handleChange}
                                     type="args"/>
                    </div>
                  }
                  { hasOptions &&
                    <div className={classes.args}>
                      <div className={classes.span}>Options</div>
                      <CommandInputs content={options}
                                     handleChange={this.handleChange}
                                     type="options"/>
                    </div>
                  }
                </div>
              </div>
              <Button className={classes.button}
                      onClick={this.handleKernelResponse}>Run</Button>
            </div>
        );
    }
}

export default withStyles(styles)(Command);
