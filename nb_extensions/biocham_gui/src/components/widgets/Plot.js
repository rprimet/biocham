import React, { Component, createRef } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = ({
    root: {
        overflowY: 'auto',
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});


class Plot extends Component {

    ref = createRef();
        
    componentDidMount() {
        this.handlePlot();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.plot.div !== this.props.plot.div) this.handlePlot();
    }

    handlePlot = () => {
        const { plot } = this.props;

        this.ref.current.innerHTML = '';

        if (plot.script && plot.div) {
            const range = document.createRange();
            range.setStart(this.ref.current, 0);
            this.ref.current.appendChild(
                range.createContextualFragment(plot.div + plot.script));
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root} ref={this.ref}>
            </div>
        );
    }
}

export default withStyles(styles)(Plot);
