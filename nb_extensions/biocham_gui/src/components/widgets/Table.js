import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Add from '@material-ui/icons/Add';
import Clear from '@material-ui/icons/Clear';
import { NumberInput, SliderInput, TextInput } from 'Components/widgets/inputs';
import { createComm, addToNotebook } from 'Utils/comms';
import { cleanEmpty } from 'Utils/sections';
import { step, precision, round } from 'Utils/utils';
import { format } from 'Utils/formats';


const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit
    },
    title: {
        flex: '0 0 auto',
        fontWeight: 600
    }
});

let TableToolbar = props => {
    const { classes, title } = props;
    
    return (
        <Toolbar className={classes.root}>
          <div className={classes.title}>
            <Typography variant="h5">
              {title}
            </Typography>
          </div>
        </Toolbar>
    );
};

TableToolbar = withStyles(toolbarStyles)(TableToolbar);


const headerStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit
    },
    headerCell: {
        fontSize: '13px'
    },
});


/**
 * Table Header component
 */
let TableHeader = props => {
    const { classes, split, title } = props;
    // format the title
    // e.g. : list_initial_state -> Initial State
    const cleanTitle = title.replace('list', '')
          .replace(/_/g, ' ')
          .split(' ')
          .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
          .join(' ');
    
    return (
        <TableHead className={classes.root}>
          <TableRow>
            <TableCell>
              {cleanTitle}
            </TableCell>
            {split && 
             <TableCell>
               Value
             </TableCell>
            }
          </TableRow>
        </TableHead>
    );
};

TableHeader = withStyles(headerStyles)(TableHeader);


const rowStyles = {
    row: {
        height: '36px',
        fontFamily: 'monospace'
    },
    inputRow: {
        height: '50px',
    },
    form: {
        display: 'flex',
        flexDirection: 'row'
    },
    input: {
        paddingTop: '10px'
    },
    delete: {
        height: '20px'
    },
    button: {
        backgroundColor: 'lightgrey',
        fontSize: '11px',
        marginLeft: '10px'
    }
};

class BTableRow extends Component {
    
    handleDelete = () => {
        const { content, output } = this.props;
        const code = `${content.del.name}(${output.name}).`;
        addToNotebook(code);
    }

    render() {
        const { classes, output, content, kernelTarget,
                handleKernelResponse, autoNumSim } = this.props;
        
        if (typeof output === 'object') {
            return (
                <TableRow className={classes.row}>
                  <TableCell>
                    {content.del &&
                     <IconButton onClick={this.handleDelete}>
                       <Clear className={classes.delete}/>
                     </IconButton>}
                    {output.name}
                  </TableCell>
                  <TableCell>
                    <SliderInput value={output.value}
                                 name={output.name}
                                 disabled={true}
                                 crud={content}
                                 kernelTarget={kernelTarget}
                                 handleChange={this.props.handleChange}
                                 min={output.min}
                                 max={output.max}
                                 step={output.step}
                                 precision={output.precision}
                                 handleKernelResponse={handleKernelResponse}
                                 autoNumSim={autoNumSim}/>
                  </TableCell>
                </TableRow>
            );
        } else {
            return (
                <TableRow key={`item-${output}`}
                            className={classes.row}>
                    <TableCell>
                      {output}
                    </TableCell>
                </TableRow>
            );
        }
    }
}

BTableRow = withStyles(rowStyles)(BTableRow);


const footerStyles = {
    toolbar: {
        fontSize: '11px'
    },
    caption: {
        fontSize: '11px'
    }
};

let BTableFooter = props => {
    const { classes,
            rowsPerPage,
            page,
            rowsPerPageOptions,
            count,
            handleChangePage,
            handleChangeRowsPerPage } = props;

    return (
        <TableFooter>
          <TableRow>
            <TablePagination classes={classes}
                             count={count}
                             rowsPerPage={rowsPerPage}
                             rowsPerPageOptions={rowsPerPageOptions}
                             page={page}
                             backIconButtonProps={{
                                 'aria-label': 'Previous Page',
                             }}
                             nextIconButtonProps={{
                                 'aria-label': 'Next Page',
                             }}
                             onChangePage={handleChangePage}
                             onChangeRowsPerPage={handleChangeRowsPerPage}/>
          </TableRow>
        </TableFooter>
    );
};

BTableFooter = withStyles(footerStyles)(BTableFooter);


class TableInput extends Component {
    state = {
        param: '',
        value: ''
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }
    
    handleClick = event => {
        const { add, sep, kernelTarget, handleKernelResponse } = this.props;
        const { param, value } = this.state;

        // TODO redo add
        const code = formatCode(add, {args: `${param}${ value && sep + value}`});
        addToNotebook(code);
        this.setState({param: '', value: ''});
    }

    render() {
        const { classes, split } = this.props;
        const { param, value } = this.state;
        
        return (
            <TableRow className={classes.inputRow}>
              <TableCell>
                <FormControl className={classes.form}>
                  <Input className={classes.input}
                         type='text'
                         placeholder=''
                         name='param'
                         onChange={this.handleChange}
                         value={param}
                         fullWidth/>
                  {!split &&
                   <Button className={classes.button}
                           onClick={this.handleClick}>
                     Add
                   </Button>
                  }
                </FormControl>
              </TableCell>
              {split &&
               <TableCell>
                 <FormControl className={classes.form}>
                   <Input className={classes.input}
                          type="text"
                          placeholder="value"
                          name='value'
                          onChange={this.handleChange}
                          value={value}
                          fullWidth/>
                   <Button className={classes.button}
                           onClick={this.handleClick}>
                     Add
                   </Button>
                 </FormControl>
               </TableCell>
              }
            </TableRow>
        );
    }
}

TableInput = withStyles(rowStyles)(TableInput);


const styles = theme => ({
    root: {
        marginTop: '10px',
        marginBottom: '10px',
        overflowX: 'auto',
        marginLeft: '6px',
        marginRight: '6px',
        boxShadow: '0px 1px 5px 3px rgba(0, 0, 0, 0.2), \
0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)'
    },
    row: {
      height: '38px'
    },
    emptyTable: {
        paddingLeft: '24px',
        paddingBottom: '8px'
    },
});


class BTable extends Component {

    state = {
        output: '',
        page: 0,
        rowsPerPage: 5,
    }

    componentDidMount() {
        this.update();
    }

    componentDidUpdate(prevProps, prevState) {
        const { executionCount } = this.props;
        const { output } = this.state;
        
        if (prevProps.executionCount !== executionCount) {
            this.update();
        }

        /*
        // update table rows number
        const prevLength = prevState.output.length ||
              Object.keys(prevState.output).length;
        const presLength = output.length ||
              Object.keys(output).length;
        
        if (prevState.rowsPerPage > 10 &&
            prevLength !== presLength &&
            presLength > 15) {
            this.setState({rowsPerPage: presLength});
        }
        */
    }
    
    update = () => {
        const { kernelTarget, content } = this.props;
        //if it's a crud table with add and delete the object structure is different 
        const cmd = content.name || content.read.name;
        const comm = createComm(kernelTarget);
        comm.send({code: `${cmd}.`, silent: true});
        comm.on_msg(msg => {
            let output = format({str: msg.content.data.output,
                                 type: cmd});
            this.updateOutput(output);
        });
    }

    /**
     * Update output state of table component based on output
     * types. Either output is return as an array of objects
     * (i.e. [{name: name, value: value}, ...]) or an array of values
     * (i.e. [value1, value2, ...]).
     */
    updateOutput = (output) => {
        const currentOutputs = this.state.output;
        
        if (output.length === 0) {
            this.setState({ output: '' });
        }
        else if (typeof output[0] === 'object'){
            if (currentOutputs === '') {
                this.setOutputWithProps(output);
            }
            else {
                this.setState({ output: output.map(newOutput => {
                    const curr = currentOutputs.find(out =>
                                                     out.name === newOutput.name);
                    return curr ?
                        this.updateProps(curr, newOutput) :
                        this.setProps(newOutput);
                })});
            }
        }
        else {
            this.setState({ output });
        }
    }
    
    setOutputWithProps = (arr) => {
        const proppedOutputs = arr.map(out => {
            return this.setProps(out);
        });
        this.setState({output: proppedOutputs});
    }

    setProps = (obj) => {
        if (obj.value || obj.value === 0) {
            obj.precision = precision(obj.value);
            obj.step = step(obj.value);
            if (obj.value >= 0) {
                obj.min = 0;
                obj.max = round(obj.value*10, obj.precision) || 10;
            }else {
                obj.min = round(obj.value*10, obj.precision) || -10;
                obj.max = 0;
            }
        }
        return obj;
    }

    updateProps = (prevObj, obj) => {
        const _precision = precision(obj.value);
        const shouldUpdate = _precision > prevObj.precision;

        if (prevObj.value > 0 && obj.value === 0) {
            const newPrecision = prevObj.precision + 1;
            const newStep = step(prevObj.value/10);
            return {...prevObj,
                    value: obj.value,
                    precision: newPrecision,
                    step: newStep,
                    max: prevObj.max/10};
        }

        return obj.value >= prevObj.max || obj.value <= prevObj.min || shouldUpdate ?
            this.setProps(obj) : {...prevObj, value: obj.value};
    }
    
    handleChangePage = (event, page) => {
        this.setState({ page });
    }

    handleChangeRowsPerPage = event => {
        //TODO : handle data length if all chosen in table rows
        this.setState({ rowsPerPage: event.target.value });
    }
    
    generateTableBody = (arr) => {
        const { content, handleKernelResponse,
                kernelTarget, autoNumSim } = this.props;
        const { output } = this.state;

        return (
            output.map((item, index) => {
                return (
                    <BTableRow key={`table-row${index}`}
                               output={item}
                               content={content}
                               kernelTarget={kernelTarget}
                               handleKernelResponse={handleKernelResponse}
                               autoNumSim={autoNumSim}
                               handleChange={this.update}/>
                );
            })
        );
    }

    render() {
        const { classes, content, kernelTarget, handleKernelResponse, autoNumSim } = this.props;
        const { read, create, del, commandSep } = content;
        const { output, rowsPerPage, page } = this.state;
        const len = output.length;
        const title = content.name || content.read.name;
        // check if the string should be split as name - value
        const split = !!commandSep;
        
        const rowsPerPageOptions = output ? len > 20 ? [10, 20, len] :
              [10, len] : [5, 10];
        const sep = commandSep ? commandSep : '';
        
        return (
            <Paper className={classes.root}>
              <Table className={classes.table}
                     padding="checkbox">
                <TableHeader split={split}
                             title={title}/>
                <TableBody>
                  {output ?
                   this.generateTableBody(output) :
                   <TableRow>
                     <TableCell>
                       No Entry
                     </TableCell>
                   </TableRow>
                  }
                  {create && <TableInput split={split}
                                         add={create}
                                         sep={sep}
                                         handleKernelResponse={handleKernelResponse}
                                         kernelTarget={kernelTarget}/>
                  }
                </TableBody>
              </Table>
            </Paper>
        );
    }
}

export default withStyles(styles)(BTable);







