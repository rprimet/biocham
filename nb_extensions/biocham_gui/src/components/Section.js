import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Command from 'Components/widgets/Command';
import { dashingLowercase } from 'Utils/utils';

const styles = theme => ({
    root: {
        overflow: 'auto',
        marginTop: '20px',
        marginBottom: '20px',
        boxShadow: '0px 1px 5px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)'
    },
    command: {
        paddingLeft: '8px',
        paddingRight: '8px',
    },
    title: {
        margin: '10px'
    },
    embedded: {
        marginLeft: '10px',
        marginRight: '10px'
    }
});

class Section extends Component {

    render() {
        const { classes, main, title, options,
                content, handleKernelResponse } = this.props;
                
        return (
            <Paper className={main ? classes.root:
                              classNames(classes.root, classes.embedded)}
                   id={title && dashingLowercase(title)}>
              {title &&
               <Typography variant="h4"
                           className={classes.title}>
                 {title}
               </Typography>
              }
              {content.map((command, index) => {
                  return (
                      <div key={`${command.name}-${index}`}
                           className={classes.command}>
                        <Command command={command}
                                 options={options}
                                 handleKernelResponse={handleKernelResponse}/>
                        {content.length !== index + 1 && <Divider/>}
                      </div>
                  );
              })
              }
            </Paper>
        );
    }
}

export default withStyles(styles)(Section);
