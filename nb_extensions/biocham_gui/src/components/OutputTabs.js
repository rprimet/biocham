import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tab, Tabs } from '@material-ui/core';

import GraphicalTab from 'Components/GraphicalTab';
import Console from './widgets/Console';

const styles = theme => ({
    root: {
        display: 'flex',
        alignContent: 'stretch',
        height: '100%',
        flexDirection: 'column',
    },
    tab: {
        flex: 1,
        position: 'relative',
    },
    tabHeader: {
        zIndex: 1,
        boxShadow: 'none',
        borderBottom: '1px solid #e7e7e7'
    },
    hidden: {
        display: 'none'
    }
});

function TabContainer(props) {
    return (
        <div className={props.className}>
          {props.children}
        </div>
    );
}

class OutputTabs extends Component {
    handleChange = (event, value) => {
        this.props.handleTabChange('mainTab', value);
    }

    render() {
        const { classes, graphicalOutputs, results, handleTabChange,
                graphicalTab, mainTab, handleKernelResponse, handleDelete } = this.props;

        return (
            <div className={classes.root}>
              <AppBar position="static" color="default" className={classes.tabHeader}>
                <Tabs  value={mainTab}
                       onChange={this.handleChange}
                       indicatorColor="primary"
                       textcolor="primary"
                       fullWidth>
                  <Tab label="Graphical Outputs"/>
                  <Tab label="Console"/>
                </Tabs>
              </AppBar>
              <TabContainer className={mainTab !== 0 ?
                                       classes.hidden : classes.tab}>
                <GraphicalTab outputs={graphicalOutputs}
                              current={graphicalTab}
                              graphicalOutput={graphicalOutputs[graphicalTab]}
                              handleTabChange={handleTabChange}
                              handleDelete={handleDelete}/>
              </TabContainer>
              <TabContainer className={mainTab !== 1 ?
                                       classes.hidden : classes.tab}>
                <Console output={results}
                         handleKernelResponse={handleKernelResponse}/>
               </TabContainer>
            </div>
        );
    }
}

OutputTabs = withStyles(styles)(OutputTabs);

export { OutputTabs, TabContainer };
