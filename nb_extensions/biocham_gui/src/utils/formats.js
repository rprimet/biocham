/**
 * Format code response from the kernel into an array
 */
export function format({str, type, display=false}) {
    if (type === 'list_reactions') {
        return handleOutputString({str});
    }
    else if (type === 'list_initial_state') {
        return sortOnKey(handleListInitialState(str), 'name');
    }
    else if (type === 'list_parameters') {
        return sortOnKey(handleOutputString({str: str,
                                             sep: '=',
                                             sort: true}), 'name');
    }
    else if (type === 'list_molecules') {
        return handleOutputString({str});
    }
    else if (type === 'list_influences') {
        return handleOutputString({str});
    }
    else if (type === 'list_options') {
        return handleOutputString({str: str,
                                   sep: ':',
                                   display: display,
                                   header: true});
    }
    else if (type === 'list_functions') {
        return handleOutputString({str: str, header: true});
    }
    return handleOutputString({str});
}

function handleOutputString({str, sep='', display=false, header=false, sort=false}) {
    if (str === '') {
        return [];
    }
    const stringArray = str.split(/\r\n/);
    // we replace the line number and get rid of empty strings in the array
    let cleaned = stringArray.map(str => str.replace(/\[\d+]/, '')).filter(x => x);
    // if the first str is 'From inherited initial:'
    if (header) {
        cleaned = cleaned.slice(1, cleaned.length);
    }
    
    // display is the case when we only need to display names
    // for completion purposes
    if (display) {
        return getNames(cleaned, sep);
    }
    
    if (sep && !display) {
        return splitOutputs(cleaned, sep);
    }
    
    return cleaned;
}

function splitOutputs(arr, sep) {
    return arr.map(str => {
        const name = str.match(new RegExp(`(?<=\\()(.*)(?=${sep})`))[0];
        let value = str.match(new RegExp(`(?<=${sep})(.*?)(?=\\))`))[0];
        value = formatValue(value);
        // case where option is an empty set
        return { name: name, value: value };
    });
}

function formatValue(value) {
    if (!value || value === '[{}]') return '';
    else if (isNaN(value)) return value;
    return parseFloat(value);
}

function getNames(arr, sep) {
    return arr.map(str => str.match(new RegExp(`(?<=\\()(.*)(?=${sep})`))[0]);
}

/**
 * This one is defined on its own because it has several conditions
 * and values cannot be handled like for other lists (i.e. when 
 * an object is present without an initial value, or absent)
 */
function handleListInitialState(str) {
    const arr = handleOutputString({str});
    const sep = ',';
   
    return arr.map(string => {
        const command = string.match(/\w+(?=\()/)[0];
        const fullValue = string.match(/(?<=\()(.*)(?=\))/)[0];
        
        if (command === 'present') {
            const split = fullValue.split(sep);
            // if there is no value (default value of 1 in biocham)
            if (split.length === 0) {
                return {name: split, value: 'present'};
            }
            return {name: split[0], value: isNaN(split[1]) ?
                    split[1]: parseFloat(split[1])};
        }

        return {name: fullValue, value: 'absent'};
    });
}

export function formatArgs(args={}, options={}) {
    const argsStr = Object.keys(args).length > 0 ?
          formatInputString(args, 'args') : '';
    const optionsStr = Object.keys(options).length > 0 ?
          formatInputString(options, 'options') : '';
    // format inputs
    return argsStr && optionsStr ? `(${argsStr}, ${optionsStr})` :
          argsStr ? `(${argsStr})` :
          optionsStr ? `(${optionsStr})` : '';
};

function formatInputString(obj, type) {
    //if option, need to have the syntax option: parameter_value
    if (type === 'options') {
        return Object.keys(obj).filter(key => {
            return obj[key].value;
        }).map(key => {
            return formatOptionString(key, obj[key]);
        }).join(', ');
    }
    
    return Object.keys(obj).map(key => obj[key].value).join(', ');
};

function formatOptionString(optionName, obj) {
    let value = obj.value;
    if (isSet(obj.type)) {
        value = `{${obj.value}}`;
    }
    return `${optionName}:${value}`;
}

function isSet(type) {
    return !!type.match(/{(.*?)}/);
}

function sortOnKey(arr, key){
        return arr.sort(function(a, b){
            if(a[key] < b[key]){
                return -1;
            }else if(a[key] > b[key]){
                return 1;
            }
            return 0;
        });
    }
