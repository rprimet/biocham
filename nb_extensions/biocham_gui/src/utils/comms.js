export const getJupyter = () => {
    return window.Jupyter;
};

export const getMathJax = () => {
    return window.MathJax;
};

export const createComm = (target, init={}) => {
    return getJupyter().notebook.kernel.comm_manager.new_comm(target, init);
};

export const sendCodeToKernel = (comm, code, func) => {
    comm.send({code: code});
    comm.on_msg((msg) => {
        func(msg);
        }
    );
};

export const addToNotebook = (code) => {
    const notebook = getJupyter().notebook;
    const ncells = notebook.ncells();
    const lastCell = notebook.get_cell(ncells-1);
    if (lastCell.get_text() === '') {
        makeCell(lastCell, code);
    } else {
        const newCell = notebook.insert_cell_at_bottom('code');
        makeCell(newCell, code);
        // TODO: generate cell output with what is returned by the kernel
    }
};

const makeCell = (cell, code) => {
    cell.set_text(code);
    cell.execute();
};
