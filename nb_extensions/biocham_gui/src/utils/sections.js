import {
    CHAPTERS_TO_IGNORE,
    CRUD_SECTIONS } from 'Config/config';
import { menu } from 'Kernel/cmd_tree';
import { commands_list } from 'Kernel/commands';
import * as wf from 'Config/workflows/workflows';


/**
 * Sort Crud sections and the rest based on the manual.
 * Returns flattened array with crud sections.
 */
export function getCrudSections(manual, crudSections) {
    const crudNames = crudSections.map(section => section.name);

    const crud = manual.flatMap(chapter => {
        if (crudNames.includes(chapter.name)) {
            return chapter;
        }
        return chapter.children.filter(section => {
            return crudNames.includes(section.name);
        });
    });

    // flatten then sort based on config order
    return crud.sort((a, b) => {
        return crudNames.indexOf(a.name) - crudNames.indexOf(b.name);
    });
};

/**
 * Get sections and chapters from the menu that are not
 * contained in the CRUD model, and ignore chapters
 * useless for GUI (cf. config.js CHAPTERS_TO_IGNORE).
 */
export function getOtherSections(manual, crudSections, toIgnore={}) {
     const crudNames = crudSections.map(section => section.name);

    const others = manual.map(chapter => {
        return !crudNames.includes(chapter.name) && 
            // Only get children (sections) not in crud config object
            (Object.assign({}, chapter, {
                children: chapter.children.filter(section => {
                    return !crudNames.includes(section.name) &&
                        section.children.length > 0;
                })
            }));
    });

    const cleaned = cleanEmpty(others);
    // Only return chapters not included in the ones to ignore
    return cleaned.filter(chapter => {
        return !toIgnore.includes(chapter.name);
    });
};

 /**
  * Model sections correspond to the different sections extracted from the 
  * manual (crud sections) config corresponds to a JS object containing
  * the crud section names and
  */
export function getModelSections(cruds, config) {
    return cruds.map(section => {
        // get crud property from config for particular section
        const crudProp = config.find(obj => obj.name === section.name).crud;
        const { read, create, del } = crudProp;
        //make the names a list to filter in the section commands
        const crudNames = [read, create, del];

        //get the remaining lists to display not present in the config file
        const lists = section.children.filter(cmd => {
            return cmd.name.startsWith('list_') && !crudNames.includes(cmd.name);
        });

        //only get the commands that are not lists to display
        const children = section.children.filter(cmd => {
            return !crudNames.includes(cmd.name) && !lists.includes(cmd);
        });

        if (read) {
            crudProp.read = section.children.find(cmd => cmd.name === read);
        }

        if (create) {
            //needs to filter for command `present` because there are 2 in the manual.
            //we want only the second one (this is duct tape code for now)
            const createList = section.children.filter(cmd => cmd.name === create);
            createList.length === 1 ? crudProp.create = createList[0] : crudProp.create = createList[1];
        }

        if (del) {
            crudProp.del = section.children.find(cmd => cmd.name === del);
        }

        return {
            ...section,
            display: {
                crud: crudProp,
                lists: lists
            },
            children: children
        };
    });
};

function getObjList(manual, type) {
    // commands list without duplicates
    return manual.filter(obj => {
        return (obj.kind === type);
    });
};

function getNames(list) {
    const names = list.map(c => c.name);
    // remove duplicates
    const s = new Set(names);
    return Array.from(s.values());
};


function getWorkflowCommands(wfCommands, commandsList, crudSections) {
    // this function is used to filter out the different commands that
    // shouldn't be rendered in the workflow section, such as list or crud commands
    let ignore = ['list_model'];
    crudSections.map(section => {
        const { display } = section;
        const { crud, lists } = display;
        crud.read && ignore.push(crud.read.name);
        crud.create && ignore.push(crud.create.name);
        crud.del && ignore.push(crud.del.name);
        lists.map(c => ignore.push(c.name));
    });
    
    const tmp = wfCommands.filter(c => {
        return !ignore.includes(c);
    });
    
    return commandsList.filter(c => {
        return tmp.includes(c.name);
    }).sort((a, b) => {
        return tmp.indexOf(a.name) - tmp.indexOf(b.name);
    });
};

export function generateWorkflows(workflows, commandsList, crudSections) {
    return workflows.map(wf => {
        return (
            {
                name: wf.name,
                commands: getWorkflowCommands(wf.commands,
                                              commandsList,
                                              crudSections) 
            }
        );
    });
};

export function cleanEmpty(array) {
    return array.filter(ix => ix);
};


export const crudSections = getCrudSections(menu, CRUD_SECTIONS);
export const biochamSections = getOtherSections(menu, CRUD_SECTIONS, CHAPTERS_TO_IGNORE);
export const modelSectionsWithCruds = getModelSections(crudSections, CRUD_SECTIONS);
export const listCommands = getNames(getObjList(commands_list, 'command'));
export const workflows = generateWorkflows(wf.default, getObjList(commands_list, 'command'), modelSectionsWithCruds);
