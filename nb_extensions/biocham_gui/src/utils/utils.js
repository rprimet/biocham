import commands_from_manual from 'Kernel/commands';
import {
    CHAPTERS_TO_IGNORE,
    CRUD_SECTIONS } from 'Config/config';
import { menu } from 'Kernel/cmd_tree';

/**
 * Concatenate all argument names and types for a 
 * biocham command 
 */
export function placeHolder(command) {
    //format a placeholder if input
    const args = command.arguments;
    return args.length > 0 &&
        args.map(argument => {
            return (`${argument.type} ${argument.name}`);
        });
};

function cleanEmpty(array) {
    return array.filter(ix => ix);
};

function getCommandsOnly() {
    return commands_from_manual.commands_list.filter(current => {
        return current.kind === 'command';
    });
};

export function getChapters() {
    return commands_from_manual.commands_list.filter(current => {
        return current.kind === 'chapter' &&
            !CHAPTERS_TO_IGNORE.includes(current.name);
    });
};

export function dashingLowercase(title) {
    return title.replace(new RegExp(' ', 'g'), '-').toLowerCase();
};

export function step(value) {
    return parseFloat(`1e-${precision(value)}`);
}

export function precision(value) {
    // ugly af precision but at least it computes fast
    const a = String(value);
    if (a.includes('.')) {
        const n = a.split('.');
        if (n[1].includes('e')) {
            const g = n[1].split('e');
            const t = g[0].length;
            const e = g[1];
            if (e < 0) {
                return t - e;
            } 
            else {
                return e - t;
            }  
        }
        return n[1].length;
    }
    return 0;
}

export function round(value, precision) {
    return precision > 0 ?
        parseFloat(parseFloat(value).toFixed(precision)) :
        parseInt(value);
}

export const chapters = getChapters();
