import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { MuiThemeProvider,
         createMuiTheme,
         withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import { OutputTabs } from 'Components/OutputTabs';
import NavMenu from 'Components/NavMenu';
import Chapter from 'Components/Chapter';
import Model from 'Components/Model';
import BAppBar from 'Components/AppBar';
import Settings from 'Components/Settings';

import { biochamSections,
         crudSections,
         workflows } from 'Utils/sections';
import { dashingLowercase } from 'Utils/utils';
import { addToNotebook, getJupyter, createComm } from 'Utils/comms';
import { format } from 'Utils/formats';


/**
 * MaterialUI custom theme to redefine app fonts (the same used by
 * the notebook).
 * @ignore
 */
const theme = createMuiTheme({
    typography: {
        fontFamily: [
            'Helvetica Neue',
            'Helvetica',
            'Arial',
            'sans-serif'
        ].join(','),
        fontSize: '13px',
        useNextVariants: true,
    }
});

/**
 * App css.
 * @ignore
 */
const styles = theme => ({
    root: {
        right: 0,
        left: 0,
        bottom: 0,
        top: 0,
        display: 'flex',
        // in case another extension (e.g. nbpresent) is installed
        zIndex: 99,
        position: 'absolute',
        backgroundColor: 'white'
    },
    menu: {
        minWidth: '140px',
        width: '160px',
        overflow: 'auto'
    },
    main: {
        flex: 1,
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'row',
        /*
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        */
    },
    /*
    mainShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        })
    },
    */
    inputCol: {
        flex: 2,
        alignItems: 'flex-start',
        paddingLeft: '15px',
        paddingRight: '15px',
        overflow: 'auto'
    },
    resCol: {
        flex: 3,
        alignItems: 'flex-start',
        borderLeft: '1px solid rgb(0, 0, 0, 0.12)'
    },
    hidden: {
        display: 'none'
    }
});

/**
 * React app for the biocham GUI in the Jupyter notebook. 
 */
class App extends Component {

    static defaultProps = {
        sections: biochamSections,
        modelSections: crudSections,
        workflows: workflows,
        kernelTarget: 'from_gui'
    }

    /**
     * App state.
     */
    state = {
        menuOpen: true,
        sectionsHidden: {
            model: false,
            settings: true
        },
        options: [],
        executionCount: 0,
        graphicalOutputs: [],
        results: '',
        mainTab: 0,
        graphicalTab: 0,
        numSim: true,
        workflows: true
    }

    componentDidMount() {
        
        const { sections, kernelTarget } = this.props;

        // this comm is opened from the kernel 
        getJupyter().notebook.kernel.comm_manager
            .register_target('to_gui',(comm, msg) => {
                comm.on_msg( (msg) => {
                    this.handleKernelResponse(msg.content);
                });
                comm.on_close((msg) => {
                });
            });

        let prevState = getJupyter().notebook.metadata.gui_state;
                
        if (prevState) {
            prevState = JSON.parse(prevState);
            this.setState(prevState);
        }
    
        // get all global options
        const comm = createComm(kernelTarget);
        const code = 'list_options';
        comm.send({code: `${code}.`});
        comm.on_msg(msg => {
            const options = format({str: msg.content.data.output,
                                    type: code});
            this.setState({ options });
        });

        // append all section names to an array and set their default display
        // to hidden
        const lowerDashTitles = sections.map(chapter =>
                                             dashingLowercase(chapter.name));
        lowerDashTitles.map(title => {
            this.setState(prevState => ({
                sectionsHidden: {...prevState.sectionsHidden, [title]: true}
            }));
        });
    }
    
    componentDidUpdate() {
        this.saveStateToMetadata();
    }
    
    saveStateToMetadata = () => {
        getJupyter().notebook.metadata.gui_state =
            JSON.stringify(this.state);
    }
    
    /** 
     * 
     */
    handleDrawerOpen = () => {
        this.setState({ menuOpen: true });
    }

    /** 
     * 
     */
    handleDrawerClose = () => {
        this.setState({ menuOpen: false });
    }

    /** 
     * 
     */
    generateLayout = () => {
        const { sections, kernelTarget } = this.props;
        const { sectionsHidden, options } = this.state;

        return sections.map((container, index) => {
            const name = dashingLowercase(container.name);
            return (
                container.children.length > 0 && !sectionsHidden[name] &&
                    <Chapter key={`chapter-container-${index}`}
                             content={container.children}
                             kernelTarget={kernelTarget}
                             options={options}
                             handleKernelResponse={this.handleKernelResponse}
                             appendResults={this.appendResults}/>
            );
        });
    }

    /** 
     * Handle kernel messages which can be either strings or graphical outputs.
     * @param {object} e - The full response object.
     */
    handleKernelResponse = (e) => {
        const { graphicalOutputs, graphicalTab } = this.state;
        const lastTab = graphicalOutputs.length > 0 ?
              graphicalOutputs.length - 1: 0;
        const data = e.data;
        const { type, code, show } = data;
        
        if (data.execution_count) {
            this.setState({ executionCount: data.execution_count });
        }

        // ignore comments and unwanted outputs 
        if (code && show && !code.startsWith('%')) {
            this.appendResults(code);
        }

        switch (type) {
        case 'plot':
        case 'image':
            this.handleGraphicalOutput(data);
            this.handleTabChange('mainTab', 0);
            break;
        case 'str':
            this.handleOutput(data);
            this.handleTabChange('mainTab', 1);
        default:
            break;
        }
    }

    /** 
     * Append output string to current outputs.
     */
    appendResults = (str) => {
        let output = `> ${str}\n`;
        this.setState((prevState) => {
            return {results: prevState.results + output};
        });
    }

    /** 
     * Handle tab change; tabType needs to be specified as there are several
     * tab lists in the app.
     * @param {string} tabType - the name of tab list in the state.
     * @param {number} value - the index of the current tab.
     */
    handleTabChange = (tabType, value) => {
        this.setState({[tabType]: value});
    }

    /** 
     * Handle graphical output deletion, removes it from graphical outputs array.
     * @param {number} index - the index of the output to remove.
     */
    handleOutputDelete = (index, tab) => {
        const { graphicalOutputs } = this.state;
        this.setState({ graphicalOutputs:
                        [
                            ...graphicalOutputs.slice(0, index),
                            ...graphicalOutputs.slice(index+1)
                        ] });
    }
        
    /** 
     * Append graphical output to graphical outputs array.
     * @param {object} data - the object graphical output.
     */
    handleGraphicalOutput = (data) => {
        this.setState(prevState => ({
            graphicalOutputs: [
                ...prevState.graphicalOutputs,
                data
            ]
        }));
    }

    /** 
     * Handle string output.
     * @param {object} data - the object containing string output. 
     */
    handleOutput = (data) => {
        if (data.output) {
            this.appendResults(data.output);
        }
    }

    /** 
     * 
     */
    handleState = (name, value) => {
        this.setState({[name]: value});
    }

    /** 
     * 
     */
    showContainer = (id) => {
        const keys = Object.keys(this.state.sectionsHidden);
        keys.map(k => {
            this.setState(prevState => ({
                sectionsHidden: {...prevState.sectionsHidden, [k]: true}
            }));
        });
        this.setState(prevState => ({
            sectionsHidden: {...prevState.sectionsHidden, [id]: false}
        }));
        // scroll to the top of the section
        document.getElementById('biocham-container').scrollTop = 0;
    }

    render() {
        const { classes, sections, modelSections, kernelTarget } = this.props;

        const { sectionsHidden, mainTab, menuOpen, numSim, workflows, graphicalTab,
                graphicalOutputs, results, executionCount, options } = this.state;
        
        return (
            <MuiThemeProvider theme={theme}>
              <div className={classes.root}>
                <div className={classes.menu}>
                  <NavMenu modelSections={modelSections}
                           sections={sections}
                           open={menuOpen}
                           workflows={workflows}
                           handleSelect={this.showContainer}
                           handleDrawerClose={this.handleDrawerClose}/>
                </div>
                <div className={classes.main}>
                  <div className={classes.inputCol} id="biocham-container">
                    <div className={sectionsHidden.model ? classes.hidden : ''}>
                      <Model toggleWorkflows={workflows}
                             kernelTarget={kernelTarget}
                             options={options}
                             handleKernelResponse={this.handleKernelResponse}
                             executionCount={executionCount}
                             autoNumSim={numSim}/>
                    </div>
                    {this.generateLayout()}
                    <div className={sectionsHidden.settings ? classes.hidden: ''}>
                      <Settings numSim={numSim}
                                options={options}
                                handleChange={this.handleState}/>
                    </div>
                  </div>
                  <div className={classes.resCol}>
                    <OutputTabs mainTab={mainTab}
                                handleTabChange={this.handleTabChange}
                                handleKernelResponse={this.handleKernelResponse}
                                graphicalOutputs={graphicalOutputs}
                                handleDelete={this.handleOutputDelete}
                                results={results}/>
                  </div>
                </div>
              </div>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = {
    /**
     * The sections of the manual (model sections excluded).
     */
    sections: PropTypes.array.isRequired,
    /**
     * The model sections defined in config file.
     */
    modelSections: PropTypes.array.isRequired,
    /**
     * Array containing lists of biocham commands.
     */
    workflows: PropTypes.array.isRequired,
    /**
     * The kernel target for Jupyter comms. The string should be identical in the kernel.
     */
    kernelTarget: PropTypes.string.isRequired,
};

App.defaultProps = {
    sections: biochamSections,
    modelSections: crudSections,
    workflows: workflows,
    kernelTarget: 'from_gui'
};

export default withStyles(styles)(App);
