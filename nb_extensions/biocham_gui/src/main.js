import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import { BOKEH, MATERIAL_UI } from 'Config/config';
import { getJupyter } from 'Utils/comms';
import './styles/styles.css';

const Jupyter = getJupyter();

/**
 * Create the switch button (for the GUI) in the Jupyter notebook
 * header. 
 */
function createSwitchButton () {
    // just to init for the actual switch to detect the main toolbar toggle status
    const toolbar = document.getElementById('maintoolbar');
    if (!toolbar.style.display || toolbar.style.display === 'none') {
        toolbar.style.display = 'block';
    }

    const header = document.getElementById('header');
    // small divider on bottom of header
    const divider = document.createElement('div');
    divider.classList.add('bottom-header-bar');
    header.appendChild(divider);

    let toolbarButton = document.createElement('button');
    toolbarButton.id = 'switch_btn';
    toolbarButton.classList.add('btn', 'btn-xs', 'navbar-btn', 'notification_widget');
    toolbarButton.innerText = 'GUI';
    let helpMenu = document.getElementById('help_menu').parentElement;
    helpMenu.parentElement.insertBefore(toolbarButton, helpMenu);
    toolbarButton.onclick = switchView;
}

/**
 * Switch the view between the notebook and the GUI. We don't 
 * use `display=none` for the GUI because of the way bokeh plots
 * are handled (the sizing_mode options of the plot behave weirdly
 * when display is none). The Jupyter keyboard manager is also
 * disabled in the GUI as it forbid the user to use the keyboard
 * normally.
 */
export function switchView () {
    const notebookPanel = document.getElementById('ipython-main-app');
    const guiPanel = document.getElementById('gui_panel');
    const btn = document.getElementById('switch_btn');
    const header = document.getElementById('header');
    const toolbar = document.getElementById('maintoolbar');
    const toolbarHeight = toolbar.offsetHeight;
    const body = document.getElementsByTagName('body')[0];
    const nbpApp = document.getElementsByClassName('nbp-app');
        
    if (!isVisible(notebookPanel)) {
        Jupyter.notebook.metadata.biocham_gui = false;
        Jupyter.keyboard_manager.enable();

        // css handling, see styles.css for more info
        notebookPanel.classList.remove('hidden-notebook');
        notebookPanel.classList.add('visible-notebook');
        guiPanel.style.display = 'none';
        btn.innerText = 'GUI';
        toolbar.style.display = 'block';
        header.classList.remove('gui_header');
        body.classList.add('notebook_app');
    }
    else if (isVisible(notebookPanel)) {
        Jupyter.notebook.metadata.biocham_gui = true;
        // Need to disable the notebook shortcuts when in the GUI
        Jupyter.keyboard_manager.disable();

        // css handling 
        notebookPanel.classList.remove('visible-notebook');
        notebookPanel.classList.add('hidden-notebook');
        guiPanel.style.display = 'block';
        btn.innerText = 'Notebook';
        toolbar.style.display = 'none';
        header.classList.add('gui_header');
        body.classList.remove('notebook_app');
    }

    // adjust container height
    const $ = window.$;
    $('div#site').height($(window).height() - $('#header').height());
}

/**
 * Hide an html node.
 *
 * 
 */
function hideElement(element) {
    if (element.style.display !== 'none') {
        element.style.display = 'none';
    }
}

/**
 * Show an html node with given style (i.e. block, inline, ...)
 */
function showElement(element, style) {
    if (element.style.display === 'none') {
        element.style.display = style;
    }
}

/**
 * Check if an html node is visible
 */
function isVisible(element) {
    return element.offsetWidth > 0 && element.offsetHeight > 0;
}

/**
 * Append css link to html document head.
 */
function loadCss(href, id) {
    let head = document.getElementsByTagName('head')[0];
    const link = document.createElement('link');
    link.id = id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    head.appendChild(link);
}

/**
 * Append JS script to document head.
 */
function loadJS(src, id) {
    let head = document.getElementsByTagName('head')[0];
    const link = document.createElement('script');
    link.id = id;
    link.type = 'text/javascript';
    link.src = src;
    link.async = false;
    head.appendChild(link);
}

/**
 * Load BokehJS resources (css and js).
 */
function loadBokehJS() {
    BOKEH.css.map((href, index) => {
        loadCss(href, `bokeh-css-${index}`);
    });

    loadJS(BOKEH.js.bokeh, `bokeh-js`);
    BOKEH.js.plugins.map((src, index) => {
        loadJS(src, `bokeh-js-${index}`);
    });

}

/**
 * Load MaterialUI css.
 */
function loadMaterialUI() {
    MATERIAL_UI.css.map((href, index) => {
        loadCss(href, `materialui-css-${index}`);
    });
}

/** 
 * Create container for the GUI, create switch button.
 */
function initGUI() {
    const guiPanel = document.createElement('div');
    guiPanel.id = 'gui_panel';
    document.getElementById('site').appendChild(guiPanel);
    createSwitchButton();
}

/**
 * Load all resources needed for the GUI, render the GUI in its
 * container.
 */
function loadGUI(kernelName) {
    //if (kernelName === 'biocham') {
        loadMaterialUI();
        loadBokehJS();
        if (!document.getElementById('gui_panel')) {
            initGUI();
            const root = document.getElementById('gui_panel');
            ReactDOM.render(
                <App appType='notebook'/>,
                root
            );
        }
    //}
}

/**
 * Jupyter extension loading, only load the GUI if Jupyter uses biocham 
 * kernel.
 */
export function load_ipython_extension() {
    // ensure that we are currently using a kernel biocham, otherwise
    // the GUI is not loaded.
    try {
        loadGUI(Jupyter.notebook.kernel.name);
        if (Jupyter.notebook.metadata.biocham_gui) {
            document.getElementById('switch_btn').click();
        }
    } catch(e) {
        Jupyter.notebook.events.on('kernel_ready.Kernel', function () {
            loadGUI(Jupyter.notebook.kernel.name);
            if (Jupyter.notebook.metadata.biocham_gui) {
                document.getElementById('switch_btn').click();
            }
        });
    }
}

