/**
 *
 */

import {
    CheckboxInput,
    TextInput,
    NumberInput,
    FileInput,
    AutocompleteInput } from "Components/widgets/inputs";
import { createComm } from 'Utils/comms';


export const BIOCHAM_TYPE_WIDGETS = {
    "option": {
        type: AutocompleteInput,
        complete: 'list_options'
    },
    "number": NumberInput,
    "name": TextInput,
    "=(object)": TextInput,
    "object": TextInput,
    "*(parameter_name=number)": TextInput,
    "*(parameter_name)": TextInput,
    "*(function_prototype=arithmetic_expression)": TextInput,
    "*(functor)": TextInput,
    "{object}": TextInput,
    "concentration": TextInput,
    "reaction": {
        type: AutocompleteInput,
        complete: 'list_reactions'
    },
    "up_type": TextInput,
    "*(name)": TextInput,
    "*(edge)": TextInput,
    "*(edgeref)": TextInput,
    "{graph_object}": TextInput,
    "attribute": TextInput,
    "graph_object": TextInput,
    "yesno": CheckboxInput,
    "output_file": TextInput,
    "influence": TextInput,
    "condition": TextInput,
    "*(parameter_name=arithmetic_expression)": TextInput,
    "input_file": FileInput,
    "{ref}": TextInput,
    "*([range])": NumberInput,
    "*(ref)": TextInput,
    "integer": NumberInput,
    "*(ode)": TextInput,
    "*(oderef)": TextInput,
    "*(name=arithmetic_expression)": TextInput,
    "parameter_name": TextInput,
    "solution": TextInput,
    "boolean_semantics": TextInput,
    "ctl": TextInput,
    "nusmv_initial_states": TextInput,
    "{input_file}": FileInput,
    "time": NumberInput,
    "method": TextInput,
    "filter": TextInput,
    "axes": TextInput,
    "arithmetic_expression": TextInput,
    "*(column)": TextInput,
    "column": TextInput,
    "*(number)": NumberInput,
    "foltl": TextInput,
    "[variable_objective]": TextInput,
    "*(function_prototype=foltl)": TextInput,
    "[parameter_name]": TextInput,
    "[parameter_between_interval]": TextInput,
    "*(term=term)": TextInput,
    "{wgpac}": TextInput,
    "pivp": TextInput,
};

export const CRUD_SECTIONS = [
    {
        name: 'Molecules and initial state',
        crud: {
            read: 'list_initial_state', create: 'present', del: 'undefined',
            commandSep: ',' 
        },
    },
    {
        name: 'Parameters',
        crud: {
            read: 'list_parameters', create: 'parameter', del: 'delete_parameter',
            commandSep: '='
        },
    },
    {
        name: 'Functions',
        crud: {
            read: 'list_functions', create: 'function', del: 'delete_function',
        },
    },
    {
        name: 'Reaction editor',
        crud: {
            read: 'list_reactions', create: 'add_reaction', del: 'delete_reaction',
        },
    },
    {
        name: 'Influence editor',
        crud: {
            read: 'list_influences', create: 'add_influence'
        },
    },
    {
        name: 'Events',
        crud: {
            read: 'list_events', create: 'add_event'
        },
    },
    {
        name: 'Conservation laws and invariants',
        crud: {
            read: 'list_conservations', create: 'add_conservation', del: 'delete_conservation'
        },
    },
];

export const CHAPTERS_TO_IGNORE = [
    'Overview',
    'Index',
    'Bibliography',
    'Toplevel'
];

export const MATERIAL_UI = {
    /**
     * CSS links at https://material-ui.com/getting-started/installation/
     */
    css: [
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500',
        'https://fonts.googleapis.com/icon?family=Material+Icons'
    ]
};

function getBokehVersion() {
    const comm = createComm('from_gui');
    comm.send({bokeh: true});
    comm.on_msg(msg => {
        return msg.content.data.version;
    });
    // fallback
    return '1.0.1';
}

const BOKEH_VERSION = getBokehVersion();

export const BOKEH = {
    /**
     * JS and CSS links at https://bokeh.pydata.org/en/latest/docs/installation.html
     */
    css: [
        `http://cdn.pydata.org/bokeh/release/bokeh-${BOKEH_VERSION}.min.css`,
    ],
    js: {
        bokeh: `http://cdn.pydata.org/bokeh/release/bokeh-${BOKEH_VERSION}.min.js`,
        plugins: [
            `http://cdn.pydata.org/bokeh/release/bokeh-api-${BOKEH_VERSION}.min.js`
        ]
    },
};
