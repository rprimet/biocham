const path = require('path');

module.exports = {
    entry: [
        './src/main.js'
        ],
    output: {
        filename: 'nbextension.js',
        path: path.resolve(__dirname, 'gui-build'),
        libraryTarget: 'amd'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Kernel: path.resolve(__dirname, '../../biocham_kernel'),
            Utils: path.resolve(__dirname, 'src/utils'),
            Config: path.resolve(__dirname, 'config'),
            Components: path.resolve(__dirname, 'src/components')
        }
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: [
                  { loader: "style-loader" },
                  { loader: "css-loader" }
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use: {
                    loader: 'url-loader?limit=100000'
                }
            }
        ]
    }
};
