:- module(
  gsl,
  [
    % Public API
    solve/2,
    write_gsl/1,
    write_field/1
  ]).


:- use_module(library(csv)).


:- devdoc('\\section{Public API}').


solve(Options, Table) :-
  \+ memberchk(jacobian: _, Options),
  !,
  memberchk(equations: Equations, Options),
  jacobian(Equations, Jacobian),
  solve([jacobian: Jacobian | Options], Table).

solve(Options, Table) :-
  CFilename = 'ode.inc',
  ExecutableFilename = 'ode',
  TableFilename = 'ode.csv',
  with_output_to_file(CFilename, write_gsl(Options)),
  compile_gsl_cpp_program(ExecutableFilename),
  call_subprocess(ExecutableFilename, [], []),
  csv_read_file(TableFilename, Table),
  (
    have_to_delete_temporary_files
  ->
    delete_file(CFilename),
    delete_file(ExecutableFilename),
    delete_file(TableFilename)
  ;
    true
  ).


write_field(t) :-
  write(t).

write_field(x(VariableIndex)) :-
  format('x[~d]', [VariableIndex]).

write_field(p(ParameterIndex)) :-
  format('p[~d]', [ParameterIndex]).

write_field(expression(Expr)) :-
  write_arithmetic_expression(Expr).


write_gsl(Options) :-
  write_config(Options),
  write_equations(Options),
  write_jacobian(Options),
  write_initial_values(Options),
  write_initial_parameter_values(Options),
  write_events(Options),
  write_print_headers(Options),
  generate_add_row(Options),
  write_print(Options).


:- devdoc('\\section{Private predicates}').


write_config(Options) :-
  memberchk(precision: Precision, Options),
  format('#define PRINT_ROW "%.~dg"\n', [Precision]),
  memberchk(filter: Filter, Options),
  format('#define FILTER(t) ~w(t)\n\n', [Filter]),
  memberchk(equations: Equations, Options),
  length(Equations, VariableCount),
  memberchk(initial_parameter_values: InitialParameterValues, Options),
  length(InitialParameterValues, ParameterCount),
  (
    memberchk(events: Events, Options)
  ->
    length(Events, EventCount)
  ;
    EventCount = 0
  ),
  write('
  void
  init_gsl_solver_config(struct gsl_solver_config *config)
  {
  '),
  write_config_field('variable_count', '~d', VariableCount),
  write_config_field('parameter_count', '~d', ParameterCount),
  write_config_field('event_count', '~d', EventCount),
  write_option('method', 'gsl_odeiv2_step_~a', method, Options),
  write_option('error_epsilon_absolute', '~g', error_epsilon_absolute, Options),
  write_option('error_epsilon_relative', '~g', error_epsilon_relative, Options),
  write_option('initial_step_size', '~g', initial_step_size, Options),
  write_option('maximum_step_size', '~g', maximum_step_size, Options),
  write_option('time_initial', '~g', time_initial, Options),
  write_option('time_final', '~g', time_final, Options),
  write('}\n').

write_config_field(Variable, Format, Value) :-
  format('   config->~a = ', [Variable]),
  format(Format, [Value]),
  write(';\n').

write_option(Variable, Format, Option, Options) :-
  memberchk(Option: Value, Options),
  write_config_field(Variable, Format, Value).

write_equations(Options) :-
  memberchk(equations: Equations, Options),
  write('
  int
  functions(double t, const double x[], double f[], void *data)
  {
  double *p = (double *) data;
  '),
  \+ (
  nth0(I, Equations, Equation),
  \+ (
  write_equation(I, Equation)
)
  ),
  write('  \c
  return GSL_SUCCESS;
  }
  ').


write_equation(I, Equation) :-
  format('  f[~d] = ', [I]),
  write_arithmetic_expression(Equation),
  write(';\n').


write_jacobian(Options) :-
  memberchk(jacobian: Jacobian, Options),
  length(Jacobian, VariableCount),
  format('
  int
  jacobian(double t, const double x[], double *dfdx, double dfdt[], void *data)
  {
  double *p = (double *) data;
  gsl_matrix_view dfdx_mat = gsl_matrix_view_array(dfdx, ~d, ~d);
  gsl_matrix *m = &dfdx_mat.matrix;
  ', [VariableCount, VariableCount]),
  \+ (
  nth0(I, Jacobian, JacobianLine),
  nth0(J, JacobianLine, Expression),
  \+ (
  write_jacobian_cell(I, J, Expression)
)
  ),
  write('  \c
  dfdt[0] = 0.0;
  dfdt[1] = 0.0;
  return GSL_SUCCESS;
  }
  ').


write_jacobian_cell(I, J, Expression) :-
  format('  gsl_matrix_set(m, ~d, ~d, ', [I, J]),
  write_arithmetic_expression(Expression),
  write(');\n').

write_arithmetic_expression(Value) :-
  number(Value),
  !,
  format('~e', [Value]).

write_arithmetic_expression([VariableIndex]) :-
  format('x[~d]', [VariableIndex]).

write_arithmetic_expression(p(ParameterIndex)) :-
  format('p[~d]', [ParameterIndex]).

write_arithmetic_expression(f(ParameterIndex)) :-
  format('f[~d]', [ParameterIndex]).

write_arithmetic_expression(- A) :-
  write('- '),
  write_arithmetic_expression_with_parentheses(A).

write_arithmetic_expression(A + B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' + '),
  write_arithmetic_expression_with_parentheses(B).

write_arithmetic_expression(A - B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' - '),
  write_arithmetic_expression_with_parentheses(B).

write_arithmetic_expression(A * B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' * '),
  write_arithmetic_expression_with_parentheses(B).

write_arithmetic_expression(A / B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' / '),
  write_arithmetic_expression_with_parentheses(B).

write_arithmetic_expression(A ^ B) :-
  write('pow('),
  write_arithmetic_expression(A),
  write(', '),
  write_arithmetic_expression(B),
  write(')').

write_arithmetic_expression(sqrt(A)) :-
  write('sqrt('),
  write_arithmetic_expression(A),
  write(')').

write_arithmetic_expression(cos(A)) :-
  write('cos('),
  write_arithmetic_expression(A),
  write(')').

write_arithmetic_expression(sin(A)) :-
  write('sin('),
  write_arithmetic_expression(A),
  write(')').

write_arithmetic_expression(exp(A)) :-
  write('exp('),
  write_arithmetic_expression(A),
  write(')').

write_arithmetic_expression(log(A)) :-
  write('log('),
  write_arithmetic_expression(A),
  write(')').

write_arithmetic_expression_with_parentheses(Expression) :-
  write('('),
  write_arithmetic_expression(Expression),
  write(')').


write_condition(A = B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' == '),
  write_arithmetic_expression_with_parentheses(B).

write_condition(A < B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' < '),
  write_arithmetic_expression_with_parentheses(B).

write_condition(A > B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' > '),
  write_arithmetic_expression_with_parentheses(B).

write_condition(A <= B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' <= '),
  write_arithmetic_expression_with_parentheses(B).

write_condition(A >= B) :-
  write_arithmetic_expression_with_parentheses(A),
  write(' >= '),
  write_arithmetic_expression_with_parentheses(B).

write_condition(true) :-
  write('true').

write_condition(false) :-
  write('false').

write_condition(A or B) :-
  write_condition_with_parentheses(A),
  write(' || '),
  write_condition_with_parentheses(B).

write_condition(A and B) :-
  write_condition_with_parentheses(A),
  write(' && '),
  write_condition_with_parentheses(B).

write_condition(not(A)) :-
  write('!'),
  write_condition_with_parentheses(A).


write_condition_with_parentheses(Condition) :-
  write('('),
  write_condition(Condition),
  write(')').


write_initial_parameter_values(Options) :-
  memberchk(initial_parameter_values: InitialParameterValues, Options),
  write('
  void
  initial_parameter_values(double p[])
  {
  '),
  \+ (
  nth0(ParameterIndex, InitialParameterValues, ParameterValue),
  \+ (
  format('   p[~d] = ~g;\n', [ParameterIndex, ParameterValue])
)
  ),
  write('\c
  }
  ').


write_initial_values(Options) :-
  memberchk(initial_values: InitialValues, Options),
  write('
  void
  initial_values(double x[], double p[])
  {
  '),
  \+ (
  nth0(VariableIndex, InitialValues, InitialValue),
  \+ (
  format('  x[~d] = ', [VariableIndex]),
  write_arithmetic_expression(InitialValue),
  write(';\n')
)
  ),
  write('\c
  }
  ').


write_events(Options) :-
  (
    memberchk(events: Events, Options)
  ->
    true
  ;
    Events = []
  ),
  write('
void
events(struct gsl_solver_state *state, double *t_upper)
{
  const struct gsl_solver *solver = state->solver;
  const struct gsl_solver_config *config = solver->config;
  gsl_odeiv2_driver *d = solver->d;
  double f[config->variable_count];
  double t = state->t;
  double *p = solver->p;
  double *x = state->x;
  bool *e = state->e;\n'),
  \+ (
    nth0(EventIndex, Events, (Condition -> ParameterValues)),
    \+ (
      format('  if (!e[~d] && (', [EventIndex]),
      write_condition(Condition),
      format(
        ')) {
    e[~d] = true;\n',
        [EventIndex]
      ),
      \+ (
        member(ParameterIndex = Value, ParameterValues),
        \+ (
          format(
            '    p[~d] = ',
            [ParameterIndex]
          ),
          write_arithmetic_expression(Value),
          write(';\n')
        )
      ),
      format(
        '    gsl_odeiv2_driver_reset(d);\n  } else if (e[~d] && !(',
        [EventIndex]
      ),
      write_condition(Condition),
      format(
        ')) {\n    e[~d] = false;\n  }\n',
        [EventIndex]
      ),
  (
    % Accurate Event Detection for Simulating Hybrid Systems
    % Joel M. Esposito, Vijay Kumar, and George J. Pappas
    % \cite{EKP01hscc}
    is_linear(Condition, Derivatives, Options, Normalized, Sign)
  ->
    format('  else if (!e[~d] && !(', [EventIndex]),
    write_condition(Condition),
    write(')) {
    functions(t, x, f, p);
    if ('),
    write_arithmetic_expression(Derivatives),
    (
      Sign =:= 1
    ->
      write(' > 0)')
    ;
      write(' < 0)')
    ),
    % if smaller than initial_step_size will be maxed in gsl_solver.cc
    write(' {
      *t_upper = fmin(*t_upper, t + fabs(('),
    write_arithmetic_expression(Normalized),
    write(')/'),
    write_arithmetic_expression(Derivatives),
    write('));
    }
  }
  ')
  ;
    true
  )
)
  ),
  write('\n}\n').


write_print_headers(Options) :-
  memberchk(fields: [FirstField: _ | Fields], Options),
  format('
  void
  print_headers(FILE *csv)
  {
  fprintf(csv, "#\\"~a\\"', [FirstField]),
  \+ (
  member(Field: _, Fields),
  \+ (
  format(',\\"~a\\"', [Field])
)
  ),
  write('\\n");
  }
  ').


write_print(Options) :-
  memberchk(precision: Precision, Options),
  memberchk(fields: Fields, Options),
  format('
void
print(FILE *csv, struct gsl_solver_state *state)
{
    double t = state->t;
    double *p = state->solver->p;
    double *x = state->x;
    bool *e = state->e;
    fprintf(csv, "%.~dg', [Precision]),
  \+ (
    Fields = [_First | Others],
    member(_Field, Others),
    \+ (
      format(',%.~dg', [Precision])
    )
  ),
  write('\\n"'),
  \+ (
    member(_Header: Field, Fields),
    \+ (
      format(', '),
      write_field(Field)
    )
  ),
  write(');
}
').


generate_add_row(Options) :-
  memberchk(fields: Fields, Options),
  write('\c
static void
add_row(Row &row, gsl_solver_state &state)
{
    double t = state.t;
    double *p = state.solver->p;
    double *x = state.x;
    bool *e = state.e;
'),
  \+ (
    member(_: Field, Fields),
    \+ (
      write('    row.push_back('),
      write_field(Field),
      write(');\n')
    )
  ),
  write('};\n').


compile_gsl_cpp_program(ExecutableFilename) :-
  gsl_compile_options(GslCompileOptions),
  compile_cpp_program(
      ['gsl_loop.cc', 'gsl_solver.o'], GslCompileOptions, ExecutableFilename).


% FIXME and=max and or=min but after normalization
is_linear(not C, Derivatives, Options, Normalized, Sign) :-
  !,
  is_linear(C, Derivatives, Options, Normalized, NotSign),
  Sign is -NotSign.

is_linear(C, Derivatives, Options, A - B, Sign) :-
  C =.. [Op, A, B],
  (
    member(Op, ['>', '>>', '>='])
  ->
    Sign = 1
  ;
    member(Op, ['<', '<<', '<='])
  ->
    Sign = -1
  ),
  is_linear_aux(A - B, Derivatives, Options).


is_linear_aux(Condition, ScalarDerivatives, Options) :-
  memberchk(equations: Equations, Options),
  length(Equations, VariableCount),
  VarMax is VariableCount - 1,
  findall(
    [Var],
    between(0, VarMax, Var),
    Vars
  ),
  maplist(derivate(Condition), Vars, Derivatives),
  foreach(member(D, Derivatives), \+ sub_term([_], D)),
  maplist(my_mult, Derivatives, Vars, D),
  reaction_editor:list_to_solution(D, SDerivatives),
  simplify(SDerivatives, ScalarDerivatives).


my_mult(X, [Y], X*f(Y)).
