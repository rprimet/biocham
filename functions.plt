:- use_module(library(plunit)).

:- begin_tests(functions).

test(
  'function',
  [true(Functions == [function(a = 1), function(b(y) = y + 2)])]
) :-
  clear_model,
  command(function(a = 1, b(x) = x + 1)),
  command(function(b(y) = y + 2)),
  all_items([kind: function, no_inheritance], Functions).

test(
  'function overload',
  [true(Functions == [function(f = 1), function(f(y) = y + 2)])]
) :-
  clear_model,
  command((function f = 1, f(x) = x + 1)),
  command(function f(y) = y + 2),
  all_items([kind: function, no_inheritance], Functions).

test(
  'delete function',
  [true(Functions == [function(f = 1)])]
) :-
  clear_model,
  command((function f = 1, f(x) = x + 1)),
  command(function f(y) = y + 2),
  command(delete_function(f/1)),
  all_items([kind: function, no_inheritance], Functions).

test(
  'delete functions',
  [true(Functions == [function(g = 2)])]
) :-
  clear_model,
  command((function f = 1, f(x) = x + 1, g = 2)),
  command(function f(y) = y + 2),
  command(delete_function(f)),
  all_items([kind: function, no_inheritance], Functions).

:- end_tests(functions).
