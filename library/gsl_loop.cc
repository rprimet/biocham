#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <vector>

#include "gsl_solver.hh"
#include "ode.inc"
#include "filter.inc"


void print_row(FILE *file, Row row) {
    bool first = true;
    for (double x: row) {
        if (!first) {
            fprintf(file, ",");
        } else {
            first = false;
        }
        fprintf(file, PRINT_ROW, x);
    }
    fprintf(file, "\n");
}


int
main(void)
{
    FILE *csv = fopen("ode.csv", "w");
    struct gsl_solver_config config;
    struct gsl_solver solver;
    struct gsl_solver_state state;
    init_gsl_solver_config(&config);
    init_gsl_solver(&solver, &config);
    init_gsl_solver_state(&state, &solver);
    print_headers(csv);
    Table table;
    gsl_solver_iterate(&state, &table);
    free_gsl_solver_state(&state);
    free_gsl_solver(&solver);
    for (Row row: table) {
        print_row(csv, row);
    }
    return EXIT_SUCCESS;
}
