void
no_filter(Table *table) {
    return;
}

void
only_extrema(Table *table) {
    Table::iterator stored_row = table->begin();
    int j;
    bool extremum;
    double s;
    Table::iterator current_row = stored_row;
    current_row++;
    Table::iterator previous_row = current_row;
    current_row++;
    for (; current_row != table->end(); previous_row = current_row++) {
        extremum = false;
        for (int j=0; j<stored_row->size(); j++) {
            s = ((*previous_row)[j] - (*stored_row)[j])*((*current_row)[j] - (*previous_row)[j]);
            if (s < 0) {
                extremum = true;
                break;
            }
        }
        if (extremum) {
            stored_row = previous_row;
        } else {
            table->erase(previous_row);
        }
    }
}

void
gsl_solver_iterate(struct gsl_solver_state *state, Table *table)
{
    do {
        Row row;
        add_row(row, *state);
        (*table).push_back(row);
    } while (gsl_solver_step(state));
    FILTER(table);
}
