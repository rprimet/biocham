#include "csv_reader.hh"

Table
read_CSV_stream(std::istream &stream)
{
    Table result;
    std::string line;
    while (std::getline(stream, line)) {
        if (line[0] == '#') {
            continue;
        }
        std::stringstream lineStream(line);
        Row row;
        std::string cell;
        while (std::getline(lineStream, cell, ',')) {
            row.push_back(atof(cell.c_str()));
        }
        result.push_back(row);
    }
    return result;
}
