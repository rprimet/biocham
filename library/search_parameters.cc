#include <stdlib.h>
#include <cstddef>
#include <ppl.hh>
#include "ppl_validity_domain.hh"
#include "gsl_solver.hh"
#include "cmaes_interface.h"
#include "satisfaction_degree.hh"
#include "ode.inc"
#include "check.inc"
#include "search_parameters.inc"
#include "domain_distance.inc"
#include "filter.inc"

// shared-memory parallelism for free
#if defined(_OPENMP)
#include <omp.h>
#else
typedef int omp_int_t;
inline omp_int_t omp_get_thread_num() { return 0; }
inline omp_int_t omp_get_max_threads() { return 1; }
#endif

static double
fitfun(gsl_solver *solver, gsl_solver_config *config, double values[DIMENSION])
{
    gsl_solver_state state;
    int i, j;
    double *p = solver->p;
    Table table;
    double result = 0;
    double max_distance = -INFINITY;
    Domain domain;

    for (i = 0; i < CONDITIONS; i++) {
        init_gsl_solver_state(&state, solver);
        // reset step size and evolution, forces again initial step size because
        // otherwise seems useless
        gsl_odeiv2_driver_reset_hstart(solver->d, config->initial_step_size);
        for (j = 0; j < DIMENSION; j++) {
            const struct search_parameter *param = &search_parameters[j];
            double value = values[j];
            if (i == 0) { // first condition
                if (value < param->min) {
                    result += param->min - value;
                }
                else if (value > param->max) {
                    result += value - param->max;
                }
            }
            if (LOGNORMAL) {
                p[param->index] = exp(value);
            } else {
                p[param->index] = value;
            }
            /* std::cerr << "param " << j << " = "  << value << std::endl; */
        }
        // first condition, out-of-bounds penalty
        if (i == 0 && result > 0) {
            result += 10000;
            // if violation, simply return with no simulation
            return result;
        }
        (*setup_condition[i])(solver);

        // initial values might depend on parameters
        initial_values(state.x, p);

        gsl_solver_iterate(&state, &table);
        free_gsl_solver_state(&state);

        // FIXME cannot use OpenMP to parallelize PPL
#pragma omp critical(ppl)
        {
            PPL::set_rounding_for_PPL();
            domain = (*compute_condition_domain[i])(table);
            /* using PPL::IO_Operators::operator<<; */
            /* std::cerr << domain << "." << std::endl; */
            /* std::cerr << j << " rows" << std::endl; */
            max_distance = std::max(max_distance, domain_distance(domain, i));
            /* std::cerr << "distance " << max_distance << std::endl; */
            PPL::restore_pre_PPL_rounding();
        }
    }

    result += max_distance;
    return result;
}


int
main(int argc, char *argv[])
{
    cmaes_t evo;
    double *arFunvals, *const*pop, *xfinal;
    double xstart[DIMENSION], stddev[DIMENSION];
    int i;
    /* int j; */
    int lambda;
    gsl_solver_config config[omp_get_max_threads()];
    gsl_solver solver[omp_get_max_threads()];
#pragma omp parallel for
    for (i = 0; i < omp_get_max_threads(); i++) {
        init_gsl_solver_config(&config[i]);
        init_gsl_solver(&solver[i], &config[i]);
    }
    double best_vd = 1;
    for (i = 0; i < DIMENSION; i++) {
        const struct search_parameter *param = &search_parameters[i];
        double min = param->min;
        double max = param->max;
        xstart[i] = param->init;
        stddev[i] = (max - min) / 2;
    }
    arFunvals = cmaes_init(
        &evo, DIMENSION, xstart, stddev, SEED, 0, "cmaes_initials.par");
    cmaes_ReadSignals(&evo, "cmaes_signals.par");
    evo.sp.stStopFitness.flg = 1;
    evo.sp.stStopFitness.val = STOPFIT;
    // somehow still have to test for best_vd, not sure why...
    lambda = cmaes_Get(&evo, "lambda");
    while(!cmaes_TestForTermination(&evo) && best_vd >= STOPFIT) {
        pop = cmaes_SamplePopulation(&evo);
#pragma omp parallel for
        for (i = 0; i < lambda; ++i) {
            arFunvals[i] = fitfun(&solver[omp_get_thread_num()], &config[omp_get_thread_num()], pop[i]);
            /* fprintf(stderr, "distance: %f\n", arFunvals[i]); */
            /* for (j = 0; j < DIMENSION; ++j) { */
            /*     fprintf(stderr, "   %.17g\n", pop[i][j]); */
            /* } */
        }
        cmaes_UpdateDistribution(&evo, arFunvals);
        best_vd = cmaes_Get(&evo, "fbestever");
        /* fprintf(stderr, "Best satisfaction degree: %f\n", */
        /*         1 / (1 + best_vd)); */
        /* xfinal = cmaes_GetNew(&evo, "xbestever"); */
        /* for (i = 0; i < DIMENSION; i++) { */
        /*     fprintf(stderr, "   %f\n", xfinal[i]); */
        /* } */
        cmaes_ReadSignals(&evo, "cmaes_signals.par");
        fflush(stdout);
    }
    cmaes_WriteToFile(&evo, "all", "allcmaes.dat");
    if (cmaes_TestForTermination(&evo)) {
        char reason[3024];
        strncpy(reason, cmaes_TestForTermination(&evo), 3024);
        strtok(reason, "\n");
        printf("%s\n", reason);
    } else {
        printf("Biocham stop with best fitness %g < stop fitness %g\n",
                best_vd, STOPFIT);
    }
    xfinal = cmaes_GetNew(&evo, "xbestever");
    for (i = 0; i < DIMENSION; i++) {
        /* fprintf(stderr, "%.17g\n", xfinal[i]); */
        if (LOGNORMAL) {
            printf("%.17g\n", exp(xfinal[i]));
        }
        else {
            printf("%.17g\n", xfinal[i]);
        }
    }
    printf("%f\n", 1 / (1 + best_vd));
    cmaes_exit(&evo);
    free(xfinal);
#pragma omp parallel for
    for (i = 0; i < omp_get_max_threads(); i++) {
        free_gsl_solver(&solver[i]);
    }
    return EXIT_SUCCESS;
}
