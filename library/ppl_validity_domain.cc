#include <fstream>
#include <stdlib.h>

#include "ppl_validity_domain.hh"
#include "csv_reader.hh"

#include "check.inc"


int
main(int argc, char *argv[])
{
    PPL::set_rounding_for_PPL();
    std::ifstream file(argv[1]);
    Table table = read_CSV_stream(file);
    Domain result = compute_domain(table);
    using PPL::IO_Operators::operator<<;
    /* std::cerr << result << "." << std::endl; */
    std::cout << result << "." << std::endl;
    return EXIT_SUCCESS;
}
