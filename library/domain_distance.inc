static double
domain_distance(const Domain &domain, int condition_index)
{
    /* using PPL::IO_Operators::operator<<; */
    PPL::Constraint_System cs;
    int i;
    int j = objective_count[condition_index];
    bool contained = false;
    for (i = 0; i < j; i++) {
        const struct variable_objective *o = &(var_obj[condition_index])[i];
        PPL::Variable x(o->index);
        cs.insert(x * FOLTL_PRECISION == o->value * FOLTL_PRECISION);
        /* std::cerr << o->index << " -> " << o->value << std::endl; */
    }
    PPL::NNC_Polyhedron objective(cs);
    if (PPL::check_containment(objective, domain)) {
        /* std::cerr << "contained" << std::endl; */
        contained = true;
    }
    double result = INFINITY;
    double distance, satisfaction;
    long max_coeff, coeff;
    // we take the min of the distances to all generators (always
    // (closure-)points?) as an over-approximation of the true distance
    // we then compare with another over-approximation:
    // the sum of distances to each constraint
    // TODO use the approximation to get better, or even compute the true
    // distance
    for (auto &polyhedron: domain) {
        if (contained) {
            if (polyhedron.pointset().contains(objective)) {
                /* fprintf(stderr, "contained\n"); */
            } else {
                /* fprintf(stderr, "not contained\n"); */
                continue;
            }
        }

        for (auto &objective_generator: objective.generators()) {
            for (auto &generator: polyhedron.pointset().generators()) {
                PPL::Checked_Number<mpq_class, PPL::Extended_Number_Policy> e_d;
                if (PPL::euclidean_distance_assign(
                        e_d, generator, objective_generator, PPL::ROUND_UP)) {
                    distance = e_d.raw_value().get_d();
                    /* std::cerr << "generator " << generator << std::endl; */
                    /* std::cerr << "objective generator " << objective_generator << std::endl; */
                    /* std::cerr << "euclidean distance: " << distance << std::endl; */
                    if (distance < result) {
                        result = distance;
                    }
                }
            }
        }

        if (contained) {
            distance = INFINITY;
        } else {
            distance = 0;
        }
        for (auto &constraint: polyhedron.pointset().constraints()) {
            /* std::cerr << "constraint " << constraint << std::endl; */
            satisfaction = constraint.inhomogeneous_term().get_d();
            max_coeff = 0;
            for (i = 0; i < j; i++) {
                const struct variable_objective *o = &(var_obj[condition_index])[i];
                /* std::cerr << "coeff(" << i << ") = " << */
                /*    constraint.coefficient(PPL::Variable(o->index)) << std::endl; */
                coeff = constraint.coefficient(PPL::Variable(o->index)).get_si();
                satisfaction += coeff*o->value;
                coeff = std::abs(coeff);
                max_coeff = std::max(max_coeff, coeff);
            }
            // constraints are always coefficients greater (or equal) 0
            // satisfaction is therefore the opposite of the sum
            // we divide by max_coeff to get a distance in the steepest
            // coordinate
            satisfaction = satisfaction/max_coeff;
            if (constraint.is_equality()) {
                if (contained) {
                    distance = 0;
                } else {
                    distance += std::abs(satisfaction);
                }
            } else {
                if (contained) {
                    distance = std::min(distance, satisfaction);
                    /* fprintf(stderr, "distance: %g\n", distance); */
                } else if (satisfaction < 0) {
                    // only take intou account non-satisfied constraints
                    distance -= satisfaction;
                }
            }
        }
        if (distance >= 0 && distance < result) {
            result = distance;
        }
    }

    /* std::cerr << "result: " << result << std::endl; */
    if (contained) {
        result = 1/(1 + result) - 1;
    }

    /* std::cerr << "distance: " << result << std::endl; */
    /* if (contained) { */
    /*     return 0; */
    /* } */

    return result;
}
