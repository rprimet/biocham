#ifndef ppl_validity_domain_hh
#define ppl_validity_domain_hh

#include <ppl.hh>

#include "gsl_types.hh"

namespace PPL = Parma_Polyhedra_Library;

typedef PPL::Pointset_Powerset<PPL::NNC_Polyhedron> Domain;

#endif /* ppl_validity_domain_hh */
