#include <stdlib.h>
#include <cstddef>
#include <random>
#include <ppl.hh>
#include "ppl_validity_domain.hh"
#include "gsl_solver.hh"
#include "cmaes_interface.h"
#include "satisfaction_degree.hh"
#include "ode.inc"
#include "check.inc"
#include "search_parameters.inc"
#include "domain_distance.inc"
#include "filter.inc"

// shared-memory parallelism for free
#if defined(_OPENMP)
#include <omp.h>
#else
typedef int omp_int_t;
inline omp_int_t omp_get_thread_num() { return 0; }
inline omp_int_t omp_get_max_threads() { return 1; }
#endif

static double
satisfaction_degree(gsl_solver *solver, gsl_solver_config *config, double values[DIMENSION])
{
    gsl_solver_state state;
    int j;
    double *p = solver->p;
    Table table;
    double max_distance;
    Domain domain;

    init_gsl_solver_state(&state, solver);
    // reset step size and evolution, forces again initial step size because
    // otherwise seems useless
    gsl_odeiv2_driver_reset_hstart(solver->d, config->initial_step_size);
    for (j = 0; j < DIMENSION; j++) {
        const struct search_parameter *param = &search_parameters[j];
        double value = values[j];
        p[param->index] = value;
        /* std::cerr << "param " << j << " = "  << value << std::endl; */
    }

    // initial values might depend on parameters
    initial_values(state.x, p);

    gsl_solver_iterate(&state, &table);
    free_gsl_solver_state(&state);

    // FIXME cannot use OpenMP to parallelize PPL
#pragma omp critical(ppl)
    {
        PPL::set_rounding_for_PPL();
        domain = (*compute_condition_domain[0])(table);
        /* using PPL::IO_Operators::operator<<; */
        /* std::cerr << domain << "." << std::endl; */
        /* std::cerr << j << " rows" << std::endl; */
        // Ignore negative distances for robustness computation
        max_distance = std::max(0.0, domain_distance(domain, 0));
        /* std::cerr << "distance " << max_distance << std::endl; */
        PPL::restore_pre_PPL_rounding();
    }

    return 1/(1 + max_distance);
}


int
main(int argc, char *argv[])
{
    double population[DIMENSION];
    std::default_random_engine generator;
    generator.seed(SEED);
    std::normal_distribution<double> distribution[DIMENSION];
    int i, j;
    // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    double robustness_mean = 0, relative_robustness_error;
    double robustness_ssd = 0, delta, delta2, new_value;
    gsl_solver_config config[omp_get_max_threads()];
    gsl_solver solver[omp_get_max_threads()];
#pragma omp parallel for
    for (i = 0; i < omp_get_max_threads(); i++) {
        init_gsl_solver_config(&config[i]);
        init_gsl_solver(&solver[i], &config[i]);
    }
    for (i = 0; i < DIMENSION; i++) {
        const struct search_parameter *param = &search_parameters[i];
        /* fprintf(stderr, "param %d init %g\n", i, param->init); */
        distribution[i] = std::normal_distribution<double>(param->init, COEFF_VAR*param->init);
    }
    for (j = 0; j < SAMPLES; ++j) {
#pragma omp parallel for
        for (i = 0; i < DIMENSION; i++) {
            population[i] = distribution[i](generator);
        }
        new_value = satisfaction_degree(&solver[omp_get_thread_num()], &config[omp_get_thread_num()], population);
        delta = new_value - robustness_mean;
        robustness_mean += delta/(j+1);
        delta2 = new_value - robustness_mean;
        robustness_ssd += delta*delta2;
        // https://en.wikipedia.org/wiki/Monte_Carlo_integration
        // https://en.wikipedia.org/wiki/Variance#Population_variance_and_sample_variance
        relative_robustness_error = sqrt(robustness_ssd)/((j+1)*robustness_mean);
        /* fprintf(stderr, "%d: %g %g\n", j, robustness_mean, relative_robustness_error); */
        if (j > SAMPLES*RELATIVE_ROBUSTNESS_ERROR &&
                relative_robustness_error < RELATIVE_ROBUSTNESS_ERROR) {
            break;
        }
    }
    printf("%f\n", robustness_mean);
#pragma omp parallel for
    for (i = 0; i < omp_get_max_threads(); i++) {
        free_gsl_solver(&solver[i]);
    }
    return EXIT_SUCCESS;
}
