# make jupyter will make biocham without checking unitary tests (avoiding potential problems of access to BioModels in sbml.plt)

ADDITIONAL_MODULES= \
  modules/sbml/sbml_utils.pl \
  modules/partialfrac/partialfraction.pl \
  modules/glucose/glucose.pl
MODULES=$(shell sed -n -E 's/^[+-] (.*\.pl)$$/\1/p' toc.org) \
  $(ADDITIONAL_MODULES)
# load_test_files/1 should make this useless, but I cannot find how to use it
TEST_MODULES=$(wildcard $(MODULES:.pl=.plt)) junit.pl

ifndef $(PREFIX)
  PREFIX=/usr/local
endif

$(foreach var, CC PLBASE PLARCH PLLIB PLCFLAGS PLLDFLAGS PLSOEXT, \
  $(eval \
    $(shell \
      swipl --dump-runtime-variables | \
      grep ^$(var)= | \
      sed -E 's/^/export /;s/="/=/;s/";$$//')))

SUBDIRS=$(dir $(wildcard modules/*/Makefile))

# glucose source needs its own header files
INCLUDEDIRS=$(PLBASE)/include $(SUBDIRS) \
  modules/glucose/glucose-syrup modules/glucose/glucose-syrup/core

RPATH=
ifneq ($(LD_RUN_PATH),)
  RPATH=-Wl,-rpath=$(LD_RUN_PATH)
endif
CFLAGS=$(addprefix -I, $(INCLUDEDIRS)) $(PLCFLAGS)
CXXFLAGS=$(CFLAGS) -Wno-logical-op-parentheses `pkg-config --cflags libgvc`

LDFLAGS=$(PLLDFLAGS) $(RPATH) $(addprefix -L, $(PLBASE)/lib/$(PLARCH)/)

LIBSBML=$(shell pkg-config --silence-errors --libs libsbml)
ifeq ($(strip $(LIBSBML)),)
  LIBSBML=-lsbml
endif
LDLIBS=$(PLLIB) `pkg-config --libs libgvc` $(LIBSBML) -lgsl -lgslcblas -lm

SWIPL=$(PWD)/swipl-biocham

CMAES_LIB=library/cmaes.c library/cmaes.h library/cmaes_interface.h

WORKFLOWS_DIR = $(PWD)/nb_extensions/biocham_gui/config/workflows

all: biocham biocham_debug test doc/index.html pldoc jupyter

quick: unit_tests

.PHONY: all slow test unit_tests clean web cabernet cabernet_restart \
  jupyter gadagne gadagne_restart jupyter_tests install pldoc devdoc

# should we depend on jupyter target?
install: biocham biocham_debug
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/share/biocham
	cp $? $(PREFIX)/share/biocham/
	ln -fs $(PREFIX)/share/biocham/biocham $(PREFIX)/bin/biocham
	ln -fs $(PREFIX)/share/biocham/biocham_debug $(PREFIX)/bin/biocham_debug
	cp -r library $(PREFIX)/share/biocham/
	cp -r doc $(PREFIX)/share/biocham/
	cp -r biocham_kernel $(PREFIX)/share/biocham/

biocham: $(SWIPL) $(MODULES) toc.org $(CMAES_LIB) \
	library/gsl_solver.o library/cmaes.o library/csv_reader.o Makefile
	$(SWIPL) -q -o biocham \
		--goal="\
			set_prolog_flag(verbose, normal), \
			start" \
		--toplevel=toplevel --stand_alone=true -O -c $(MODULES)

biocham_debug: $(SWIPL) $(MODULES) $(TEST_MODULES) toc.org $(CMAES_LIB) \
	library/gsl_solver.o library/cmaes.o library/csv_reader.o Makefile
	$(SWIPL) -q -o biocham_debug \
		--goal="\
			set_prolog_flag(verbose, normal), \
			initialize" --stand_alone=true -c $(MODULES) $(TEST_MODULES)

$(SWIPL): $(SWIPL).c \
		modules/graphviz/graphviz_swiprolog.c \
		modules/sbml/sbml_swiprolog.c \
		modules/partialfrac/roots.c \
		modules/glucose/glucose_swiprolog.cc \
		modules/glucose/glucose-syrup/core/Solver.cc
	for dir in $(SUBDIRS) ; do $(MAKE) -C $$dir ; done
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) $(LDLIBS) -lstdc++ -o $@

# patch line is to remove output of noisy comment on stdout
modules/glucose/glucose-syrup/core/Solver.cc: modules/glucose/Solver.patch
	curl -fsSLO http://www.labri.fr/perso/lsimon/downloads/softwares/glucose-syrup.tgz
	tar -C $(dir $<) -xf glucose-syrup.tgz
	rm -f glucose-syrup.tgz
	patch $@ < $<

doc/index.html doc/commands.js: biocham
	./biocham --generate-doc

pldoc:
	echo "doc_save(., [if(true), doc_root('doc/pldoc')])." | swipl -q

devdoc:
	./biocham --generate-devdoc

# quick ones only
unit_tests: biocham_debug
	echo "run_tests_and_halt." | ./biocham_debug

# slow tests too
test: biocham_debug jupyter_tests
	echo "flag(slow_test, _, true), run_tests_and_halt." | ./biocham_debug

jupyter_tests: jupyter
	PATH=$(PWD):$(PATH) PYTHONPATH=$(PWD) jupyter nbconvert --execute --stdout library/examples/cmsb_2017/sigmoids.ipynb > /dev/null

# runs test unit %
test_%: biocham_debug
	echo "flag(slow_test, _, true), run_tests_and_halt('$*')." | ./biocham_debug

jupyter: biocham biocham_kernel/commands.js biocham_kernel/commands.py gui extensions
	- jupyter kernelspec install --user --name=biocham biocham_kernel

gui: workflows
	- cd nb_extensions/biocham_gui && npm install && npm run dev
	- jupyter nbextension install --user nb_extensions/biocham_gui/gui-build/
	- jupyter nbextension enable --user gui-build/nbextension

qgui:
	cd nb_extensions/biocham_gui && npm run dev
	- jupyter nbextension install --user nb_extensions/biocham_gui/gui-build/
	- jupyter nbextension enable --user gui-build/nbextension

extensions:
	- jupyter nbextension install --user nb_extensions/autosavetime
	- jupyter nbextension enable --user autosavetime/main

biocham_kernel/commands.py: biocham
	rm -f $@
	echo '# Auto generated file, please do not edit manually' > $@
	echo 'commands = [' >> $@
	./biocham --list-commands | tail -n +4 | sort -u | sed -e 's/^\(.*\)$$/    "\1",/' >> $@
	echo ']' >> $@
	cp $@ nb_extensions/biocham_gui/config/workflows/

biocham_kernel/commands.js: doc/commands.js
	cp -f $< $@

workflows: 
	for file in $(WORKFLOWS_DIR)/nb/*.ipynb ; do \
		if test -f $$file; then jupyter nbconvert --to script $$file ; fi ; done
	python3 $(WORKFLOWS_DIR)/make_workflows.py -i $(WORKFLOWS_DIR)/nb/ -o $(WORKFLOWS_DIR)/workflows.js

clean:
	- for dir in $(SUBDIRS) ; do $(MAKE) -C $$dir clean ; done
	- rm biocham
	- rm biocham_debug
	- rm biocham_tests
	- rm biocham_full_tests
	- rm swipl-biocham
	- rm swipl-biocham.o
	- rm library/gsl_solver.o
	- rm library/cmaes.o
	- rm library/csv_reader.o

$(CMAES_LIB): modules/c-cmaes/src
	cp $</$(notdir $@) $@

modules/c-cmaes/src:
	if [ -d .git ] ; then \
		git submodule init && \
		git submodule update ; \
	else \
		git clone https://github.com/CMA-ES/c-cmaes.git modules/c-cmaes ; \
	fi

# size issue, so we exclude the video
web/biocham.zip:
	git archive --prefix=biocham/ -o $@ HEAD
	-zip $@ -d biocham/web/biocham4.mov biocham/web/\*.zip

web: web/biocham.zip web/index.html web/logo.png doc/index.html
	-rsync -avz $^ doc lifeware:/nfs/web-epi/biocham4/
	ssh -t lifeware 'sudo chmod -R g+w /nfs/web-epi/biocham4/ && sudo chown -R www-data.lifeware /nfs/web-epi/biocham4/'

cabernet: web
	-rsync -avz tmpnb/* lifeware@cabernet:/usr/local/share/tmpbc/
	ssh -t lifeware@cabernet 'cd /usr/local/share/tmpbc/lifeware_biocham && sudo docker build --no-cache -t lifeware/biocham:$(TAG) --build-arg tag=$(TAG) .'

cabernet_restart: cabernet
	ssh -t lifeware@cabernet 'cd /usr/local/share/tmpbc/ && sudo ./cleanup.sh && sudo ./tmpnb_biocham.sh'

gadagne: web
	-rsync -avz tmpnb/* gadagne:/opt/tmpnb/
	ssh -t gadagne 'cd /opt/tmpnb/lifeware_biocham && sudo docker build --no-cache -t lifeware/biocham:$(TAG) --build-arg tag=$(TAG) .'

gadagne_restart: gadagne
	ssh -t gadagne 'cd /opt/tmpnb/ && sudo ./cleanup.sh && sudo ./tmpnb_biocham.sh'

release:
	@read -p "Version number (e.g. 4.1.0): " version && \
	sed -i'' -e "s/^version('.*')/version('$$version')/" about.pl && \
	LC_ALL=C sed -i'' -e "s+^<h1>version.*</h1>+<h1>version $$version $(shell date '+%B %Y')</h1>+" web/index.html && \
	git commit about.pl web/index.html -m "Version $$version" && \
	git tag -a v$$version -m "Version $$version" && \
	git push --tags
