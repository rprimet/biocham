:- module(
  numerical_simulation,
  [
    % Grammars
    method/1,
    time/1,
    filter/1,
    % Commands
    numerical_simulation/0,
    continue/1,
    % Public API
    prepare_numerical_simulation_options/1
  ]
).


% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).


:- devdoc('\\section{Grammars}').


:- doc('
  Biocham v4 uses the GNU Scientific Library (GSL \\url{http://www.gnu.org/software/gsl/}) to perform numerical simulations.
  The page
  \\url{http://www.gnu.org/software/gsl/manual/html_node/Stepping-Functions.html#Stepping-Functions}
  gives a detailed description of the numerical integration methods and options listed below.
The implicit method \\texttt{bsimp} for stiff systems is the default one.
').


:- grammar(method).


method(rk2).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta (2, 3) method}').

method(rk4).
:- grammar_doc('\\emphright{Explicit 4th order (classical) Runge-Kutta. Error estimation is carried out by the step doubling method. For more efficient estimate of the error, use the embedded methods described below}').

method(rkf45).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta-Fehlberg (4, 5) method. This method is a good general-purpose integrator}').

method(rkck).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta Cash-Karp (4, 5) method.}').

method(rk8pd).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta Prince-Dormand (8, 9) method.}').

method(rk1imp).
:- grammar_doc('\\emphright{Implicit Gaussian first order Runge-Kutta. Also known as implicit Euler or backward Euler method. Error estimation is carried out by the step doubling method.}').

method(rk2imp).
:- grammar_doc('\\emphright{Implicit Gaussian second order Runge-Kutta. Also known as implicit mid-point rule. Error estimation is carried out by the step doubling method.}').

method(rk4imp).
:- grammar_doc('\\emphright{Implicit Gaussian 4th order Runge-Kutta. Error estimation is carried out by the step doubling method.}').

method(bsimp).
:- grammar_doc('\\emphright{\\emph{Default method}. Implicit Bulirsch-Stoer method of Bader and Deuflhard. The method is generally suitable for stiff problems.}').

method(msadams).
:- grammar_doc('\\emphright{A variable-coefficient linear multistep Adams method in Nordsieck form. This stepper uses explicit Adams-Bashforth (predictor) and implicit Adams-Moulton (corrector) methods in P(EC)^m functional iteration mode. Method order varies dynamically between 1 and 12.}').

method(msbdf).
:- grammar_doc('\\emphright{\\emph{Perhaps the most robust method but may be slow}, a variable-coefficient linear multistep backward differentiation formula (BDF) method in Nordsieck form. This stepper uses the explicit BDF formula as predictor and implicit BDF formula as corrector. A modified Newton iteration method is used to solve the system of non-linear equations. Method order varies dynamically between 1 and 5.}').

method(ssa).
:- grammar_doc('\\emphright{Stochastic simulation of a Continuous-Time Markov Chain,
  defined as per Gillespie''s algorithm \\cite{Gillespie76jcp}. Note that
  there is no conversion of the initial state, besides rounding to the nearest
  integer.}').

method(spn).
:- grammar_doc('\\emphright{Stochastic Petri net simulation. Similar to the SSA
  algorithm above but with a discrete/logical time.}').

method(pn).
:- grammar_doc('\\emphright{Random Petri net simulation run. All transitions are
  equiprobable.}').

method(sbn).
:- grammar_doc('\\emphright{Stochastic Boolean net simulation. Similar to the SSA
  algorithm above but with a discrete/logical time and Boolean states.}').

method(bn).
:- grammar_doc('\\emphright{Random Boolean net simulation run. All transitions are
  equiprobable.}').


:- grammar(time).


time(Number) :-
  number(Number).


%%% filters are defined in library/filter.inc and forward declared in
%%% library/gsl_solver.hh
:- grammar(filter).


filter(no_filter).
:- grammar_doc('\\emphright{Does no filtering at all.}').

filter(only_extrema).
:- grammar_doc('\\emphright{Only keeps the points that are an extremum for some variable.}').


:- initial(option(stochastic_conversion: 100)).
:- initial(option(stochastic_thresholding: 1000)).
:- initial(option(filter: no_filter)).



:- devdoc('\\section{Commands}').


numerical_simulation :-
  biocham_command,
  option(time, time, Time, 'time horizon of the numerical integration'),
  option(method, method, Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of the total time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    stochastic_conversion, number, _,
    'Conversion factor used to scale 1 mole to the given number of molecules.'
  ),
  option(
    stochastic_thresholding, number, _,
    'Do not write (but still compute) stochastic steps below one fraction of
    the total time'
  ),
  option(
    filter, filter, _Filter,
    'filtering function for the trace'
  ),
  doc('performs a numerical simulation from time 0 up to a given time.'),
  (
    Method = ssa
  ->
    stochastic_simulation(Time, continuous, continuous, false)
  ;
    Method = spn
  ->
    stochastic_simulation(Time, continuous, discrete, false)
  ;
    Method = pn
  ->
    stochastic_simulation(Time, boolean, discrete, false)
  ;
    Method = sbn
  ->
    stochastic_simulation(Time, continuous, discrete, true)
  ;
    Method = bn
  ->
    stochastic_simulation(Time, boolean, discrete, true)
  ;
    with_current_ode_system(
      with_clean(
        [
          numerical_simulation:variable/2,
          numerical_simulation:equation/2,
          numerical_simulation:parameter_index/2,
          numerical_simulation:conditional_event/2
        ],
        solve
      )
    )
  ).


continue(Time) :-
  biocham_command,
  type(Time, time),
  doc('Sets the initial state to the last state of the last simulation trace
    and continues for \\argument{Time} time units.'),
  get_table_headers(['Time' | Variables]),
  get_table_data(OldData),
  last(OldData, LastRow),
  LastRow=..[row, LastTime | Values],
  get_initial_values(Variables, InitialValues),
  set_initial_values(Variables, Values),
  % FIXME uses global options, not the ones used for previous simulation
  with_option(time: Time, numerical_simulation),
  get_table_data(NewData),
  translate_data(LastTime, NewData, ContinuedData),
  append(OldData, ContinuedData, FullData),
  HeaderRow =.. [row, '#Time' | Variables],
  add_table(numerical_simulation, [HeaderRow | FullData]),
  set_initial_values(Variables, InitialValues).


prepare_numerical_simulation_options(Options) :-
  get_option(time, Time),
  get_option(method, Method),
  get_option(error_epsilon_absolute, ErrorEpsilonAbsolute),
  get_option(error_epsilon_relative, ErrorEpsilonRelative),
  get_option(initial_step_size, InitialStepSize),
  get_option(maximum_step_size, MaximumStepSize),
  get_option(precision, Precision),
  get_option(filter, Filter),
  check_no_free_identifiers,
  enumerate_variables,
  convert_ode,
  gather_equations(Equations),
  gather_initial_values(InitialValues),
  gather_events(Events),
  gather_fields(Events, Fields),
  gather_initial_parameter_values(InitialParameterValues),
  jacobian(Equations, Jacobian),
  Options = [
    fields: Fields,
    equations: Equations,
    initial_values: InitialValues,
    initial_parameter_values: InitialParameterValues,
    events: Events,
    method: Method,
    error_epsilon_absolute: ErrorEpsilonAbsolute,
    error_epsilon_relative: ErrorEpsilonRelative,
    initial_step_size: InitialStepSize,
    maximum_step_size: MaximumStepSize,
    precision: Precision,
    filter: Filter,
    time_initial: 0,
    time_final: Time,
    jacobian: Jacobian
  ].

:- devdoc('\\section{Private predicate}').


:- dynamic(variable/2).


:- dynamic(equation/2).


:- dynamic(parameter_index/2).


:- dynamic(conditional_event/2).


solve :-
  prepare_numerical_simulation_options(Options),
  solve(Options, Table),
  add_table('numerical_simulation', Table).


gather_fields(Events, Fields) :-
  findall(
    Field,
    (
      Field = 'Time': t
    ;
      enumerate_variables(Field)
    ;
      enumerate_nonconstant_parameters(Events, Field)
    ;
      enumerate_nonparametric_functions(Field)
    ),
    Fields
  ).


enumerate_variables(Header: x(VariableIndex)) :-
  peek_count(variable_counter, VariableCount),
  VariableMax is VariableCount - 1,
  between(0, VariableMax, VariableIndex),
  variable(Molecule, VariableIndex),
%  format(atom(Header), '~a', [Molecule]).
  format(atom(Header), '~w', [Molecule]).


enumerate_nonconstant_parameters(Events, Header: Parameter) :-
  setof(
    ParameterIndex,
    Cond^Action^Value^(
      member(Cond -> Action, Events),
      member(ParameterIndex = Value, Action)
    ),
    NCParameters
  ),
  member(ParameterIndex, NCParameters),
  Parameter = p(ParameterIndex),
  convert_identifier(Header, Parameter).


enumerate_nonparametric_functions(Header: expression(Expr)) :-
  item([kind: function, item: function(Header = Body)]),
  atomic(Header),
  kinetics([], [], Body, NoFuncBody),
  convert_expression(NoFuncBody, Expr).


gather_equations(Equations) :-
  peek_count(variable_counter, VariableCount),
  VariableMax is VariableCount - 1,
  findall(
    Equation,
    (
      between(0, VariableMax, VariableIndex),
      equation(VariableIndex, Equation)
    ),
    Equations
  ).


gather_initial_values(InitialValues) :-
  peek_count(variable_counter, VariableCount),
  VariableMax is VariableCount - 1,
  findall(
    InitialValue,
    (
      between(0, VariableMax, VariableIndex),
      variable(Molecule, VariableIndex),
      get_initial_state(Molecule, State),
      (
        State = present(Concentration)
      ->
        (
          parameter_index(Concentration, Index)
        ->
          InitialValue = p(Index)
        ;
          InitialValue = Concentration
        )
      ;
        InitialValue = 0
      )
    ),
    InitialValues
  ).


gather_initial_parameter_values(InitialParameterValues) :-
  peek_count(parameter_counter, ParameterCount),
  ParameterMax is ParameterCount - 1,
  findall(
    InitialParameterValue,
    (
      between(0, ParameterMax, ParameterIndex),
      parameter_index(Parameter, ParameterIndex),
      parameter_value(Parameter, InitialParameterValue)
    ),
    InitialParameterValues
  ).


gather_events(Events) :-
  findall(
    Event,
    (
      bagof(
        ParameterValue,
        ParameterValues ^ (
          item([kind: event, item: event(Condition, ParameterValues)]),
          member(ParameterValue, ParameterValues)
        ),
        ParameterValues
      ),
      convert_condition(Condition, ConditionIndexed),
      maplist(convert_parameter, ParameterValues, IndexedParameterValues),
      Event = (ConditionIndexed -> IndexedParameterValues)
    ;
      conditional_event(ConditionIndexed, ParameterIndex),
      (
        Event = (ConditionIndexed -> [ParameterIndex = 1])
      ;
        Event = (not ConditionIndexed -> [ParameterIndex = 0])
      )
    ),
    Events
  ).


convert_parameter(Parameter = Value, ParameterIndex = ValueIndexed) :-
  parameter_index(Parameter, ParameterIndex),
  convert_expression(Value, ValueIndexed).


enumerate_variables :-
  set_counter(variable_counter, 0),
  \+ (
    ode(Molecule, _),
    \+ (
      count(variable_counter, VariableIndex),
      assertz(variable(Molecule, VariableIndex))
    )
  ).


convert_ode :-
  set_counter(parameter_counter, 0),
  \+ (
    ode(Molecule, Expression),
    \+ (
      variable(Molecule, VariableIndex),
      convert_expression(Expression, IndexedExpression),
      assertz(equation(VariableIndex, IndexedExpression)),
      % also consider as parameters those used in initial values
      % so that they can be searched
      get_initial_state(Molecule, Initial),
      (
        Initial = present(Parameter),
        parameter_value(Parameter, _)
      ->
        convert_identifier(Parameter, _)
      ;
        true
      )
    )
  ).


convert_condition(Expression, IndexedExpression) :-
  grammar_map(
    condition,
    [
      condition: (numerical_simulation:convert_condition),
      arithmetic_expression: (numerical_simulation:convert_expression)
    ],
    Expression,
    IndexedExpression
  ).


convert_identifier(Molecule, [VariableIndex]) :-
  variable(Molecule, VariableIndex),
  !.

convert_identifier(Parameter, p(ParameterIndex)) :-
  atom(Parameter),
  !,
  (
    parameter_index(Parameter, ParameterIndex)
  ->
    true
  ;
    count(parameter_counter, ParameterIndex),
    assertz(parameter_index(Parameter, ParameterIndex))
  ).


convert_expression([Object], IndexedExpression) :-
  !,
  convert_identifier(Object, IndexedExpression).


convert_expression(if Condition then IfTrue else IfFalse, IndexedExpression) :-
  !,
  convert_condition(Condition, IndexedCondition),
  (
    conditional_event(IndexedCondition, ParameterIndex)
  ->
    true
  ;
    count(parameter_counter, ParameterIndex),
    assertz(conditional_event(IndexedCondition, ParameterIndex))
  ),
  convert_expression(IfTrue, IndexedIfTrue),
  convert_expression(IfFalse, IndexedIfFalse),
  IndexedExpression =
    p(ParameterIndex) * IndexedIfTrue
    + (1 - p(ParameterIndex)) * IndexedIfFalse.


convert_expression(Expression, IndexedExpression) :-
  grammar_map(
    arithmetic_expression,
    [
      number: (=),
      identifier: (numerical_simulation:convert_identifier),
      arithmetic_expression: (numerical_simulation:convert_expression)
    ],
    Expression,
    IndexedExpression
  ).
