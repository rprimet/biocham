:- module(
  sepi,
  [
    % Public API
    search_reduction/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(biocham).



:- doc('This section describes commands to detect model reduction relationships between reaction models based solely on the structure of their reaction graph \\cite{GSF10bi}.
The commands below check the existence of a subgraph epimorphism (SEPI) \\cite{GFS14dam}, i.e. a graph reduction from one graph to a second graph, obtained by deleting and/or merging species and/or reactions. A SAT solver is used to solve this NP-complete problem.').


:- initial(option(mapping_restriction: [])).
:- initial(option(merge_restriction: no)).
:- initial(option(timeout: 180)).

search_reduction(FileName1, FileName2) :-
	biocham_command,
  type(FileName1, input_file),
  type(FileName2, input_file),
	doc('checks whether there exists one model reduction from a first 
		Biocham model to a second model, 
		and returns the first model reduction found, as a description of a graph morphism (sepi) from the reaction graph of the first model to the second. Optionally, a partial mapping of the form  [\'label1\' -> \'label2\', \'label3\' -> \'deleted\'] can be given to restrict the search to reductions satisfying this mapping of molecular names.
Another option restricts the merge operation to (species or reactions) nodes sharing at least one neigbour in the reaction graph.
A timeout option can be given in seconds (default is 180s).'),
  option(mapping_restriction, [basic_influence], Mapping, 'mapping restriction'),
  option(merge_restriction, yesno, Merge, 'merge restriction'),
  option(timeout, number, _Timeout, 'timeout for SAT solver'),
	(get_graphs_infos(FileName1, FileName2, G1, G2),
	(Merge=no
   ->
       once(search_sepi(G1, G2, Stream))
   ;
       once(search_sepi_merge(G1, G2, Stream))),
	once(constraints(G1, G2, Mapping, Stream)),
  close(Stream),
	solve_sat(G1, G2); write('no sepi found\n')), !.



:- doc('\\begin{example}Detecting Michaelis-Menten reductions:').
:- biocham_silent(clear_model).
:- biocham('load(library:examples/sepi/MM1.bc)').
:- biocham('list_model').
:- biocham('load(library:examples/sepi/MM2.bc)').
:- biocham('list_model').
:- biocham('search_reduction(library:examples/sepi/MM2.bc, library:examples/sepi/MM1.bc)').
:- biocham('search_reduction(library:examples/sepi/MM1.bc, library:examples/sepi/MM2.bc, mapping_restriction: [E->A])').
:- doc('\\end{example}').


m_to_morph(I, G1, G2) :-
	G1 = [N1,_, _, Id1],
	G2 = [N2,_, _, Id2],
	(
	I_1 is I - 1,
	(I =< N1 * (N2 + 1), I > 0) ->
	X1 is div(I_1, N2 + 1),
	species(E1, X1, Id1),
	X2 is I_1 mod (N2 + 1),
	((X2 = N2) -> (format("~s -> ~s~n", [E1, "deleted"]));
	species(E2, X2, Id2),
	format("~s -> ~s~n", [E1, E2]))
	); true.

%get the Nth element of a list
nieme(1,[X|_],X) :- !.
nieme(N,[_|R],X) :- N1 is N - 1, nieme(N1,R,X).

% variables for SAT solving :
	% mu(i) = j
	m_ij(I, J, N2, X) :-
		X is ((N2 + 1) * I + J + 1) .

	% mu(i) < j
	m_inf_ij(I, J, N1, N2, X) :-
		m_ij(I, J, N2, X1),
		X is (N1 * (N2 + 1) + X1).

	% mu((i, j)) = (i', j')
	m_arcs(I, J, N1, N2, Narcs2, X) :-
		N1_1 is N1 - 1,
		m_inf_ij(N1_1, N2, N1, N2, X1),
		X is (Narcs2 * I + J + 1 + X1).

	% I is dummy
	is_dummy(I, N1, N2, Narcs1, Narcs2, X) :-
		m_arcs(Narcs1, Narcs2, N1, N2, Narcs2, X1),
		X is X1 + I.

	% I and J merge
	merge(I, J, N1, N2, Narcs1, Narcs2, X) :-
		Na1_1 is Narcs1 - 1,
		is_dummy(Na1_1, N1, N2, Na1_1, Narcs2, X1),
		X is (X1 + N1 * I + J + 1).
		%debug : format("merge (~d ~d) = ~d~n", [I, J, X]).

	% variable for Tseitin transformation 
	% tseitin(I, A, B) implies mu(a) = i & mu(b) = i
	tseitin(I, A, B, N1, N2, Narcs1, Narcs2, X) :-
		merge(N1, N1, N1, N2, Narcs1, Narcs2, X1),
		X is (X1 + N1 * A + B + 1 + I * N1 * N1).
		%debug : format("~d ~d ~d ~d~n", [X, I, A, B]).




% logical traduction to CnF :
	implies(A, B, Stream) :-
		A1 is -A,
		format(Stream, '~d ~d 0~n', [A1, B]).

	a_and_b_implies(A, B, C, Stream) :-
		A1 is -A,
		B1 is -B,
		format(Stream, '~d ~d ~d 0~n', [A1, B1, C]).


% mu(i) < j => mu(i) != j
inf_imp_not_equ(I, J, N1, N2, Stream) :-
	m_inf_ij(I, J, N1, N2, X1),
	m_ij(I, J, N2, X2),
	X3 is -X2,
	implies(X1, X3, Stream).

% mu(i) = j => mu(i) < j + 1
equ_impl_inf(I, J, N1, N2, Stream) :-
	J1 is J + 1,
	m_ij(I, J, N2, X1),
	m_inf_ij(I, J1, N1, N2, X2),
	implies(X1, X2, Stream).

% mu(i) < j => mu(i) < j + 1
inf_impl_inf(I, J, N1, N2, Stream) :-
	J1 is J + 1,
	m_inf_ij(I, J, N1, N2, X1),
	m_inf_ij(I, J1, N1, N2, X2),
	implies(X1, X2, Stream).


left_totality(N1, N2, Stream) :-
	N1_1 is N1 - 1,
	left_totality(N1_1, N2, 0, 0, Stream).
left_totality(N1, N2, N1, N, Stream) :-
	N is N2 + 1,
	write(Stream, "0\n"), !.
left_totality(N1, N2, I, N, Stream) :-
	N is N2 + 1,
	I1 is I + 1,
	write(Stream, "0\n"),
	left_totality(N1, N2, I1, 0, Stream).
left_totality(N1, N2, I, J, Stream) :-
	m_ij(I, J, N2, X),
	format(Stream, "~d ", [X]),
	J1 is J + 1,
	left_totality(N1, N2, I, J1, Stream).

right_totality(N1, N2, Stream) :-
	N2_1 is N2 - 1,
	right_totality(N1, N2_1, 0, 0, Stream).
right_totality(N1, N2, N1, N2, Stream) :-
	write(Stream, "0\n"), !.
right_totality(N1, N2, N1, J, Stream) :-
	J1 is J + 1,
	write(Stream, "0\n"),
	right_totality(N1, N2, 0, J1, Stream).
right_totality(N1, N2, I, J, Stream) :-
	N is N2 + 1,
	m_ij(I, J, N, X),
	write(Stream, X), write(Stream, " "),
	I1 is I + 1,
	right_totality(N1, N2, I1, J, Stream).

functionality(N1, N2, Stream) :-
	functionality(N1, N2, 0, 0, Stream).
functionality(N1, N2, N, N2, Stream) :-
	N is N1 - 1,
	inf_imp_not_equ(N, N2, N1, N2, Stream).
functionality(N1, N2, I, N2, Stream) :-
	I1 is I + 1,
	inf_imp_not_equ(I, N2, N1, N2, Stream),
	functionality(N1, N2, I1, 0, Stream).
functionality(N1, N2, I, J, Stream) :-
	J1 is J + 1,
	inf_imp_not_equ(I, J, N1, N2, Stream),
	inf_impl_inf(I, J, N1, N2, Stream),
	equ_impl_inf(I, J, N1, N2, Stream),
	functionality(N1, N2, I, J1, Stream).


left_totality_arcs(N1, N2, Na1, Na2, Stream)  :-
	Na1_1 is Na1 - 1,
	left_totality_arcs(N1, N2, Na1_1, Na2, 0, 0, Stream).
left_totality_arcs(N1, N2, Na1, Na2, Na1, Na2, Stream) :-
	is_dummy(Na1, N1, N2, Na1, Na2, X),
	format(Stream, '~d 0\n', X).
left_totality_arcs(N1, N2, Na1, Na2, I, Na2, Stream) :-
	I1 is I + 1,
	is_dummy(I, N1, N2, Na1, Na2, X),
	format(Stream, '~d 0\n', X),
	left_totality_arcs(N1, N2, Na1, Na2, I1, 0, Stream).
left_totality_arcs(N1, N2, Na1, Na2, I, J, Stream) :-
	m_arcs(I, J, N1, N2, Na2, X),
	format(Stream, '~d ', X),
	J1 is J + 1,
	left_totality_arcs(N1, N2, Na1, Na2, I, J1, Stream).

right_totality_arcs(N1, N2, Na1, Na2, Stream) :-
	Na2_1 is Na2 - 1,
	right_totality_arcs(N1, N2, Na1, Na2_1, 0, 0, Stream).
right_totality_arcs(_, _, Na1, Na2, Na1, Na2, Stream) :-
	write(Stream, "0\n"), !.
right_totality_arcs(N1, N2, Na1, Na2, Na1, J, Stream) :-
	J1 is J + 1,
	write(Stream, "0\n"),
	right_totality_arcs(N1, N2, Na1, Na2, 0, J1, Stream).
right_totality_arcs(N1, N2, Na1, Na2, I, J, Stream) :-
	N is Na2 + 1,
	m_arcs(I, J, N1, N2, N, X),
	format(Stream, '~d ', X),
	I1 is I + 1,
	right_totality_arcs(N1, N2, Na1, Na2, I1, J, Stream).



get_edge_ij(Arcs, I, J, X) :-
	nieme(I, Arcs, X1),
	X1 = (A, B),
	(J = 0 -> X = A; true),
	(J = 1 -> X = B; true).

write_graph_morph(N1, N2, Arcs1, Arcs2, I, J, Stream) :-
	J1 is J + 1,
	I1 is I + 1,
	length(Arcs2, Na2),
	get_edge_ij(Arcs1, I1, 0, A1i0),
	get_edge_ij(Arcs1, I1, 1, A1i1),
	get_edge_ij(Arcs2, J1, 0, A2j0),
	get_edge_ij(Arcs2, J1, 1, A2j1),
	m_arcs(I, J, N1, N2, Na2, X1),
	m_ij(A1i0, A2j0, N2, X2),
	m_ij(A1i1, A2j1, N2, X3),
	implies(X1, X2, Stream),
	implies(X1, X3, Stream),
	a_and_b_implies(X2, X3, X1, Stream).

graph_morph(N1, N2, Arcs1, Arcs2, Stream) :-
	graph_morph(N1, N2, Arcs1, Arcs2, 0, 0, Stream).
graph_morph(N1, N2, Arcs1, Arcs2, I, Na2, Stream) :-
	length(Arcs2, Na2),
	I1 is I + 1,
	graph_morph(N1, N2, Arcs1, Arcs2, I1, 0, Stream).
graph_morph(N1, N2, Arcs1, Arcs2, Na1, Na2, Stream) :-
	length(Arcs1, Na1_1),
	length(Arcs2, Na2_1),
	Na1 is  Na1_1 - 1,
	Na2 is Na2_1 - 1,
	write_graph_morph(N1, N2, Arcs1, Arcs2, Na1, Na2, Stream), !.
graph_morph(N1, N2, Arcs1, Arcs2, I, J, Stream) :-
	J1 is J + 1,
	write_graph_morph(N1, N2, Arcs1, Arcs2, I, J, Stream),
	graph_morph(N1, N2, Arcs1, Arcs2, I, J1, Stream).


write_subgraph_morph(N1, N2, Arcs1, Arcs2, I, Stream) :-
	I1 is I + 1,
	length(Arcs2, Na2),
	length(Arcs1, Na1),
	Na1_1 is Na1 - 1,
	is_dummy(I, N1, N2, Na1_1, Na2, X1),
	get_edge_ij(Arcs1, I1, 0, A1i0),
	get_edge_ij(Arcs1, I1, 1, A1i1),
	m_ij(A1i0, N2, N2, X2),
	m_ij(A1i1, N2, N2, X3),
	X_2 is -X2,
	a_and_b_implies(X1, X_2, X3, Stream),
	implies(X2, X1, Stream),
	implies(X3, X1, Stream).

subgraph_morph(N1, N2, Arcs1, Arcs2, Stream) :-
	subgraph_morph(N1, N2, Arcs1, Arcs2, 0, Stream).
subgraph_morph(N1, N2, Arcs1, Arcs2, Na1, Stream) :-
	length(Arcs1, Na1_1),
	Na1 is Na1_1 - 1,
	write_subgraph_morph(N1, N2, Arcs1, Arcs2, Na1, Stream).
subgraph_morph(N1, N2, Arcs1, Arcs2, I, Stream) :-
	I1 is I + 1,
	write_subgraph_morph(N1, N2, Arcs1, Arcs2, I, Stream),
	subgraph_morph(N1, N2, Arcs1, Arcs2, I1, Stream).

is_in_arcs(I, J, [(I, J)|_]).
is_in_arcs(I, J, [_|T]) :- is_in_arcs(I, J, T).


write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, Stream) :-
	write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, 0, Stream).
write_redundant_morph(_, N2, Arcs1, Arcs2, I, J, N2_1, Stream) :-
	N2_1 is N2 - 1,
	I1 is I + 1,
	(not(is_in_arcs(J, N2_1, Arcs2)) -> 
	 (get_edge_ij(Arcs1, I1, 0, E1),
	 get_edge_ij(Arcs1, I1, 1, E2),
	 m_ij(E1, J, N2, X1),
	 m_ij(E2, N2_1, N2, X2),
	 X_2 is -X2,
	 implies(X1, X_2, Stream)); true).
write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, K, Stream) :-
	K1 is K + 1,
	I1 is I + 1,
	(not(is_in_arcs(J, K, Arcs2)) -> 
	 (get_edge_ij(Arcs1, I1, 0, E1),
	 get_edge_ij(Arcs1, I1, 1, E2),
	 m_ij(E1, J, N2, X1),
	 m_ij(E2, K, N2, X2),
	 X_2 is -X2,
	 implies(X1, X_2, Stream)); true),
	write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, K1, Stream).
	

redundant_morph_prop(N1, N2, Arcs1, Arcs2, Stream) :-
	redundant_morph_prop(N1, N2, Arcs1, Arcs2, 0, 0, Stream).
redundant_morph_prop(_, _, Arcs1, _, Na1, _, _) :-
	length(Arcs1, Na1).
redundant_morph_prop(N1, N2, Arcs1, Arcs2, I, N2_1, Stream) :-
	I1 is I + 1,
	N2_1 is N2 - 1,
	write_redundant_morph(N1, N2, Arcs1, Arcs2, I, N2_1, Stream),
	redundant_morph_prop(N1, N2, Arcs1, Arcs2, I1, 0, Stream).
redundant_morph_prop(N1, N2, Arcs1, Arcs2, I, J, Stream) :-
	J1 is J + 1,
	write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, Stream),
	redundant_morph_prop(N1, N2, Arcs1, Arcs2, I, J1, Stream).


partial_surj(N1, N2, Stream) :-
	N1 >= N2,
	left_totality(N1, N2, Stream),
	right_totality(N1, N2, Stream),
	functionality(N1, N2, Stream).

subgraph_epi(G1, G2, Stream) :-
	G1 = [N1, N1r, Arcs1, _],
	G2 = [N2, N2r, Arcs2, _],
	length(Arcs1, Na1),
	length(Arcs2, Na2),
	left_totality_arcs(N1, N2, Na1, Na2, Stream),
	right_totality_arcs(N1, N2, Na1, Na2, Stream),
	graph_morph(N1, N2, Arcs1, Arcs2, Stream),
	subgraph_morph(N1, N2, Arcs1, Arcs2, Stream),
	redundant_morph_prop(N1, N2, Arcs1, Arcs2, Stream),
	bigraph_constraint(N1, N2, N1r, N2r, Stream).

bigraph_constraint(N1, N2, N1r, N2r, Stream) :-
	bigraph_constraint(N1, N2, N1r, N2r, 0, 0, Stream).
bigraph_constraint(N1, N2, _, _, N1_1, N2_1, _) :-
	N2_1 is N2 - 1,
	N1_1 is N1 - 1, !.
bigraph_constraint(N1, N2, N1r, N2r, I, N2_1, Stream) :-
	N2_1 is N2 - 1,
	I1 is I + 1,
	bigraph_constraint(N1, N2, N1r, N2r, I1, 0, Stream).
bigraph_constraint(N1, N2, N1r, N2r, I, J, Stream) :-
	(((not((I < N1r, J < N2r); (I >= N1r, J >= N2r)), 
	(m_ij(I, J, N2, X), X1 is -X, format(Stream, "~d 0~n", X1))); true), 
	J1 is J + 1,
	bigraph_constraint(N1, N2, N1r, N2r, I, J1, Stream)); true.

write_morph(Tabint, G1, G2) :-
	write_morph(Tabint, G1, G2, 0).
write_morph(_, [N1|_], [N2|_], N) :-
	N is N1 * (N2 + 1) + 1.
write_morph([X|Tail], G1, G2, I) :-
	X = "SAT",
	write_morph(Tail, G1, G2, I).
write_morph([X|Tail], G1, G2, I) :-
	atom_number(X, Xnum),
	(m_to_morph(Xnum, G1, G2); true),
	I1 is I + 1,
	write_morph(Tail, G1, G2, I1).


	
run_glucose(Filename) :-
	process_create(path(glucose), ['-verb=0',Filename, '/tmp/out.sat'],
  [process(Id), stderr(null), stdout(null)]),
  get_option(timeout,Timeout),
	catch(call_with_time_limit(Timeout,wait(Id,_)), _, (process_kill(Id), write('\ntimed out '), write(Timeout), write('sec\n'))).


:- dynamic(species/3).

number_species(Id) :-
	set_counter(species, 0),
	retractall(species(_, _, Id)),
	forall((item([parent: Id, kind: vertex, item: VertexName]), not(get_attribute(Id, VertexName, kind = transition))), 
		(count(species, Count), assertz(species(VertexName, Count, Id)))); true.

number_reactions(Id) :-
	peek_count(species, Nsp),
	set_counter(reactions, 0),
	forall((item([parent: Id, kind: vertex, item: Item]), get_attribute(Id, Item, kind = transition)),
		(count(reactions, Count), N is Nsp + Count, assertz(species(Item, N, Id)))); true.


set_list(ListName, InitialList) :-
	nb_setval(ListName, InitialList).

add_to_list(ListName, AddToList) :-
	nb_getval(ListName, List),
	NextList = [AddToList | List],
	nb_setval(ListName, NextList).

peek_list(ListName, List) :-
	nb_getval(ListName, List).


list_edges(Id) :-
	set_list(edges, []),
	forall(item([parent: Id, kind: edge, item: Edge, id: _]),
		add_to_list(edges, Edge)); true.

unlable_edges(LabledEdges, Edges, Id) :-
	unlable_edges(LabledEdges, [], Edges, Id),
	length(Edges, _).

unlable_edges([], Edges, E, _) :-
	E = Edges.
unlable_edges([(VertexA -> VertexB)|LabledEdges], Edges, E, Id) :-
	species(VertexA, Na, Id),
	species(VertexB, Nb, Id),
	unlable_edges(LabledEdges, [(Na, Nb)|Edges], E, Id).

get_graph_info(FileName, Ns, Edges, N, Id) :-
	load(FileName),
	reaction_graph,
	get_current_graph(Id),
	number_species(Id),
	number_reactions(Id),
	list_edges(Id),
	peek_count(species, Ns),
	peek_count(reactions, Nr),
	N is Ns + Nr,
	peek_list(edges, LabEdges),
	unlable_edges(LabEdges, Edges, Id).

get_graphs_infos(FileName1, FileName2, G1, G2) :-
	once(get_graph_info(FileName1, Ns1, E1, N1, Id1)),
	once(get_graph_info(FileName2, Ns2, E2, N2, Id2)),
	G1 = [N1, Ns1, E1, Id1],
	G2 = [N2, Ns2, E2, Id2],
	N1 >= N2,
	Ns1 >= Ns2.

label_constraints(G1, G2, Stream) :-
	G1 = [_, Ns1, _, Id1],
	G2 = [N2, Ns2, _, Id2],
	forall((item([parent: Id1, kind: vertex, item: VertexName]), not(get_attribute(Id1, VertexName, kind = transition)),
			item([parent: Id2, kind: vertex, item: VertexName]), not(get_attribute(Id2, VertexName, kind = transition))),
			(species(VertexName, X1, Id1), species(VertexName, X2, Id2), X1 < Ns1, X2 < Ns2, m_ij(X1, X2, N2, X), m_ij(X1, N2, N2, Dummy), format(Stream, "~d ~d 0~n", [X, Dummy]))), !.

constraints(_, _, [], _).
constraints(G1, G2, [(Label1 -> deleted)|T], Stream) :-
	G1 = [_, _, _, Id1],
	G2 = [N2, _, _, _],
	species(Label1, X, Id1),
	m_ij(X, N2, N2, Dummy),
	format(Stream, "~d 0~n", [Dummy]),
	constraints(G1, G2, T, Stream), !.
constraints(G1, G2, [(Label1 -> Label2)|T], Stream) :-
	G1 = [_, _, _, Id1],
	G2 = [N2, _, _, Id2],
	species(Label1, X1, Id1),
	species(Label2, X2, Id2),
	m_ij(X1, X2, N2, X),
	format(Stream, "~d 0~n", [X]),
	constraints(G1, G2, T, Stream).
constraints(G1, G2, [(_Label1 -< _Label2)|T], Stream) :-
  print_message(warning, 'mapping restriction -< is ignored in search_reduction'),
	constraints(G1, G2, T, Stream).



write_neg_sol(_, [], _, _).
write_neg_sol(Stream, [H|_], N1, N2) :-
	atom_number(H, Hnum),
	(Hnum is N1 * (N2 + 1); Hnum is -N1 * (N2 + 1)),
	H_ is -Hnum,
	format(Stream, '~d 0~n', H_).
write_neg_sol(Stream, [H|T], N1, N2) :-
	atom_number(H, Hnum),
	H_ is -Hnum,
	format(Stream, '~d ', [H_]),
	write_neg_sol(Stream, T, N1, N2).

add_solution(G1, G2, String) :-
	G1 = [N1|_],
	G2 = [N2|_],
	open('/tmp/in.sat', append, Stream),
	split_string(String, " ", "", Tabint),
	write_morph(Tabint, G1, G2),
	write_neg_sol(Stream, Tabint, N1, N2),
	close(Stream).

% solve_sat(+Graph1,+Graph2)
%
% search and write a SEpi between the 2 input graphs
% /!\ this function useq indirectly /tmp/in.sat

solve_sat(G1, G2) :-
	run_glucose('/tmp/in.sat'),
	open('/tmp/out.sat', read, Stream, []),
	read_string(Stream, '', "\n\r", _, String),
	(
		(
			String = "UNSAT",
			write('no sepi found\n')
		)
	;
		(
			split_string(String, " \n", " \n", Tabint)
		),
		write_morph(Tabint, G1, G2)
	); 
	write('no sepi found\n'); 
	true.

search_sepi(G1, G2, Stream) :-
	open('/tmp/in.sat', write, Stream),
	G1 = [N1| _],
	G2 = [N2| _],
	partial_surj(N1, N2, Stream),
	subgraph_epi(G1, G2, Stream).

search_sepi_merge(G1, G2, Stream) :-
	search_sepi(G1, G2, Stream),
	merge_cond(G1, G2, Stream),
	restrict_merge(G1, G2, Stream), !.


merge_cond(G1, G2, Stream) :-
	G1 = [N1, _, E1, Id1],
	G2 = [N2, _, E2, _],
	length(E1, Na1),
	length(E2, Na2),
	forall((item([parent: Id1, kind: vertex, item: VertexName1]), not(get_attribute(Id1, VertexName1, kind = transition)),
			item([parent: Id1, kind: vertex, item: VertexName2]), not(get_attribute(Id1, VertexName2, kind = transition)),
			species(VertexName1, X1, Id1), species(VertexName2, X2, Id1), not(X1 = X2)), 
			(write_merge_cond(X1, X2, N1, N2, Na1, Na2, Stream), write_merge_cond2(X1, X2, N1, N2, Na1, Na2, Stream), merge(X1, X2, N1, N2, Na1, Na2, M1), merge(X2, X1, N1, N2, Na1, Na2, M2), format(Stream, "-~d ~d 0~n-~d ~d 0~n", [M1, M2, M2, M1]))).



% write_merge_cond and write_merge_cond2 : writes the equivalance : merge(a,b) <-> exist x such that mu(a) = x and mu(b) = x
write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, Stream) :-
	write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, 0, Stream).
write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, N2_1, Stream) :-
	N2_1 is N2 - 1,
	merge(A, B, N1, N2, Narcs1, Narcs2, X),
	m_ij(A, N2_1, N2, X1),
	m_ij(B, N2_1, N2, X2),
	tseitin(N2_1, A, B, N1, N2, Narcs1, Narcs2, X3),
	format(Stream, "-~d ~d 0~n", [X3, X1]),
	format(Stream, "-~d ~d 0~n", [X3, X2]),
	format(Stream, "~d -~d -~d 0~n", [X, X1, X2]), !.
write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, I, Stream) :-
	merge(A, B, N1, N2, Narcs1, Narcs2, X),
	m_ij(A, I, N2, X1),
	m_ij(B, I, N2, X2),
	tseitin(I, A, B, N1, N2, Narcs1, Narcs2, X3),
	format(Stream, "-~d ~d 0~n", [X3, X1]),
	format(Stream, "-~d ~d 0~n", [X3, X2]),
	format(Stream, "~d -~d -~d 0~n", [X, X1, X2]),
	I1 is I + 1,
	write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, I1, Stream).

write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, Stream) :-
	merge(A, B, N1, N2, Narcs1, Narcs2, X),
	format(Stream, "-~d ", [X]),
	write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, 0, Stream).
write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, N2_1, Stream) :-
	N2_1 is N2 - 1,
	tseitin(N2_1, A, B, N1, N2, Narcs1, Narcs2, X),
	format(Stream, "~d 0~n", [X]), !.
write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, I, Stream) :-
	tseitin(I, A, B, N1, N2, Narcs1, Narcs2, X),
	format(Stream, "~d ", [X]),
	I1 is I + 1,
	write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, I1, Stream).


% merge restriction conditions : 
restrict_merge(G1, G2, Stream) :-
	(G1 = [_, Ns1, E1, Id1],
	G2 = [_, _, E2, _],
	length(E1, Na1),
	length(E2, Na2),
	forall((item([parent: Id1, kind: vertex, item: VertexName1]),
			item([parent: Id1, kind: vertex, item: VertexName2]),
			not(VertexName1 = VertexName2),
			(species(VertexName1, X1, Id1), X1 < Ns1,
			species(VertexName2, X2, Id1), X2 < Ns1); (species(VertexName1, X1, Id1), X1 > Ns1,species(VertexName2, X2, Id1), X2 > Ns1)),

			(((((item([parent: Id1, kind: edge, item: (VertexName1 -> React)]); item([parent: Id1, kind: edge, item: (React -> VertexName1)])),
			(item([parent: Id1, kind: edge, item: (VertexName2 -> React)]); item([parent: Id1, kind: edge, item: (React -> VertexName2)]))) -> true); 
			write_restrict_merge(VertexName1, VertexName2, G1, G2, Na1, Na2, Stream); true))))
		; true.

write_restrict_merge(Vertex1, Vertex2, G1, G2, Na1, Na2, Stream) :-
	G1 = [N1, _, _, Id1],
	G2 = [N2, _, _, _],
	species(Vertex1, V1, Id1),
	species(Vertex2, V2, Id1),	
	merge(V1, V2, N1, N2, Na1, Na2, Me),
	format(Stream, "-~d ", [Me]),
	forall(((item([parent: Id1, kind: edge, item: (Vertex1 -> React)]); item([parent: Id1, kind: edge, item: (React -> Vertex1)])),
			(item([parent: Id1, kind: edge, item: (Vertex3 -> React)]); item([parent: Id1, kind: edge, item: (React -> Vertex3)])), not(Vertex1 = Vertex3)),
			(species(Vertex3, V3, Id1), merge(V1, V3, N1, N2, Na1, Na2, X), format(Stream, "~d ", [X]))),
	format(Stream, "0~n", []), !.
