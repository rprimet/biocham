:- module(
  qual_files,
  [
    load_qual/1,
    add_qual/1,
    load_ginml/1,
    add_ginml/1,
    % API
    add_qual_file/1,
    add_ginml_file/1
  ]
).

:- use_module(library(sgml)).
:- use_module(library(xpath)).

:- devdoc('\\section{Commands}').


load_qual(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{load_biocham/1} but importing influences and
    initial state (and only that!) from an SBML3qual .sbml file.
  '),
  load_all('sbml', InputFile).


add_qual(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{add_biocham/1} but importing influences and
    initial state (and only that!) from an SBML3qual .sbml file.
  '),
  models:add_all('sbml', InputFile).


load_ginml(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{load_biocham/1} but importing influences
    from a GINsim .ginml file.
  '),
  load_all('ginml', InputFile).


add_ginml(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{add_biocham/1} but importing influences
    from a GINsim .ginml file.
  '),
  models:add_all('ginml', InputFile).


:- devdoc('\\section{Public API}').


models:add_file_suffix('sbml', add_qual_file).
models:add_file_suffix('ginml', add_ginml_file).


:- dynamic(qual_species/1).


add_qual_file(Filename) :-
  % FIXME should check for level=3 and qual:required=true in attributes of
  % sbml element
  load_xml(Filename, [element(sbml, _, Model)], [space(remove)]),
  retractall(qual_species(_)),
  forall(
    (
      xpath(Model, '//'('qual:qualitativeSpecies'),
        element('qual:qualitativeSpecies', Attributes, [])),
      member('qual:id'=Id, Attributes)
    ),
    (
      member('qual:initialLevel'=Level, Attributes)
    ->
      assertz(qual_species(Id)),
      present(Id, Level)
    ;
      assertz(qual_species(Id))
    )
  ),
  forall(
    xpath(Model, '//'('qual:transition'),
      element('qual:transition', _, Transition)),
    add_qual_transition(Transition)
  ).


add_ginml_file(Filename) :-
  load_xml(Filename, [element(gxl, _, Graph)], [space(remove)]),
  forall(
    (
      xpath(
        Graph,
        '//'('node'),
        % FIXME check maxvalue?
        element('node', [id = Id, maxvalue = _], Node)
      ),
      member(
        element(value, [val='1'], [element(exp, [str=Function], [])]),
        Node
      )
    ),
    (
      assertz(qual_species(Id)),
      add_ginml_function(Function, Id)
    )
  ).


:- dynamic(function_terms/2).
:- dynamic(default_term/1).


add_qual_transition(Transition) :-
  retractall(function_terms(_, _)),
  retractall(default_term(_)),
  (
    member(element('qual:listOfOutputs', [], Outputs), Transition)
  ->
    add_qual_transition(Outputs, Transition)
  ;
    true
  ).


add_qual_transition([], _Transition).

add_qual_transition([element('qual:output', Output, []) | ListOfOutputs], Transition) :-
  member('qual:qualitativeSpecies'=OutputId, Output),
  member(element('qual:listOfFunctionTerms', [], Functions), Transition),
  (
    % already computed for another output
    default_term(_)
  ->
    add_qual_functions([], OutputId)
  ;
    add_qual_functions(Functions, OutputId)
  ),
  add_qual_transition(ListOfOutputs, Transition).


add_qual_functions([], OutputId) :-
  default_term(
    element('qual:defaultTerm', ['qual:resultLevel'=ResultLevel], [])
  ),
  atom_bool(ResultLevel, AtomLevel),
  forall(
    function_terms(MathElt, Level),
    add_qual_influence(MathElt, OutputId, Level)
  ),
  findall(Cond, function_terms(Cond, _), Conds),
  sort(Conds, SConds),
  findall(
    SimpleCond,
    (
      maplist(member, NonCond, SConds),
      \+ (member(X, NonCond), member(not(X), NonCond)),
      notlist(NonCond, NegatedCond),
      sort(NegatedCond, SimpleCond)
    ),
    CondList
  ),
  simplify_conds(CondList, SCondList),
  forall(
    member(SimpleCond, SCondList),
    add_qual_influence(SimpleCond, OutputId, AtomLevel)
  ).

add_qual_functions([Function | L], OutputId) :-
  Function = element('qual:functionTerm', ['qual:resultLevel'=Level], [Maths]),
  !,
  atom_bool(Level, Result),
  Maths = element(math, _NameSpace, [MathElt]),
  mathml_to_bool(MathElt, Bool),
  dnf(Bool, DBool),
  bool_simplify(DBool, SBool),
  forall(
    member(OBool, SBool),
    assertz(function_terms(OBool, Result))
  ),
  add_qual_functions(L, OutputId).

add_qual_functions([Default | L], OutputId) :-
  assertz(default_term(Default)),
  add_qual_functions(L, OutputId).


add_qual_influence(L, OutputId, Level) :-
  pos_neg(L, Pos, Neg),
  level_arrow(Level, Arrow),
  influence_editor:inputs(Inputs, Pos, Neg),
  Influence =.. [Arrow, Inputs, OutputId],
  add_influence(Influence).

add_qual_influence(false, _, _).


add_ginml_function(String, Id) :-
  str_to_bool(String, Bool),
  % avoid self loops
  dnf(and(not(Id), Bool), DBool),
  bool_simplify(DBool, SBool),
  forall(
    member(OBool, SBool),
    add_qual_influence(OBool, Id, 1)
  ),
  dnf(and(Id, not(Bool)), NDBool),
  bool_simplify(NDBool, NSBool),
  forall(
    member(OBool, NSBool),
    add_qual_influence(OBool, Id, 0)
  ).


level_arrow(Level, Arrow) :-
  (
    Level = 0
  ->
    Arrow = -<
  ;
    Arrow = '->'
  ).


pos_neg([], [], []).

pos_neg([not(A) | L], Pos, [A | Neg]) :-
  !,
  pos_neg(L, Pos, Neg).

pos_neg([A | L], [A | Pos], Neg) :-
  pos_neg(L, Pos, Neg).


mathml_to_bool(element(apply, [], [element(Functor, [], []) | Args]), B) :-
  maplist(mathml_to_bool, Args, BArgs),
  B =.. [Functor | BArgs].

mathml_to_bool(element(ci, [], [Id]), Bool) :-
  (
    qual_species(Id)
  ->
    Bool = Id
  ;
    % Id is a threshold, so we consider it equal to 1
    Bool = 1
  ).

mathml_to_bool(element(cn, [type=integer], [Int]), Bool) :-
  atom_bool(Int, Bool).


atom_bool(Atom, Bool) :-
  atom_number(Atom, Int),
  (
    Int > 0
  ->
    Bool = 1
  ;
    Bool = 0
  ).


bool_simplify_lit(geq(_, 0), true) :-
  !.

bool_simplify_lit(geq(X, 1), X) :-
  !.

bool_simplify_lit(gt(X, _), X) :-
  !.

bool_simplify_lit(leq(_, 1), true) :-
  !.

bool_simplify_lit(leq(X, 0), not(X)) :-
  !.

bool_simplify_lit(lt(X, _), not(X)) :-
  !.

bool_simplify_lit(eq(X, 0), not(X)) :-
  !.

bool_simplify_lit(eq(X, 1), X) :-
  !.

bool_simplify_lit(X, X).


bool_simplify_and(L, And) :-
  !,
  maplist(bool_simplify_lit, L, LL),
  list_to_set(LL, S),
  subtract(S, [true], LLL),
  (
    LLL = []
  ->
    And = true
  ;
    memberchk(false, LLL)
  ->
    And = false
  ;
    member(X, LLL),
    member(not(X), LLL)
  ->
    And = false
  ;
    And = LLL
  ).


bool_simplify_or(L, Or) :-
  !,
  maplist(bool_simplify_and, L, LL),
  list_to_set(LL, S),
  subtract(S, [false], LLL),
  (
    memberchk(true, LLL)
  ->
    Or = true
  ;
    Or = LLL
  ).


bool_simplify(X, Y) :-
  bool_simplify_or(X, Y).


% handle n-ary functors
dnf(Term, Dnf) :-
  Term =.. [Functor | Args],
  length(Args, NArgs),
  NArgs > 2,
  !,
  Args = [FirstArg | OtherArgs],
  T =.. [Functor | OtherArgs],
  TT =.. [Functor, FirstArg, T],
  dnf(TT, Dnf).

dnf(and(X, Y), And) :-
  !,
  dnf(X, XX),
  dnf(Y, YY),
  findall(
    C,
    (
      member(LX, XX),
      member(LY, YY),
      append(LX, LY, C)
    ),
    And
  ).

dnf(or(X, Y), Or) :-
  !,
  dnf(X, XX),
  dnf(Y, YY),
  append(XX, YY, Or).

dnf(not(or(X, Y)), Not) :-
  !,
  dnf(and(not(X), not(Y)), Not).

dnf(not(and(X, Y)), Not) :-
  !,
  dnf(or(not(X), not(Y)), Not).

dnf(not(not(X)), Not) :-
  !,
  dnf(X, Not).

dnf(X, [[X]]).


notlist([], []).

notlist([not(H) | T], [H | NotT]) :-
  !,
  notlist(T, NotT).

notlist([H | T], [not(H) | NotT]) :-
  notlist(T, NotT).


str_to_bool(String, Bool) :-
  atom_substitute(
    [('&', ' and '), ('|', ' or '), ('!', 'not ')],
    String,
    CString
  ),
  read_term_from_atom(CString, Bool, [variable_names(V)]),
  name_variables_and_anonymous(Bool, V).


atom_substitute([], A, A).

atom_substitute([(From, To) | H], A, B) :-
  split_string(A, From, "", L),
  atomic_list_concat(L, To, AA),
  atom_substitute(H, AA, B).


simplify_conds([], []).

simplify_conds([H | T], Conds) :-
  (
    member(HH, T),
    ord_subset(HH, H)
  ->
    % strictly subsumed
    simplify_conds(T, Conds)
  ;
    member(HH, T),
    ord_subset(H, HH)
  ->
    % strictly subsuming
    subtract(T, [HH], TT),
    simplify_conds([H | TT], Conds)
  ;
    member(HH, T),
    ord_symdiff(H, HH, [X, not(X)])
  ->
    % two conditions differing only by X and not X, X is irrelevant
    subtract(T, [HH], TT),
    ord_intersection(H, HH, HHH),
    simplify_conds([HHH | TT], Conds)
  ;
    Conds = [H | TT],
    simplify_conds(T, TT)
  ).
