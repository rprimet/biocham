define([
    './commands'
], function(Commands) {
    
    function give_id(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].id = i + 1;
        }

        let curr_chapter, curr_section;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i].kind === 'chapter') {
                curr_chapter = arr[i].id;
            }
            else if (arr[i].kind === 'section') {
                curr_section = arr[i].id;
                arr[i].parent_id = curr_chapter;
            }
            else if (arr[i].kind === 'command') {
                // add args for option command for the GUI
                if (arr[i].name === 'option') {
                    arr[i].arguments = [{name: 'Option', type: 'option'}];
                }
                if (curr_section > curr_chapter) {
                    arr[i].parent_id = curr_section;
                }
                else {
                    arr[i].parent_id = curr_chapter;
                }
            }
        }
        return arr;
    }


    function unflatten(arr) {
        let tree = [],
            mapped_arr = {},
            arr_elem,
            mapped_elem;

        // First map the nodes of the array to an object -> create a hash table.
        for (let i = 0, len = arr.length; i < len; i++) {
            arr_elem = arr[i];
            mapped_arr[arr_elem.id] = arr_elem;
            mapped_arr[arr_elem.id].children = [];
        }


        for (let id in mapped_arr) {
            if (mapped_arr.hasOwnProperty(id)) {
                mapped_elem = mapped_arr[id];
                // If the element is not at the root level, add it to its parent array of children.
                if (mapped_elem.parent_id) {
                    mapped_arr[mapped_elem['parent_id']].children.push(mapped_elem);
                }
                // If the element is at the root level, add it to first level elements array.
                else {
                    tree.push(mapped_elem);
                }
            }
        }
        return tree;
    }

    function make_menu(tree) {
        let menu = [];
        for (let i=0; i<tree.length; i++) {
            if (tree[i].children.length > 0) {
                menu.push({name: tree[i].name,
                           children: make_menu(tree[i].children)});
            }

            else {
                if (tree[i].kind === 'command') {
                    let snippet;
                    if (tree[i].arguments.length === 0) {
                        snippet = tree[i].name + '.';
                    }
                    else {
                        let args = [];
                        for (let k=0; k<tree[i].arguments.length; k++) {
                            // type is undefined if not present for now
                            if (tree[i].arguments.type !== undefined) {
                                args.push('<<' + tree[i].arguments[k].type + ' ' + tree[i].arguments[k].name + '>>');
                            }
                            else {
                                args.push('<<' + tree[i].arguments[k].name + '>>');
                            }
                        }
                        snippet = tree[i].name + '(' + args.join(', ') + ').';
                    }
                    menu.push({'name': tree[i].name,
                               'snippet': snippet,
                               'container': 'section_' + tree[i].parent_id});

                }

            }

        }
        return menu;
    }

    function menu_object_builder(menu_list) {
        const id_menu = give_id(menu_list);
        return unflatten(id_menu);
    }


    return {
        menu: menu_object_builder(Commands.commands_list),
        html_menu: make_menu(menu_object_builder(Commands.commands_list))
    };
});


