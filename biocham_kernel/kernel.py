'''Biocham kernel wrapper'''
from subprocess import check_output
from bisect import bisect_left
from signal import SIGINT
import re
from os import getenv, path, environ, pathsep
from decimal import Decimal

from ipykernel.ipkernel import IPythonKernel
from ipykernel.comm import Comm
from pexpect import replwrap, exceptions
from IPython.core.display import display
import ipywidgets as widgets
import numpy as np
from traitlets import Float

from .images import extract_image_filenames, display_data_for_image
from .commands import commands

# for the sliders, to avoid the bump to 0 when slider values are
# updated
max_reached, desc = 0, ''

class BiochamKernel(IPythonKernel): # pylint: disable=too-many-ancestors
    '''kernel class wrapping the Biocham REPL'''
    implementation = 'Biocham'
    implementation_version = '0.1.0'
    language = 'biocham'
    language_info = {
        'name': 'biocham',
        'mimetype': 'text/plain',
        'file_extension': '.bc',
        'pygments_lexer': 'prolog',
        'codemirror_mode': 'biocham',
    }
    banner = check_output(['biocham', '--version']).decode('utf-8')
    language_version = banner.split()[1]
    help_links = [{
        'text': 'Biocham Manual',
        'url': 'https://lifeware.gitlabpages.inria.fr/biocham/doc_v' +
        language_version + '/index.html'
    }]
    shell = None
    command_sep = re.compile(r'\.\s+')
    timeout = Float(getenv('BIOCHAM_TIMEOUT', 120))

    # KernelBase actually increases the execution count but IPythonKernel
    # overrides this using self.shell.execution_count (which we don't have)
    @property
    def execution_count(self):
        return self._execution_count

    @execution_count.setter
    def execution_count(self, value):
        self._execution_count = value

    def __init__(self, **kwargs):
        # undo the stupid _path_with_self from jupyter_core.command that puts
        # jupyter's path in front of PATH
        environ['PATH'] = pathsep.join(getenv('PATH').split(pathsep)[1:])
        super(BiochamKernel, self).__init__(**kwargs)
        self._execution_count = 0
        self.register_comm('from_gui', self.handle_comm)
        self._start_biocham()

    def handle_comm(self, comm, msg):
        # pylint: disable=unused-argument
        ''' handle code received from biocham GUI '''
        @comm.on_msg
        def _recv(msg):
            data = msg['content']['data']
            if 'code' in data:
                code = data['code']
                if 'silent' in data:
                    silent = data['silent']
                else:
                    silent = True
                self.do_execute(code, silent=silent, comm=comm)
            if 'file' in data and 'content' in data:
                saved = self.handle_file(data['file'], data['content'])
                # here we don't increment the execution_count, because the
                # file handling is done in two step: when the user chooses
                # the file (saves it in the current dir) and when he clicks
                # the loading button (triggers the increment of execution_count
                comm.send({
                    'code': code,
                    'type': 'str',
                    'saved': saved,
                    'show': True,
                })
            if 'bokeh' in data:
                import bokeh
                comm.send({
                    'type': 'bokeh_version',
                    'version': bokeh.__version__
                })

    def register_comm(self, target, func):
        ''' generic method to register a target of comm opened from the GUI'''
        if self.comm_manager is not None:
            self.comm_manager.register_target(target, func)
        else:
            raise Exception('Comm handler is not initialized')

    def _start_biocham(self):
        """Start the Biocham process"""
        self.biocham = replwrap.REPLWrapper(
            'biocham --jupyter',
            'biocham: ',
            None,
            continuation_prompt='|:'
        )
        # log = open('foo.log', 'w')
        # self.biocham.child.logfile = log

    def do_execute(self, code, silent, store_history=True,
                   user_expressions=None, allow_stdin=False, comm=None):
        payload, image_filenames = [], []
        status = 'ok'
        output_type = ''
        # self.comm_manager.register_target('gui_func', handle_comm)
        # inform IPython users that biocham does not support shell kernel
        if code.startswith('!'):
            output = 'No system kernel available.'

        elif code.startswith('%') and \
                not code.startswith('% ') and '\n' not in code:
            output, payload = self._do_magic(code[1:])

        else:
            code = '\n'.join([line.strip() for line in code.splitlines() if
                              line.strip() and not line.startswith('%')])
            try:
                if code:
                    output = self.biocham.run_command(code,
                                                      timeout=self.timeout)
                else:
                    output = ''
                image_filenames, output = extract_image_filenames(output)
                if output.startswith('ERROR:'):
                    status = 'error'
            except exceptions.TIMEOUT:
                self.biocham.child.kill(SIGINT)
                self.biocham.run_command('a')
                output = 'non-terminated command'
                image_filenames = []
                status = 'error'

        # create a comm to send messages to the GUI as well when code is executed
        if not comm:
            comm = Comm(target_name='to_gui', data={})

        if image_filenames:
            for filename in image_filenames:
                try:
                    display_data_for_image(filename, comm=comm)
                except ValueError as excpt:
                    message = {'name': 'stdout', 'text': str(excpt)}
                    self.send_response(self.iopub_socket, 'stream', message)
                    status = 'error'
        else:
            output_type = 'str'

        if not silent and output:
            if status == 'error':
                self.send_response(self.iopub_socket,
                                   'error',
                                   {
                                       'execution_count': self.execution_count,
                                       'ename': '',
                                       'evalue': output,
                                       'traceback': []
                                   })
            self.send_response(self.iopub_socket,
                               'execute_result',
                               {
                                   'execution_count': self.execution_count,
                                   'data': {'text/plain': output},
                                   'metadata': {},
                               })

        response = {
            'status': status,
            'code': code,
            'ename': '',
            'output': output,
            # The base class increments the execution count
            'execution_count': self.execution_count,
            'payload': payload,
            'user_expressions': user_expressions,
            'type': output_type,
            'show': not silent
        }

        comm.send(response)
        return response

    def do_complete(self, code, cursor_pos):
        current = code[:cursor_pos]
        prefix = self.command_sep.split(current)[-1]
        return {
            'status': 'ok',
            'cursor_start': cursor_pos - len(prefix),
            'cursor_end': cursor_pos,
            'matches': _find(prefix),
        }

    def _do_magic(self, magic):
        '''handle biocham kernel %magic
        returns a pair output_string, payload'''
        if magic.startswith('load '):
            bcfile = magic[5:].split()[0]
            if not bcfile.endswith('.bc') or bcfile.startswith('.'):
                return 'Cannot read {}'.format(bcfile), []
            try:
                with open(bcfile, 'r') as bcinput:
                    lines = bcinput.read().split('.\n')
                # create a cell after current cell for each dotted command
                return 'loading {}'.format(bcfile), [{
                    'source': 'set_next_input',
                    'text': line.strip() + '.',
                    'replace': False,
                } for line in reversed(lines) if line.strip()]
            except FileNotFoundError:
                return "File doesn't exists", []

        if magic.startswith('timeout '):
            self.timeout = int(magic[8:].split()[0])
            return 'Timeout set to ' + str(self.timeout), []

        if magic.startswith('slider '):
            parameters = magic[7:].split()

            list_parameters = self.biocham.run_command('list_parameters.')
            current_val = []
          
            for par in parameters:
                cur = re.search(r'] parameter\({}=([^)]*)\)'.format(par),
                                list_parameters)
                if cur is None or float(cur.group(1)) == 0:
                    current_val.append(float(0))
                else:
                    current_val.append(float(cur.group(1)))
                    
            def get_options(value):
                """Return selection options for a selection slider.
                """
                if value == 0:
                    return np.arange(0, 1.01, 0.01)
                maxi = value * 10
                t = maxi/100
                return [i*t if i != 10 else value for i in range(101)]
            
            display_ui = [widgets.SelectionSlider(
                description=par,
                value=cur,
                continuous_update=False,
                layout=widgets.Layout(min_width='300px'),
                options=[("%g"%i, i) for i in get_options(cur)])
                          for (par, cur) in zip(parameters, current_val)
            ]

            box_layout = widgets.Layout(display='flex',
                                        flex_flow='row wrap')

            box = widgets.Box(children=display_ui, layout=box_layout)

            display(box)
            
            # show a first result as placeholder
            image_filenames, _ = extract_image_filenames(
                self.biocham.run_command('numerical_simulation. plot.'))
            handle = display_data_for_image(image_filenames[0])

            if handle is not None:
                handle, data_source = handle
            
            def on_change(change):
                '''observer for the changes of the slider in the notebook'''
        
                try:
                    global max_reached, desc
                    if change.new == 0 and change.old == max_reached:
                        max_reached = 0
                        return
                    if change.new == change.owner.options[-1][-1]:
                        max_reached = change.new
                        desc = change.owner.description
                        change.owner.options = [("%g"%i, i) for i in get_options(change.new)]
                        change.owner.value = change.owner.options[10][-1]
                    elif change.new == 0:
                        main_val = change.owner.options[10][-1]
                        change.owner.options = [("%g"%i, i) for i in get_options(main_val/10)]
                        
                    self.biocham.run_command(
                        'parameter({param}={value}).'.format(
                            param=change.owner.description,
                            value=change.new)
                    )
                    # run a simulation with default options
                    self.biocham.run_command('numerical_simulation.',
                                             timeout=self.timeout)

                    # plot and get corresponding HTML output
                    image_filenames, _ = extract_image_filenames(
                        self.biocham.run_command('plot.'))
                
                    display_data_for_image(image_filenames[0],
                                           handle=handle,
                                           data_source=data_source)

                except IndexError:
                    print('No plot to render.')

            for ui_item in display_ui:
                ui_item.observe(on_change, 'value')

            return '', []

        return '''The available kernel are:
    %lsmagic\tLists available kernel
    %load [file.bc]\tImport a biocham file as a notebook cell
    %timeout [s]\tSets the timeout for the communication with the kernel
    %slider <parameter> [parameter parameter]\tCreates a slider to change \
given parameter(s) in default simulation
''', []

    def do_apply(self, content, bufs, msg_id, reply_metadata):
        '''deprecated'''
        pass

    def do_clear(self):
        '''deprecated'''
        pass

    @staticmethod
    def handle_file(filename, file_content):
        '''
        Write file_content in local dir. Necessary for now for file input
        widgets used in the GUI.
        '''
        if not path.isfile(filename):
            with open(filename, 'w') as output:
                output.write(file_content)
        if path.isfile(filename):
            return True
        return False


def _find(prefix):
    '''find matching commands in the global command list'''
    matches = []
    index = bisect_left(commands, prefix)
    while commands[index].startswith(prefix):
        matches.append(commands[index])
        index += 1
    return matches

