'''generate output data for interactive plotting and other images'''
import base64
import os
import csv
import copy

from bokeh.plotting import figure
from bokeh.io.notebook import publish_display_data
from bokeh.io import output_notebook, show, push_notebook
from bokeh.models import HoverTool, ColumnDataSource, Legend, Range1d, \
     CDSView, IndexFilter
from bokeh.palettes import Category10_10    # pylint:disable=no-name-in-module
from bokeh.resources import CDN
from bokeh.embed import components

bokeh_loaded = False    # pylint:disable=invalid-name
STEP_N = 25

def display_data_for_image(filename, handle=None, data_source=None, comm=None):
    '''return display_data record for dynamic plots of the given filename'''

    image_type = filename.split('.')[-1]

    if image_type == 'csv':
        plot_data = get_data_and_meta(filename)
        # plot_data is modified in create_bokeh_plot
        # this is just a hack to have correct plot in GUI for now
        gui_plot_data = copy.deepcopy(plot_data)
        fig, cds, lines = create_bokeh_plot(plot_data)
        gui_fig, _, _ = create_bokeh_plot(gui_plot_data, gui=True)
        against = plot_data['against']
        return handle_bokeh_plot(fig, gui_fig, against=against, lines=lines, cds=cds,
                                 old_handle=handle, old_data=data_source, comm=comm) 
        
    with open(filename, 'rb') as file:
        image = file.read()
    os.unlink(filename)

    if image_type == 'svg':
        image_type = 'image/svg+xml'
        image_data = image.decode('utf-8')
    elif image_type == 'tex':
        image_type = 'text/latex'
        image_data = image.decode('utf-8')
    else:
        image_type = 'image/' + image_type
        image_data = base64.b64encode(image).decode('ascii')

    if comm:
        comm.send({
            'type': 'image',
            'image_type': image_type,
            'image_data': image_data,
        });
        
    publish_display_data(data={image_type: image_data})

    return None


def get_data_and_meta(filename):
    '''read data and metadata from filename and filenameX'''
    with open(filename + 'X', 'rb') as file:
        metadata = file.read().decode('utf-8').splitlines()
    os.unlink(filename + 'X')
    logscale = metadata[0].split(' ')[1]
    to_show = metadata[1].split(' ')[1:]
    against = metadata[2].split(' ')[1]

    with open(filename, newline='') as csvfile:
        lines = list(csv.reader(csvfile))
    os.unlink(filename)
    
    return {'lines': lines, 'logscale': logscale, 'to_show': to_show, 'against': against}


def create_bokeh_plot(data, gui=False):
    '''plot using bokeh from python'''
    lines, logscale, to_show, against = data['lines'], data['logscale'], data['to_show'], data['against']
    
    headers = lines.pop(0)
    headers[0] = 'Time'

    cds = ColumnDataSource({name: [float(lines[i][index])
                                   for i in range(len(lines))]
                            for index, name in enumerate(headers)})
   
    global bokeh_loaded     # pylint:disable=global-statement,invalid-name
    if not bokeh_loaded:
        output_notebook(CDN, hide_banner=True)
        bokeh_loaded = True

    fig = figure(
        x_axis_label=against,
        x_axis_type='log' if 'x' in logscale else 'linear',
        y_axis_type='log' if 'y' in logscale else 'linear',
        tools='pan,wheel_zoom,box_zoom,reset,save,undo',
        active_drag='box_zoom',
        active_scroll=None,
        toolbar_location='below',
        toolbar_sticky=False,
        plot_height=400,
        plot_width=540,     # will increase by 100 for each caption column
        sizing_mode=('fixed' if not gui else 'scale_width'),
    )
    # no simple way to have the Legend show in the tooltip :'(
    fig.add_tools(HoverTool(tooltips=[
        ("x,y", "$x, $y"),
    ]))
    
    # dashed grid
    fig.xgrid.grid_line_dash = [6, 4]
    fig.ygrid.grid_line_dash = [6, 4]
    
    # customize appearance
    fig.background_fill_color = 'oldlace'
    fig.background_fill_alpha = 0.3
    
    view = CDSView(source=cds,
                   filters=[IndexFilter(
                       list(range(0, 0 if against != 'Time' else
                                  len(cds.data['Time']))))])
    lines = []
    minimum, maximum = None, None
    headers.remove(against)
    if against != 'Time':
        headers.remove('Time')
    for idx, val in enumerate(headers):
        lines.append(
            fig.line(x=against, y=val,
                     line_width=2, color=Category10_10[idx % 10],
                     source=cds, view=view)
        )
        if to_show != ['{}']:
            if val not in to_show:
                lines[idx].visible = False
            else:
                if minimum is None:
                    minimum = min(cds.data[val])
                    maximum = max(cds.data[val])
                else:
                    minimum = min(minimum, min(cds.data[val]))
                    maximum = max(maximum, max(cds.data[val]))
                    
    if to_show != ['{}']:
        if maximum == minimum:
            maximum, minimum = minimum + 1, minimum - 1
        # could look better if rounded to next tick
        pad = 0.1*(maximum - minimum)
        fig.y_range = Range1d(minimum - pad, maximum + pad)
        
    full_items = [(header, [line]) for header, line in zip(headers, lines)]
    nlegends = 1 + len(full_items) // 13
    for legend_index in range(nlegends):
        legend = Legend(
            items=full_items[legend_index*13:legend_index*13+13],
            location=(10, 0),
            click_policy='hide',
            border_line_color='gray',
        )
        fig.add_layout(legend, 'right')
        fig.plot_width += 100
        
    return fig, cds, lines


def handle_bokeh_plot(fig, gui_fig, against, lines, cds=None, old_handle=None, old_data=None, comm=None):
    if old_handle is None:
        if against == 'Time':
            if comm:
                send_plot_to_gui(gui_fig, comm)
                if comm.target_name == 'from_gui':
                    return
            return show(fig, notebook_handle=True), cds

        handle = show(fig, notebook_handle=True)
        time_col = cds.data['Time']
        time_max = time_col[-1] / float(STEP_N)
            
        for i in range(STEP_N):
            indices = [j for j in range(0, len(time_col))
                       if time_col[j] <= time_max * i]

            view = CDSView(source=cds,
                           filters=[IndexFilter(indices)])
            for line in lines:
                line.view = view
            push_notebook(handle=handle)

            if i == STEP_N-1:
                fig.sizing_mode = 'scale_width'
                send_plot_to_gui(fig, comm)
                fig.sizing_mode = 'fixed'
                
        return handle, cds

    # set the data of the old plot to what we just read
    old_data.data = cds.data
    # and display it
    push_notebook(old_handle)
    return old_handle, old_data


def extract_image_filenames(output):
    '''return a pair with the list of image filenames found in the output,
    and the remainder'''
    output_lines = []
    image_filenames = []

    for line in output.split("\n"):
        if line.startswith('<img src="'):
            filename = line.rstrip().split('"')[1]
            image_filenames.append(filename)
        else:
            output_lines.append(line)

    output = "\n".join(output_lines)
    return image_filenames, output

def send_plot_to_gui(fig, comm):
    script, div = components(fig)
    comm.send({
        'type': 'plot',
        'div': div,
        'script': script,
        'show': True
    })
    
