:- module(
  influence_properties,
  [
    % Commands
    list_stable_states/0,
    list_tscc_candidates/0,
    % Public API
    mark_subsumed_influences/1,
    subsumed_influence/1
  ]
).

:- use_module(library(assoc)).
:- use_module(library(ordsets)).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(influence_rules).


:- doc('The following commands refer to the Boolean dynamics of BIOCHAM models which can be either positive (i.e. without negation, the inhibitors are ignored)
or negative (the inhibitors of a reaction or an influence are interpreted as negative conditions) \\cite{FMSR16cmsb}.').


list_stable_states :-
  biocham_command,
  doc('
    lists stable steady states of the state transition graph corresponding
    to the current boolean semantics of the current influence model.
  '),
  option(
    boolean_semantics,
    boolean_semantics,
    BoolSem,
    'Use positive or negative boolean semantics for inhibitors.'
  ),
  list_stable_states(BoolSem),
  forall(
    stable_state(State),
    (
      write_bool_state(State),
      nl
    )
  ).


:- dynamic(stable_state/1).


list_stable_states(BoolSem) :-
  retractall(stable_state(_)),
  with_influence_model(influence_properties:(
    mark_subsumed_influences(BoolSem),
    enumerate_objects(Objects),
    create_bool_assoc(Objects, BoolState),
    add_stable_constraints(BoolState, BoolSem),
    assoc_to_values(BoolState, Bools),
    forall(
      labeling(Bools),
      assertz(stable_state(BoolState))
    )
  )),!.

% if there's a failure when adding constraints, we just do nothing
list_stable_states(_) :-
  true.


:- dynamic(candidate/1).


list_tscc_candidates :-
  biocham_command,
  doc('
    lists possible representative states of Terminal Strongly Connected
    Components (TSCC) of the state transition graph corresponding to the
    positive semantics of the current influence model.
  '),
  retractall(candidate(_)),
  statistics(walltime, _),
  with_influence_model(influence_properties:(
    mark_subsumed_influences(positive),
    enumerate_objects(Objects),
    create_bool_assoc(Objects, BoolState),
    add_tscc_constraints(BoolState),
    add_reversibility_constraints(BoolState),
    assoc_to_values(BoolState, Bools),
    forall(
      labeling(Bools),
      (
        (
          has_enabled_effective('-', BoolState, _)
        ->
          assertz(candidate(BoolState))
        ;
          write_bool_state(BoolState),
          write(' stable\n')
        )
      )
    )
  )),
  candidates_in_positive_tscc(NCandidates, NTSCCs),
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  write('Candidates from constraints: '), write(NCandidates),nl,
  write('Complex TSCCs computed: '), write(NTSCCs),nl,
  write('Time: '), write(Time),nl.


% all outgoing influences do not change the state
add_stable_constraints(BoolState, BoolSem) :-
  findall(
    (PositiveInputs, NegativeInputs, Sign, Output),
    (
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegInputs,
        sign: Sign,
        output: Output
      ]),
      \+ subsumed_influence(Item),
      (
        BoolSem == positive
      ->
        NegativeInputs = []
      ;
        NegativeInputs = NegInputs
      )
    ),
    InfluenceQuads
  ),
  add_stable_constraint(InfluenceQuads, BoolState).


% all outgoing positive influences do not change the state
add_tscc_constraints(BoolState) :-
  findall(
    (PositiveInputs, [], Sign, Output),
    (
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        sign: Sign,
        output: Output
      ]),
      Sign = '+',
      \+ subsumed_influence(Item)
    ),
    InfluenceQuads
  ),
  add_stable_constraint(InfluenceQuads, BoolState).


% all outgoing negative influences can be reversed
%
% formerly as test:
% look_ahead_filter_terminal(State) :-
%   forall(
%     has_enabled_effective('-', State, NewState),
%     has_enabled_effective('+', NewState, State)
%   ).
add_reversibility_constraints(BoolState) :-
  findall(
    (PositiveInputs, Output),
    (
      % FIXME we decompose Item first to filter, but result is dependent on
      % precise influence operators
      Item = (_ -< _),
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        sign: '-',
        output: Output
      ]),
      \+ subsumed_influence(Item)
    ),
    InfluenceDoubles
  ),
  % write(InfluenceDoubles),nl,
  add_reversible_constraint(InfluenceDoubles, BoolState).


add_stable_constraint([], _).


% given influence triples do not change the state
add_stable_constraint([(PositiveInputs, NegativeInputs, Sign, Output) | InfluenceQuads], BoolState) :-
  get_assoc_list(PositiveInputs, BoolState, BoolPositiveInputs),
  get_assoc_list(NegativeInputs, BoolState, BoolNegativeInputs),
  get_assoc(Output, BoolState, BoolOutput),
  (
    Sign = '+'
  ->
    sat(~(*(BoolPositiveInputs)) + +(BoolNegativeInputs) + BoolOutput)
  ;
    sat(~(*(BoolPositiveInputs)) + +(BoolNegativeInputs) + ~(BoolOutput))
  ),
  add_stable_constraint(InfluenceQuads, BoolState).


add_reversible_constraint([], _).

add_reversible_constraint([(PositiveInputs, Output) | InfluenceDoubles], BoolState) :-
  get_assoc_list(PositiveInputs, BoolState, BoolPositiveInputs),
  get_assoc(Output, BoolState, BoolOutput),
  findall(
    *(BoolPositiveInputs2),
    (
      Item = (_ -> Output),
      item([kind: influence, item: Item]),
      % FIXME we decompose Item first to filter, but result is dependent on
      % precise influence operators
      influence(Item, [positive_inputs: PositiveInputs2]),
      \+ subsumed_influence(Item),
      \+ member(Output, PositiveInputs2),
      get_assoc_list(PositiveInputs2, BoolState, BoolPositiveInputs2)
    ),
    Reverse
  ),
  % for any negative influence, either it is not enabled, or it does not
  % change anything, or there is some positive influence on the same output,
  % that does not need it as input and is enabled
  sat(~(*(BoolPositiveInputs)) + ~(BoolOutput) + +(Reverse)),
  add_reversible_constraint(InfluenceDoubles, BoolState).


get_assoc_list([], _, []).

get_assoc_list([H | T], Assoc, [V | U]) :-
  get_assoc(H, Assoc, V),
  get_assoc_list(T, Assoc, U).


create_bool_assoc(Objects, Assoc) :-
  create_var_list(Objects, BoolList),
  list_to_assoc(BoolList, Assoc).


create_var_list([], []).

create_var_list([H | T], [H-_ | U]) :-
  create_var_list(T, U).


write_bool_state(BoolState) :-
  assoc_to_list(BoolState, BoolList),
  write(BoolList).


enumerate_objects(Objects) :-
  setof(
    Object,
    enumerate_object(Object),
    Objects
  ).


enumerate_object(Object) :-
  item([kind: influence, item: Item]),
  influence(Item, [
    positive_inputs: PositiveInputs,
    negative_inputs: NegativeInputs,
    output: Output
  ]),
  (
    member(Object, PositiveInputs)
  ;
    member(Object, NegativeInputs)
  ;
    Object = Output
  ).


has_enabled_effective(Sign, BoolState, NewBoolState) :-
  item([kind: influence, item: Item]),
  influence(Item, [
    positive_inputs: PositiveInputs,
    sign: Sign,
    output: Output
  ]),
  \+ subsumed_influence(Item),
  get_assoc_list(PositiveInputs, BoolState, BoolInputs),
  maplist(=(1), BoolInputs),
  (
    Sign = '+'
  ->
    get_assoc(Output, BoolState, 0),
    put_assoc(Output, BoolState, 1, NewBoolState)
  ;
    get_assoc(Output, BoolState, 1),
    put_assoc(Output, BoolState, 0, NewBoolState)
  ).

has_enabled_backward_effective(Sign, BoolState, PrevBoolState) :-
  item([kind: influence, item: Item]),
  influence(Item, [
    positive_inputs: PositiveInputs,
    sign: Sign,
    output: Output
  ]),
  (
    Sign = '+'
  ->
    get_assoc(Output, BoolState, 1),
    put_assoc(Output, BoolState, 0, PrevBoolState)
  ;
    get_assoc(Output, BoolState, 0),
    put_assoc(Output, BoolState, 1, PrevBoolState)
  ),
  get_assoc_list(PositiveInputs, PrevBoolState, BoolInputs),
  maplist(=(1), BoolInputs).


candidates_in_positive_tscc(NCandidates, NTSCCs) :-
  devdoc('
    Hide initial_state, then ask NuSMV if everywhere State => AG(EF(State))
    for each State given by candidate/1. if the result is true we have a T-SCC
    '
  ),
  list_stable_states(negative),
  findall(C, candidate(C), Candidates),
  length(Candidates, NCandidates),
  maplist(state_to_spec, Candidates, Specs),
  maplist(spec_to_terminal, Specs, TSCCSpecs),
  setup_call_cleanup(
    hide_initial_state(Initial),
    (
      check_several_queries(TSCCSpecs, positive, Results),
      subtract(Results, [false], TSCCs),
      length(TSCCs, NTSCCs),
      forall(
        member(C, Candidates),
        (
          nth1(N, Candidates, C),
          nth1(N, Results, R),
          nth1(N, Specs, Spec),
          nth1(N, TSCCSpecs, TSCCSpec),
          (
            R == 'true'
          ->
            write_bool_state(C),
            write(' terminal (positive)\n'),
            (
              fail,   % too slow for negative semantics unfortunately
              check_ctl_impl(TSCCSpec, all, no, negative, Result),
              write(Result),nl,
              Result == true
            ->
              write('*** truly terminal\n')
            ;
              (
                can_reach_stable(Spec)
              ->
                write('positive TSCC contains a (negative) stable state\n')
              ;
                write('contains a complex attractor\n')
              )
            )
          ;
            true
          )
        )
      )
    ),
    restore_initial_state(Initial)
  ).


spec_to_terminal(Spec, (Spec -> 'AG'('EF'(Spec)))).


can_reach_stable(Spec) :-
  findall(
    StableSpec,
    (
      stable_state(Stable),
      state_to_spec(Stable, StableSpec)
    ),
    Stables
  ),
  maplist(reachable_spec(Spec), Stables, Reachables),
  join_with_op(Reachables, '\\/', AnyReachable),
  check_ctl_impl(AnyReachable, all, no, positive, Result),
  Result == true.


reachable_spec(From, To, (From -> 'EF'(To))).


hide_initial_state(Initial) :-
  enumerate_all_molecules(M),
  maplist(get_initial_state, M, Initial),
  maplist(initial_state:undefined_object, M).


restore_initial_state(Initial) :-
  enumerate_all_molecules(M),
  maplist(set_initial_state, M, Initial).


state_to_spec(State, Spec) :-
  assoc_to_list(State, List),
  maplist(literal_to_spec, List, Specs),
  join_with_op(Specs, '/\\', Spec).


literal_to_spec(A-0, not(A)).

literal_to_spec(A-1, A).


:- dynamic(subsumed_influence/1).


mark_subsumed_influences(BoolSem) :-
  retractall(subsumed_influence(_)),
  mark_subsumed_influences_aux(BoolSem).


mark_subsumed_influences_aux(BoolSem) :-
  item([kind: influence, item: Item]),
  influence(Item, [positive_inputs: PI, negative_inputs: NI, sign: S, output: O]),
  (
    item([kind: influence, item: I2]),
    I2 \= Item,
    \+ subsumed_influence(I2),
    influence(I2, [positive_inputs: PI2, negative_inputs: NI2, sign: S, output: O]),
    subset(PI2, PI),
    (
      BoolSem == positive
    ->
      true
    ;
      subset(NI2, NI)
    )
  ->
    assertz(subsumed_influence(Item))
  ;
    true
  ),
  fail.

mark_subsumed_influences_aux(_).
