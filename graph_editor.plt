:- use_module(library(plunit)).


:- begin_tests(graph_editor).


test('new_graph', [true(Graphs == [new_graph])]) :-
  clear_model,
  new_graph,
  all_items([kind: graph], Graphs).

test('delete_graph', [true(Graphs == [])]) :-
  clear_model,
  new_graph,
  delete_graph(new_graph),
  all_items([kind: graph], Graphs).

test('set_graph_name', [true(Graphs == [my_graph])]) :-
  clear_model,
  new_graph,
  set_graph_name(my_graph),
  all_items([kind: graph], Graphs).

test('list_graphs', [true(Atom == '[0]*new_graph\n')]) :-
  clear_model,
  new_graph,
  with_output_to(atom(Atom), list_graphs).

test('select_graph', [true(Item == graphA)]) :-
  clear_model,
  new_graph,
  set_graph_name(graphA),
  new_graph,
  set_graph_name(graphB),
  select_graph(graphA),
  get_current_graph(Id),
  find_item([id: Id, item: Item]).

%    draw_graph/0,
%    export_graph/1,

test('add_vertex', [true(Vertices == ['A'])]) :-
  clear_model,
  new_graph,
  command(add_vertex('A', 'A')),
  get_current_graph(GraphId),
  all_items([parent: GraphId, kind: vertex], Vertices).

test('delete_vertex', [true(Vertices == [])]) :-
  clear_model,
  new_graph,
  command(add_vertex('A')),
  command(delete_vertex('A')),
  get_current_graph(GraphId),
  all_items([parent: GraphId, kind: vertex], Vertices).

test('add_edge', [true(Edges == ['A' -> 'B'])]) :-
  clear_model,
  new_graph,
  command(add_edge('A' -> 'B', 'A' -> 'B', 'A' -> 'C')),
  command(delete_vertex('C')),
  get_current_graph(GraphId),
  all_items([parent: GraphId, kind: edge], Edges).

test('delete_edge', [true(Edges == [])]) :-
  clear_model,
  new_graph,
  command(add_edge('A' -> 'B')),
  command(delete_edge('A' -> 'B')),
  get_current_graph(GraphId),
  all_items([parent: GraphId, kind: edge], Edges).

test('list_edges', [true(Atom == '[0] A->B\n')]) :-
  clear_model,
  new_graph,
  command(add_edge('A' -> 'B')),
  with_output_to(atom(Atom), list_edges).

test('list_isolated_vertices', [true(Atom == '[0] C\n')]) :-
  clear_model,
  new_graph,
  command(add_vertex('A', 'B', 'C')),
  command(add_edge('A' -> 'B')),
  with_output_to(atom(Atom), list_isolated_vertices).

test(
  'list_graph_objects',
  [true(Atom == '\c
[0] A->B
  [1] stochiometry=2
[2] B
  [3] kind=transition
[4] C
  [5] kind=place
[6] D
')]) :-
  clear_model,
  new_graph,
  command(add_vertex('A')),
  command(transition('B')),
  command(place('C')),
  command(add_vertex('D')),
  command(add_edge('A' -> 'B')),
  command(set_attribute(('A' -> 'B'), stochiometry = 2)),
  with_output_to(atom(Atom), list_graph_objects).

test('set_attribute vertex', [true(Attributes == ['object', 'item'])]) :-
  clear_model,
  new_graph,
  command(add_vertex('A')),
  command(set_attribute('A', object)),
  command(set_attribute('A', item)),
  command(set_attribute('A', object)),
  get_current_graph(GraphId),
  find_item([parent: GraphId, kind: vertex, key: 'A', id: VertexId]),
  all_items([parent: VertexId, kind: attribute], Attributes).

test('set_attribute edge', [true(Attributes == [stochiometry = 2])]) :-
  clear_model,
  new_graph,
  command(add_vertex('A', 'B')),
  command(add_edge('A' -> 'B')),
  command(set_attribute(('A' -> 'B'), stochiometry = 2)),
  get_current_graph(GraphId),
  find_item([parent: GraphId, kind: edge, key: ('A' -> 'B'), id: EdgeId]),
  all_items([parent: EdgeId, kind: attribute], Attributes).

test('place', [true(Attributes == [kind = place])]) :-
  clear_model,
  new_graph,
  command(transition('A')),
  command(place('A')),
  command(transition('B')),
  get_current_graph(GraphId),
  find_item([parent: GraphId, kind: vertex, key: 'A', id: VertexId]),
  all_items([parent: VertexId, kind: attribute], Attributes).

test('transition', [true(Attributes == [kind = transition])]) :-
  clear_model,
  new_graph,
  command(place('A')),
  command(transition('A')),
  command(place('B')),
  get_current_graph(GraphId),
  find_item([parent: GraphId, kind: vertex, key: 'A', id: VertexId]),
  all_items([parent: VertexId, kind: attribute], Attributes).

test('delete_attribute', [true(Attributes == [])]) :-
  clear_model,
  new_graph,
  command(add_vertex('A')),
  command(set_attribute('A', object)),
  command(delete_attribute('A', object)),
  get_current_graph(GraphId),
  find_item([parent: GraphId, kind: vertex, key: 'A', id: VertexId]),
  all_items([parent: VertexId, kind: attribute], Attributes).

test('list_attributes vertex', [true(Atom == '[0] kind=place\n')]) :-
  clear_model,
  new_graph,
  command(place('A')),
  with_output_to(atom(Atom), list_attributes('A')).

test('list_attributes edge', [true(Atom == '[0] stochiometry=2\n')]) :-
  clear_model,
  new_graph,
  command(add_vertex('A')),
  command(add_vertex('B')),
  command(add_edge('A' -> 'B')),
  command(set_attribute(('A' -> 'B'), stochiometry = 2)),
  with_output_to(atom(Atom), list_attributes('A' -> 'B')).

test('get_attribute', [true(Kind == transition)]) :-
  clear_model,
  new_graph,
  command(transition('A')),
  get_current_graph(GraphId),
  once(get_attribute(GraphId, 'A', kind = Kind)).

:- end_tests(graph_editor).
