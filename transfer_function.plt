:- use_module(library(plunit)).

:- begin_tests(transfer_function).

test('compile_transfer_function') :-
	clear_model,
	disable_p_m_mode,
	set_kinetics_rate(1000),
	compile_transfer_function([22, 51, 47, 19, 3], [30, 66, 67, 36, 10, 1], a, b).

:- end_tests(transfer_function).
