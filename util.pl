:- module(
  util,
  [
    add_list/3,
    name_variables_and_anonymous/2,
    camel_case/2,
    set_to_list/2,
    list_enumeration/2,
    write_successes/3,
    equals_to_list/2,
    list_to_equals/2,
    list/1,
    nth0_eqq/3,
    executable_filename/1,
    indent/1,
    indent/2,
    view_image/1,
    set_image_viewer_driver/1,
    list_file_lines/2,
    list_stream_lines/2,
    file_line_by_line/2,
    with_output_to_file/2,
    stream_line_by_line/2,
    term_morphism/3,
    substitute/4,
    rewrite/3,
    grammar_iter/3,
    grammar_map/4,
    call_subprocess/3,
    with_clean/2,
    clean/1,
    check_cleaned/1,
    join_with_op/3,
    extract_sublist/2,
    uppercase_char/1,
    lowercase_char/1,
    alphabetic_char/1,
    alphanumeric_char/1,
    numeric_char/1,
    identifier_name/1,
    check_valid_identifier_name/1,
    unify_records/2,
    field_default/4,
    watch_error/2,
    open_file/1
  ]).

:- use_module(library(process)).

name_variables(L) :-
  doc('
    instantiates a list of bindings returned by \\texttt{read_term/3}\'s
    \\texttt{variable_names} option (\\texttt{X = \'X\'}).
  '),
  maplist(call, L).


name_anonymous(L) :-
  doc('associates the atom \\texttt{\'_\'} to each variable in \\argument{L}.'),
  maplist(=('_'), L).


name_variables_and_anonymous(Goal, VariableNames) :-
  doc('
    instantiates all variables in \\argument{Goal}, either through the
    \\argument{VariableNames} bindings or to the atom \\texttt{\'_\'}.
  '),
  name_variables(VariableNames),
  term_variables(Goal, AnonymousVariables),
  name_anonymous(AnonymousVariables).


camel_case(Atom, CamelCaseAtom) :-
  atom_chars(Atom, AtomChars),
  camel_case_chars(AtomChars, CamelCaseAtomChars),
  atom_chars(CamelCaseAtom, CamelCaseAtomChars).


camel_case_chars([], []).

camel_case_chars([First | Chars], [UpperFirst | CamelChars]) :-
  upper_lower(UpperFirstCode, First),
  atom_codes(UpperFirst, [UpperFirstCode]),
  camel_case_chars_tail(Chars, CamelChars).


camel_case_chars_tail([], []).

camel_case_chars_tail(['_' | Chars], CamelChars) :-
  !,
  camel_case_chars(Chars, CamelChars).

camel_case_chars_tail([Head | Chars], [Head | CamelChars]) :-
  camel_case_chars_tail(Chars, CamelChars).


set_to_list({ Enum }, List) :-
  !,
  list_enumeration(List, Enum).

set_to_list(Item, [Item]).


list_enumeration([], '_') :-
  !.

list_enumeration([H], H) :-
  H \= (_, _),
  !.

list_enumeration([H | List], (H, Enum)) :-
  list_enumeration(List, Enum).


:- dynamic(first/0).


write_successes(Goal, WriteSeparator, WriteSuccess) :-
  assertz(first),
  \+ (
    Goal,
    \+ (
      (
        retract(first)
      ->
        true
      ;
        WriteSeparator
      ),
      WriteSuccess
    )
  ).


equals_to_list(A = B, List) :-
  !,
  equals_to_list(A, LA),
  append(LA, LB, List),
  equals_to_list(B, LB).

equals_to_list(A, [A]).


list_to_equals([A], A) :-
  !.

list_to_equals([Head | Tail], Head = EqTail) :-
  list_to_equals(Tail, EqTail).


list(List) :-
  nonvar(List),
  (
    List = []
  ;
    List = [_ | _]
  ),
  !.


%! add_list(+List1,+List2,-ListSum)
%
% Add two lists term at term

add_list([],[],[]).

add_list([Head1|Tail1],[Head2|Tail2],[HeadSum|TailSum]) :-
   HeadSum is Head1 + Head2,
   add_list(Tail1,Tail2,TailSum).


nth0_eqq(0, [Head | _Tail], Result) :-
  Head == Result.

nth0_eqq(N, [_Head | Tail], Result) :-
  nth0_eqq(M, Tail, Result),
  N is M + 1.


executable_filename(Filename) :-
  current_prolog_flag(argv, [Exec | _]),
  (
    absolute_file_name(Exec, Absolute, [access(execute), file_errors(fail)])
  ->
    true
  ;
    absolute_file_name(path(Exec), Absolute, [access(execute)])
  ),
  (
     read_link(Absolute, _, Filename)
  ->
     true
  ;
     Filename = Absolute
   ).


indent(Level) :-
  indent(current_output, Level).


indent(Stream, Level) :-
  \+ (
    between(1, Level, _),
    \+ (
      write(Stream, '  ')
    )
  ).


view_image(Filename) :-
  get_image_viewer_driver(Driver),
  Goal =.. [Driver, Filename],
  Goal.


img_tag(Filename) :-
  format('<img src="~a">\n', [Filename]).


get_image_viewer_driver(Driver) :-
  nb_getval(image_viewer_driver, Driver).


set_image_viewer_driver(Driver) :-
  nb_setval(image_viewer_driver, Driver).


list_file_lines(Filename, List) :-
  findall(
    Line,
    file_line_by_line(Filename, Line),
    List
  ).


list_stream_lines(Stream, List) :-
  findall(
    Line,
    stream_line_by_line(Stream, Line),
    List
  ).


file_line_by_line(Filename, Line) :-
  setup_call_cleanup(
    open(Filename, read, Stream),
    stream_line_by_line(Stream, Line),
    close(Stream)
  ).


stream_line_by_line(Stream, Line) :-
  repeat,
  read_line_to_codes(Stream, Codes),
  (
    Codes = end_of_file
  ->
    !,
    fail
  ;
    atom_codes(Line, Codes)
  ).


:- meta_predicate with_output_to_file(+, 0).


with_output_to_file(Filename, Goal) :-
  setup_call_cleanup(
    open(Filename, write, Stream),
    with_output_to(Stream, Goal),
    close(Stream)
  ).


:- meta_predicate term_morphism(2, +, -).

term_morphism(F, TermIn, TermOut) :-
  devdoc('General term morphism.'),
  functor(TermIn, Functor, Arity),
  functor(TermOut, Functor, Arity),
  TermIn =.. [Functor | ArgumentsIn],
  TermOut =.. [Functor | ArgumentsOut],
  maplist(F, ArgumentsIn, ArgumentsOut).


substitute(Parameters, Arguments, ExpressionIn, ExpressionOut) :-
  devdoc('General substitution of terms by other terms in an expression.'),
  nth0(Index, Parameters, ExpressionIn),
  !,
  nth0(Index, Arguments, ExpressionOut).

substitute(Parameters, Arguments, ExpressionIn, ExpressionOut) :-
  term_morphism(substitute(Parameters, Arguments), ExpressionIn, ExpressionOut).


:- meta_predicate rewrite(2, +, -).

rewrite(System, In, Out) :-
  (
    term_morphism(rewrite(System), In, Step0)
  ->
    (
      call(System, Step0, Step1),
      Step1 \= Step0
    ->
      rewrite(System, Step1, Out)
    ;
      Out = Step0
    )
  ;
    call(System, In, Step),
    In \= Step
  ->
    rewrite(System, In, Out)
  ;
    Out = In
  ).


grammar_iter(Grammar, Rules, Term) :-
  devdoc('
Iterates over the nodes of a \\argument{Term} of type \\argument{Grammar}.
\\argument{Rules} is an associative list that maps (a subset of)
non-terminals of \\argument{Grammar} to predicates.
If \\texttt{non_terminal: p(Args)} is in \\argument{Rules}, then
\\texttt{p(Args, Subterm)} is executed for every sub-term of
\\argument{Term} of type \\texttt{non_terminal}.
  '),
  Head =.. [Grammar, Term],
  (
    clause(Head, Body),
    Body
  ->
    true
  ;
    throw(error(unexpected_term(Term, Grammar), grammar_iter))
  ),
  !,
  grammar_iter_aux(Body, Rules).


grammar_iter_aux(true, _Rules) :-
  !.

grammar_iter_aux((A, B), Rules) :-
  !,
  grammar_iter_aux(A, Rules),
  grammar_iter_aux(B, Rules).

grammar_iter_aux(function_application(Grammar, Term), Rules) :-
  !,
  Term =.. [_F | Arguments],
  maplist(grammar_iter(Grammar, Rules), Arguments).

grammar_iter_aux(G, Rules) :-
  G =.. [F, Argument],
  (
    memberchk((F: P), Rules)
  ->
    call(P, Argument)
  ;
    atomic_type(F)
  ->
    true
  ;
    grammar_iter(F, Rules, Argument)
  ).


atomic_type(untyped).

atomic_type(atom).

atomic_type(number).

atomic_type(integer).


grammar_map(Grammar, Rules, In, Out) :-
  devdoc('
Transforms the term \\argument{In} of type \\argument{Grammar}
into the term \\argument{Out}.
\\argument{Rules} is an associative list that maps (a subset of)
non-terminals of \\argument{Grammar} to predicates.
If \\texttt{non_terminal: p(Args)} is in \\argument{Rules}, then
\\texttt{p(Args, SubIn, SubOut)} is executed for every sub-term
\\argument{SubIn} of \\argument{Term} of type \\texttt{non_terminal},
and the sub-term is substituted by \\argument{SubOut}.
Nodes that have a type that does not correspond to a
non-terminal mentioned in \\argument{Rules} are left as is.
  '),
  (
    atomic_type(Grammar)
  ->
    Out = In
  ;
    ProtoHead =.. [Grammar, ProtoArg],
    clause(ProtoHead, ProtoBody),
    copy_term((ProtoArg, ProtoBody), (In, BodyIn)),
    BodyIn
  ->
    Out = ProtoArg,
    BodyOut = ProtoBody,
    grammar_map_aux(BodyIn, BodyOut, Rules)
  ;
    throw(error(unexpected_term(In, Grammar), grammar_map))
  ).


grammar_map_aux(true, true, _Rules) :-
  !.

grammar_map_aux((AIn, BIn), (AOut, BOut), Rules) :-
  !,
  grammar_map_aux(AIn, AOut, Rules),
  grammar_map_aux(BIn, BOut, Rules).

grammar_map_aux(
  function_application(Grammar, In), function_application(Grammar, Out), Rules
) :-
  !,
  In =.. [F | InArguments],
  length(InArguments, L),
  length(OutArguments, L),
  Out =.. [F | OutArguments],
  maplist(grammar_map(Grammar, Rules), InArguments, OutArguments).

grammar_map_aux(PIn, POut, Rules) :-
  PIn =.. [F, ArgumentIn],
  POut =.. [F, ArgumentOut],
  (
    memberchk((F: P), Rules)
  ->
    call(P, ArgumentIn, ArgumentOut)
  ;
    grammar_map(F, Rules, ArgumentIn, ArgumentOut)
  ).


call_subprocess(ExecutableFilename, Arguments, Options) :-
  absolute_file_name(ExecutableFilename, Absolute),
  process_create(Absolute, Arguments, [process(Pid) | Options]),
  process_wait(Pid, exit(0)).


user:message_hook(_Msg, warning, _Lines) :-
  catch(
    count(warnings, _),
    _,
    true
  ),
  fail.


:- meta_predicate with_clean(+, 0).

with_clean(Prototypes, Goal) :-
  setup_call_cleanup(
    maplist(check_cleaned, Prototypes),
    Goal,
    maplist(clean, Prototypes)
  ).


clean(F / N) :-
  functor(H, F, N),
  retractall(H).


clean(M : F / N) :-
  functor(H, F, N),
  retractall(M : H).


check_cleaned(F / N) :-
  functor(H, F, N),
  (
    H
  ->
    throw(error(check_cleaned(F / N)))
  ;
    true
  ).


check_cleaned(M : F / N) :-
  functor(H, F, N),
  (
    M : H
  ->
    throw(error(check_cleaned(M : F / N)))
  ;
    true
  ).


join_with_op([Item], _Op, Item) :-
   % indexing not enough to efficiently remove choice point
   !.

join_with_op([I1, I2 | Items], Op, Term) :-
  Term =..[Op, I1, T],
  join_with_op([I2 | Items], Op, T).


extract_sublist([H | T], [H | TT]) :-
   extract_sublist(T, TT).

extract_sublist([_ | T], TT) :-
   extract_sublist(T, TT).


uppercase_char(Char) :-
  Char @>= 'A', Char @=< 'Z'.


lowercase_char(Char) :-
  Char @>= 'a', Char @=< 'z'.


alphabetic_char(Char) :-
  (
    uppercase_char(Char)
  ;
    lowercase_char(Char)
  ),
  !.


numeric_char(Char) :-
  Char @>= '0', Char @=< '9'.


alphanumeric_char(Char) :-
  (
    alphabetic_char(Char)
  ;
    numeric_char(Char)
  ).


identifier_name(Atom) :-
  atom_chars(Atom, [First | Chars]),
  alphabetic_char(First),
  \+ (
    member(Char, Chars),
    \+ (
      (
        alphanumeric_char(Char)
      ;
        Char = '-'
      ;
        Char = '_'
      ;
        Char = '.'
      )
    )
  ).


check_valid_identifier_name(Atom) :-
  (
    identifier_name(Atom)
  ->
    true
  ;
    throw(error(invalid_identifier_name(Atom)))
  ).


unify_records(Record0, Record1) :-
  (
    (var(Record0) ; var(Record1))
  ->
    Record0 = Record1
  ;
    maplist(unify_field(Record1), Record0)
  ).


unify_field(Record1, Field: Value0) :-
  (
    member(Field: Value1, Record1)
  ->
    Value0 = Value1
  ;
    throw(error(unknown_field(Field)))
  ).


field_default(Field, Default, Record, Value) :-
  (
    member(Field: Value, Record)
  ->
    true
  ;
    Value = Default
  ).


watch_error(Goal, OnError) :-
  devdoc('
    Executes \\argument{Goal} once.
    If \\argument{Goal} fails or throws an exception,
    \\argument{OnError} is executed once and the failure or the exception is
    propagated.
  '),
  catch(
    (
      Goal
    ->
      true
    ;
      once(OnError),
      fail
    ),
    E,
    (
      once(OnError),
      throw(E)
    )
  ).


open_file(Filename) :-
  current_prolog_flag(arch, Arch),
  (
    sub_atom(Arch, _, 6, _, darwin)
  ->
    Open = 'open'
  ;
    Open = 'xdg-open'
  ),
  process_create(path(Open), [Filename], [process(Pid)]),
  process_wait(Pid, exit(0)).
