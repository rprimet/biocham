:- module(
  reaction_rules,
  [
    % Grammars
    reaction/1,
    rule_name/1,
    basic_reaction/1,
    reactants/1,
    solution/1,
    patch_solution/2,
    kinetics/1,
    op(1100, xfx, for),
    op(1050, xfx, =>),
    op(1050, xfx, <=>),
    op(1200, xfx, '--'),
    % Public API
    reaction_predicate/1,
    patch_reactants/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).

:- doc('(Bio)chemical reaction networks (CRNs) can be described in BIOCHAM by a reaction model, 
i.e. a finite (multi)set of directed reaction rules, possibly given with a rate function, using the syntax defined by the grammar below.
BIOCHAM reaction models are compatible with the Systems Biology Markup Language SBML version 2.').

:- devdoc('\\section{Grammars}').


:- grammar(reaction).


reaction(Kinetics for BasicReaction) :-
  kinetics(Kinetics),
  basic_reaction(BasicReaction).

reaction(Name -- Kinetics for BasicReaction) :-
  rule_name(Name),
  kinetics(Kinetics),
  basic_reaction(BasicReaction).

reaction(Name -- BasicReaction) :-
  rule_name(Name),
  basic_reaction(BasicReaction).

reaction(BasicReaction) :-
  basic_reaction(BasicReaction).


:- grammar(rule_name).


rule_name(Name):-
    name(Name).


:- grammar(basic_reaction).


basic_reaction(Reactants =[ Solution0 ]=> Solution1) :-
  reactants(Reactants),
  solution(Solution0),
  solution(Solution1).

basic_reaction(Reactants <=[ Solution0 ]=> Solution1) :-
  reactants(Reactants),
  solution(Solution0),
  solution(Solution1).

basic_reaction(Reactants => Solution) :-
  reactants(Reactants),
  solution(Solution).

basic_reaction(Reactants <=> Solution) :-
  reactants(Reactants),
  solution(Solution).


:- grammar(reactants).


reactants(Solution0 / Solution1) :-
  solution(Solution0),
  enumeration(Solution1).

reactants(Solution) :-
  solution(Solution).


:- grammar(solution).


solution('_').

solution(Solution0 + Solution1) :-
  solution(Solution0),
  solution(Solution1).

solution(Integer * Object) :-
  number(Integer),
  object(Object).

solution(Object) :-
  object(Object).


patch_reactants((A / B, C), A / (B, C)) :-
  !.

patch_reactants(Reactants, Reactants).


patch_solution(S1, S2) :-
  format(atom(A), '~q', [S1]),
  current_op(Pminus, yfx, -),
  current_op(Pat, yfx, @),
  op(300, yfx, -),
  op(350, yfx, @),
  read_term_from_atom(A, S2, []),
  op(Pminus, yfx, -),
  op(Pat, yfx, @).


:- grammar(kinetics).


kinetics((ArithmeticExpression0, ArithmeticExpression1)) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

kinetics(ArithmeticExpression) :-
  arithmetic_expression(ArithmeticExpression).



:- devdoc('\\section{Public API}').

reaction_predicate(_ -- Reaction) :-
  reaction_predicate(Reaction).

reaction_predicate(_ for Reaction) :-
  reaction_predicate(Reaction).

reaction_predicate(_ => _).

reaction_predicate(_ <=> _).
