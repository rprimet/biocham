:- use_module(library(plunit)).

:- begin_tests(tables).

test(
  'add_table',
  [true(Tables == [table])]
) :-
  clear_model,
  add_table(table, [row(1, 2)]),
  all_items([kind: table], Tables).

test(
  'get_table_data',
  [true(Data == [row(1, 2)])]
) :-
  clear_model,
  add_table(table, [row(1, 2)]),
  get_table_data(Data).

test(
  'delete_column',
  [true(Data == [row(1, 3), row(4, 6)])]
) :-
  clear_model,
  add_table(table, [row(1, 2, 3), row(4, 5, 6)]),
  command(delete_column(1)),
  get_table_data(Data).

:- end_tests(tables).
