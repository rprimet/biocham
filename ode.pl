:- module(
  ode,
  [
    % Grammars
    oderef/1,
    % Commands
    new_ode_system/0,
    delete_ode_system/1,
    set_ode_system_name/1,
    list_ode_systems/0,
    select_ode_system/1,
    ode/1,
    add_ode/1,
    delete_ode/1,
    list_ode/0,
    ode_system/0,
    import_ode/1,
    import_reactions_from_ode_system/0,
    import_influences_from_ode_system/0,
    (init)/1,
%    load_ode_system/1,
    load_reactions_from_ode/1,
%    add_ode_system/1,
    add_reactions_from_ode/1,
    load_influences_from_ode/1,
    add_influences_from_ode/1,
    export_ode/1,
    export_ode_as_latex/1,
    % Public API
    add_ode_system_file/1,
    new_ode_system/1,
    get_current_ode_system/1,
    set_current_ode_system/1,
    set_ode_system_name/2,
    import_ode/2,
    set_ode_initial_value/2,
    set_ode_initial_value/3,
    add_ode/2,
    import_reactions_from_ode_system/1,
    import_influences_from_ode_system/1,
    with_current_ode_system/1,
    edit_current_ode_system/1,
    all_odes/1,
    ode/2,
    ode/3,
    ode_add_expression_to_molecule/2,
    ode_predicate/1
  ]).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(reaction_rules).

:- op(1010, fx, init).  % comma is 1000
:- op(1010, fx, par).
:- op(1010, fx, num).
:- op(1010, fx, func).

:- dynamic(flag_influences/0).

:- doc('Biocham can also manipulate ODE systems, import and export ODE systems in XPP syntax, export in LaTeX, and also infer an equivalent reaction network or influence network from the ODEs \\cite{FGS15tcs}. This is useful for importing MatLab models in SBML, and using Biocham analyzers on ODE models.').

:- doc('\\begin{remark} XPP format has restrictions on names and does not distinguish between upper case and lower case letters. As a consequence, some complex names may be not properly exported in ode files, and when an ode file is imported, the names are read in lower case.
\\end{remark}').

:- devdoc('\\section{Grammars}').

:- grammar(ode).


ode(d(X)/dt = E) :-
  object(X),
  arithmetic_expression(E).

ode(DX/dt = E) :-
  name(DX),
  arithmetic_expression(E).


:- grammar(oderef).


oderef(X) :-
  name(X).




:- doc('\\section{Listing ODEs} ').

list_ode :-
  biocham_command,
  doc('
    returns the set of ordinary differential equations
    and initial concentrations (one line per molecule)
    associated to the current model.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(a => b),
  biocham(list_ode),
  doc('
    \\end{example}
  '),
  nb_getval(ode_viewer, Viewer),
  (
    Viewer == inline
  ->
    with_current_ode_system(
      (
        get_current_ode_system(Id),
        list_items([parent: Id, kind: ode])
      )
    )
  ;
    export_ode_as_latex('ode.tex'),
    util:img_tag('ode.tex')
  ).

:- doc('Exporting ODEs').

export_ode(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('exports the ODE system of the current model.'),
  with_current_ode_system(with_output_to_file(OutputFile, print_ode_system)).


export_ode_as_latex(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('exports the ODE system of the current model as LaTeX code.'),
  with_current_ode_system(
    with_output_to_file(OutputFile, print_ode_system_as_latex)).

:- doc('\\section{Inferring reaction or influence networks from an ODE file}').

:- doc("
\\begin{example}
\\clearmodel
\\trace{
biocham: parameter(k=10). 
biocham: MA(k) for 2*a + b  => 3*c.
biocham: list_ode.
[0] d(c)/dt=3*k*a^2*b
[1] d(b)/dt= - (k*a^2*b)
[2] d(a)/dt= - (2*k*a^2*b)
biocham: export_ode('test2.ode').
biocham: load_reactions_from_ode('test2.ode').
biocham: list_model.
k*a^2*b for 2*a+b=>3*c.
present(c,0).
present(b,0).
present(a,0).
parameter(k = 10).
biocham: load_influences_from_ode('test2.ode').
biocham: list_model.
3* (k*a^2*b) for b,a -> c.
2* (k*a^2*b) for b,a -< a.
1* (k*a^2*b) for b,a -< b.
present(c,0).
present(b,0).
present(a,0).
parameter(
  k = 10
).
biocham: list_ode.
[0] d(b)/dt= - (k*a^2*b)
[1] d(a)/dt= - (2*k*a^2*b)
[2] d(c)/dt=3*k*a^2*b
}
\\end{example}
").

%load_ode_system(InputFile) :-
load_reactions_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc("
    infers a set of reactions equivalent to an ODE system, and loads it as \\command{load_biocham/1}.
  "),
  retractall(flag_influences),
  models:load_all('ode', InputFile).


%add_ode(InputFile) :-
add_reactions_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    infers a set of reactions equivalent to an ODE system, and adds it to the current model as \\command{add_biocham/1}.
  '),
  retractall(flag_influences),
  models:add_all('ode', InputFile).


load_influences_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    infers a set of influences equivalent to an ODE system, and loads it as \\command{load_biocham/1}.
  '),
  (assertz(flag_influences);retractall(flag_influences)),
  models:load_all('ode', InputFile),
  retractall(flag_influences).


%add_ode(InputFile) :-
add_influences_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    infers a set of influences equivalent to an ODE system, and adds it to the current model as \\command{add_biocham/1}.
  '),
  (assertz(flag_influences);retractall(flag_influences)),
  models:add_all('ode', InputFile),
  retractall(flag_influences).



:- doc('\\section{ODE systems}').

ode_system :-
  biocham_command,
  doc('
    builds the set of ODEs of of the current model.
  '),
  delete_ode_system('ode_system'),
  new_ode_system,
  set_ode_system_name('ode_system'),
  with_clean(
    [ode:assoc/2],
    (
      ode:compute_ode,
      ode:simplify_ode,
      ode:put_ode_into_system
    )
  ),
  get_current_ode_system(Id),
  forall(
    item([kind: parameter, item: parameter(Parameter = Value)]),
    set_ode_parameters(Id, Parameter = Value)
  ),
  forall(
    item([no_inheritance, kind: function, item: function(Function = Definition)]),
    set_ode_function(Id, Function = Definition)
  ).

import_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('imports a set of ODEs.'),
  import_ode(InputFile, Id),
  set_current_ode_system(Id).


new_ode_system :-
  biocham_command,
  doc('creates an ODE system.'),
  new_ode_system(Id),
  set_current_ode_system(Id).


delete_ode_system(Name) :-
  biocham_command,
  type(Name, name),
  doc('deletes an ODE system.'),
  delete_items([kind: ode_system, key: Name]).


set_ode_system_name(Name) :-
  biocham_command,
  type(Name, name),
  doc('sets the name of the current ODE system.'),
  get_current_ode_system(Id),
  set_ode_system_name(Id, Name).


list_ode_systems :-
  biocham_command,
  doc('lists the ODE systems of the current model.'),
  list_items([kind: ode_system]).


select_ode_system(Name) :-
  biocham_command,
  type(Name, name),
  doc('selects an ODE system'),
  find_item([kind: ode_system, key: Name, id: Id]),
  set_current_ode_system(Id).



add_ode(ODE) :-
  ode(ODE),
  get_current_ode_system(Id),
  !,
  add_ode(Id, ODE).


add_ode(ODEList) :-
  biocham_command(*),
  type(ODEList, '*'(ode)),
  doc('
    If there is a current ODE system, adds the given set of ODEs to it.
    If there is no current ODE system,
    infers reactions equivalent to the given set of ODEs.
  '),
  edit_current_ode_system((
    \+ (
      member(ODE, ODEList),
      \+ (
        add_ode(ODE)
      )
    )
  )).


delete_ode(ODERefList) :-
  biocham_command(*),
  type(ODERefList, '*'(oderef)),
  doc('removes the given set of ODEs from the current ODE system.'),
  \+ (
    member(Name, ODERefList),
    \+ (
      delete_single_ode(Name)
    )
  ).


init(InitList) :-
  biocham_command,
  type(InitList, '*'(name = arithmetic_expression)),
  doc('sets the initial value of a variable in the current set of ODEs.'),
  \+ (
    member(Variable = Value, InitList),
    \+ (
      set_ode_initial_value(Variable, Value)
    )
  ).

import_reactions_from_ode_system :-
  biocham_command,
  doc('adds a set of reactions equivalent to the current ODE system.'),
  get_current_ode_system(Id),
  !,
  retractall(flag_influences),
  import_reactions_from_ode_system(Id).
import_reactions_from_ode_system :-
  print_message(warning,'there is no current ode system').



import_influences_from_ode_system :-
  biocham_command,
  doc('adds a set of influences equivalent to the current ODE system.'),
  get_current_ode_system(Id),
  !,
  (assertz(flag_influences);retractall(flag_influences)),
  import_influences_from_ode_system(Id),
  retractall(flag_influences).
import_influences_from_ode_system :-
  print_message(warning,'there is no current ode system').




:- devdoc('\\section{Public API}').


new_ode_system(Id) :-
  add_item([kind: ode_system, key: new_ode_system, id: Id]).


get_current_ode_system(Id) :-
  get_selection(current_model, current_ode_system, [Id]).


set_current_ode_system(Id) :-
  set_selection(current_model, current_ode_system, [Id]).


set_ode_system_name(Id, Name) :-
  replace_item(Id, ode_system, Name, Name).


import_ode(InputFile, Id) :-
  new_ode_system(Id),
  set_ode_system_name(Id, InputFile),
  setup_call_cleanup(
    open(InputFile, read, Stream),
    read_ode(Stream, Id),
    close(Stream)
  ).


all_odes(ODEs) :-
  get_current_ode_system(Id),
  all_items([parent: Id, kind: ode], ODEs).


ode(X, E) :-
  get_current_ode_system(Id),
  ode(Id, X, E).


ode(Id, X, E) :-
  (
    var(X)
  ->
    item([parent: Id, kind: ode, item: (d(X)/dt = E)])
  ;
    find_item([parent: Id, kind: ode, key: X, item: (d(X)/dt = E)])
  ).


ode_variables(Id, X) :-
  ode(Id, X, _).


ode_variables(Id, X) :-
  item([parent: Id, kind: initial_concentration, item: init(X = _)]),
  \+ item([parent: Id, kind: ode, key: X]).


set_ode_initial_value(Variable, Value) :-
  get_current_ode_system(Id),
  set_ode_initial_value(Id, Variable, Value).


set_ode_initial_value(Id, Variable, Value) :-
  change_item([parent: Id], initial_concentration, Variable, init(Variable = Value)).


set_ode_parameters(Id, K = V) :-
  change_item([parent: Id], parameter, K, par(K = V)).

set_ode_parameters(Id, (K = V, L)) :-
  set_ode_parameters(Id, K = V),
  set_ode_parameters(Id, L).


set_ode_function(Id, Function = Definition) :-
  change_item([parent: Id], function, Function, func(Function = Definition)).


add_ode(Id, ODE) :-
  (
    parse_ode(ODE, X, Item)
  ->
    true
  ;
    throw(error(illformed_ode(ODE)))
  ),
  add_item([parent: Id, kind: ode, key: X, item: Item]).


import_reactions_from_ode_system(Id) :-
  check_cleaned(ode:assoc/2),
  enumerate_terms_in_odes(Id),
  add_terms_as_reactions(Id),
  clean(ode:assoc/2),
  forall(
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    set_initial_concentration(X, E)
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    set_parameter(K, V)
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    set_macro(function, F, D)
  ).


import_influences_from_ode_system(Id) :-
  check_cleaned(ode:assoc/2),
  enumerate_terms_in_odes(Id),
  add_terms_as_influences(Id),
  clean(ode:assoc/2),
  forall(
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    set_initial_concentration(X, E)
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    set_parameter(K, V)
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    set_macro(function, F, D)
  ).


ode_predicate(Command) :-
  parse_ode(Command, _, _).


:- devdoc('\\section{Private predicates}').


parse_ode(ODE, X, ODE) :-
  ODE = (d(X)/dt = _),
  !.

parse_ode(ODE, X, Item) :-
  ODE = (DX/dt = E),
  atom_concat(d, X, DX),
  !,
  Item = (d(X)/dt = E).


enumerate_terms_in_odes(Id) :-
  \+ (
    ode(Id, X, E),
    \+ (
      enumerate_terms_in_ode(X, E)
    )
  ).


enumerate_terms_in_ode(X, E) :-
  \+ (
    non_decomposable_term_in_expression(Coefficient, Term, E),
    \+ (
      (
        retract(assoc(Term, Occurrences))
      ->
        true
      ;
        Occurrences = []
      ),
      (
        select((X, OtherCoefficient), Occurrences, OtherOccurrences)
      ->
        NewCoefficient is Coefficient + OtherCoefficient
      ;
        NewCoefficient = Coefficient,
        OtherOccurrences = Occurrences
      ),
      (
        NewCoefficient = 0
      ->
        NewOccurrences = OtherOccurrences
      ;
        NewOccurrences = [(X, NewCoefficient) | OtherOccurrences]
      ),
      assertz(assoc(Term, NewOccurrences))
    )
  ).


non_decomposable_term_in_expression(Coefficient, Term, E) :-
  substitute_functions(E, ENoFunc),
  additive_normal_form(ENoFunc, ENormal),
  non_decomposable_term_in_additive_normal_form(Coefficient, Term, ENormal).


substitute_functions(E, ENoFunc) :-
  (
    get_current_ode_system(Id)
  ->
    all_items([parent: Id, kind: function], Functions),
    maplist(split_func, Functions, Definitions, Bodies),
    maplist(
      rewrite(substitute(Definitions, Bodies)),
      Bodies,
      NoFuncBods
    ),
    substitute(Definitions, NoFuncBods, E, ENoFunc)
  ;
    % case of add_function, there is no current system, but no functions
    % either
    ENoFunc = E
  ).


split_func(func(F = D), F, D).


non_decomposable_term_in_additive_normal_form(Coefficient, Term, A + B) :-
  !,
  (
    non_decomposable_term_in_additive_normal_form(Coefficient, Term, A)
  ;
    non_decomposable_term_in_additive_normal_form(Coefficient, Term, B)
  ).

non_decomposable_term_in_additive_normal_form(Coefficient, Term, Coefficient * Term) :-
  number(Coefficient),
  !.

non_decomposable_term_in_additive_normal_form(1, Term, Term).


:- dynamic(reactant/2).


:- dynamic(product/2).


:- dynamic(inhibitor/2).


add_terms_as_reactions(Id) :-
  \+ (
    assoc(Term, Occurrences),
    \+ (
      add_term_as_reactions(Id, Term, Occurrences)
    )
  ).


add_term_as_reactions(Id, Term, Occurrences) :-
  check_cleaned(ode:reactant/2),
  check_cleaned(ode:product/2),
  check_cleaned(ode:inhibitor/2),
  \+ (
    member((X, Coefficient), Occurrences),
    \+ (
      (
        Coefficient < 0
      ->
        OppCoefficient is - Coefficient,
        assertz(reactant(X, OppCoefficient))
      ;
        assertz(product(X, Coefficient))
      )
    )
  ),
  \+ (
    ode_variables(Id, X),
    \+ (
      (
        \+ reactant(X, _),
        partial_has_pos_val(Term, X)
      ->
        assertz(reactant(X, 1)),
        (
          retract(product(X, OldCoefficient))
        ->
          true
        ;
          OldCoefficient = 0
        ),
        NewCoefficient is OldCoefficient + 1,
        assertz(product(X, NewCoefficient))
      ;
        true
      ),
      (
        partial_has_neg_val(Term, X)
      ->
        assertz(inhibitor(X, 1))
      ;
        true
      )
    )
  ),
  findall(
    Coefficient * Object,
    reactant(Object, Coefficient),
    Reactants
  ),
  findall(
    Object,
    inhibitor(Object, _Coefficient),
    Inhibitors
  ),
  findall(
    Coefficient * Object,
    product(Object, Coefficient),
    Products
  ),
  reaction(Reaction, [
    kinetics: Term,
    reactants: Reactants,
    inhibitors: Inhibitors,   
    products: Products
  ]),
  add_item([kind: reaction, item: Reaction]),
  clean(ode:reactant/2),
  clean(ode:product/2),
  clean(ode:inhibitor/2).


:- dynamic(positive_target/2).

:- dynamic(negative_target/2).

:- dynamic(source/1).

:- dynamic(inhibitor/1).


add_terms_as_influences(Id) :-
  \+ (
    assoc(Term, Occurrences),
    \+ (
      add_term_as_influences(Id, Term, Occurrences)
    )
  ).


add_term_as_influences(Id, Term, Occurrences) :-
  check_cleaned(ode:positive_target/2),
  check_cleaned(ode:negative_target/2),
  check_cleaned(ode:source/1),
  check_cleaned(ode:inhibitor/1),
  \+ (
    member((X, Coefficient), Occurrences),
    \+ (
      (
        Coefficient < 0
      ->
        OppCoeff is -Coefficient,
        assertz(negative_target(X,OppCoeff))
      ;
        assertz(positive_target(X,Coefficient))
      )
    )
  ),
  \+ (
    ode_variables(Id, X),
    \+ (
      (
        \+ source(X),
        partial_has_pos_val(Term, X)
      ->
        assertz(source(X))
      ;
        true
      ),
      (
        partial_has_neg_val(Term, X)
      ->
        assertz(inhibitor(X))
      ;
        true
      )
    )
  ),
  findall(
    Object,
    source(Object),
    Sources
  ),
  findall(
    Object,
    inhibitor(Object),
    Inhibitors
  ),
  forall(
          positive_target(Object,Coeff),
          (influence(Influence, [
                         force: (Coeff*Term),
                         positive_inputs: Sources,
                         negative_inputs: Inhibitors,
                         sign: '+',
                         output: Object
                    ]),
           add_item([kind: influence, item: Influence]))),
  forall(
          negative_target(Object,Coeff),
          (influence(Influence, [
                         force: (Coeff*Term),
                         positive_inputs: Sources,
                         negative_inputs: Inhibitors,
                         sign: '-',
                         output: Object
                    ]),
           add_item([kind: influence, item: Influence]))),
  clean(ode:positive_target/2),
  clean(ode:negative_target/2),
  clean(ode:source/1),
  clean(ode:inhibitor/1).


partial_has_pos_val(Term, X) :- % >0
  derivate(Term, X, DTerm),
  (
    always_negative(DTerm)
  ->
    fail
  ;
    true
  ).

partial_has_neg_val(Term, X) :- % <0
  derivate(Term, X, DTerm),
  (
    always_positive(DTerm)
  ->
    fail
  ;
    true
  ).


:- dynamic(assoc/2).


compute_ode :-
  enumerate_molecules(Molecules),
  % get an empty ODE for molecules, even those that are constant
  % TODO transform them into parameters?
  forall(
    member(Molecule, Molecules),
    assertz(assoc(Molecule, 0))
  ),
  compute_ode_for_reactions,
  compute_ode_for_influences.


simplify_ode :-
  \+ (
    retract(assoc(Variable, Expression)),
    \+ (
      simplify(Expression, SimplifiedExpression),
      assertz(assoc(Variable, SimplifiedExpression))
    )
  ).


put_ode_into_system :-
  \+ (
    assoc(X, Expression),
    \+ (
      add_ode(d(X)/dt = Expression),
      get_initial_concentration(X, Concentration),
      set_ode_initial_value(X, Concentration)
    )
  ).


%! delete_single_ode(+Name:name) is det
%
% deletes the ODE associated with variable Name
% necessitates a current ODE system
delete_single_ode(Name) :-
  get_current_ode_system(Id),
  delete_item([parent: Id, kind: ode, key: Name]).


read_ode(Stream, Id) :-
  repeat,
  read_line_to_string(Stream, String),
  (
    String = end_of_file
  ->
    !
  ;
    is_xpp_ignored(String)
  ->
    fail
  ;
    string_lower(String, Lower),
    remove_single_secondary_notations(Lower, Codes),
    read_term_from_codes(Codes, Expression,
      [variable_names(VariableNames), module(ode)]),
    name_variables_and_anonymous(Expression, VariableNames),
    (
      Expression = done
    ->
      !
    ;
      (
        Expression = init(X = E)
      ->
        set_ode_initial_value(Id, X, E)
      ;
        (
          Expression = par(P)
        ;
          Expression = p(P)
        ;
          Expression = num(P)
        )
      ->
        set_ode_parameters(Id, P)
      ;
        Expression = (_DX/dt = _E)
      ->
        add_ode(Id, Expression)
      ;
        Expression = (F = D)
      ->
        set_ode_function(Id, F = D)
      ;
        Expression = end_of_file
      ->
        true
      )
    ->
      fail
    ;
      throw(error(syntax_error_in_ode_system(Codes, Expression)))
    )
  ).


remove_single_secondary_notations(String, Codes) :-
  devdoc(
    'ugly hack to handle XPP quote-based derivative and (0) notation in some cases'
  ),
  (
    (
      sub_string(String, Before, _Length, After, "'=")
    ;
      sub_string(String, Before, _Length, After, "' =")
    )
  ->
    sub_string(String, 0, Before, _, Name),
    sub_string(String, _, After, 0, Definition),
    atomics_to_string(['d', Name, '/dt = ', Definition], NewString)
  ;
    (
      sub_string(String, Before, _Length, After, "(0)=")
    ;
      sub_string(String, Before, _Length, After, "(0) =")
    )
  ->
    sub_string(String, 0, Before, _, Name),
    sub_string(String, _, After, 0, Value),
    atomics_to_string(['init ', Name, ' = ', Value], NewString)
  ;
    sub_string(String, 0, 1, After, "!")
  ->
    sub_string(String, _, After, 0, NewString)
  ;
    sub_string(String, _Before, _Length, _After, "=-")
  ->
    split_string(String, "=", "\s\t", L),
    atomics_to_string(L, " = ", NewString)
  ;
    sub_string(String, 0, 2, After, "p "),
    sub_string(String, 2, After, 0, Rest)
  ->
    atomics_to_string(['par ', Rest], NewString)
  ;
    NewString = String
  ),
  string_codes(NewString, Codes).


is_xpp_ignored(String) :-
  (
    % comment
    sub_string(String, 0, _, _, "#")
  ;
    sub_string(String, 0, _, _, "\"")
  ;
    % XPP internal option
    sub_string(String, 0, _, _, "@")
  ;
    % ignored keyword
    sub_string(String, 0, _, _, "wiener")
  ;
    sub_string(String, 0, _, _, "solve")
  ;
    sub_string(String, 0, _, _, "volterra")
  ;
    sub_string(String, 0, _, _, "options")
  ;
    sub_string(String, 0, _, _, "special")
  ;
    sub_string(String, 0, _, _, "table")
  ;
    sub_string(String, 0, _, _, "markov")
  ;
    % parameter set definition
    sub_string(String, 0, _, _, "set")
  ;
    % display
    sub_string(String, 0, _, _, "aux ")
  ),
  !.


monomial(Monomial, A + B) :-
  !,
  (
    monomial(Monomial, A)
  ;
    monomial(Monomial, B)
  ).

monomial(Monomial, A - B) :-
  !,
  (
    monomial(Monomial, A)
  ;
    monomial_opposite(Monomial, B)
  ).

monomial(A, A).


monomial_opposite(Monomial, A + B) :-
  !,
  (
    monomial_opposite(Monomial, A)
  ;
    monomial_opposite(Monomial, B)
  ).

monomial_opposite(Monomial, A - B) :-
  !,
  (
    monomial_opposite(Monomial, A)
  ;
    monomial(Monomial, B)
  ).

monomial_opposite(- A, A).


coefficient_species(Coefficient * Species, Coefficient, Species) :-
  !.

coefficient_species(Species, 1, Species).


models:add_file_suffix('ode', add_ode_system_file).


add_ode_system_file(InputFile) :-
  import_ode(InputFile),
  (flag_influences
   ->
       import_influences_from_ode_system
   ;
   import_reactions_from_ode_system),
  get_current_ode_system(Id),
  delete_item(Id).


print_ode_system :-
  get_current_ode_system(Id),
  \+ (
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    \+ (
      format('init ~w = ~w\n', [X, E]) % ~p would quote atoms which is not accepted in xpp syntax
    )
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    format('par ~w = ~w\n', [K, V])
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    format('~w = ~w\n', [F, D])
  ),
  \+ (
    item([parent: Id, kind: ode, item: (d(X)/dt = E)]),
    \+ (
      format('d~w/dt = ~w\n', [X, E])
    )
  ),
  format('done\n').


print_ode_system_as_latex :-
  get_current_ode_system(Id),
  format('\\begin{align*}\n'),
  forall(
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    format('{~w}_0 &= ~w\\\\\n', [X, E])
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    format('~w &= ~w\\\\\n', [K, V])
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    format('~w &= ~w\\\\\n', [F, D])
  ),
  forall(
    item([parent: Id, kind: ode, item: (d(X)/dt = E)]),
    format('\\frac{d~w}{dt} &= ~w\\\\\n', [X, E])
  ),
  format('\\end{align*}\n').


:- meta_predicate with_current_ode_system(0).


with_current_ode_system(Goal) :-
  (
    get_current_ode_system(_)
  ->
    Goal
  ;
    setup_call_cleanup(
      ode_system,
      Goal,
      (
        get_current_ode_system(Id),
        delete_item(Id)
      )
    )
  ).


:- meta_predicate edit_current_ode_system(0).


edit_current_ode_system(Goal) :-
  (
    get_current_ode_system(_)
  ->
    Goal
  ;
    setup_call_cleanup(
      new_ode_system,
      Goal,
      (
        get_current_ode_system(Id),
        import_reactions_from_ode_system(Id),
        delete_item(Id)
      )
    )
  ).


ode_add_expression_to_molecule(NewExpression, Molecule) :-
  (
    retract(assoc(Molecule, Expression))
  ->
    asserta(assoc(Molecule, Expression + NewExpression))
  ;
    asserta(assoc(Molecule, NewExpression))
  ).
