:- use_module(library(plunit)).

:- begin_tests(initial_state).

test(
  'present',
  [true(InitialState == [present(a), present(b)])]
) :-
  clear_model,
  command(present({ a, b })),
  all_items([kind: initial_state], InitialState).

test(
  'absent',
  [true(InitialState == [absent(a), absent(b)])]
) :-
  clear_model,
  command(absent({ a, b })),
  all_items([kind: initial_state], InitialState).

test(
  'undefined',
  [true(InitialState == [present(a), absent(b)])]
) :-
  clear_model,
  command(present({ a, b, c })),
  command(absent({ b, c, e })),
  command(undefined({ c, d, e })),
  all_items([kind: initial_state], InitialState).

test(
  'make_absent_not_present',
  [true(InitialState == [present(a), absent(b)])]
) :-
  clear_model,
  add_reaction(a => b),
  command(present(a)),
  make_absent_not_present,
  all_items([kind: initial_state], InitialState).

test(
  'make_present_not_absent',
  [true(InitialState == [absent(a), present(b)])]
) :-
  clear_model,
  add_reaction(a => b),
  command(absent(a)),
  make_present_not_absent,
  all_items([kind: initial_state], InitialState).

test(
  'present/2',
  [true((InitialState, Concentration) == ([present(a, 1), present(b, k)], 3))]
) :-
  clear_model,
  command(present(a, 1)),
  command(present(b, k)),
  command(parameter(k=3)),
  all_items([kind: initial_state], InitialState),
  get_initial_concentration(b, Concentration).


:- end_tests(initial_state).
