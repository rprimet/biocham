:- use_module(library(plunit)).

:- begin_tests(numerical_simulation).


test('mapk', [condition(flag(slow_test, true, true))]) :-
  clear_model,
  command(load(library:examples/mapk/mapk)),
  command(numerical_simulation(time:100)).

test('events') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, k = 0)),
  command(present(a)),
  numerical_simulation.

test('conditional') :-
  clear_model,
  command(if a < 0.5 then 'MA'(2) else 'MA'(1) for a => b),
  command(present(a)),
  command(numerical_simulation(initial_step_size:0.001)).

:- end_tests(numerical_simulation).
