:- module(
  initial_state,
  [
    % Commands
    list_initial_state/0,
    clear_initial_state/0,
    present/1,
    present/2,
    absent/1,
    undefined/1,
    make_absent_not_present/0,
    make_present_not_absent/0,
    % Public API
    get_initial_state/2,
    set_initial_state/2,
    get_initial_concentration/2,
    set_initial_concentration/2,
    list_model_initial_state/0,
    check_aliased_states/1,
    set_initial_values/2,
    get_initial_values/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).


:- devdoc('\\section{Commands}').


list_initial_state :-
  biocham_command,
  doc('
    lists the objects which are present (including their initial concentration)
    and absent from the initial state.'),
  list_items([kind: initial_state]).


clear_initial_state :-
  biocham_command,
  doc('
    makes undefined all objects possibly present or absent in the initial
    state.'),
  delete_items([kind: initial_state]).


present(ObjectSet) :-
  biocham_command,
  type(ObjectSet, {object}),
  doc('
    Every object in \\argument{ObjectSet} is made present in the initial
    state with initial concentration equal to 1.'),
  set_state(ObjectSet, present(_Object)).


present(ObjectSet, Concentration) :-
  biocham_command,
  type(ObjectSet, {object}),
  type(Concentration, concentration),
  doc('
    Every object in \\argument{ObjectSet} is initialized
    with the given initial \\argument{Concentration}, which can be a parameter
    name to indicate that the parameter value will be used.
    An initial value equal to 0 is equivalent to absent.'),
  set_state(ObjectSet, present(_Object, Concentration)).


absent(ObjectSet) :-
  biocham_command,
  type(ObjectSet, {object}),
  doc('
    Every object in \\argument{ObjectSet} is made absent in the initial
    state.'),
  set_state(ObjectSet, absent(_Object)).


undefined(ObjectSet) :-
  biocham_command,
  type(ObjectSet, {object}),
  doc('
    Every object in \\argument{ObjectSet} is made possibly present or absent
    in the initial state. This is the default.'),
  \+ (
    member(Object, ObjectSet),
    \+ (
      undefined_object(Object)
    )
  ).


make_present_not_absent :-
  biocham_command,
  doc('
    makes all objects (appearing in the instances of the current set of rules)
    which are not declared absent, present in the initial state.'),
  enumerate_molecules(Objects),
  \+ (
    member(Object, Objects),
    \+ defined(Object),
    \+ (
      present([Object])
    )
  ).


make_absent_not_present :-
  biocham_command,
  doc('
    makes all objects (appearing in the instances of the current set of rules)
    which are not declared present, absent in the initial state.'),
  enumerate_molecules(Objects),
  \+ (
    member(Object, Objects),
    \+ defined(Object),
    \+ (
      absent([Object])
    )
  ).


:- devdoc('\\section{Public API}').


get_initial_state(Object, State) :-
  canonical(Object, Canonical),
  (
    item([kind: initial_state, key: Canonical, item: Item])
  ->
    (
      Item = present(Canonical)
    ->
      State = present(1)
    ;
      Item = present(Canonical, Concentration)
    ->
      (
        % if we have a parameter, keep it as present(param)
        parameter_value(Concentration, _Value)
      ->
        State = present(Concentration)
      ;
        Concentration =:= 0
      ->
        State = absent
      ;
        State = present(Concentration)
      )
    ;
      Item = absent(Canonical)
    ->
      State = absent
    )
  ;
    State = undefined
  ).


set_initial_state(Object, State) :-
  (
    State == undefined
  ->
    undefined_object(Object)
  ;
    State =.. [Functor | Args],
    NewState =.. [Functor, _ | Args],
    set_state([Object], NewState)
  ).


get_initial_concentration(Object, Concentration) :-
  get_initial_state(Object, State),
  (
    State = present(Present)
  ->
    (
      % get the actual value for parameters
      parameter_value(Present, Concentration)
    ->
      true
    ;
      Concentration = Present
    )
  ;
    Concentration = 0
  ).


list_model_initial_state :-
  devdoc('
    lists the initial state in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  \+ (
    item([no_inheritance, kind: initial_state, item: InitialState]),
    \+ (
      format('~w.\n', [InitialState])
    )
  ).


check_aliased_states(EquivalenceClassList) :-
  EquivalenceClassList = [Canonical | _],
  findall(
    (Id, State),
    (
      member(Object, EquivalenceClassList),
      item([kind: initial_state, key: Object, id: Id, item: State])
    ),
    List
  ),
  (
    (
      List = []
    ;
      List = [(_, State)],
      rename_state(_, Canonical, State)
    )
  ->
    true
  ;
    (
      select((Id, State), List, Others),
      rename_state(_, Canonical, State)
    ;
      List = [(Id, State) | Others]
    )
  ->
    rename_state(State, Canonical, CanonicalState),
    findall(
      ConflictingState,
      (
        member((_, ConflictingState), Others),
        \+ rename_state(ConflictingState, Canonical, CanonicalState)
      ),
      ConflictingStates
    ),
    (
      ConflictingStates = []
    ->
      true
    ;
      print_message(warning, conflicting_states(State, ConflictingStates))
    ),
    \+ (
      member((OtherId, _), List),
      \+ (
        delete_item(OtherId)
      )
    ),
    add_item([kind: initial_state, key: Canonical, item: CanonicalState])
  ).


prolog:message(conflicting_states(State, ConflictingStates)) -->
  ['Conflicting states ~p. ~p kept.'-[ConflictingStates, State]].


:- devdoc('\\section{Private predicates}').


rename_state(absent(_), Object, absent(Object)).

rename_state(present(_), Object, present(Object)).

rename_state(present(_, Concentration), Object, present(Object, Concentration)).


set_state(ObjectSet, State) :-
  \+ (
    member(Object, ObjectSet),
    \+ (
      canonical(Object, Canonical),
      rename_state(State, Canonical, CanonicalState),
      undefined_object(Canonical),
      add_item([kind: initial_state, key: Canonical, item: CanonicalState])
    )
  ).


undefined_object(Object) :-
  (
    item([kind: initial_state, key: Object, id: Id])
  ->
    delete_item(Id)
  ;
    true
  ).


defined(Object) :-
  once(item([kind: initial_state, key: Object])).


set_initial_values(Variables, Values) :-
  maplist(set_initial_concentration, Variables, Values).


get_initial_values(Variables, Values) :-
  maplist(get_initial_concentration, Variables, Values).


set_initial_concentration(X, Y) :-
  present([X], Y).
