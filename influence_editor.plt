:- use_module(library(plunit)).


:- begin_tests(influence_editor).


test(
  'add_influence',
  [Influences == [((a, b) / c -> d), (b -< c), (/ a -< a)]]
) :-
  clear_model,
  command((a, b / c -> d)),
  command(b -< c),
  command(/ a -< a),
  all_items([kind: influence], Influences).

test(
  'influence_model',
  [
    Influences
    ==
    [
      (a, b -< b), (a, b -< a),
      (a, b -> c), (a, b -> d)
    ]
  ]
) :-
  clear_model,
  command(a + b => c + d),
  influence_model,
  all_items([kind: influence], Influences).

test(
  'influence builds',
  [Influence == (i -- 2 for (a, b) / c -> d)]
) :-
  influence(Influence, [
    name: 'i',
    force: 2,
    positive_inputs: [a, b],
    negative_inputs: [c],
    sign: +,
    output: d
  ]).

test(
  'influence builds with positive inputs',
  [Influence == (b -< c)]
) :-
  influence(Influence, [
    positive_inputs: [b],
    sign: -,
    output: c
  ]).

test(
  'influence builds with negative inputs',
  [Influence == (/ a -< a)]
) :-
  influence(Influence, [
    negative_inputs: [a],
    sign: -,
    output: a
  ]).

test(
  'influence builds with no inputs',
  [Influence == ('_' -> a)]
) :-
  influence(Influence, [
    sign: +,
    output: a
  ]).

test(
  'influence destructs',
  [
    result(Name, Force, PositiveInputs, NegativeInputs, Sign, Output)
    ==
    result(i, 2, [a, b], [c], +, d)
  ]
) :-
  influence((i -- 2 for (a, b) / c -> d), [
    name: Name,
    force: Force,
    positive_inputs: PositiveInputs,
    negative_inputs: NegativeInputs,
    sign: Sign,
    output: Output
  ]).

test(
  'influence destructs with positive inputs',
  [
    result(Name, Force, PositiveInputs, NegativeInputs, Sign, Output)
    ==
    result(Name, 'MA'(1), [b], [], -, c)
  ]
) :-
  influence((b -< c), [
    name: Name,
    force: Force,
    positive_inputs: PositiveInputs,
    negative_inputs: NegativeInputs,
    sign: Sign,
    output: Output
  ]).

test(
  'influence destructs with negative inputs',
  [
    result(Force, Name, PositiveInputs, NegativeInputs, Sign, Output)
    ==
    result('MA'(1), '', [], [a], -, a)
  ]
) :-
  influence((/ a -< a), [
    name: Name,
    force: Force,
    positive_inputs: PositiveInputs,
    negative_inputs: NegativeInputs,
    sign: Sign,
    output: Output
  ]).

test(
  'compute_ode_for_influence_model',
  [SortedODEs == [d(a)/dt = -a, d(b)/dt = a]]
) :-
  clear_model,
  command(a -> b),
  command(a -< a),
  ode_system,
  all_odes(ODEs),
  msort(ODEs, SortedODEs).



:- end_tests(influence_editor).
