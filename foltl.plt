:- use_module(library(plunit)).
:- use_module(foltl).
:- use_module(objects).

:- begin_tests(foltl, [condition(flag(slow_test, true, true))]).

test('validity_domain F', [true(Domain == (v >= 10))]) :-
  clear_model,
  add_table(table, [row('#x'), row(20), row(10), row(30), row(15)]),
  validity_domain('reachable'('x' <= 'v'), Domain).

test(
  'validity_domain =',
  [true(Domain == (v=20\/v=10\/v=30\/v=15))] 
) :-
  clear_model,
  add_table(table, [row('#x'), row(20), row(10), row(30), row(15)]),
  validity_domain('F'(x = v), Domain).

test('validity_domain X', [true(Domain == (v = 10 \/ v = 30))]) :-
  clear_model,
  add_table(table, [row('#x'), row(20), row(10), row(30), row(15)]),
  validity_domain('F'(x = v /\ 'X'(x > 10)), Domain).

test('linearity', [throws(error(not_linear(_)))]) :-
  clear_model,
  command('present(A). present(B).'),
  command('k1*A*B for A+B => 2*B'),
  command('k2*A for A => 2*A'),
  command('k3*B for B => _'),
  command('parameter(k1=2, k2=2, k3=1)'),
  numerical_simulation,
  command('validity_domain(F(exists(c, exists(t1, exists(t2, Molecule=c /\\ Time=t1 /\\ t1<=8.1 /\\ X(Time=t2 /\\ t2>8.1 /\\ Concentration=c+(8.1-t1)*(Molecule-c)/(t2-t1)))))))').

test('satisfaction_degree', [all(D =:= [0.5, 2.0, 1.0])]) :-
  clear_model,
  command('present(a)'),
  command('a => b'),
  numerical_simulation,
  (
    satisfaction_degree(x > 1 \/ x < -2, [x -> 0], D)
  ;
    satisfaction_degree(x >= 1 \/ x < -2, [x -> 2], D)
  ;
    satisfaction_degree(x = 1, [x -> 1], D)
  ).

:- end_tests(foltl).
