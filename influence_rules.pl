:- module(
  influence_rules,
  [
    % Grammars
    influence/1,
    basic_influence/1,
    inputs/1,
    enumeration/1,
    op(1050, xfx, (->)),
    op(1050, xfx, -<),
    op(1025, fx, (/)),
    % Public API
    influence_predicate/1,
    patch_inputs/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(reaction_rules).

:- doc('BIOCHAM models can also be defined by influence systems with forces, possibly mixed to reactions with rates.').

:- doc('In the syntax described by the grammar below, one influence rule (either positive "->" or negative "-<") expresses that a conjunction of sources (or their negation after the separator "/" for inhibitors) has an influence on a target molecular species. This syntax necessitates to write the Boolean activation (resp. deactivation) functions of the molecular species in Disjunctive Normal Form, i.e. with several positive (resp. negative) influences in which the sources are interpreted by a conjunction \\cite{FMRS18tcbb}.').

:- devdoc('\\section{Grammars}').


:- grammar(influence).


influence(Force for BasicInfluence) :-
  arithmetic_expression(Force),
  basic_influence(BasicInfluence).

influence(BasicInfluence) :-
  basic_influence(BasicInfluence).


:- grammar(basic_influence).


basic_influence(Inputs -> Output) :- 
  inputs(Inputs),
  object(Output).

basic_influence(Inputs -< Output) :- 
  inputs(Inputs),
  object(Output).

:- grammar(inputs).


inputs(PositiveInputs / NegativeInputs) :-
  enumeration(PositiveInputs),
  enumeration(NegativeInputs).


inputs(/ NegativeInputs) :-
  enumeration(NegativeInputs).


inputs(PositiveInputs) :-
  enumeration(PositiveInputs).


:- grammar(enumeration).


enumeration('_').


enumeration((A, B)) :-
  object(A),
  enumeration(B).


enumeration(A) :-
  object(A).


:- devdoc('\\section{Public API}').


influence_predicate(_ for Reaction) :-
  influence_predicate(Reaction).

influence_predicate(_ -> _).

influence_predicate(_ -< _).


patch_inputs((A / B, C), A / (B, C)) :-
  !.

patch_inputs((A, B), (A, B0) / B1) :-
  patch_inputs(B, B0 / B1),
  !.

patch_inputs(Inputs, Inputs).
