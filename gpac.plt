:- use_module(library(plunit)).

:- use_module(gpac).

:- begin_tests(gpac).

%%% Test for format_pivp %%%

test(format_pivp_exp) :-
   gpac:format_pivp(((1,(d(y)/dt=(-2*y)))),
                    [1,[[[-2,[1]]]],[1]] ).

test(format_pivp_cosinus) :-
   gpac:format_pivp(((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y)))),
                    [2,[[[1,[0, 1]]],[[-1,[1, 0]]]],[1,0]] ).

% This PIVP generates choicepoints in the cutting of y*z+3*z, hence the once
test(format_pivp_higher_order) :-
   once(gpac:format_pivp(((1,(d(y)/dt=y*z+3*z)); (5,(d(z)/dt= (-1*y^3)))),
                    [2,[[[1,[1, 1]],[3,[0, 1]]],[[-1,[3, 0]]]],[1,5]] )).

%%% Test of g_to_c %%%
test(g_to_c_PIVP) :-
   Old_PIVP = [1, [[[-1,[1]]]], [1]],
   New_PIVP = [2, [[[-1,[1,1]]],[[-1,[0,1]]]], [1,2]],
   gpac:g_to_c_PIVP(Old_PIVP,New_PIVP, 2).

%%% Test of binomial reduction %%%

test(compute_number_species) :-
   gpac:compute_number_species([3,1],8,0),
   gpac:compute_number_species([0,3,0,1,0],8,3),
   gpac:compute_number_species([2,2,0],9,1).

test(generate_variable) :-
   gpac:generate_variable([3,1],7,[[1,0],[2,0],[3,0],[0,1],[1,1],[2,1],[3,1]]),
   gpac:generate_variable([2,2],8,[[1,0],[2,0],[0,1],[1,1],[2,1],[0,2],[1,2],[2,2]]),
   gpac:generate_variable([0,2,0,1],7,[[1,0,0,0],[0,0,1,0],[0,1,0,0],[0,2,0,0],[0,0,0,1],[0,1,0,1],[0,2,0,1]]).

test(reduce_to_binomial_nomodif) :-
   once(gpac:reduce_to_binomial([2,[[[1,[1, 1]]],[[-1,[1, 0]]]],[1,2]],P)),
   P = [2,[[[1,[1, 1]]],[[-1,[1, 0]]]],[1,2]].

test(reduce_to_binomial_simple) :-
   once(gpac:reduce_to_binomial([2,[[[1,[1, 2]]],[[-1,[1, 0]]]],[1,2]],P)),
   P = [5,[[[1,[0,0,0,0,1]]],
           [[-1,[1,0,0,0,0]]],
           [[1,[0,1,0,0,1]],[-1,[2,0,0,0,0]]],
           [[-2,[1,1,0,0,0]]],
           [[1,[0,0,0,1,1]],[-2,[1,0,1,0,0]]]],[1,2,2,4,4]].

test(scan_order_multivar) :-
   gpac:scan_order_multivar([[1,[1, 0]],[1,[1, 1]]],2),
   gpac:scan_order_multivar([[1,[1, 2]],[1,[1, 1]]],3),
   gpac:scan_order_multivar([[1,[1, 2]],[1,[12, 1]]],13).

test(add_new_variable_monomial) :-
   gpac:add_new_variable_monomial([1,[1,1,1]],[1,0,1],[1,[0,1,0,1]]),
   gpac:add_new_variable_monomial([2,[1,1,0]],[1,0,1],[2,[1,1,0,0]]),
   gpac:add_new_variable_monomial([2,[2,1,3]],[1,0,1],[2,[0,1,1,2]]).

test(add_new_variable) :-
   PODE = [ [[1,[0,1]],[2,[1,1]]] , [[3,[1,0]]] ],
   Modified_PODE = [[[1,[0,1,0]],[2,[0,0,1]]],[[3,[1,0,0]]],[[3,[2,0,0]],[1,[0,2,0]],[2,[0,1,1]]]],
   gpac:add_new_variable([2,PODE,[1,2]],[1,1],[3,Modified_PODE,[1,2,2]]).

test(compute_derivative) :-
   PODE = [ [[1,[0,1]],[2,[1,1]]] , [[3,[1,0]]] ],
   gpac:compute_derivative([1,1],PODE,1,[[3,[2,0]],[1,[0,2]],[2,[1,2]]]).

:- end_tests(gpac).
