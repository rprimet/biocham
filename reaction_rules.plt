:- use_module(library(plunit)).


:- begin_tests(reaction_rules).


test(
  'patch_arithmetic_expression',
  [Patched == (if a then (if b then c else d) else (if e then f else g))]
) :-
  patch_arithmetic_expression(
    if a then
      if b then c else d
    else
      if e then f else g,
    Patched
  ).

test(
  'patch_solution',
  [
    forall(
      member(
        (In-Out),
        [
          (a * b-c-d + e * f-g-h, a * (b-c-d) + e * (f-g-h)),
          (2*d@e+b-c@f, 2*(d@e)+(b-c@f)),
          (a+b@c+3*a-b@f+2*g, a+(b@c)+3*a-(b@f)+2*g)
        ]
      )
    ),
    true(Patched == Out)
  ]
) :-
  patch_solution(In, Patched).


:- end_tests(reaction_rules).
