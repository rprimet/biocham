:- use_module(library(plunit)).


:- begin_tests(reaction_graphs).


test(
  'reaction_graph',
  [
    result(Edges, Stoichiometry, Kinetics)
    ==
    result(['A' -> 'reaction0', 'reaction0' -> 'B'], 2, 'MA'(3))
  ]
) :-
  clear_model,
  add_reaction('MA'(3) for 'A' => 2 * 'B'),
  reaction_graph,
  get_current_graph(GraphId),
  all_items([parent: GraphId, kind: edge], Edges),
  once((
    get_attribute(GraphId, 'reaction0', kinetics = Kinetics),
    get_attribute(GraphId, ('reaction0' -> 'B'), stoichiometry = Stoichiometry)
  )).

test(
  'import_reactions_from_graph',
  [true(Reactions == [ a =[b]=> c, 'MA'(2) for a + 3 * c => 2 * d ])]
) :-
  clear_model,
  new_graph,
  command(transition('reaction0')),
  command(
    add_edge(
      'a' -> 'reaction0',
      'b' -> 'reaction0',
      'reaction0' -> 'b',
      'reaction0' -> 'c',
      'a' -> 'reaction1'
    )
  ),
  command(set_attribute('reaction1', kinetics = 'MA'(2))),
  command(set_attribute(('c' -> 'reaction1'), stoichiometry = 3)),
  command(set_attribute(('reaction1' -> 'd'), stoichiometry = 2)),
  import_reactions_from_graph,
  all_items([kind: reaction], Reactions).

:- end_tests(reaction_graphs).
