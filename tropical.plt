:- use_module(library(plunit)).

:- begin_tests(tropical).


test(
  'tyson_full_equilibration',
  [
    setup(load('library:examples/cell_cycle/Tyson_1991.bc')),
    cleanup(clear_model),
    true(N == 3)
  ]
) :-
  with_output_to(
    atom(Output),
    with_option(
      tropical_max_degree: 2,
      tropicalize
    )
  ),
  findall(
    Pos,
    (
     sub_atom(Output, Pos, _, _, 'found a complete equilibration'),
     once(sub_atom(Output, _, _, _, 'ε'))
    ),
    Positions
  ),
  length(Positions, N).


:- end_tests(tropical).
