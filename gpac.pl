/** <gpac> Tools to generate set of chemical reactions

The two main functions to be exported are compile_from_expression and
compile_from_pivp that generates set of equations starting from a
mathematical expression or a PIVP define here as a tuple like object.

Three main type of objects are manipulated here:

expression - mathematical description of function
example : cos

pivp_string - tuple like description of a set of ODE (corresponding to a
chemical reaction set)
example : ((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y))))

pivp_list - a list description of the PIVP used for internal computation
in the format [N_species,[reactions_s1,reactions_s2,...],Initial_concentration]
where reactions_s1 is a list of lists of the form
[k, [exponent s1,..., exponent sn]]
example : [2, [ [[1, [0, 1]]] , [[-1, [1, 0]]] ], [1, 0]]

Rq: The distinction between pivp_string and pivp_list is not always obvious.

@authors Guillaume Le Guludec, M. Hemery
@license GNU-GPL v.2
*/

:- module(
  gpac,
  [
    %Commands
    compile_from_expression/3,
    compile_from_pivp/3,
    %Grammar
    polynomial/1,
    monomial/1,
    pode/1,
    pivp/1,
    %API
    test_2908/0,
    format_pivp/2
  ]).

:- use_module(doc).
:- use_module(biocham).
:- use_module(lazy_negation_gpac).
:- use_module(objects).
:- use_module(reaction_rules).
:- use_module(util).

:- doc('The Turing completeness of continuous CRNs \\cite{FLBP17cmsb} states that any computable function over the reals can be computed by a CRN over a finite set of molecular species. Biocham uses the proof of that result to compile any computable real function presented as the solution of a polynomial differential equation system into a finite CRN. The restriction to reactions with at most two reactants is an option.').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Main Tools of the module %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- initial(option(binomial_reduction: no)).
:- initial(option(lazy_negation: yes)).

%! compile_from_expression(+Expr, +Input, +Output)
%
% biocham command
%
% Creates a biochemical reaction network such that Output = Expr(Input)
%
% Typical usage will be:
% ==
% :- compile_from_expression(4 + id*id, time, output).
% ==

compile_from_expression(Expr, Input, Output) :-
  biocham_command,
  type(Expr, arithmetic_expression),
  type(Input, name),
  type(Output, name),
  doc('
    creates a biochemical reaction network such that Output = Expr(Input). Use \'time\' as
    input to get Output = Expr(t).
  '),
  option(
    binomial_reduction,
    yesno,
    Reduction,
    'Determine if the binomial reduction has to be performed'
  ),
  option(
    lazy_negation,
    yesno,
    _Lazyness,
    'Switch between a brutal or a lazy negation'
  ),
  expression_to_PIVP(Expr, PIVP),
  pivp_compile(PIVP, Input, Output, Reduction).

:- doc('
  \\begin{example} Compilation of the expression 4+time^2:\n
').
:- biocham(compile_from_expression(4 + id*id, time, output)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:4)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
  ').

:- doc('
  \\begin{example} Compilation of the expression cos(time):\n
').
:- biocham(compile_from_expression(cos, time, f)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:10)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
').


:- doc('
  \\begin{example} Compilation of the expression cos(x) and computation of cos(4):\n
').
:- biocham(compile_from_expression(cos, x, r)).
:- biocham(list_model).
:- biocham(present(x_p, 4)).
:- biocham(numerical_simulation(time:10)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
').

:- devdoc('\\begin{todo} share common subexpression\\end{todo}').

%FF To bring back when lazy negation will be an option for all commands
%:- doc('
%  \\begin{example} Compilation of a Hill function of order 2 as a function of time\n
%').
%:- biocham(compile_from_expression(id*id/(1+id*id), time, hill)).
%:- biocham(list_model).
%:- biocham(numerical_simulation(time:20)). % method:msbdf
%:- biocham(plot(show:{hill_p,hill_m})).
%:- biocham(plot(show:{hill_p,hill_m}, logscale:x)).
%:- doc('
%  \\end{example}
%').

:- doc('One can also compile real valued functions defined as solutions of Polynomial Initial Value Problems (PIVP), i.e. solutions of polynomial differential equations, using the following syntax:').

% Definition of the various grammars used in this module:
% pivp, pode, polynomial and monomial
% the tagging ':- grammar(_)' is here for the documentation


%! pivp(+Expression)
%
% Test if the expression correspond to a valid PIVP_string

:- grammar(pivp).

pivp((X;Y)):-
  pivp(X),
  pivp(Y).

pivp((X,Y)):-
  number(X),
  pode(Y).


%! pode(+Expression)
%
% Test if the expression correspond to a valid pODE

:- grammar(pode).

pode((d(X)/dt = Y)):-
  name(X),
  polynomial(Y).


%! polynomial(+Expression)
%
% Test if the expression correspond to a valid polynomial

:- grammar(polynomial).

polynomial(+ X):-
  polynomial(X).

polynomial(- X):-
  polynomial(X).

polynomial(X + Y):-
  polynomial(X),
  polynomial(Y).

polynomial(X - Y):-
  polynomial(X),
  monomial(Y).

polynomial(X):-
  monomial(X).


%! monomial(+Expression)
%
% Test if the expression correspond to a valid monomial

:- grammar(monomial).

monomial(X ^ Y):-
  name(X),
  number(Y).

monomial(X * Y):-
  monomial(X),
  monomial(Y).

monomial(X) :-
  number(X).

monomial(X) :-
  name(X).


%! compile_from_pivp(+PIVP_string, +Input , +Output)
%
% biocham command
%
% Creates a biochemical reaction network such that Output = f(Input)
% where f is the solution of the PIVP.
%
% Typical usage will be:
% ==
% :- compile_from_pivp( ((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y)))),time,output).
% ==

compile_from_pivp(PIVP, Input, Output) :-
  biocham_command,
  type(PIVP, pivp),
  type(Input, name),
  type(Output, name),
  doc('
    compiles a PIVP into biochemical reactions. Use \'time\' as input
    to get output f(t), or use any other input x to get f(x) as output.
  '),
  option(
    binomial_reduction,
    yesno,
    Reduction,
    'Determine if the binomial reduction has to be performed'
  ),
  format_pivp(PIVP, P),
  pivp_compile(P, Input, Output, Reduction).

:- devdoc('\\begin{todo} Compilation of the cosine function as a function of time defined as the solution of a PIVP)\n
\\texttt{
 :- biocham(compile_from_pivp(((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y)))), time, output)).
 :- biocham(list_model).
 :- biocham(numerical_simulation(time:10)).
 :- biocham(plot).
}
   \\end{todo}
   ').

%! test_2908
%
% Test runs at the compilation of biocham

test_2908 :-
%  biocham_command,
  exp_PIVP(EXP_PIVP),
  invert_PIVP(EXP_PIVP, INVEXP_PIVP),
  clear_model,
  compile_PIVP(INVEXP_PIVP, input, output).



:- devdoc('
  The internal data structure for PIVPs (Polynomial differential equations
  Initial Value Problems) is
  \\texttt{[ max(1,dimension), [ differential equations ] , [initial values] ]}

  The differential equations are represented by the list of lists (sums) of
  monomials (list of exponents) for each variable.

  \texttt{[ [ [coeff, [exponent x1,..., exponent xn]], ... other monomials for
  x1 differential... ], ... other differential functions for xi differential... ]}
').


%! pivp_compile(+PIVP_list, +Input, +Output, +Reduction)
%
% Compile a PIVP_list, this predicate dispatch the various options of compilation
% it first check if the PIVP should be reduce to a binomial form
% then implement a lazy negation if asked
% when Input is not time it then call g_to_c_PIVP/3.
% and finally make a call to compile_PIVP to make the final implementation

pivp_compile(PIVP_non_bin, Input, Output, Reduction):-
   clear_model,
   get_option(fast_rate, Fast),
   (
      Reduction = yes
   ->
      reduce_to_binomial(PIVP_non_bin,PIVP_unsigned)
   ;
      PIVP_unsigned = PIVP_non_bin
   ),
   get_option(lazy_negation, Lazyness),
   (
      Lazyness = yes
   ->
      lng:rewrite_PIVP(PIVP_unsigned, PIVP, VarNeg),
      (
         \+(VarNeg = [])
      ->
         lazy_anihilation_reactions(VarNeg,0),
         set_parameter(fast, Fast)
      ;
         true
      )
   ;
      PIVP = PIVP_unsigned,
      set_parameter(fast, Fast)
   ),
   (
      Input = time
   ->
      compile_PIVP(PIVP, Input, Output)
   ;
      g_to_c_PIVP(PIVP, PIVPc, 1),
      assert(name_spec),
      compile_PIVP(PIVPc, Input, Output),
      retract(name_spec)
   ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools to format a PIVP_string to a PIVP_list %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! format_pivp(+PIVP_string, -PIVP_list)
%
% Generates the PIVP_list object corresponding to a given PIVP_string

format_pivp(P, [N, Pode, Init]):-
   find_variables(P, L, Init),
   length(L, N), % should put 1 if no variable ?
   parse_pivp(P, L, Pode).

:- devdoc('
  \\command{format_pivp/2} encodes a PIVP syntax tree into the internal list
  representation for PIVPs.
').


%! find_variables(+PIVP_string, -ListVariableNames, -InitialConcentrations)
%
% Scan a PIVP_string to extract the variables and initial concentrations.

find_variables((I, (d(X)/dt=_)), [X], [I]).

find_variables((X;Y), L, Init):-
   find_variables(X, L1, I1),
   find_variables(Y, L2, I2),
   append(L1, L2, L),
   append(I1, I2, Init).


%! parse_pivp(+PIVP_string, +ListVariableNames, -Pode)
%
% Giving the PIVP_string and its variable, extract the list representation
% of the polynomials associated with PIVP_list

parse_pivp((X;Y), L, Pode):-
   parse_pivp(X, L, P1),
   parse_pivp(Y, L, P2),
   append(P1, P2, Pode).

parse_pivp((_,(d(_)/dt=P)), L, [Pol]):-
   parse_polynomial(P, L, Pol).


%! parse_polynomial(+Polynomial_string, +ListVariableNames, -Polynomial_list)
%
% Scan the right hand side of the PIVP_string representation (a polynomial)
% to produce the list representation used by PIVP_list
% Work by cutting the polynomial in the different monomial and delegates to
% parse_monomial/3.

parse_polynomial(+ X, L, P):-
   parse_polynomial(X, L, P).

parse_polynomial(- X, L, [D, R]):-
   parse_polynomial(X, L, [C, R]),
   D is -C.

parse_polynomial(X + Y, L, R):-
   parse_polynomial(X, L, R1),
   parse_polynomial(Y, L, R2),
   append(R1, R2, R).

parse_polynomial(X - Y, L, [[D, M] | R1]):-
   parse_polynomial(X, L, R1),
   parse_monomial(Y, L, [C, M]),
   D is -C.

parse_polynomial(X, L, [M]):-
   parse_monomial(X, L, M).

%! parse_monomial(+Monomial_string, +ListVariableNames, -Monomial_list)
%
% Scan a Monomial_string representation to produce the list representation

parse_monomial(N * X, L, [N, Monom_term]) :-
   number(N),!,
   parse_monomial(X, L, [1, Monom_term]).

parse_monomial(X * Y, L, M) :-
   parse_monomial(X, L, [K,Pol1]),
   parse_monomial(Y, L, [1,Pol2]),
   add_list(Pol1,Pol2,Pol12),
   M = [K,Pol12].

parse_monomial(X ^ Y, L, [1,M]):-
   name(X),
   number(Y),
   make_monomial(L, X, Y, M).

parse_monomial(X, L , [X, ListOfZero]) :-
   number(X),
   make_monomial(L,_Dummy,0,ListOfZero).

parse_monomial(X, L, [1, M]) :-
   name(X),
   make_monomial(L, X, 1, M).


%! make_monomial(+ListVariableNames, +Variable_name, +Coefficient, -Monomial)
%
% Given a variable name and a coefficient, return the corresponding monomial.

make_monomial([], _, _, []).

make_monomial([X|L], X, Y, [Y|M]) :-
   !,
   make_monomial(L, X, Y, M).

make_monomial([_|L], X, Y, [0|M]) :-
   make_monomial(L, X, Y, M).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools to manipulate PIVP_list internal objects %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% A multivar is a list of monomial
% A monomial is a unique object of the form: [K,[Exp1,Exp2,Exp3]]
% corresponding to the term K v1^Exp1 V2^Exp2 etc.

:- dynamic(input_name/1,
  output_name/1,
  name_spec/0).

%! compare_exponants(+ExponentSet1,+ExponentSet2,-Comparator)
%
% Compare ExponentSet1 to ExponentSet2 (in this order)
% Comparator will be g (greater), l (lower) or e (equal).
% Note that the ordering is done with respect to the firsts not equal
% coefficients so that:
% ==
% ?- compare_exponants([1,1,0],[1,0,9],g).
% true.
% ==
% It thus provides a complete order on the set of exponent sets.

compare_exponants([A1], [A2], C) :-
  (
    A1 < A2
  ->
    C = l
  ;
    A1 =:= A2
  ->
    C = e
  ;
    A1 > A2
  ->
    C = g
  ),
  !.

compare_exponants([A1|T1], [A2|T2], C) :-
  compare_exponants([A1], [A2], K),
  !,
  (
    K = e
  ->
    compare_exponants(T1, T2, C)
  ;
    C = K
  ).

%! add_multivar(+ReactionSet1,+ReactionSet2,-ReactionSetSum)
%
% Add two sets of reactions so that terms with same exponents are added

add_multivar([], ReactionSet, ReactionSet) :- !.

add_multivar(ReactionSet, [], ReactionSet) :- !.

add_multivar([[K1, ExpList1]|T1], [[K2, ExpList2]|T2], ReactionSum) :-
   compare_exponants(ExpList1, ExpList2, C),
   (
      C = l
   ->
      add_multivar(T1, [[K2, ExpList2]|T2], T),
      ReactionSum = [[K1, ExpList1]|T]
   ;
      C = e
   ->
      add_multivar(T1, T2, T),
      K is K1 + K2,
      (
         K =\= 0
      ->
         ReactionSum = [[K, ExpList1]|T]
      ;
         ReactionSum = T
      )
   ;
      C = g
   ->
      add_multivar([[K1, ExpList1]|T1], T2, T),
      ReactionSum = [[K2, ExpList2]|T]
   ).


%! multiply_multivar(+ReactionSet1,+ReactionSet2,-ReactionProduct)
%
% Multiply two sets of reactions

multiply_multivar(P, one, P) :- !.

multiply_multivar(one, P, P) :- !.

multiply_multivar(_, [], []) :- !.

multiply_multivar(Polynom, [Monom|Tail], ReactionProduct) :-
   multiply_multivar_monomial(Polynom, Monom, Polynom_times_Monom),
   multiply_multivar(Polynom, Tail, Polynom_times_Tail),
   add_multivar(Polynom_times_Monom, Polynom_times_Tail, ReactionProduct).


%! multiply_multivar_monomial(+ReactionSet1,+Reaction2,-ReactionProduct)
%
% Multiply a set of reactions by a unique monome to gives ReactionProduct

multiply_multivar_monomial([], _, []) :- !.

multiply_multivar_monomial([[K1, Exp1]|Tail], [K2, Exp2], [[K, Exp]|TailProd]):-
   K is K1 * K2,
   add_list(Exp1, Exp2, Exp),
   multiply_multivar_monomial(Tail, [K2, Exp2], TailProd).


%! multiply_multivar_vector(+MultivarVector,+Multivar,-MultivarVectorResult)
%
% Multiply all the Multivar in MultivarVector by the second argument

multiply_multivar_vector([], _, []) :- !.

multiply_multivar_vector([P1|T1], P2, [P1P2|T]) :-
   multiply_multivar(P1, P2, P1P2),
   multiply_multivar_vector(T1, P2, T).


%! split(+List, -OddTerm, -EvenTerm)
%
% Split a list between odd and even indexes, that is:
% ==
% ?- split([1,a,b,3,15,6],OddList,EvenList).
% OddList = [1, b, 15],
% EvenList = [a, 3, 6].
% ==

split([], [], []) :- !.

split([M], [M], []) :- !.

split([M1, M2|T], [M1|T1], [M2|T2]) :-
   split(T, T1, T2).

%! sort_multivar(+Multivar,-SortedMultivar)
%
% Sort a multivar according to the order defined by compare_exponants
% (Due to the fact that Add procede according to this order)

sort_multivar([], []) :- !.

sort_multivar([Monomial], [Monomial]) :- !.

sort_multivar(Multivar, SortedMultivar) :-
   split(Multivar, M1, M2),
   sort_multivar(M1, Sorted_M1),
   sort_multivar(M2, Sorted_M2),
   add_multivar(Sorted_M1, Sorted_M2, SortedMultivar).


%! translate_exponant(+ExpSet,+N,-ExpSetResult)
%
% Add N '0' to the Left of ExpSet

translate_exponant(E, 0, E) :- !.

translate_exponant(E, N, [0|Ep]) :-
   Np is N - 1,
   translate_exponant(E, Np, Ep).


%! extend_exponant(+ExpSet,+N,-ExpSetResult)
%
% Add N '0' to the Right of ExpSet

extend_exponant([], N, Z) :-
   translate_exponant([], N, Z), !.

extend_exponant([A|T], N, [A|ET]) :-
   extend_exponant(T, N, ET).


%! displace_exponent(+ExpSet,+Left,+Right,-ExpSetResult)
%
% Add 0 to the Left and Right of ExpSet

displace_exponent(E, Left, Right, Ed) :-
   translate_exponant(E, Left, Et),
   extend_exponant(Et, Right, Ed).


%! displace_multivar(+Multivar,+Left,+Right,-MultivarResult)
%
% Add 0 to the Left and Right of all the Exponants of Multivar

displace_multivar([], _, _, []) :- !.

displace_multivar([[X, E]|T], Left, Right, [[X, Ed]|Td]) :-
   displace_exponent(E, Left, Right, Ed),
   displace_multivar(T, Left, Right, Td).


%! displace_multivar_vector(+MultivarVector,+Left,+Right,-MultivarVectorResult)
%
% Add 0 to the Left and Right of all the Exponants of MultivarVector

displace_multivar_vector([], _, _, []) :- !.

displace_multivar_vector([P|T], Left, Right, [Pd|Td]) :-
   displace_multivar(P, Left, Right, Pd),
   displace_multivar_vector(T, Left, Right, Td).


%! get_element(+List,+N,-Element)
%
% Get the Nth Element in the List

get_element([H|_], 1, H) :- !.

get_element([_|T], N, I) :-
   Np is N - 1,
   get_element(T, Np, I).


%! unitary(+N,List)
%
% test if List is a list of exactly N zeros.

unitary(0, []) :- !.

unitary(N, [0|Tail]) :-
   Nm is N - 1,
   unitary(Nm, Tail).


%! power_multivar(+N,+Multivar,-MultivarResult)
%
% return Multivar to the power N

power_multivar(0, [[_, Exp]|_], [[1, Result]]) :-
   length(Exp, N), !,
   unitary(N, Result).

power_multivar(N, Multivar, Result) :-
   Np is N - 1,
   power_multivar(Np, Multivar, MultivarTempo),
   multiply_multivar(Multivar, MultivarTempo, Result).

%! scalar_multivar(+N,+Multivar,-MultivarResult)
%
% return Multivar multiplied by N

scalar_multivar(0, _, []) :- !.

scalar_multivar(_, [], []) :- !.

scalar_multivar(N, [[K, Exponant]|Tail], [[NK, Exponant]|TailResult]) :-
   NK is N * K,
   scalar_multivar(N, Tail, TailResult).


%! compose_multivar_monomial(+Monomial,+MultivarVector,-MultivarResult)
%
% If Monomial is [K,[E1,E2]] and MultivarVector [M1,M2]
% MultivarResult will be K * M1^E1 * M2^E2

compose_multivar_monomial([X, E], PL, P) :-
   compose_multivar_unitary(E, PL, P0),
   scalar_multivar(X, P0, P).


%! compose_multivar_unitary(+ExponentList,+MultivarVector,-MultivarResult)
%
% If ExponentList is [E1,E2] and MultivarVector [M1,M2]
% MultivarResult will be M1^E1 * M2^E2

compose_multivar_unitary([], [], one) :- !.

compose_multivar_unitary([A|AT], [P|PT], Q) :-
   power_multivar(A, P, P0),
   compose_multivar_unitary(AT, PT, P1),
   multiply_multivar(P0, P1, Q).


%! compose_multivar(+Multivar,+MultivarVector,-MultivarResult)
%
% same as compose_multivar_monomial but with a multivar (ie. a sum of monomial)

compose_multivar([], _, []) :- !.

compose_multivar([[X, E]|T], PL, Q) :-
   compose_multivar_monomial([X, E], PL, P0),
   compose_multivar(T, PL, P1),
   add_multivar(P0, P1, Q).


%! compose_multivar_vector(+MultivarVector,+MultivarVector,-MultivarVectorResult)
%
% Composition of two MultivarVectors to gives a third one

compose_multivar_vector([], _, []) :- !.

compose_multivar_vector([P|PV], PL, [Q|QV]) :-
   compose_multivar(P, PL, Q),
   compose_multivar_vector(PV, PL, QV).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reduction to binomial PIVP %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! reduce_to_binomial(PIVP,Modified_PIVP)
%
% Reduce a given PIVP to a new one which only need binomial
% TODO - erase un-needed variable after that (if any)

reduce_to_binomial([N,PODE,IV],[NewN,NewPODE,NewIV]) :-
   scan_order_multivarvector(PODE,Order),
   (
      Order > 2
   ->
      highest_exponant_vector(PODE,HighestExp),
      generate_variable(HighestExp,NewN,ListVariable),
      once(generate_pode(ListVariable,PODE,ListVariable,HighestExp,NewPODE)),
      generate_iv(ListVariable,IV,NewIV)
   ; % PIVP is already binomial
      NewN = N,
      NewPODE = PODE,
      NewIV = IV
   ).


%! add_new_variable(+PIVP,+Exponent,-Modified_PIVP)
%
% Modify PIVP to bundle Exponent in a single new variable

add_new_variable([N,PODE,Init],Exponent,[N_new,PODE_new,Init_new]):-
   N_new is N+1,
   set_new_iv(Exponent,Init,NewValue),
   once(compute_derivative(Exponent,PODE,1,Derivative)),
   append(PODE,[Derivative],PODE_modif),
   add_new_variable_multivarvector(PODE_modif,Exponent,PODE_new),
   append(Init,[NewValue],Init_new).

%! compute_derivative(+Exponent,+PODE,+Counter,-Derivative)
%
% compute the derivative of Exponent in PODE framework

compute_derivative(Exponent,_PODE,Counter,[]) :-
   CounterM is Counter - 1,
   length(Exponent,CounterM), !.

compute_derivative(Exponent,PODE,Counter,Derivative) :-
   CounterP is Counter + 1,
   get_element(Exponent,Counter,N),
   (
      N =:= 0
   ->
      compute_derivative(Exponent,PODE,CounterP,Derivative)
   ;
      length(Exponent,Len),
      CounterM is Counter - 1,
      Reminder is Len - Counter,
      displace_exponent([1],CounterM,Reminder,Spec),
      substract_N_exp(Exponent,1,Spec,ExpWithoutSpec),
      get_element(PODE,Counter,This_Derivative),
      multiply_multivar_monomial(This_Derivative,[N,ExpWithoutSpec],ToAdd),
      append(Derivative_modif,ToAdd,Derivative),
      compute_derivative(Exponent,PODE,CounterP,Derivative_modif),
      !
   ).


%! add_new_variable_multivarvector(PODE,Exponent,PODE_modif)
%
% Modify multivarvector to bundle Exponent in a single new variable

add_new_variable_multivarvector([],_Exp,[]).

add_new_variable_multivarvector([Vector|PODE_T],Exp,[Vector_new|PODE_T_new]):-
   add_new_variable_multivar(Vector,Exp,Vector_new),
   add_new_variable_multivarvector(PODE_T,Exp,PODE_T_new).


%! add_new_variable_multivar(Multivar,Exponent,Multivar_modif)
%
% Modify multivar to bundle Exponent in a single new variable

add_new_variable_multivar([],_Exp,[]).

add_new_variable_multivar([Monomial|Multivar_T],Exp,[Monomial_new|Multivar_T_new]):-
   add_new_variable_monomial(Monomial,Exp,Monomial_new),
   add_new_variable_multivar(Multivar_T,Exp,Multivar_T_new).


%! add_new_variable_monomial(Monomial,Exp,Monomial_new)
%
% Add a new variable corresponding to Exp to the monomial

add_new_variable_monomial([K,Exp_m],Exp,[K,Exp_m_new]):-
   check_presence(Exp_m,Exp,N),
   substract_N_exp(Exp_m,N,Exp,Exp_m_modif),
   append(Exp_m_modif,[N],Exp_m_new).

%! check_presence(Exponent_m,Exponent_s,N)
%
% P is how many times Exponent_s is present in Exponent_m

check_presence([],[],999).

check_presence([Head_m|Tail_m],[Head_s|Tail_s],N):-
   (
      Head_s =:= 0
   ->
      check_presence(Tail_m,Tail_s,N)
   ;
      N1 is div(Head_m,Head_s),
      check_presence(Tail_m,Tail_s,N2),
      N is min(N1,N2)
   ).

%! substract_N_exp(Exp_m,N,Exp,Exp_m_modif)
%
% As list: Exp_m_modif = Exp_m - N * Exp

substract_N_exp([],_N,[],[]).

substract_N_exp([Head_m|Tail_m],N,[Head_s|Tail_s],[Head_new|Tail_new]):-
   Head_new is Head_m - N*Head_s,
   substract_N_exp(Tail_m,N,Tail_s,Tail_new).

%! generate_variable(+HighestExp,-NewN,-ListVariable)
%
% Generate all the variables in the new frame

generate_variable(HighestExp,NewN,ListVariable) :-
   compute_number_species(HighestExp,NewSpeciesP1,UntouchedSpecies),
   NewSpecies is NewSpeciesP1 - 1,
   NewN is NewSpecies + UntouchedSpecies,
   length(HighestExp,N),

   generate_new_variable(HighestExp,NewSpecies,ListNewVariable),
   generate_old_variable(ListOldVariable,N,0),
   ( % trick to keep the first variable (output) in head
      [[1|_Tail1]|TruncListNewVariable] = ListNewVariable
   ->
      union(ListOldVariable,TruncListNewVariable,ListVariable)
   ;
      union(ListOldVariable,ListNewVariable,ListVariable)
   ).


%! generate_new_variable(+HighestExp,+NewN,-ListVariable)
%
% list all the new variables needed to reduce the system
% NewN is the number of species in the new PIVP

generate_new_variable(_HighestExp,0,[]) :- !.

generate_new_variable(HighestExp,N,List) :-
   Nm is N - 1,
   generate_new_variable(HighestExp,Nm,ListTempo),
   index_exponant(N,HighestExp,Var),
   append(ListTempo,[Var],List).


%! generate_old_variable(List,Length,Current)
%
% Generate the list of old indexes

generate_old_variable([],Length,Length) :- !.

generate_old_variable([Head|Tail],Length,Current) :-
   NC is Current + 1,
   Right is Length - Current - 1,
   displace_exponent([1],Current,Right,Head),
   generate_old_variable(Tail,Length,NC).

%! exponant_index(?OldExp,+ListVariable,?Index)
%
% Compute the index of OldExp in the new list of variable set

exponant_index(OldExp,ListVariable,Index) :-
   nth1(Index,ListVariable,OldExp).


%! index_exponant(+Index,+HighestExp,-OldExp)
%
% Gives an OldExp from its index
% delegate to index_exponant_acc

index_exponant(Ind,HE,OE) :- index_exponant_acc(Ind,HE,OE,[]).

index_exponant_acc(0,[],Acc,Acc) :- !.

index_exponant_acc(Index,[Head|Tail],Acc,ExpList) :-
   HeadP is Head + 1,
   Exp is div(Index,HeadP),
   Rest is mod(Index,HeadP),
   index_exponant_acc(Exp,Tail,AccTempo,ExpList),
   append([Rest],AccTempo,Acc).


%! compute_number_species(+HighestExp,Acc,AccZero).
%
% subroutine to return the number of variables in the new set of variable

compute_number_species([],1,0).

compute_number_species([Head|Tail],Acc,AccZero) :-
   (
      Head =:= 0
   ->
      compute_number_species(Tail,Acc,AccZeroTempo),
      AccZero is AccZeroTempo+1
   ;
      compute_number_species(Tail,AccTempo,AccZero),
      Acc is AccTempo * (Head+1)
   ).


%! change_exponant(+OldExp,+ListVariable,-NewExp)
%
% Switch from the old variable set to the new one

change_exponant(_OldExp,[],[]).

change_exponant(OldExp,[OldExp|Tail],[1|T]) :- change_exponant(OldExp,Tail,T),!.

change_exponant(OldExp,[_Head|Tail],[0|T]) :- change_exponant(OldExp,Tail,T),!.


%! generate_pode(+ListVariable,+PODE,+ListVariable,+HighestExp,-NewPODE)
%
% Generate the MultivarVector corresponding to PODE in the new set of variables

generate_pode([],_PODE,_ListVariable,_HighestExp,[]) :- !.

generate_pode([Var|TailVar],PODE,ListVariable,HighestExp,[Multivar|Tail]) :-
   sum_list(Var,VarTot),
   (
      VarTot =:= 1
   ->
      % The variable already exist in the old set
      extract_from_vector(Var,PODE,OldMultivar),
      set_to_new_variable(OldMultivar,ListVariable,Multivar)
   ;
      % This is a new variable
      derive_new_variable([],Var,PODE,ListVariable,HighestExp,Multivar)
   ),
   generate_pode(TailVar,PODE,ListVariable,HighestExp,Tail).


%! extract_from_vector(+Mask,+Vector,-Result)
%
% given a Mask of the form [0,0,.,1,.,0,0] and a Vector of same length
% return the corresponding element

extract_from_vector([],_Vector,[]).

extract_from_vector([1|_Tail],[Element|_Vector],Element) :- !.

extract_from_vector([0|Tail],[_Skip|Vector],Element) :-
   extract_from_vector(Tail,Vector,Element).


%! set_to_new_variable(+Multivar,+ListVariable,-NewMultivar)
%
% Rephrase an old pODE in the new variable set

set_to_new_variable([],_,[]).

set_to_new_variable([[K,Exp]|Tail],ListVariable,[[K,NewExp]|NewTail]) :-
   change_exponant(Exp,ListVariable,NewExp),
   set_to_new_variable(Tail,ListVariable,NewTail).


%! derive_new_variable(Done,+Exponent,+MultivarVector,+ListVariable,+HighestExp,-Multivar)
%
% Gives the derivative of an old exponent set in the new variable setting
% The MultivarVector is the core of the old PIVP, that is the derivative of
% the old (basic) variables.

derive_new_variable(_,[],[],_ListVariable,_HighestExp,[]).

derive_new_variable(Done,[Exp|Tail],[Deriv|TailDeriv],ListVariable,HighestExp,Multivar) :-
   (
      Exp >= 1
   ->
      Expm is Exp-1,
      append(Done,[Expm],Exp_Tempo),
      append(Exp_Tempo,Tail,ExpDeriv),
      append(Done,[Exp],DoneE),
      derive_new_variable(DoneE,Tail,TailDeriv,ListVariable,HighestExp,MultivarTempo),
      multiply_derivative(ExpDeriv,Deriv,ListVariable,HighestExp,ThisDerivative),
      scalar_multivar(Exp,ThisDerivative,K_time_ThisDerivative),
      append(K_time_ThisDerivative,MultivarTempo,Multivar)
   ;
      Exp =:= 0
   ->
      append(Done,[Exp],DoneE),
      derive_new_variable(DoneE,Tail,TailDeriv,ListVariable,HighestExp,Multivar)
   ).


%! multiply_derivative(ExpSet,Deriv,ListVariable,HighestExp,ThisDerivative)
%
% Given an old ExpSet and a derivative of a basic variable
% return the corresponding Multivar in the new variable set

multiply_derivative(_ExpSet,[],_ListVariable,_HighestExp,[]).

multiply_derivative(ExpSet,[[K,Exp]|Deriv],ListVariable,HighestExp,[[K,NewExp]|ThisDerivative]) :-
   change_exponant(ExpSet,ListVariable,NewExpSet),
   change_exponant(Exp,ListVariable,NewExpDeriv),
   add_list(NewExpSet,NewExpDeriv,NewExp),
   multiply_derivative(ExpSet,Deriv,ListVariable,HighestExp,ThisDerivative).


%! generate_iv(+ListVariable,+IV,-NewIV)
%
% generates all the initial concentration for the new set of variables

generate_iv([],_IV,[]) :- !.

generate_iv([Var|TailVar],IV,[NewValue|TailValue]) :-
   set_new_iv(Var,IV,NewValue),
   generate_iv(TailVar,IV,TailValue).


%! set_new_iv(+Exponent,+IV,-NewValue)
%
% determine the concentration of one variable (corresponding to Exponent)
% in the new set

set_new_iv([],_IV,1) :- !.

set_new_iv([Exp|TailE],[Value|TailV],NewValue) :-
   set_new_iv(TailE,TailV,NewValueTempo),
   NewValue is NewValueTempo * (Value ** Exp).


%! highest_exponant(+Multivar,-ExponantSet)
%
% Return the highest possible exponant in every monomial of the Multivar
% for each variable

highest_exponant([],[]).

highest_exponant([[_K,Exp]],Exp).

highest_exponant([[_K,Exp]|Tail],MaxExp) :-
   highest_exponant(Tail,MaxExpTempo),
   max_list(Exp,MaxExpTempo,MaxExp).


%! highest_exponant_vector(+MultivarVector,-ExponantSet)
%
% Return the highest possible exponant in every monomial of the MultivarVector

highest_exponant_vector([Multivar],Exp) :-
   highest_exponant(Multivar,Exp).

highest_exponant_vector([Head|Tail],MaxExp) :-
   highest_exponant_vector(Tail,MaxExpTail),
   highest_exponant(Head,MaxExpHead),
   max_list(MaxExpHead,MaxExpTail,MaxExp).


%! scan_order_multivarvector(+MultivarVector,-Order)
%
% Scan a MultivarVector to determine its order through recursive call to
% scan_order_multivar/2

scan_order_multivarvector([],0).

scan_order_multivarvector([Head|Tail],Order) :-
   scan_order_multivarvector(Tail,OrderTail),
   scan_order_multivar(Head,OrderHead),
   Order is max(OrderHead,OrderTail).


%! scan_order_multivar(+Multivar,-Order)
%
% Scan a Multivar to determine its order

scan_order_multivar([],0).

scan_order_multivar([[_K,Exponent]|Tail],Order) :-
   scan_order_multivar(Tail,OrderTail),
   sum_list(Exponent,OrderHead),
   Order is max(OrderHead,OrderTail).


%! max_list(List1,List2,MaxList)
%
% determine the maximum at each index of two lists

max_list([],[],[]).

max_list([],List,List).

max_list(List,[],List).

max_list([Head1|Tail1],[Head2|Tail2],[HeadMax|TailMax]) :-
   HeadMax is max(Head1,Head2),
   max_list(Tail1,Tail2,TailMax).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools to manipulate directly PIVP_list objects %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This part is intented to manipulate PIVP_list as a representation of function
% that are their first component.
% So when adding, multiplying and so on, we actually create a new PIVP that
% realize the function that correspond to the addition of the function realized
% by the PIVP given in arguments.

%! add_PIVP(+PIVP1,+PIVP2,-PIVP_Sum)
%
% add the two PIVP and create a new variable that corresponds to the sum
% of the two first variables of PIVP1 and PIVP2 and is the new first variable.

add_PIVP([D1, PV1, IV1], [D2, PV2, IV2], [D, PV, IV]) :-
   D1_p_1 is D1 + 1,
   displace_multivar_vector(PV1, 1, D2, PV1d),
   displace_multivar_vector(PV2, D1_p_1, 0, PV2d),
   get_element(PV1d, 1, P1d),
   get_element(PV2d, 1, P2d),
   get_element(IV1, 1, I1),
   get_element(IV2, 1, I2),
   add_multivar(P1d, P2d, P),
   I is I1 + I2,
   D is D1_p_1 + D2,
   append([[P], PV1d, PV2d], PV),
   append([[I], IV1, IV2], IV).

%! multiply_PIVP(+PIVP1,+PIVP2,-PIVP_Product)
%
% add the two PIVP and create a new variable that corresponds to the product
% of the two first variables of PIVP1 and PIVP2 and is the new first variable.

multiply_PIVP([D1, PV1, IV1], [D2, PV2, IV2], [D, PV, IV]) :-
   D1_p_1 is D1 + 1,
   Right1 is D1 + D2 - 1,
   Right2 is D2 - 1,
   displace_multivar_vector(PV1, 1, D2, PV1d),
   displace_multivar_vector(PV2, D1_p_1, 0, PV2d),
   get_element(PV1d, 1, P1d),
   get_element(PV2d, 1, P2d),
   get_element(IV1, 1, I1),
   get_element(IV2, 1, I2),
   displace_exponent([1], 1, Right1, E1),
   displace_exponent([1], D1_p_1, Right2, E2),
   multiply_multivar_monomial(P1d, [1, E2], Q1),
   multiply_multivar_monomial(P2d, [1, E1], Q2),
   add_multivar(Q1, Q2, Q),
   I is I1 * I2,
   D is D1_p_1 + D2,
   append([[Q], PV1d, PV2d], PV),
   append([[I], IV1, IV2], IV).

:- devcom('\\begin{todo} ^, log, compose PIVP\\end{todo}').

:- devcom('\\begin{todo}automatic simulation time horizon option for having stabilization on one component at epsilon from the middle (or two third) of the time horizon\\end{todo}').


%! invert_PIVP(+PIVP,-PIVP_Inverse)
%
% copy the PIVP and create a new variable that corresponds to the inverse
% of the first variable, it became the new first variable.

invert_PIVP([D1, PV1, IV1], [D, PV, IV]) :-
   D is D1 + 1,
   displace_multivar_vector(PV1, 1, 0, PV1d),
   get_element(PV1d, 1, P1d),
   get_element(IV1, 1, I1),
   displace_exponent([2], 0, D1, E1),
   multiply_multivar_monomial(P1d, [-1, E1], Q),
   I is 1 / I1,
   append([[Q], PV1d], PV),
   append([[I], IV1], IV).

%! g_to_c_PIVP(+PIVP,-PIVP_modified,+X)
%
% Used to switch from a response to time to a response to an input
% The last species is initialize to output and slow down to zero with
% a caracteristic time of 1.

g_to_c_PIVP([D, PV, IV], [Dc, PVc, IVc], X) :-
  Dc is D + 1,
  displace_multivar_vector(PV, 0, 1, PVd),
  displace_exponent([1], D, 0, Egamma),
  multiply_multivar_vector(PVd, [[1,Egamma]], PVm),

  append([ PVm, [[[-1,Egamma]]] ], PVc),
  append([IV, [X]], IVc).


:- devcom('\\begin{todo}compilation option for not splitting the variables, assumed positive.\\end{todo}').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compilation Tools for PIVP_list %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! compile_PIVP(+PIVP, +Input, +Output)
%
% Compilation setup

compile_PIVP([D, PV, IV], Input, Output) :-
   assert(input_name(Input)),
   assert(output_name(Output)),
   get_option(lazy_negation,Lazyness),
   (
      Lazyness = yes
   ->
      set_initial_vector_ln(IV, 1),
      chemical_conversion(PV, 1)
   ;
      diff_list(D, Diff, D),
      compose_multivar_vector(PV, Diff, PVdiff),
      sign_split_multivar_vector(PVdiff, PVdiff_p, PVdiff_m),
      set_initial_vector(IV, 1),
      chemical_conversion(PVdiff_p, PVdiff_m, 1),
      anihilation_reactions(D,D)
   ),
   retract(input_name(Input)),
   retract(output_name(Output)).


%! diff_list(+N,-Diff,+N)
%
% Produce a MultivarVector that will be used to split N signed variables
% into 2N unsigned variables through composition with the natural PIVP.

diff_list(0, [], _) :- !.

diff_list(N, Diff, D) :-
   Left_p is 2*(N-1),
   Right_p is 2*(D-N)+1,
   Left_m is 2*N-1,
   Right_m is 2*(D-N),

   Np is N - 1,
   diff_list(Np, Diffp, D),

   displace_exponent([1], Left_p, Right_p, E_p),
   displace_exponent([1], Left_m, Right_m, E_m),
   P = [[1, E_p], [-1, E_m]],

   append([Diffp, [P]], Diff).


%! sign_split_multivar(+Multivar,-PositiveMultivar,-NegativeMultivar)
%
% Split a multivar with respect to its positive and negative terms
% Collect them both in Plus and Minus with only positive coefficients

sign_split_multivar([], [], []) :- !.

sign_split_multivar([[X, E]|T], Plus, Minus) :-
   sign_split_multivar(T, T_p, T_m),
   (
      X > 0
   ->
      Plus = [[X, E]|T_p],
      Minus = T_m
   ;
      X < 0
   ->
      Plus = T_p,
      Y is -X,
      Minus = [[Y, E]|T_m]
   ;
      Plus = T_p,
      Minus = T_m
   ).

%! sign_split_multivar_vector(+MultivarVector,-Positive,-Negative)
%
% Recursive call of sign_split_multivar on a vector, return vectors to.

sign_split_multivar_vector([], [], []) :- !.

sign_split_multivar_vector([P|PV], [Plus|PlusV], [Minus|MinusV]) :-
   sign_split_multivar(P, Plus, Minus),
   sign_split_multivar_vector(PV, PlusV, MinusV).

%! name_p_m(+Name,-Positive_name,-Negative_name)
%
% Produce names for positive and negative terms

name_p_m(X,Xp,Xm):-
   atom_concat(X,'_p',Xp),
   atom_concat(X,'_m',Xm).

%! name_p_m(+Name1,+Name2,-Positive_name,-Negative_name)
%
% Concatenate Name1 and Name2 and call name_p_m/3

name_p_m(X,I,Xp,Xm):-
   atom_concat(X,I,XI),
   name_p_m(XI,Xp,Xm).


%! set_initial_vector(+InitialValueVector,+Index)
%
% Set up the initial concentration for the simulation, see initial_state.pl

set_initial_vector([], _) :- !.

set_initial_vector([X|T], D) :-
   (
      D =:= 1
   ->
      output_name(Z),
      name_p_m(Z, Z_p, Z_m)
   ;
      name_spec,
      T = []
   ->
      input_name(Z),
      name_p_m(Z, Z_p,  Z_m)
   ;
      name_p_m('z', D, Z_p, Z_m)
   ),
   (
      X > 0
   ->
      present([Z_p], X)
   ;
      X < 0
   ->
      Y is -X,
      present([Z_m], Y)
   ;
      true
   ),
   Dp is D + 1,
   set_initial_vector(T, Dp).

set_initial_vector_ln([], _) :- !.

set_initial_vector_ln([X|Tail], N) :-
   (
     N_shift is N + 64,
     char_code(Name,N_shift)
   ),
   (
      X > 0
   ->
      present([Name], X)
   ;
      true
   ),
   Np is N+1,
   set_initial_vector_ln(Tail, Np).


%! anihilation_reactions(+NumberOfSpecies,Marker)
%
% Add the anihiliation reactions (X_p + X_m -> 0) to the simulation

anihilation_reactions(0,_) :- !.

anihilation_reactions(D,N) :-
   (
      D =:= 1
   ->
      output_name(Z),
      name_p_m(Z, Z_p, Z_m)
   ;
      D =:= N,
      name_spec
   ->
      input_name(Z),
      name_p_m(Z, Z_p,  Z_m)
   ;
      name_p_m('z', D, Z_p, Z_m)
   ),
   add_reaction(fast*Z_m*Z_p for Z_m+Z_p=>_),
   Dm is D - 1,
   anihilation_reactions(Dm,N).


%! lazy_anihilation_reactions(+VarNeg,+Shift)
%
% Add the anihiliation reactions in the case of the lazy negation: A+B => _

lazy_anihilation_reactions([],_Shift) :- !.

lazy_anihilation_reactions(VarNeg,Shift) :-
   min_list(VarNeg,Min),
   Spc1 is Min+Shift+64,
   Spc2 is Min+Shift+65,
   char_code(Name1,Spc1),
   char_code(Name2,Spc2),
   add_reaction(fast*Name1*Name2 for Name1+Name2=>_),
   delete(VarNeg,Min,VarNegRed),
   Shiftp is Shift+1,
   lazy_anihilation_reactions(VarNegRed,Shiftp).

%! chemical_conversion(+PIVP,+counter)
%
% Add the whole PIVP as chemical reactions in the biological system
% Work by constructing the correct names and calling polynomial_to_reactions

chemical_conversion([], _) :- !.

chemical_conversion([Poly|Tail], N) :-
   (
     N_shift is N + 64,
     char_code(Name,N_shift)
   ),
   polynomial_to_reactions(Poly, Name),
   Nxt is N + 1,
   chemical_conversion(Tail, Nxt).

%! chemical_conversion(+PIVP_p,+PIVP_m,+counter)
%
% Add the whole PIVP as chemical reactions in the biological system
% Work by constructing the correct names and calling polynomial_to_reactions

chemical_conversion([], [], _) :- !.

chemical_conversion([Plus|PlusV], [Minus|MinusV], N) :-
   (
      N =:= 1
   ->
      output_name(Z),
      name_p_m(Z, Z_p, Z_m)
   ;
      name_spec,
      PlusV = []
   ->
      input_name(Z),
      name_p_m(Z, Z_p,  Z_m)
   ;
      name_p_m('z', N, Z_p, Z_m)
   ),
   polynomial_to_reactions(Plus, Z_p),
   polynomial_to_reactions(Minus, Z_m),
   Nxt is N + 1,
   chemical_conversion(PlusV, MinusV, Nxt).

%! polynomial_to_reactions(+Polynom,+species_name)
%
% Add the polynom of the derivative of species_name in the biological system

polynomial_to_reactions([], _) :- !.
polynomial_to_reactions([M|P], Y) :-
  monomial_to_reaction(M, Y),
  polynomial_to_reactions(P, Y).


%! monomial_to_reaction(+Monomial,+Product)
%
% Add a single monomial in the biological system

monomial_to_reaction([X, E], Y) :-
  get_option(lazy_negation,Lazyness),
  (
    Lazyness = yes
  ->
    (
      X > 0
    ->
      exponant_to_solution_ln(E, 1, Sol, Kin),
      Reac = '_',
      Rate = X,
      Prod = Y
    ;
      exponant_to_solution_ln(E, 1, _Sol_dummy, Kin),
      char_code(Y, N_Shift),
      N is N_Shift-64,
      length(E, Len),
      Left is N-1,
      Right is Len-N,
      displace_exponent([-1],Left,Right,EmY),
      add_list(E, EmY, E_Sol),
      exponant_to_solution_ln(E_Sol, 1, Sol, _Kin_dummy),
      Reac = Y,
      Rate is -X,
      Prod = '_'
    )
  ;
    exponant_to_solution(E, 1, Sol, Kin),
    Reac = '_',
    Rate = X,
    Prod = Y
  ),
  (
    Sol = '_'
  ->
    add_reaction(Rate*Kin for Reac=>Prod)
  ;
    add_reaction(Rate*Kin for Reac=[Sol]=>Prod)
  ).


%! exponant_to_solution(+Exponant,+Counter,-Enzyme,-Reaction_rate)
%
% Forge the expression that correspond to a monomial in the all separated case

exponant_to_solution([], _, '_', 1) :- !.

exponant_to_solution([A_p, A_m|A], N, Sol, Kin) :-
   Np is N + 1,
   exponant_to_solution(A, Np, Sol0, Kin0),
   (
      N =:= 1
   ->
      output_name(Z),
      name_p_m(Z, Z_p, Z_m)
   ;
      name_spec,
      A = []
   ->
      input_name(Z),
      name_p_m(Z, Z_p,  Z_m)
   ;
      name_p_m('z', N, Z_p, Z_m)
   ),
   (
      A_p =:= 0
   ->
      Sol1 = Sol0,
      Kin1 = Kin0
   ;
      Sol0 = '_'
   ->
      Sol1 = A_p*Z_p,
      Kin1 = Z_p^A_p
   ;
      Sol1 = A_p*Z_p + Sol0,
      Kin1 = Z_p^A_p * Kin0
   ),
   (
      A_m =:= 0
   ->
      Sol = Sol1,
      Kin = Kin1
   ;
      Sol1 = '_'
   ->
      Sol = A_m*Z_m,
      Kin = Z_m^A_m
   ;
      Sol = A_m*Z_m + Sol1,
      Kin = Z_m^A_m * Kin1
   ).

exponant_to_solution_ln([], _, '_', 1) :- !.

exponant_to_solution_ln([A|Tail], N, Sol, Kin) :-
   Np is N + 1,
   exponant_to_solution_ln(Tail, Np, Sol_Tempo, Kin_Tempo),
   (
      N_shift is N + 64,
      char_code(Name,N_shift)
   ),
   (
      A =:= 0
   ->
      Sol = Sol_Tempo,
      Kin = Kin_Tempo
   ;
      Sol_Tempo = '_'
   ->
      Sol = A*Name,
      Kin = Name^A
   ;
      Sol = A*Name + Sol_Tempo,
      Kin = Name^A * Kin_Tempo
   ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools for conversion from expression to PIVP_list %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

id_PIVP([1, [ [ [1, [0]] ] ], [0]]).

inv_PIVP([1, [ [ [-1, [2]] ] ], [1]]).

exp_PIVP([1, [ [ [1, [1]] ] ], [1]]).

invexp_PIVP([1, [ [ [-1, [1]] ] ], [1]]).

cos_PIVP([2, [ [[1, [0, 1]]] , [[-1, [1, 0]]] ], [1, 0]]).

sin_PIVP([2, [ [[1, [0, 1]]] , [[-1, [1, 0]]] ], [0, 1]]).


%! expression_to_PIVP(+expression,-PIVP_list)
%
% Construct the PIVP_list object for a given expression

expression_to_PIVP(N, PIVP) :-
  number(N),
  !,
  PIVP = [1, [ [] ], [N]], !.

expression_to_PIVP(id, PIVP) :-
  !,
  id_PIVP(PIVP).

expression_to_PIVP(exp, PIVP) :-
  !,
  exp_PIVP(PIVP).

expression_to_PIVP(cos, PIVP) :-
  !,
  cos_PIVP(PIVP).

expression_to_PIVP(sin, PIVP) :-
  !,
  sin_PIVP(PIVP).

expression_to_PIVP(E + F, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  expression_to_PIVP(F, PIVP2),
  add_PIVP(PIVP1, PIVP2, PIVP).

expression_to_PIVP(E * F, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  expression_to_PIVP(F, PIVP2),
  multiply_PIVP(PIVP1, PIVP2, PIVP).

expression_to_PIVP(1 / E, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  invert_PIVP(PIVP1, PIVP), !.

expression_to_PIVP(E / F, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  expression_to_PIVP(1 / F, PIVP2),
  multiply_PIVP(PIVP1, PIVP2, PIVP).

expression_to_PIVP(E, _) :-
  throw(error(illegal_expression(E), expression_to_PIVP)).
