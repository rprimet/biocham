:- module(
  influence_editor,
  [
    s/0,
    % Commands
    add_influence/1,
    list_influences/0,
    influence_model/0,
    reaction_model/0,
    % Public API
    is_influence_model/0,
    check_influence_model/0,
    with_influence_model/1,
    list_model_influences/0,
    influence/2,
    compute_ode_for_influences/0,
    print_influence/2,
    substract_list/3
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(reaction_rules).
:- use_module(influence_rules).


s :-
    write('id parent kind item\n'),
    models:item(W,X,Y,Z),
    write(W), write(' '),
    write(X), write(' '),
    write(Y), write(' '),
    write(Z), write(' '), nl, fail.
s.


:- devdoc('\\section{Commands}').


add_influence(Influence) :-
  biocham_command,
  type(Influence, influence),
  doc('
    adds influence rules to the current set of influences.
    This command is implicit: influence rules can be added directly in
    influence models.
  '),
  add_item([kind: influence, item: Influence]).


:- doc('
\\begin{example}
This example shows a positive influence on b with both a positive source, a, and a negative source (i.e. an inhibitor), b.
The positive and negative sources have opposite effects on the force of the influence, 
but do not change the nature of the influence, i.e. the activation of b.
This motivates the use of the positive Boolean semantics which simply ignores the inhibitors and the forces.
On the other hand, the negative Boolean semantics interprets the inhibitors as negative conditions for the influence.
The second influence in this example is added to show the difference between both Boolean dynamics.
\\clearmodel
\\trace{
biocham: parameter(k=1).
biocham: present(a,1). 
biocham: MA(k)/(1+b) for a / b -> b.
biocham: list_ode.
biocham: numerical_simulation.
biocham: plot.
biocham: absent(b). absent(c).
biocham: MA(k)/(1+a) for b / a -> c.
biocham: option(boolean_semantics:positive).
biocham: generate_ctl_not.
biocham: option(boolean_semantics:negative).
biocham: generate_ctl_not.
}
\\end{example}
').

list_influences :-
  biocham_command,
  doc('
    lists the current set of influence rules.
  '),
  list_items([kind: influence]).


influence_model :-
  biocham_command,
  doc('
    creates a new influence model by inferring
    the influences between all molecular objects of the current hybrid model
  '),
  single_model(HybridModel),
  new_model,
  set_model_name(influence_model),
  single_model(InfluenceModel),
  influence_model(HybridModel, InfluenceModel).


reaction_model :-
  biocham_command,
  doc('
    creates a new reaction model by inferring
    the reactions between all molecular objects of the current hybrid model
  '),
  single_model(HybridModel),
  new_model,
  set_model_name(reaction_model),
  single_model(ReactionModel),
  reaction_model(HybridModel, ReactionModel).


:- devdoc('\\section{Public API}').


is_influence_model :-
   devdoc('
     succeeds if the current model is an influence model
     (i.e., does not contain any reaction rules).
   '),
   \+ item([kind: reaction]).


check_influence_model :-
  devdoc('
     succeeds if the current model is an influence model
     (i.e., does not contain any reaction rules)
     and throws an exception otherwise.
  '),
  (
    is_influence_model
  ->
    true
  ;
    throw(error(not_an_influence_model, check_influence_model))
  ).


:- meta_predicate with_influence_model(0).


with_influence_model(G) :-
  (
    is_influence_model
  ->
    G
  ;
    single_model(ReactionModel),
    setup_call_cleanup(
      new_model(InfluenceModel),
      (
        influence_model(ReactionModel, InfluenceModel),
        set_current_models([InfluenceModel]),
        G
      ),
      (
        delete_item(InfluenceModel),
        set_current_models([ReactionModel])
      )
    )
  ).


list_model_influences :-
  devdoc('
    lists all the influence rules in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  \+ (
    item([no_inheritance, kind: influence, id: Id, item: Influence]),
    \+ (
      print_influence(Id, Influence),
      write('.\n')
    )
  ).


prolog:error_message(not_an_influence_model) -->
  ['Not an influence model'].


influence(InfluenceRule, Name, Force, PositiveInputs, NegativeInputs, Sign, Output) :-
  devdoc(
    'builds or decomposes an influence rule.'
  ),
  once((
    Name = '',
    (
      var(InfluenceRule)
    ->
      true
    ;
      InfluenceRule \= (_ -- _)
    ),
    AnonymousInfluenceRule = InfluenceRule
  ;
    InfluenceRule = (Name -- AnonymousInfluenceRule)
  )),
  once((
    Force = 'MA'(1),
    (
      var(AnonymousInfluenceRule)
    ->
      true
    ;
      AnonymousInfluenceRule \= (_ for _)
    ),
    BasicInfluenceRule = AnonymousInfluenceRule
  ;
    AnonymousInfluenceRule = (Force for BasicInfluenceRule)
  )),
  once((
    BasicInfluenceRule = (Inputs -> Output),
    Sign = +
  ;
    BasicInfluenceRule = (Inputs -< Output),
    Sign = -
  )),
  inputs(Inputs, PositiveInputs, NegativeInputs).


influence(InfluenceRule, Components) :-
  devdoc('
    builds or decomposes an influence rule.
\\argument{InfluenceRule} is an infuence term.
\\argument{Components} is a record-style list with items of the form
\\begin{itemize}
\\item\\texttt{name: Name} where \\texttt{Name} is an atom,
\\item\\texttt{force: Force} where \\texttt{Force} is an arithmetic expression,
\\item\\texttt{positive_inputs: PositiveInputs},
\\item\\texttt{negative_inputs: NegativeInputs}
where \\texttt{PositiveInputs} and \\texttt{NegativeInputs} are list of objects,
\\item\\texttt{sign: Sign}
where \\texttt{Sign} is either \\texttt{+} or \\texttt{-},
\\item\\texttt{output: Output} where \\texttt{Output} is an object.
\\end{itemize}
  '),
  (
    var(InfluenceRule)
  ->
    field_default(name, '', Components, Name),
    field_default(force, 'MA'(1), Components, Force),
    field_default(positive_inputs, [], Components, PositiveInputs),
    field_default(negative_inputs, [], Components, NegativeInputs),
    field_default(sign, '+', Components, Sign),
    field_default(output, '_', Components, Output),
    influence(
      InfluenceRule, Name, Force, PositiveInputs, NegativeInputs, Sign, Output
    )
  ;
    influence(
      InfluenceRule, Name, Force, PositiveInputs, NegativeInputs, Sign, Output
    ),
    unify_records(Components, [
      name: Name,
      force: Force,
      positive_inputs: PositiveInputs,
      negative_inputs: NegativeInputs,
      sign: Sign,
      output: Output
    ])
  ).


compute_ode_for_influences :-
  \+ (
    item([kind: influence, item: Item]),
    influence(Item, _Name, Force, PositiveInputs, NegativeInputs, Sign, Output),
    \+ (
      maplist(add_coefficient, PositiveInputs, PositiveInputsWithCoefficient),
      kinetics(PositiveInputsWithCoefficient, NegativeInputs, Force, ForceExpression),
      sign(Sign, ForceExpression, SignedForce),
      ode_add_expression_to_molecule(SignedForce, Output)
    )
  ).


print_influence(_Id, Item) :-
  (
    Item = (Force for BasicInfluence)
  ->
    format('~w for ', [Force])
  ;
    BasicInfluence = Item
  ),
  (
    BasicInfluence = (Inputs -> Output)
  ->
    Arrow = '->'
  ;
    BasicInfluence = (Inputs -< Output)
  ->
    Arrow = -<
  ),
  (
    Inputs = (PositiveInputs / NegativeInputs)
  ->
    format('~w / ~w', [PositiveInputs, NegativeInputs])
  ;
    Inputs = (/ NegativeInputs)
  ->
    format('/ ~w', [NegativeInputs])
  ;
    write(Inputs)
  ),
  format(' ~w ~w', [Arrow, Output]).


substract_list([], Difference, Difference).

substract_list([Coefficient0 * Object | Tail], Products, Difference) :-
  (
    select(Coefficient1 * Object, Products, Others)
  ->
    Coefficient is Coefficient1 - Coefficient0,
    (
      Coefficient = 0
    ->
      substract_list(Tail, Others, Difference)
    ;
      substract_list(Tail, [Coefficient * Object | Others], Difference)
    )
  ;
    Coefficient is - Coefficient0,
    substract_list(Tail, [Coefficient * Object | Products], Difference)
  ).


:- devdoc('\\section{Private predicates}').


sign(-, Force, - Force).

sign(+, Force, Force).


influence_model(HybridModel, InfluenceModel) :-
  \+ (
    item([parent: HybridModel, kind: influence, item: Influence]),
    \+ (
      add_item([kind: influence, item: Influence])
    )
  ),
  \+ (
    item([parent: HybridModel, kind: reaction, item: Reaction]),
    reaction(Reaction, [
      kinetics: Kinetics,
      reactants: Reactants,
      inhibitors: Inhibitors,
      products: Products
    ]),
    \+ (
      substract_list(Reactants, Products, Difference),
      create_influences(Reactants, Inhibitors, Difference, Kinetics, InfluenceModel)
    )
  ).


reaction_model(HybridModel, ReactionModel) :-
  \+ (
    item([parent: HybridModel, kind: reaction, item: Reaction]),
    \+ (
      add_item([kind: reaction, item: Reaction])
    )
  ),
  \+ (
    item([parent: HybridModel, kind: influence, item: Influence]),
    influence(Influence, [
      name: Name,
      force: Force,
      positive_inputs: PositiveInputs,
      negative_inputs: NegativeInputs,
      sign: Sign,
      output: Output
    ]),
    \+ (
      (
        Sign = +
      ->
        reaction(Reaction, [
          name: Name,
          kinetics: Force,
          reactants: PositiveInputs,
          inhibitors: NegativeInputs,
          products: [Output | PositiveInputs]
        ])
      ;
        reaction(Reaction, [
          name: Name,
          kinetics: Force,
          reactants: [Output | PositiveInputs],
          inhibitors: NegativeInputs,
          products: PositiveInputs
        ])
      ),
      add_item([parent: ReactionModel, kind: reaction, item: Reaction])
    )
  ).


create_influences(Reactants, Inhibitors, Difference, Kinetics, InfluenceModel) :-
  \+ (
    findall(
      Input,
      member(_ * Input, Reactants),
      PositiveInputs
    ),
    member(Coefficient * Output, Difference),
    \+ (
      (
        Coefficient > 0
      ->
        Sign = +,
        simplify(Coefficient * Kinetics, CoefficientKinetics)
      ;
        Sign = -,
        simplify(- Coefficient * Kinetics, CoefficientKinetics)
      ),
      influence(Influence, [
        force: CoefficientKinetics,
        positive_inputs: PositiveInputs,
        negative_inputs: Inhibitors,
        sign: Sign,
        output: Output
      ]),
      add_item([parent: InfluenceModel, kind: influence, item: Influence])
    )
  ).


inputs(PositiveInputEnum, PositiveInputList, []) :-
  (
    var(PositiveInputEnum)
  ->
    true
  ;
    PositiveInputEnum \= (_ / _),
    PositiveInputEnum \= (/ _)
  ),
  list_enumeration(PositiveInputList, PositiveInputEnum),
  !.

inputs(/ NegativeInputEnum, [], NegativeInputList) :-
  list_enumeration(NegativeInputList, NegativeInputEnum),
  !.

inputs(
  PositiveInputEnum / NegativeInputEnum,
  PositiveInputList,
  NegativeInputList
) :-
  list_enumeration(PositiveInputList, PositiveInputEnum),
  list_enumeration(NegativeInputList, NegativeInputEnum),
  !.
