:- use_module(library(plunit)).

% Only for separate compilation/linting
:- use_module(ode).
:- use_module(reaction_rules).


:- begin_tests(ode).


test('ode_system', [ODEs == [d(b)/dt = a, d(a)/dt = -a]]) :-
  clear_model,
  command(a => b),
  ode_system,
  all_odes(ODEs).

test(
  'import_ode',
  [true(
    ODEs == [init(a = 0), init(b = 0), d(a)/dt = -a, par(k = -2),
    par(kk = 1.0), d(b)/dt = k*a])]
) :-
  setup_call_cleanup(
    open('test.ode', write, TestOde),
    write(TestOde, '
# Comment
init a = 0
B(0) = 0
wiener q
da/dt = -a
par k =-2
p kk = 1.0
B''= k*a
@ meth=cvode
done
'),
    close(TestOde)
  ),
  import_ode('test.ode'),
  delete_file('test.ode'),
  get_current_ode_system(Id),
  all_items([parent: Id], ODEs).

test(
  'import_reactions_from_ode_system',
  [true(Reactions == [a => b])]
) :-
  clear_model,
  new_ode_system,
  add_ode(da/dt = -a),
  add_ode(db/dt = a),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  import_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).


test(
  'export_ode',
  [true((Lines, Reactions) == (
    [
      'init b = 0', 'init a = 1', 'par k = 2',
      'db/dt = k*a', 'da/dt = - (k*a)', 'done'
    ],
    ['MA'(k) for a => b]
  ))]
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 2)),
  command(present(a)),
  export_ode('test.ode'),
  list_file_lines('test.ode', Lines),
  import_ode('test.ode'),
  delete_file('test.ode'),
  single_model(Id),
  all_items([parent: Id, kind: reaction], Reactions).

test(
  'export_ode_as_latex',
  [true(Lines == [
      '\\begin{align*}', '{b}_0 &= 0\\\\',
      '{a}_0 &= 1\\\\', 'k &= 2\\\\',
      '\\frac{db}{dt} &= k*a\\\\',
      '\\frac{da}{dt} &= - (k*a)\\\\',
      '\\end{align*}'
    ])]
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 2)),
  command(present(a)),
  export_ode_as_latex('test.tex'),
  list_file_lines('test.tex', Lines).

:- end_tests(ode).
