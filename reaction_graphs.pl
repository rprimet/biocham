:- module(
  reaction_graphs,
  [
    up_type/1,
    % Commands
    reaction_graph/0,
    rule_graph/0,
    import_reactions_from_graph/0,
    draw_reactions/0,
    draw_rules/0,
    % Public API
    reaction_graph/1,
    set_sources/1
  ]
).


:- use_module(doc).
:- use_module(biocham).


:- grammar(up_type).

up_type(none).

up_type(present).

up_type(sources).


:- doc('\\emphright{Defines if in the drawing of a graph using Graphviz, some species should appear first (i.e. on top of the drawing, or at the left if dran from left to right. The default value is \\texttt{present}, i.e. draw first the species that are present in the initial state.}').


:- initial(option(draw_first: present)).


:- devdoc('\\section{Commands}').


:- devcom('\\begin{todo}It would be nice to have a notion of input/output and draw graphs accordingly, with inputs on top and outputs bottom in all (reaction or influence) graphs. 

The input species could be those present at initial_state. The output species could be those not reactant (nor catalyst) of any reaction, nor source of any nfluence.

Alternatively we could have input/output annotations on molecular species which could also be useful also for some analyses.

Also the linear invariants could be used to enforce theur drawing in row like in MAPK...\\end{todo}').

:- devdoc('\\begin{todo} export_graph in dot file\\end{todo}').


:- doc('
The bipartite graph of molecular reactions can be drawn with the following command.
').

draw_reactions :-
  biocham_command,
  doc('
    Draws the reaction graph of the current model.
    Equivalent to \\texttt{reaction_graph. draw_graph.}
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(load(library:examples/mapk/mapk)),
  biocham(draw_reactions),
  doc('
    \\end{example}
  '),
  reaction_graph,
  draw_graph.


reaction_graph :-
  biocham_command,
  option(draw_first, up_type, _ForceUp, 'Put species of this type
    at the top of the graph when drawing it.'),
  doc('Builds the reaction graph of the current model.
  \\begin{example}'),
  biocham(option(draw_first: present, left_to_right: yes)),
  biocham(draw_reactions),
  % biocham(option(draw_first: sources)),
  % biocham(draw_reactions),
  doc('\\end{example}'),
  biocham_silent(option(draw_first: none, left_to_right: no)),
  delete_items([kind: graph, key: reaction_graph]),
  new_graph,
  set_graph_name(reaction_graph),
  get_current_graph(GraphId),
  reaction_graph(GraphId).


rule_graph :-
  biocham_command,
  doc('
    Builds the rule graph of the current model, \\emph{i.e.} the union of
    the reaction graph and the influence graph.
  '),
  delete_items([kind: graph, key: rule_graph]),
  new_graph,
  set_graph_name(rule_graph),
  get_current_graph(GraphId),
  reaction_graph(GraphId),
  influence_hypergraph(GraphId).

import_reactions_from_graph :-
  biocham_command,
  doc('
    Updates the set of reactions of the current model with the current graph.
  '),
  make_bipartite_graph,
  make_reactions.

draw_rules :-
  biocham_command,
  doc('
    Draws the reaction graph of the current model.
    Equivalent to \\texttt{rule_graph. draw_graph.}
  '),
  rule_graph,
  draw_graph.


:- devdoc('\\section{Public API}').


reaction_graph(GraphId) :-
  set_counter(reaction, 0),
  \+ (
    item([kind: reaction, item: Item]),
    reaction(Item, [
      name: Name,
      kinetics: Kinetics,
      reactants: Reactants,
      inhibitors: Inhibitors,
      products: Products
    ]),
    \+ (
      (
        Name = ''
      ->
        count(reaction, ReactionCount),
        format(atom(ReactionName), 'reaction~d', [ReactionCount])
      ;
        ReactionName = Name
      ),
      transition(GraphId, ReactionName, TransitionId),
      (
        Kinetics = 'MA'(1)
      ->
        true
      ;
        set_attribute(TransitionId, kinetics = Kinetics)
      ),
      \+ (
        (
          member(Stoichiometry * Object, Reactants),
          From = ObjectId,
          To = TransitionId
        ;
          member(Stoichiometry * Object, Products),
          From = TransitionId,
          To = ObjectId
        ),
        \+ (
          place(GraphId, Object, ObjectId),
          add_edge(GraphId, From,  To, EdgeId),
          (
            get_attribute(EdgeId, stoichiometry = OldStoichiometry)
          ->
            NewStoichiometry is OldStoichiometry + Stoichiometry
          ;
            NewStoichiometry is Stoichiometry
          ),
          (
            NewStoichiometry = 1
          ->
            catch(
              delete_attribute(EdgeId, stoichiometry),
              error(unknown_item),
              true
            )
          ;
            set_attribute(EdgeId, stoichiometry = NewStoichiometry)
          )
        )
      ),
      \+ (
        member(Inhibitor, Inhibitors),
        \+ (
          place(GraphId, Inhibitor, PlaceId),
          add_edge(GraphId, PlaceId, TransitionId, EdgeId),
          set_attribute(EdgeId, inhibits = true)
        )
      )
    )
  ),
  set_sources(GraphId).


set_sources(GraphId) :-
  get_option(draw_first, Up),
  (
    Up == present
  ->
    enumerate_molecules(M),
    forall(
      (
        member(Species, M),
        place(GraphId, Species, SpeciesId),
        get_initial_concentration(Species, C),
        C > 0
      ),
      set_attribute(SpeciesId, source = true)
    )
  ;
    Up == sources
  ->
    forall(
      (
        proper_source(GraphId, VertexId),
        get_attribute(VertexId, kind = place)
      ),
      set_attribute(VertexId, source = true)
    )
  ;
    true
  ).


:- devdoc('\\section{Private predicates}').


:- dynamic(vertex_transition/1).


:- dynamic(vertex_place/1).


:- dynamic(fixpoint/0).


make_bipartite_graph :-
  get_current_graph(GraphId),
  retractall(vertex_transition(_)),
  retractall(vertex_place(_)),
  retractall(vertex_unknown(_)),
  \+ (
    item([parent: GraphId, kind: vertex, id: VertexId, item: Vertex]),
    get_attribute(VertexId, kind = Kind),
    \+ (
      (
        Kind = place
      ->
        assertz(vertex_place(Vertex))
      ;
        Kind = transition
      ->
        assertz(vertex_transition(Vertex))
      ;
        true
      )
    )
  ),
  \+ \+ (
    repeat,
    assertz(fixpoint),
    \+ (
      item([parent: GraphId, kind: edge, item: (A -> B)]),
      \+ (
        reaction_object(A, B),
        object_reaction(A, B),
        reaction_object(B, A),
        object_reaction(B, A)
      )
    ),
    fixpoint
  ),
  check_bipartite_graph.


check_bipartite_graph :-
  get_current_graph(GraphId),
  findall(
    AmbiguousVertex,
    (
      vertex_transition(AmbiguousVertex),
      vertex_place(AmbiguousVertex)
    ),
    AmbiguousVertices
  ),
  (
    AmbiguousVertices = []
  ->
    true
  ;
    throw(error(ambiguous_vertices(AmbiguousVertices)))
  ),
  findall(
    AmbiguousEdge,
    (
      item([parent: GraphId, kind: edge, item: AmbiguousEdge]),
      AmbiguousEdge = (From -> _To),
      \+ vertex_transition(From),
      \+ vertex_place(From)
    ),
    AmbiguousEdges
  ),
  (
    AmbiguousEdges = []
  ->
    true
  ;
    throw(error(ambiguous_edges(AmbiguousEdges)))
  ).


reaction_object(A, B) :-
  (
    vertex_transition(A),
    \+ vertex_place(B)
  ->
    assertz(vertex_place(B)),
    retractall(fixpoint)
  ;
    true
  ).


object_reaction(A, B) :-
  (
    vertex_place(A),
    \+ vertex_transition(B)
  ->
    assertz(vertex_transition(B)),
    retractall(fixpoint)
  ;
    true
  ).


is_inhibitor(EdgeId) :-
  get_attribute(EdgeId, inhibits = true).


make_reactions :-
  delete_items([kind: reaction]),
  get_current_graph(GraphId),
  \+ (
    vertex_transition(ReactionVertex),
    \+ (
      get_kinetics(GraphId, ReactionVertex, Kinetics),
      findall(
        Stoichiometry * Reactant,
        (
          vertex_place(Reactant),
          item([
            parent: GraphId,
            kind: edge,
            item: (Reactant -> ReactionVertex),
            id: EdgeId
          ]),
          \+ is_inhibitor(EdgeId),
          get_stoichiometry(EdgeId, Stoichiometry)
        ),
        Left
      ),
      findall(
        Inhibitor,
        (
          vertex_place(Inhibitor),
          item([
            parent: GraphId,
            kind: edge,
            item: (Inhibitor -> ReactionVertex),
            id: EdgeId
          ]),
          is_inhibitor(EdgeId)
        ),
        Inhibitors
      ),
      findall(
        Stoichiometry * Product,
        (
          vertex_place(Product),
          item([
            parent: GraphId,
            kind: edge,
            item: (ReactionVertex -> Product),
            id: EdgeId
          ]),
          get_stoichiometry(EdgeId, Stoichiometry)
        ),
        Right
      ),
      reaction(Reaction, [
        kinetics: Kinetics,
        reactants: Left,
        inhibitors: Inhibitors,
        products: Right
      ]),
      add_item([kind: reaction, item: Reaction])
    )
  ).


get_kinetics(GraphId, Vertex, Kinetics) :-
  (
    get_attribute(GraphId, Vertex, kinetics = Kinetics)
  ->
    true
  ;
    Kinetics = 'MA'(1)
  ).


get_stoichiometry(EdgeId, Stoichiometry) :-
  (
    get_attribute(EdgeId, stoichiometry = Stoichiometry)
  ->
    true
  ;
    Stoichiometry = 1
  ).
