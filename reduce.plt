:- use_module(library(plunit)).
% not necessary, except for separate compilation
:- use_module(reaction_rules).

:- begin_tests(reduce).
    % condition(flag(slow_test, true, true)),

test(
  'reduce_3_inhibitions',
  [
    setup(clear_model)
  ]
) :-
  command('_' => a),
  command('_' => b),
  command('_' => c),
  command(a => '_'),
  command(b => '_'),
  command(c => '_'),
  once(
    reduce:reduce_model(
      'oscil'('a'),
      Removed
    )
  ),
  assertion(Removed = ['_'=>b,'_'=>c,b=>'_',c=>'_']).

test(
  'reduce_Qu1',
  [
    setup(clear_model)
  ]
) :-
  command(load('library:examples/cell_cycle/Qu_et_al_2003.bc')),
  once(
    reduce:reduce_model(
      'AG'(not('CycB-CDK~{p1}') -> checkpoint('C25~{p1,p2}','CycB-CDK~{p1}')),
      Removed
    )
  ),
  assertion(Removed = [ (k1 for '_'=>'CycB'), (k2*['CycB']for'CycB'=>'_'), (k2u*['APC']*['CycB']for'CycB'=['APC']=>'_'), (k3*['CDK']*['CycB']for'CDK'+'CycB'=>'CycB-CDK~{p1,p2}'), (k4*['CycB-CDK~{p1,p2}']for'CycB-CDK~{p1,p2}'=>'CDK'+'CycB'), (k5*['CycB-CDK~{p1,p2}']for'CycB-CDK~{p1,p2}'=>'CycB-CDK~{p1}'), (k5u*['C25~{p1,p2}']*['CycB-CDK~{p1,p2}']for'CycB-CDK~{p1,p2}'=['C25~{p1,p2}']=>'CycB-CDK~{p1}'), (k6*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=>'CycB-CDK~{p1,p2}'), (['Wee1']*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=['Wee1']=>'CycB-CDK~{p1,p2}'), (k7*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=>'CDK'), (k7u*['APC']*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=['APC']=>'CDK'), (k8 for '_'=>'C25'), (k9*['C25']for'C25'=>'_'), (k9*['C25~{p1}']for'C25~{p1}'=>'_'), (k9*['C25~{p1,p2}']for'C25~{p1,p2}'=>'_'), (bz*['C25']for'C25'=>'C25~{p1}'), (cz*['CycB-CDK~{p1}']*['C25']for'C25'=['CycB-CDK~{p1}']=>'C25~{p1}'), (az*['C25~{p1}']for'C25~{p1}'=>'C25'), (bz*['C25~{p1}']for'C25~{p1}'=>'C25~{p1,p2}'), (cz*['CycB-CDK~{p1}']*['C25~{p1}']for'C25~{p1}'=['CycB-CDK~{p1}']=>'C25~{p1,p2}'), (az*['C25~{p1,p2}']for'C25~{p1,p2}'=>'C25~{p1}'), (k10 for '_'=>'Wee1'), (k11*['Wee1']for'Wee1'=>'_'), (k11*['Wee1~{p1}']for'Wee1~{p1}'=>'_'), (bw*['Wee1']for'Wee1'=>'Wee1~{p1}'), (cw*['CycB-CDK~{p1}']*['Wee1']for'Wee1'=['CycB-CDK~{p1}']=>'Wee1~{p1}'), (aw*['Wee1~{p1}']for'Wee1~{p1}'=>'Wee1'), (['CycB-CDK~{p1}']^2/ (a^2+['CycB-CDK~{p1}']^2)/tho for '_'=['CycB-CDK~{p1}']=>'APC'), (['APC']/tho for 'APC'=>'_'), (k12 for '_'=>'CKI'), (k13*['CKI']for'CKI'=>'_'), (k14*['CKI']*['CycB-CDK~{p1}']for'CKI'+'CycB-CDK~{p1}'=>'CKI-CycB-CDK~{p1}'), (k15*['CKI-CycB-CDK~{p1}']for'CKI-CycB-CDK~{p1}'=>'CKI'+'CycB-CDK~{p1}'), (bi*['CKI-CycB-CDK~{p1}']for'CKI-CycB-CDK~{p1}'=>'(CKI-CycB-CDK~{p1})~{p2}'), (ci*['CycB-CDK~{p1}']*['CKI-CycB-CDK~{p1}']for'CKI-CycB-CDK~{p1}'=['CycB-CDK~{p1}']=>'(CKI-CycB-CDK~{p1})~{p2}'), (ai*['(CKI-CycB-CDK~{p1})~{p2}']for'(CKI-CycB-CDK~{p1})~{p2}'=>'CKI-CycB-CDK~{p1}'), (k16*['(CKI-CycB-CDK~{p1})~{p2}']for'(CKI-CycB-CDK~{p1})~{p2}'=>'CDK'), (k16u*['APC']*['(CKI-CycB-CDK~{p1})~{p2}']for'(CKI-CycB-CDK~{p1})~{p2}'=['APC']=>'CDK')]).


test(
  'reduce_Qu2',
  [
    setup(clear_model)
  ]
) :-
  command(load('library:examples/cell_cycle/Qu_et_al_2003.bc')),
  once(
    reduce:reduce_model(
      reachable('CycB-CDK~{p1}'),
      Removed
    )
  ),
  assertion(Removed = [ (k2*['CycB']for'CycB'=>'_'), (k2u*['APC']*['CycB']for'CycB'=['APC']=>'_'), (k4*['CycB-CDK~{p1,p2}']for'CycB-CDK~{p1,p2}'=>'CDK'+'CycB'), (k5*['CycB-CDK~{p1,p2}']for'CycB-CDK~{p1,p2}'=>'CycB-CDK~{p1}'), (k6*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=>'CycB-CDK~{p1,p2}'), (['Wee1']*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=['Wee1']=>'CycB-CDK~{p1,p2}'), (k7*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=>'CDK'), (k7u*['APC']*['CycB-CDK~{p1}']for'CycB-CDK~{p1}'=['APC']=>'CDK'), (k9*['C25']for'C25'=>'_'), (k9*['C25~{p1}']for'C25~{p1}'=>'_'), (k9*['C25~{p1,p2}']for'C25~{p1,p2}'=>'_'), (cz*['CycB-CDK~{p1}']*['C25']for'C25'=['CycB-CDK~{p1}']=>'C25~{p1}'), (az*['C25~{p1}']for'C25~{p1}'=>'C25'), (cz*['CycB-CDK~{p1}']*['C25~{p1}']for'C25~{p1}'=['CycB-CDK~{p1}']=>'C25~{p1,p2}'), (az*['C25~{p1,p2}']for'C25~{p1,p2}'=>'C25~{p1}'), (k10 for '_'=>'Wee1'), (k11*['Wee1']for'Wee1'=>'_'), (k11*['Wee1~{p1}']for'Wee1~{p1}'=>'_'), (bw*['Wee1']for'Wee1'=>'Wee1~{p1}'), (cw*['CycB-CDK~{p1}']*['Wee1']for'Wee1'=['CycB-CDK~{p1}']=>'Wee1~{p1}'), (aw*['Wee1~{p1}']for'Wee1~{p1}'=>'Wee1'), (['CycB-CDK~{p1}']^2/ (a^2+['CycB-CDK~{p1}']^2)/tho for '_'=['CycB-CDK~{p1}']=>'APC'), (['APC']/tho for 'APC'=>'_'), (k12 for '_'=>'CKI'), (k13*['CKI']for'CKI'=>'_'), (k14*['CKI']*['CycB-CDK~{p1}']for'CKI'+'CycB-CDK~{p1}'=>'CKI-CycB-CDK~{p1}'), (k15*['CKI-CycB-CDK~{p1}']for'CKI-CycB-CDK~{p1}'=>'CKI'+'CycB-CDK~{p1}'), (bi*['CKI-CycB-CDK~{p1}']for'CKI-CycB-CDK~{p1}'=>'(CKI-CycB-CDK~{p1})~{p2}'), (ci*['CycB-CDK~{p1}']*['CKI-CycB-CDK~{p1}']for'CKI-CycB-CDK~{p1}'=['CycB-CDK~{p1}']=>'(CKI-CycB-CDK~{p1})~{p2}'), (ai*['(CKI-CycB-CDK~{p1})~{p2}']for'(CKI-CycB-CDK~{p1})~{p2}'=>'CKI-CycB-CDK~{p1}'), (k16*['(CKI-CycB-CDK~{p1})~{p2}']for'(CKI-CycB-CDK~{p1})~{p2}'=>'CDK'), (k16u*['APC']*['(CKI-CycB-CDK~{p1})~{p2}']for'(CKI-CycB-CDK~{p1})~{p2}'=['APC']=>'CDK')]).


:- end_tests(reduce).
