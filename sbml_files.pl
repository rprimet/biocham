:- module(
  sbml_files,
  [
    load_sbml/1,
    add_sbml/1,
    add_sbml_file/1,
    export_sbml/1,
    download_curated_biomodel/1
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).
:- use_module(arithmetic_rules).

:- use_module(library(http/http_open)).
:- use_module(library(sgml_write)).


:- devdoc('These commands have been modified not to fail on errors which may be just warning.').

:- devdoc('\\section{Commands}').


load_sbml(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{load_biocham/1} but importing reactions, parameters and
    initial state (and only that!) from an SBML .xml file.
  '),
  load_all('xml', InputFile).


add_sbml(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{add_biocham/1} but importing reactions, parameters and
    initial state (and only that!) from an SBML .xml file.
  '),
  add_all('xml', InputFile).


:- devdoc('\\section{Public API}').


models:add_file_suffix('xml', add_sbml_file).


add_sbml_file(Filename) :-
  automatic_suffix(Filename, '.xml', read, FilenameXML),
  setup_call_cleanup(
    readSBML(FilenameXML, SBML),
    add_sbml_document(SBML),
    sbmlDocument_free(SBML)
  ).


add_sbml_document(SBML) :-
  sbmlDocument_printErrors(SBML, user_error),
  sbmlDocument_getNumErrors(SBML, Errors),
  (
    Errors > 0
  ->
    throw(error(sbml_errors))
  ;
    true
  ),
  sbmlDocument_getModel(SBML, Model),
  add_sbml_model(Model).


add_sbml_model(Model) :-
  add_compartments(Model),
  add_global_parameters(Model),
  add_assignment_rules(Model),
  add_function_definitions(Model),
  add_species(Model),
  add_reactions(Model).


:- dynamic(has_only_substance_units/1).


add_species(Model) :-
  retractall(has_only_substance_units(_)),
  forall(
    model_species(Model, Species),
    (
      species_id_and_initial(Species, Name, Initial),
      species_constant_and_boundary(Species, _Constant, Boundary),
      (
        var(Initial)
      ->
        (
          Boundary == true
        ->
          % record that this is a boundary condition to remove it from
          % modifier species lists
          function([Name = Name])
        ;
          true
        )
      ;
        (
          Initial = Amount / Compartment
        ->
          parameter_value(Compartment, Volume),
          InitialC is Amount / Volume
        ;
          InitialC = Initial
        ),
        (
          % Constant == true
        % ->
          % set_parameter(Name, InitialC)
        % ;
          Boundary == true
        ->
          function([Name = InitialC])
        ;
          set_initial_concentration(Name, InitialC)
        )
      ),
      (
        species_has_only_substance_units(Species)
      ->
        assertz(has_only_substance_units(Name))
      ;
        true
      )
    )
  ).


add_global_parameters(Model) :-
  forall(
    model_parameter(Model, Parameter),
    (
      parameter_id_and_value(Parameter, Id, Value),
      (
        var(Value)
      ->
        true
      ;
        set_parameter(Id, Value)
      )
    )
  ).


add_assignment_rules(Model) :-
  forall(
    model_assignment_rule(Model, Rule),
    (
      rule_var_and_math(Rule, Var, Math),
      (
        parameter_value(Var, _)
      ->
        delete_parameter([Var])
      ;
        true
      ),
      function([Var = Math])
    )
  ).


add_function_definitions(Model) :-
  forall(
    model_function(Model, Function),
    (
      function_name_args_body(Function, Name, Args, Body),
      Func =.. [Name | Args],
      function([Func = Body])
    )
  ).


global_local_parameters([], _Rid, Kinetics, Kinetics).

global_local_parameters([Parameter | LocalParameters], Rid, LKinetics, GKinetics) :-
  parameter_id_and_value(Parameter, Id, Value),
  (
    var(Value)
  ->
    true
  ;
    global_id(Rid, Id, GlobalId),
    set_parameter(GlobalId, Value)
  ),
  substitute([Id], [GlobalId], LKinetics, Kinetics),
  global_local_parameters(LocalParameters, Rid, Kinetics, GKinetics).


global_id(Rid, Id, GlobalId) :-
  atomic_list_concat([Rid, Id], '__', GlobalId).


add_reactions(Model) :-
  \+ (
    model_reaction(Model, Reaction),
    \+ (
      add_sbml_reaction(Reaction)
    )
  ).

% Store SBML-definde compartments/locations
:- dynamic(compartment/1).
:- dynamic(single_compartment/1).


add_compartments(Model) :-
  retractall(compartment(_)),
  retractall(single_compartment(_)),
  forall(
    model_compartment(Model, Compartment),
    (
      compartment_volume(Compartment, Id, Volume),
      assertz(compartment(Id)),
      set_parameter(Id, Volume)
    )
  ),
  (
    findall(C, compartment(C), [Single])
  ->
    assertz(single_compartment(Single))
  ;
    true
  ).


scale_kinetics_to_concentrations(Kinetics, Scaled) :-
  (
    single_compartment(Compartment)
  ->
    term_morphism(
      sbml_files:scale_concentration(Compartment),
      Kinetics,
      ScaledNumber
    ),
    Scaled = ScaledNumber / Compartment
  ;
    % FIXME I have no idea how to make a reasonable semantics for multiple
    % compartments
    Scaled = Kinetics
  ).


scale_concentration(Compartment, A, B) :-
  (
    has_only_substance_units(A)
  ->
    B = A * Compartment
  ;
    term_morphism(sbml_files:scale_concentration(Compartment), A, B)
  ).


add_sbml_reaction(SbmlReaction) :-
  reaction_getReversible(SbmlReaction, Reversible),
  get_sbml_reactants(SbmlReaction, Reactants),
  get_sbml_modifiers(SbmlReaction, Modifiers),
  get_sbml_products(SbmlReaction, Products),
  reaction_kinetics(SbmlReaction, Kinetics, LocalParameters),
  get_reaction_id(SbmlReaction, Rid),
  global_local_parameters(LocalParameters, Rid, Kinetics, GKinetics),
  append(Reactants, Modifiers, AllReactants),
  append(Products, Modifiers, AllProducts),
  filter_out_boundaries(AllReactants, AllReactantsButBoundary),
  filter_out_boundaries(AllProducts, AllProductsButBoundary),
  scale_kinetics_to_concentrations(GKinetics, ScaledKinetics),
  (
    Reversible = false
  ->
    FullKinetics = ScaledKinetics
  ;
    split_pos_neg_kinetics(ScaledKinetics, Positive, Negative),
    FullKinetics = Positive
  ),
  % TODO inhibitors
  reaction(Reaction, [
    kinetics: FullKinetics,
    reactants: AllReactantsButBoundary,
    products: AllProductsButBoundary
  ]),
  add_item([kind: reaction, item: Reaction]),
  (
    Reversible = true
  ->
    reaction(ReverseReaction, [
      kinetics: Negative,
      reactants: AllProductsButBoundary,
      products: AllReactantsButBoundary
    ]),
    add_item([kind: reaction, item: ReverseReaction])
  ;
    true
  ).


get_sbml_reactants(Reaction, Reactants) :-
  findall(
      Object,
      (
        reaction_reactant(Reaction, ReactantReference),
        speciesReference_object(ReactantReference, Object)
      ),
      Reactants
  ).


get_sbml_modifiers(Reaction, Modifiers) :-
  findall(
      1 * Species,
      (
        reaction_modifier(Reaction, ModifierReference),
        modifierSpeciesReference_getSpecies(ModifierReference, Species)
      ),
      Modifiers
  ).


get_sbml_products(Reaction, Products) :-
  findall(
      Object,
      (
        reaction_product(Reaction, ProductReference),
        speciesReference_object(ProductReference, Object)
      ),
      Products
  ).


speciesReference_object(Reference, Stoichiometry * Species) :-
  speciesReference_getStoichiometry(Reference, FloatStoichiometry),
  normalize_number(FloatStoichiometry, Stoichiometry),
  speciesReference_getSpecies(Reference, Species).


split_pos_neg_kinetics(A - B, A, B) :-
  !.

split_pos_neg_kinetics(K*E, K*P, K*N) :-
  atomic(K),
  !,
  split_pos_neg_kinetics(E, P, N).

split_pos_neg_kinetics(E/K, P/K, N/K) :-
  atomic(K),
  !,
  split_pos_neg_kinetics(E, P, N).

split_pos_neg_kinetics(A, A, 0).


filter_out_boundaries([], []).

filter_out_boundaries([_ * H | T], TT) :-
  (
    single_model(ModelId),
    identifier_kind(ModelId, H, function)
  ->
    filter_out_boundaries(T, TT)
  ;
    TT = [H | TTT],
    filter_out_boundaries(T, TTT)
  ).


download_curated_biomodel(Id) :-
  biocham_command,
  type(Id, integer),
  doc('Downloads to the current directory the SBML file for the corresponding
  (curated) biomodel with numeric ID \\argument{Id} (e.g. "5").'),
  option(output_to_library, yesno, ToLibrary, 'outputs to the
    library/biomodels subfolder'),
  format(atom(ModelId), 'BIOMD~|~`0t~d~10+', [Id]),
  atom_concat(ModelId, '.xml', OutFile),
  (
    ToLibrary == 'yes'
  ->
    atom_concat('library:biomodels/', OutFile, OutName),
    filename(OutName, FileName)
  ;
    FileName = OutFile
  ),
  % New Biomodels but using caltech's readonly version to avoid HTTPS
  format(
    atom(Url),
    % 'http://biomodels.caltech.edu/model/download/~w?filename=~w_url.xml',
    % back to EBI, Caltech was down
    'http://www.ebi.ac.uk/biomodels/model/download/~w?filename=~w_url.xml',
    [ModelId, ModelId]
  ),
  setup_call_cleanup(
    (
      open(FileName, write, Out),
      http_open(Url, In, [])
    ),
    copy_stream_data(In, Out),
    (
      close(In),
      close(Out)
    )
  ).


:- dynamic(sbml_id/2).


export_sbml(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('
    exports the current model into an SBML \\texttt{.xml} file.
  '),
  retractall(sbml_id(_, _)),
  set_counter(sbml_ids, 0),
  automatic_suffix(OutputFile, '.xml', write, FilenameXML),
  Tree = [element(
    sbml,
    [
      xmlns ='http://www.sbml.org/sbml/level3/version1/core',
      level ='3',
      version ='1'
    ],
    [
      element(
        model,
        [id ='Model_generated_by_BIOCHAM'],
        Lists
      )
    ]
  )],
  ListOfCompartments = element(
    listOfCompartments,
    [],
    [element(
      compartment,
      [id ='default_compartment', size ='1', constant ='true'],
      []
    )]
  ),
  get_sbml_species_list(Species),
  make_sbml_list(Species, listOfSpecies, ListOfSpecies),
  get_sbml_parameter_list(Parameters),
  make_sbml_list(Parameters, listOfParameters, ListOfParameters),
  get_sbml_reaction_list(Reactions),
  make_sbml_list(Reactions, listOfReactions, ListOfReactions),
  get_sbml_event_list(Events),
  make_sbml_list(Events, listOfEvents, ListOfEvents),
  get_sbml_rule_list(Rules),
  make_sbml_list(Rules, listOfRules, ListOfRules),
  setup_call_cleanup(
    open(FilenameXML, write, Stream, [encoding(utf8)]),
    (
      flatten([ListOfCompartments, ListOfSpecies, ListOfParameters,
        ListOfReactions, ListOfEvents, ListOfRules], Lists),
      xml_write(Stream, Tree, [])
    ),
    close(Stream)
  ).


make_sbml_list(L, Container, List) :-
  (
    L == []
  ->
    List = []
  ;
    List = element(Container, [], L)
  ).


get_sbml_species_list(L) :-
  enumerate_molecules(M),
  get_initial_values(M, V),
  maplist(build_sbml_species, M, V, L).


build_sbml_species(Name, Conc, Species) :-
  count(sbml_ids, Id),
  atomic_list_concat(['s', Id], SId),
  assertz(sbml_id(Name, SId)),
  Species = element(
    species,
    [
      id = SId, compartment ='default_compartment',
      initialConcentration = Conc, name = Name,
      hasOnlySubstanceUnits ='false',
      boundaryCondition ='false',
      constant ='false'
    ],
    []
  ).


get_sbml_parameter_list(L) :-
  all_items([no_inheritance, kind: parameter], P),
  all_items([no_inheritance, kind: function], F),
  include(has_no_args, F, FF),
  append(P, FF, LL),
  maplist(build_sbml_parameter, LL, L).


build_sbml_parameter(Thing, Param) :-
  count(sbml_ids, Id),
  (
    Thing = parameter(Name = Value)
  ->
    atomic_list_concat(['p', Id], SId),
    (
      item([kind: event, item: event(_Condition, Assignments)]),
      memberchk(Name = _, Assignments)
    ->
      Constant = 'false'
    ;
      Constant = 'true'
    ),
    Attrs = [
      value = Value,
      name = Name,
      constant = Constant
    ]
  ;
    Thing = function(Name = _Body),
    atomic_list_concat(['f', Id], SId),
    Attrs = [constant = 'false']
  ),
  assertz(sbml_id(Name, SId)),
  FullAttrs = [id = SId | Attrs],
  Param = element(
    parameter,
    FullAttrs,
    []
  ).


get_sbml_rule_list(L) :-
  all_items([no_inheritance, kind: function], F),
  % FIXME use functionDefinitions for the other ones
  include(has_no_args, F, FF),
  maplist(build_sbml_rule, FF, L).


build_sbml_rule(function(Name = Body), Param) :-
  sbml_id(Name, SId),
  term_to_mathml(Body, Math),
  Param = element(
    assignmentRule,
    [
      variable = SId
    ],
    [element(
      math,
      [xmlns = 'http://www.w3.org/1998/Math/MathML'],
      [Math]
    )]
  ).


get_sbml_event_list(L) :-
  all_items([no_inheritance, kind: event], E),
  maplist(build_sbml_event, E, L).


build_sbml_event(event(Condition, AssignmentList), Event) :-
  term_to_mathml(Condition, Math),
  maplist(build_sbml_event_assignment, AssignmentList, MAssignmentList),
  Event = element(
    event,
    [
      useValuesFromTriggerTime = 'true'
    ],
    [
      element(
        trigger,
        [initialValue = 'false', persistent = 'false'],
        [
          element(
            math,
            [xmlns = 'http://www.w3.org/1998/Math/MathML'],
            [Math]
          )
        ]
      ),
      element(
        listOfEventAssignments,
        [],
        MAssignmentList
      )
    ]
  ).


build_sbml_event_assignment(Param = Value, Assignment) :-
  sbml_id(Param, SId),
  term_to_mathml(Value, MValue),
  Assignment = element(
    eventAssignment,
    [variable = SId],
    [
      element(
        math,
        [xmlns = 'http://www.w3.org/1998/Math/MathML'],
        [MValue]
      )
    ]
  ).


get_sbml_reaction_list(L) :-
  all_items([no_inheritance, kind: reaction], R),
  maplist(build_sbml_reaction, R, L).


build_sbml_reaction(Item, Reaction) :-
  count(sbml_ids, Id),
  atomic_list_concat(['r', Id], SId),
  reaction(
    Item,
    [name: Name, kinetics: Kinetics, reactants: Reactants, products: Products]
  ),
  (
    Name == ''
  ->
    SName = []
  ;
    SName = [Name]
  ),
  reaction_editor:factorize_solution(Reactants, FReactants),
  reaction_editor:factorize_solution(Products, FProducts),
  kinetics(FReactants, [], Kinetics, KineticsExpression),
  term_to_mathml(KineticsExpression, MKinetics),
  maplist(build_sbml_speciesref, FReactants, SReactants),
  make_sbml_list(SReactants, listOfReactants, ListOfReactants),
  maplist(build_sbml_speciesref, FProducts, SProducts),
  make_sbml_list(SProducts, listOfProducts, ListOfProducts),
  KineticLaw = element(
    kineticLaw,
    [],
    [
      element(
        math,
        [xmlns = 'http://www.w3.org/1998/Math/MathML'],
        [MKinetics]
      )
    ]
  ),
  flatten([ListOfReactants, ListOfProducts, KineticLaw], Content),
  Reaction = element(
    reaction,
    [id = SId, reversible = 'false', fast = 'false' | SName],
    Content
  ).


build_sbml_speciesref(S*M, SpeciesRef) :-
  !,
  sbml_id(M, SId),
  SpeciesRef = element(
    speciesReference,
    [
      species = SId,
      constant = 'true',
      stoichiometry = S
    ],
    []
  ).

build_sbml_speciesref(M, SpeciesRef) :-
  build_sbml_speciesref(1*M, SpeciesRef).


term_to_mathml(N, element(cn, [], [N])) :-
  number(N),
  !.

term_to_mathml(Name, element(ci, [], [SId])) :-
  sbml_id(Name, SId),
  !.

term_to_mathml([Name], element(ci, [], [SId])) :-
  sbml_id(Name, SId),
  !.

term_to_mathml(true, element(true, [], [])) :-
  !.

term_to_mathml(false, element(false, [], [])) :-
  !.

term_to_mathml('Time', element(csymbol, [
  encoding = 'text',
  definitionURL = 'http://www.sbml.org/sbml/symbols/time'
], ['Time'])) :-
  !.

term_to_mathml(if C then A else B, M) :-
  !,
  maplist(term_to_mathml, [A, B, C], [MA, MB, MC]),
  M = element(
    piecewise,
    [],
    [
      element(
        piece,
        [],
        [MA, MC]
      ),
      element(
        otherwise,
        [],
        [MB]
      )
    ]
  ).

term_to_mathml(min(A, B), M) :-
  !,
  term_to_mathml(if A <= B then A else B, M).

term_to_mathml(max(A, B), M) :-
  !,
  term_to_mathml(if A >= B then A else B, M).

term_to_mathml(C, element(apply, [], Contents)) :-
  C =.. [Functor | Args],
  functor_to_mathml(Functor, MFunctor),
  !,
  Contents = [element(MFunctor, [], []) | MArgs],
  maplist(term_to_mathml, Args, MArgs).


functor_to_mathml('*', times).

functor_to_mathml('/', divide).

functor_to_mathml('+', plus).

functor_to_mathml('-', minus).

functor_to_mathml('^', power).

functor_to_mathml(log, ln).

functor_to_mathml(exp, exp).

functor_to_mathml(sin, sin).

functor_to_mathml(cos, cos).

functor_to_mathml(or, or).

functor_to_mathml(and, and).

functor_to_mathml(not, not).

functor_to_mathml('=', eq).

functor_to_mathml('<', lt).

functor_to_mathml('<=', leq).

functor_to_mathml('>', gt).

functor_to_mathml('>=', geq).

% catchall, but maybe we should throw an error
functor_to_mathml(X, X).


has_no_args(function(Name = _Body)) :-
  atomic(Name),
  !.
