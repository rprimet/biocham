:- use_module(library(plunit)).
:- use_module(lazy_negation).

example(1,PIVP) :-
   PIVP = ( (1,(d(y)/dt = z));
            (0,(d(z)/dt = -y))
          ).

example(2,PIVP) :-
   PIVP = ( (-1,(d(x)/dt = x));
            (0,(d(z)/dt = -x*z))
          ).

example(3,PIVP) :-
   PIVP = ( (1,(d(y)/dt = z + y));
            (0,(d(z)/dt = -y))
          ).

example(4,PIVP) :-
   PIVP = ( (1,(d(y)/dt = z - y*z));
            (0,(d(z)/dt = -y))
          ).

example(5,PIVP) :-
   PIVP = ( (1,(d(x)/dt = 1 - x^2));
            (0,(d(y)/dt = x - y) )
          ).

:- begin_tests(lazy_negation).

test(detect_negative_variables) :-
   example(1,P1), ln:detect_negative_variables(P1,[z,y]),
   example(2,P2), ln:detect_negative_variables(P2,[x]),
   example(3,P3), ln:detect_negative_variables(P3,[z,y]),
   example(4,P4), ln:detect_negative_variables(P4,[z,y]),
   example(5,P5), ln:detect_negative_variables(P5,[]).

test(is_in) :-
   ln:is_in(x,x*y),
   ln:is_in(x,z*x^2*y),
   ln:is_in(x,2*z*x),
   ln:is_in(x,-x),
   not(ln:is_in(z,x*y)).

test(is_negated) :-
   ln:is_negated(-x,x),
   ln:is_negated(-1,1),
   ln:is_negated(-x*y,x*y),
   ln:is_negated(-2*x,2*x),
   ln:is_negated(-1*y,y).

test(replace_in) :-
   ln:replace_in(x,x*y,x_p,x_p*y),
   ln:replace_in(x,x^2*y,x_p,x_p^2*y),
   ln:replace_in(x,-x^2*y,x_p,-x_p^2*y),
   ln:replace_in(x,none,x_p,none).

test(concatenate_expression) :-
   ln:concatenate_expression(none,x,x),
   ln:concatenate_expression(xy,none,xy),
   ln:concatenate_expression(x,y,x+y),
   ln:concatenate_expression(x,+y,x+y),
   ln:concatenate_expression(x,-y,x-y).

test(rewrite_monomial) :-
   ln:rewrite_monomial(1, [x,y], 1, none),
   ln:rewrite_monomial(-1, [x,y], none, 1),
   ln:rewrite_monomial(x, [x,y], x_p, x_m),
   ln:rewrite_monomial(y, [x,y], y_p, y_m),
   ln:rewrite_monomial(x*y, [x], x_p*y, x_m*y),
   ln:rewrite_monomial(2*x*y, [x,y], 2*x_p*y_p+2*x_m*y_m, 2*x_m*y_p+2*x_p*y_m),
   ln:rewrite_monomial(-x, [x,y], x_m, x_p),
   ln:rewrite_monomial(-y, [x,y], y_m, y_p),
   ln:rewrite_monomial(-x*y, [x], x_m*y, x_p*y).

test(rewrite_derivative):-
   ln:rewrite_derivative((-1,(d(x)/dt = 1)),[x],D1),
   ln:rewrite_derivative((-1,(d(x)/dt = 1)),[x,y],D1),
   D1 = (0, d(x_p)/dt = 1-fast*x_p*x_m;1, d(x_m)/dt = -fast*x_p*x_m),

   ln:rewrite_derivative((1,(d(x)/dt = x)),[x],D2),
   D2 = (1, d(x_p)/dt = x_p-fast*x_p*x_m;0, d(x_m)/dt = x_m-fast*x_p*x_m),

   ln:rewrite_derivative((-1,(d(x)/dt = y)),[x],D3),
   D3 = (0, d(x_p)/dt = y-fast*x_p*x_m;1, d(x_m)/dt = -fast*x_p*x_m),

   ln:rewrite_derivative((1,(d(x)/dt = x-x*y)),[x],D4),
   D4 = (1, d(x_p)/dt = x_p+x_m*y-fast*x_p*x_m;0, d(x_m)/dt = x_m+x_p*y-fast*x_p*x_m),

   ln:rewrite_derivative((0,(d(y)/dt = -x*y)),[x],D5),
   D5 = (0,(d(y)/dt = x_m*y - x_p*y)).

:- end_tests(lazy_negation).
