:- use_module(library(plunit)).

:- begin_tests(aliases).

test('alias', [true(Reactions == [2 * a => c])]) :-
  clear_model,
  command(a + b => c),
  command(alias(a = b)),
  all_items([kind: reaction], Reactions).

:- end_tests(aliases).
