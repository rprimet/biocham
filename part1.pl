:- doc("This part describes the syntax of BIOCHAM models of biochemical processes. They are essentially of two forms: 
\\begin{itemize}
\\item reaction networks (i.e. Feinberg's Chemical Reaction Networks, CRNs, compatible with SBML)
\\item influence networks (variant of Thomas's regulatory networks, compatible with qualSBML). 
\\end{itemize}
Both types of models can be combined and interpreted in a hierarchy of semantics, including the differential semantics (Ordinary Differential Equations), stochastic semantics (Continuous-time Markov Chain), Petri net semantics, and Boolean semantics including several variants defined by options \\cite{FMRS18tcbb}.").
