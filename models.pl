:- module(
  models,
  [
    % Grammars
    range/1,
    ref/1,
    % Commands
    load/1,
    add/1,
    load_biocham/1,
    add_biocham/1,
    export_biocham/1,
    new_model/0,
    clear_model/0,
    list_models/0,
    list_current_models/0,
    list_model/0,
    select_model/1,
    set_model_name/1,
    delete/1,
    inherits/1,
    % Public API
    get_model_name/1,
    get_model_name/2,
    current_models/1,
    single_model/1,
    new_model/1,
    set_current_models/1,
    add_item/1,
    replace_item/4,
    change_item/4,
    set_annotation/3,
    get_annotation/3,
    delete_annotation/2,
    item/1,
    find_item/1,
    all_items/2,
    all_ids/2,
    list_items/1,
    list_ids/1,
    list_ids/2,
    print_item/1,
    deleting/1,
    delete_item/1,
    delete_items/1,
    add_dependency/2,
    add_file_suffix/2,
    inherits/2,
    inherits_from/2,
    directly_inherits_from/2,
    begin_command/0,
    get_selection/3,
    set_selection/3,
    at_delete/2,
    get_parent/2,
    get_model/2,
    load_all/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).


:- devdoc('This secton needs documentation for the developper, in particular about the invariants concerning item, parent, id, key, kind. 

The error unkown_item we get is also unsufficiently informative.').

:-devdoc('
Warning: you cannot delete items through backtracking on the item predicate, you must use all_items and backtrack with member to delete items in the list.

Par exemple pour le retrait/ajout de règles dans revision.pl, j ai dû
directement manipuler les items de type (kind) reaction.

L API de models.pl donne quelques facilités, par exemple
 findall(Item, item([kind: reaction, item: Item]), Reactions)
s écrit plus simplement
 all_items([kind: reaction], Reactions)

et tu peux plus efficacement manipuler les ids
 all_ids([kind: reaction], Ids)
de façon à pouvoir ensuite appeler directement delete_item(Id)
plutôt que de devoir rechercher les réactions dans la base.
').

         
:- devdoc('\\section{Grammars}').


:- grammar(range).


range(Integer0 - Integer1) :-
  integer(Integer0),
  integer(Integer1).

range(Integer) :-
  integer(Integer).


:- grammar(ref).


ref(Range) :-
  list(range, Range).

ref(Name) :-
  name(Name).


:- devdoc('\\section{Commands}').


load(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as the corresponding \\command{load_biocham/1} / \\command{load_sbml/1}
    / \\command{load_ode/1} / \\command{load_table/1},
    depending on the file extension
    (respectively \\texttt{.bc}, \\texttt{.xml}, \\texttt{.ode}, \\texttt{.csv}
    -- assuming no extension is \\texttt{.bc}).'),
  load_all(_Suffix, InputFile).


add(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as the corresponding \\command{add_biocham/1} / \\command{add_sbml/1}
    / \\command{add_ode/1} / \\command{add_table/1},
    depending on the file extension
    (respectively \\texttt{.bc}, \\texttt{.xml}, \\texttt{.ode}, \\texttt{.csv}
    -- assuming no extension is \\texttt{.bc}).'),
  add_all(_Suffix, InputFile).


load_biocham(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    opens a new model, loads the reaction rules and executes the commands
    (with the file directory as current directory)
    contained in the given Biocham \\texttt{.bc} file.
    The suffix \\texttt{.bc} is automatically added to the name if such a
    file exists.'),
  load_all('bc', InputFile).


add_biocham(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    the rules of the given \\texttt{.bc} file are loaded and
    \\emph{added} to the current set of rules.
    The commands contained in the file are executed
    (with the file directory as current directory).'),
  add_all('bc', InputFile).


export_biocham(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('
    exports the current model into a \\texttt{.bc} file.
  '),
  with_output_to_file(OutputFile, list_model).


new_model :-
  biocham_command,
  doc('opens a new fresh model.'),
  (
    fresh,
    current_models([_Id])
  ->
    true
  ;
    new_model(Id),
    set_current_models([Id]),
    assertz(fresh)
  ).


clear_model :-
  biocham_command,
  doc('clears the current model.'),
  current_models(CurrentModels),
  \+ (
    member(Id, CurrentModels),
    \+ (
      delete_item(Id)
    )
  ),
  set_option(show, {}),
  retractall(nusmv:query_molecules(_)),
  assertz(nusmv:query_molecules([])).


list_models :-
  biocham_command,
  doc('lists all open models.'),
  list_items([parent: top, kind: model]).


list_current_models :-
  biocham_command,
  doc('lists current models.'),
  current_models(CurrentModels),
  list_ids(CurrentModels).


list_model :-
  biocham_command,
  doc('lists the current Biocham model: reactions, influences, initial state, parameters, events, functions, LTL patterns, options.'),
  list_model_reactions,
  list_model_influences,
  list_model_initial_state,
  list_model_parameters,
  list_model_events,
  list_model_macros(function),
  list_model_macros(ltl_pattern),
  list_model_options.


select_model(RefSet) :-
  biocham_command,
  type(RefSet, {ref}),
  doc('selects some models.'),
  find_model_refs(RefSet, CurrentModels),
  set_current_models(CurrentModels).


set_model_name(Name) :-
  biocham_command,
  type(Name, name),
  doc('changes the current model name.'),
  single_model(Id),
  replace_item(Id, model, Name, Name).


delete(Indexes) :-
  biocham_command(*),
  type(Indexes, '*'([range])),
  doc('deletes the listed elements from the model.'),
  indexes_ids(Indexes, Ids),
  \+ (
    member(Id, Ids),
    \+ (
      delete_item(Id)
    )
  ).


inherits(Ancestors) :-
  biocham_command(*),
  type(Ancestors, '*'(ref)),
  doc('makes the current model inherit from the given ancestor models.'),
  single_model(Id),
  find_model_refs(Ancestors, Models),
  \+ (
    member(AncestorId, Models),
    \+ (
      inherits(Id, AncestorId)
    )
  ).


:- devdoc('\\section{Public API}').


get_model_name(Name) :-
  single_model(Id),
  get_model_name(Id, Name).


get_model_name(Id, Name) :-
  find_item([parent: top, id: Id, item: Name]).


current_models(Models) :-
  get_selection(top, current_models, Models).


single_model(Model) :-
  (
    current_models([Model])
  ->
    true
  ;
    throw(error(need_single_model))
  ).


new_model(Id) :-
  add_item([parent: top, kind: model, key: new_model, id: Id]),
  (
    item([parent: top, kind: model, key: initial, id: InitialId])
  ->
    inherits(Id, InitialId)
  ;
    true
  ).


set_current_models(Models) :-
  (
    Models = []
  ->
    throw(error(cannot_select_no_models))
  ;
    set_selection(top, current_models, Models)
  ).


add_item(Options) :-
  memberchk(kind: Kind, Options),
  optional(item: Item, Options),
  optional(parent: Parent, Options),
  optional(key: Key, Options),
  optional(id: Id, Options),
  default(Parent, current_model),
  default(Key, []),
  (
    Parent = current_model
  ->
    single_model(Parent0)
  ;
    Parent0 = Parent
  ),
  (
    var(Item)
  ->
    (
      list(Key)
    ->
      throw(error(no_item_given))
    ;
      Item = Key
    )
  ;
    true
  ),
  not_fresh,
  create_item_id(Id),
  put_item(Parent0, Kind, Key, Item, Id).


replace_item(Id, Kind, Key, Item) :-
  retract(item(Id, Parent, _Kind, _Item)),
  retractall(key(_Key, Id)),
  put_item(Parent, Kind, Key, Item, Id).


change_item(Options, Kind, Key, Item) :-
  catch(
    (
      find_item([noinheritance, kind: Kind, key: Key, id: Id | Options]),
      replace_item(Id, Kind, Key, Item)
    ),
    error(unknown_item),
    add_item([kind: Kind, key: Key, item: Item | Options])
  ).


set_annotation(Id, Kind, Annotation) :-
  retractall(annotation(Id, Kind, _)),
  assertz(annotation(Id, Kind, Annotation)).


get_annotation(Id, Kind, Annotation) :-
  annotation(Id, Kind, Annotation).


delete_annotation(Id, Kind) :-
  retract(annotation(Id, Kind, _)).


item(Options) :-
  optional(kind: Kind, Options),
  optional(id: Id, Options),
  optional(key: Key, Options),
  optional(item: Item, Options),
  item_parent_option(Options, Parent),
  (
    var(Id)
  ->
    (
      Parent0 = Parent
    ;
      \+ member(no_inheritance, Options),
      inherits_from(Parent, Parent0)
    )
  ;
    Parent0 = Parent
  ),
  (
    list(Kind)
  ->
    member(Kind0, Kind)
  ;
    Kind0 = Kind
  ),
  (
    var(Key)
  ->
    true
  ;
    list(Key)
  ->
    indexes_ids(Key, Ids),
    member(Id, Ids)
  ;
    key(Key, Id)
  ),
  item(Id, Parent0, Kind0, Item).


find_item(Options) :-
  (
    item(Options)
  ->
    true
  ;
    throw(error(unknown_item))
  ).


all_items(Options, Items) :-
  findall(
    Item,
    item([item: Item | Options]),
    Items
  ).


all_ids(Options, Ids) :-
  findall(
    Id,
    item([id: Id | Options]),
    Ids
  ).


list_items(Options) :-
  all_ids([no_inheritance | Options], Ids),
  list_ids(Options, Ids),
  (
    member(no_inheritance, Options)
  ->
    true
  ;
    item_parent_option(Options, Parent),
    (
      var(Parent)
    ->
      true
    ;
      optional(indent: Indent, Options),
      default(Indent, 0),
      \+ (
        inherits_from(Parent, Parent0),
        all_ids([parent: Parent0, no_inheritance | Options], InheritedIds),
        InheritedIds \= [],
        \+ (
          indent(Indent),
          find_item([id: Parent0, item: ParentName]),
          format('From inherited \'~w\':\n', [ParentName]),
          list_ids(Options, InheritedIds)
        )
      )
    )
  ).


list_ids(Ids) :-
  list_ids([], Ids).


list_ids(Options, Ids) :-
  (
    peek_count(list_item_counter, 0),
    \+ memberchk(no_numbering, Options)
  ->
    retractall(listed_item(_, _))
  ;
    true
  ),
  list_ids_aux(Options, Ids).


print_item(Id) :-
  item(Id, _Parent, Kind, Item),
  (
    atom_concat('print_', Kind, F),
    G =.. [F, Id, Item],
    predicate_property(G, visible)
  ->
    G
  ;
    write(Item)
  ).


:- dynamic(deleting/1).


delete_item(Options) :-
  list(Options),
  !,
  find_item([id: Id | Options]),
  delete_item(Id).


delete_item(Id) :-
  get_parent(Id, Parent),
  delete_item_aux(Id),
  update_parent_model_identifier_kinds(Parent).


delete_item_aux(Id) :-
  (
    deleting(Id)
 ->
    throw(error(circular_delete(Id), delete_item_aux))
  ;
    assertz(deleting(Id))
  ),
  \+ (
    at_delete_goal(Id, Goal),
    \+ (
      Goal
    ->
      true
    ;
      throw(error(delete_goal_failure(Id), delete_item_aux))
    )
  ),
  retract(item(Id, _Model, KindItem, _Item)),
  retractall(key(_Key, Id)),
  retractall(annotation(Id, _KindAnnotation, _Annotation)),
  retractall(listed_item(_Index, Id)),
  retractall(dependency(Id, _Master)),
  retractall(at_delete_goal(Id, _Goal)),
  \+ (
    item(SubId, Id, _SubKind, _SubItem),
    \+ (
      (
        item(SubId, __Id, __SubKind, __SubItem)
      ->
        delete_item_aux(SubId)
      ;
        true
      )
    )
  ),
  \+ (
    dependency(SubId, Id),
    \+ (
      (
        item(SubId, ___Id, ___SubKind, ___SubItem)
      ->
        delete_item_aux(SubId)
      ;
        true
      )
    )
  ),
  retractall(selection(_, _, Id)),
  (
    KindItem = model
  ->
    (
      selection(top, current_models, _)
    ->
      true
    ;
      new_model
    )
  ;
    true
  ),
  retract(deleting(Id)).


delete_items(Options) :-
  \+ (
    item([id: Id | Options]),
    \+ (
      delete_item(Id)
    )
  ).


add_dependency(IdSub, IdMaster) :-
  assertz(dependency(IdSub, IdMaster)).


:- multifile(add_file_suffix/2).


add_file_suffix('', add_biocham_file).


add_file_suffix('bc', add_biocham_file).


inherits(Id, AncestorId) :-
  (
    Id = AncestorId
  ->
    throw(error(cannot_inherit_from_itself, inherits))
  ;
    self_or_inherits_from(Child, Id),
    self_or_inherits_from(AncestorId, Parent),
    inherits_from(Parent, Child)
  ->
    throw(error(cannot_inherit_from_descendant, inherits))
  ;
    (
      item(Id, top, model, _Item0),
      item(AncestorId, top, model, _Item1),
      self_or_inherits_from(Child, Id),
      identifier_kind(Child, Ident, Kind0),
      Kind0 \= free,
      identifier_kind(AncestorId, Ident, Kind1),
      Kind1 \= free,
      Kind0 \= Kind1
    ->
      throw(error(kind_mismatch(Ident, Kind0, Kind1), inherits))
    ;
      true
    ),
    assertz(directly_inherits_from(Id, AncestorId))
  ).


prolog:error_message(cannot_inherit_from_itself) -->
  [
    'Cannot inherit from itself.'
  ].


prolog:error_message(already_inherits) -->
  [
    'There is already such an inheritance relation.'
  ].


prolog:error_message(cannot_inherit_from_descendant) -->
  [
    'Cannot inherit from descendant.'
  ].

prolog:error_message(
  kind_mismatch(Ident, Kind, NeededKind)
) -->
  {
    format(
      atom(Message),
      'There is a mismatch between the uses of ~a: ~a and ~a.',
      [Ident, Kind, NeededKind]
    )
  },
  [Message].


inherits_from(Id, AncestorId) :-
  directly_inherits_from(Id, AncestorId).

inherits_from(Id, AncestorId) :-
  directly_inherits_from(Id, IntermediateAncestorId),
  inherits_from(IntermediateAncestorId, AncestorId).


self_or_inherits_from(Id, Id).

self_or_inherits_from(Id, AncestorId) :-
    inherits_from(Id, AncestorId).


:- dynamic(directly_inherits_from/2).


begin_command :-
  set_counter(list_item_counter, 0).


get_selection(ParentOrCurrentModel, SelectionName, Ids) :-
  get_selection_parent(ParentOrCurrentModel, Parent),
  findall(
    Id,
    selection(Parent, SelectionName, Id),
    Ids
  ).


set_selection(ParentOrCurrentModel, SelectionName, IdsOrOptions) :-
  get_selection_parent(ParentOrCurrentModel, Parent),
  retractall(selection(Parent, SelectionName, _)),
  \+ (
    (
      IdsOrOptions = [_: _| _]
    ->
      item([id: Id | IdsOrOptions])
    ;
      member(Id, IdsOrOptions)
    ),
    \+ (
      assertz(selection(Parent, SelectionName, Id))
    )
  ).


at_delete(Id, Goal) :-
  assertz(at_delete_goal(Id, Goal)).


get_parent(Id, ParentId) :-
  item(Id, ParentId, _, _).


get_model(Id, ModelId) :-
  item(Id, ParentId, Kind, _),
  (
    Kind = model
  ->
    ModelId = Id
  ;
    get_model(ParentId, ModelId)
  ).


:- devdoc('\\section{Private predicates}').


default(Variable, Value) :-
  (
    var(Variable)
  ->
    Variable = Value
  ;
    true
  ).


put_item(Parent, Kind, Key, Item, Id) :-
  (
    list(Key)
  ->
    \+ (
      member(K, Key),
      \+ (
        assertz(key(K, Id))
      )
    )
  ;
    assertz(key(Key, Id))
  ),
  assertz(item(Id, Parent, Kind, Item)),
  update_parent_model_identifier_kinds(Parent).


single_model(Model, ModelId) :-
  (
    Model = current_model
  ->
    single_model(ModelId)
  ;
    ModelId = Model
  ).


item_parent_option(Options, ChosenParent) :-
  optional(parent: Parent, Options),
  optional(id: Id, Options),
  (
    var(Id)
  ->
    default(Parent, current_model),
    (
      Parent == current_model
    ->
      single_model(ChosenParent)
    ;
      Parent == current_models
    ->
      current_models(Models),
      member(ChosenParent, Models)
    ;
      ChosenParent = Parent
    )
  ;
    ChosenParent = Parent
  ).


optional(Item, List) :-
  (
    member(Item, List)
  ->
    true
  ;
    true
  ).


:- dynamic(listed_item/2).


list_ids_aux(Options, Ids) :-
  optional(indent: Indent, Options),
  default(Indent, 0),
  (
    member(recursive, Options)
  ->
    Recursive = yes
  ;
    Recursive = no
  ),
  \+ (
    member(Id, Ids),
    \+ (
      indent(Indent),
      (
        selection(_, _, Id)
      ->
        Selected = '*'
      ;
        Selected = ' '
      ),
      (
        memberchk(no_numbering, Options)
      ->
        true
      ;
        count(list_item_counter, Counter),
        assertz(listed_item(Counter, Id)),
        format('[~d]', [Counter])
      ),
      write(Selected),
      print_item(Id),
      nl,
      (
        Recursive = yes
      ->
        SubIndent is Indent + 1,
        all_ids([parent: Id | Options], SubIds),
        list_ids_aux([indent: SubIndent, recursive | Options], SubIds)
      ;
        true
      )
    )
  ).


indexes_ids(Indexes, Ids) :-
  findall(
    Id,
    (
      member(Ranges, Indexes),
      ranges_ids(Ranges, RangeIds),
      member(Id, RangeIds)
    ),
    Ids
  ).


ranges_ids(Ranges, Ids) :-
  findall(
    Id,
    (
      member(Range, Ranges),
      (
        Range = Min - Max
      ->
        (
          Min =< Max
        ->
          between(Min, Max, Range)
        ;
          throw(error(invalid_range(Min, Max)))
        )
      ;
        Index = Range
      ),
      resolve_index(Index, Id)
    ),
    Ids
  ).


resolve_index(Index, Id) :-
  (
    listed_item(Index, Id)
  ->
    true
  ;
    throw(error(no_such_element, Index))
  ).


create_item_id(Id) :-
  count(item_id, Id).


:- dynamic(dependency/2).


:- dynamic(fresh/0).


:- dynamic(at_delete_goal/2).


:- dynamic(selection/3).


get_selection_parent(ParentOrCurrentModel, Parent) :-
  (
    ParentOrCurrentModel = current_model
  ->
    single_model(Parent)
  ;
    Parent = ParentOrCurrentModel
  ).


load_all(Suffix, InputFile) :-
  current_models(OldCurrentModels),
  findall(
    Id,
    (
      filename(InputFile, Filename),
      (
        var(Suffix)
      ->
        file_name_extension(_, Suffix, Filename)
      ;
        true
      ),
      load(Suffix, Filename),
      current_models(Ids),
      member(Id, Ids)
    ),
    NewCurrentModels
  ),
  (
    NewCurrentModels = []
  ->
    restore_current_models(OldCurrentModels)
  ;
    set_current_models(NewCurrentModels)
  ).


restore_current_models(OldCurrentModels) :-
  findall(
    Model,
    (
      member(Model, OldCurrentModels),
      once(item([id: Model]))
    ),
    CurrentModels
  ),
  (
    CurrentModels = []
  ->
    new_model
  ;
    set_current_models(CurrentModels)
  ).


add_all(Suffix, InputFile) :-
  \+ (
    filename(InputFile, Filename),
    \+ (
      (
        var(Suffix)
      ->
        file_name_extension(_, Suffix, Filename)
      ;
        true
      ),
      add(Suffix, Filename)
    )
  ).


load(Suffix, Filename) :-
  new_model,
  file_base_name(Filename, FileBaseName),
  (
    file_name_extension(Base, _, FileBaseName)
  ->
    true
  ;
    Base = FileBaseName
  ),
  set_model_name(Base),
  add(Suffix, Filename).


add(Suffix, Filename) :- % e.g. calls add_ode_system_file(Filename) with add_suffix defined in ode.pl
  (
    add_file_suffix(Suffix, Functor)
  ->
    call(Functor, Filename)
  ;
    throw(error(unknown_suffix(Suffix)))
  ).


add_biocham_file(Filename) :-
  automatic_suffix(Filename, '.bc', read, FilenameBc),
  file_directory_name(FilenameBc, FileDirectory),
  setup_call_cleanup(
    (
      open(FilenameBc, read, Stream),
      working_directory(PreviousDirectory, FileDirectory)
    ),
    load_biocham_stream(Stream),
    (
      close(Stream),
      working_directory(_, PreviousDirectory)
    )
  ).


load_biocham_stream(Stream) :-
  \+ (
    repeat,
    read_term(Stream, Command, [variable_names(VariableNames)]),
    (
      Command = end_of_file
    ->
      !
    ;
      name_variables_and_anonymous(Command, VariableNames),
      command(Command)
    ),
    fail
  ).


not_fresh :-
  retractall(fresh).


:- dynamic(item/4).


:- dynamic(key/2).


:- dynamic(annotation/3).


update_parent_model_identifier_kinds(Parent) :-
  (
    Parent = top
  ->
    true
  ;
    get_model(Parent, ModelId),
    update_identifier_kinds(ModelId)
  ).


find_model_refs(RefSet, Models) :-
  findall(
    Id,
    (
      member(Ref, RefSet),
      (
        list(Ref)
      ->
        ranges_ids(Ref, Ids),
        member(Id, Ids)
      ;
        find_item([parent: top, key: Ref, id: Id])
      ),
      item([id: Id, kind: Kind]),
      (
        Kind = model
      ->
        true
      ;
        throw(error(not_a_model(Kind)))
      )
    ),
    Models
  ).
