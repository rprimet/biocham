:- module(
  junit, [
    run_junit_tests/0,
    run_junit_tests/1,
    run_tests_and_halt/0,
    run_tests_and_halt/1
  ]).


%  main test runner
run_junit_tests :-
  run_junit_tests(all).


run_junit_tests(Spec) :-
  set_prolog_flag(verbose, normal),
  (
    (
      Spec == all
    ->
      run_tests
    ;
      run_tests(Spec)
    )
  ->
    true
  ;
    % we do not want to fail even if run_tests fails
    true
  ),
  open('junit.xml', write, _, [alias(junit)]),
  format(
    junit,
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<testsuites>\n", []
  ),
  forall(
    plunit:current_test_set(Unit),
    (
      format(junit, "  <testsuite name=\"~w\">\n", [Unit]),
      output_unit_results(Unit),
      format(junit, "  </testsuite>\n", [])
    )
  ),
  format(junit, "</testsuites>\n", []),
  close(junit),
  % Now we fail if all did not go right
  plunit:check_for_test_errors.


run_tests_and_halt :-
  run_tests_and_halt(all).


run_tests_and_halt(Spec) :-
  call_cleanup(
    (
      run_junit_tests(Spec),
      halt(0)
    ),
    halt(1)
  ).


%  scans plunit dynamic predicates and outputs corresponding info to XML
output_unit_results(Unit) :-
  output_passed_results(Unit),
  output_failed_results(Unit).


%  outputs a successful testcase with its time for each plunit:passed/5 entry
output_passed_results(Unit) :-
  forall(
    plunit:passed(Unit, Name, _Line, _Det, Time),
    format(junit, "    <testcase name=\"~w\" time=\"~w\" />\n", [Name, Time])
  ).


%  outputs a failure inside a testcase for each plunit:failed/4 entry
output_failed_results(Unit) :-
  forall(
    plunit:failed(Unit, Name, _Line, Error),
    (
      format(junit, "    <testcase name=\"~w\">\n", [Name]),
      format(junit, "      <failure message=\"~w\" />\n", [Error]),
      format(junit, "    </testcase>\n", [])
    )
  ).
