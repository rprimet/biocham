:- use_module(library(plunit)).

:- begin_tests(
  multistability,
  [
    setup(load('library:biomodels/BIOMD0000000040.xml')),
    cleanup(clear_model)
  ]
).

test('acyclic_multi_graph') :-
  multistability:multistability_graph,
  \+ multistability:check_acyclicity.

test('multistability_fail') :-
  with_output_to(atom(Output), check_multistability),
  sub_atom(Output, 0, _, _, 'There may be non-degenerate multistationarity, positive circuit detected').

test('multistability_success') :-
  with_output_to(
    atom(Output),
    with_option(test_transpositions: yes, check_multistability)
  ),
  sub_atom(Output, 0, _, _, 'There is no multiple steady states with non-zero values, no positive circuit').

:- end_tests(multistability).
