#include <stdio.h>
#include <stdlib.h>
#include <SWI-Prolog.h>
#include <gsl/gsl_poly.h>

#include "roots.h"

static int
near(const double x, const double y)
{
  return (x - y < EPSILON_POL) && (y - x < EPSILON_POL);
}

/* static void
   system_solve(const double matrix_data[],
   const double vector_data[],
   const int n,
   gsl_vector * x)
   {
   int s;
   
   gsl_matrix_view m
   = gsl_matrix_view_array(matrix_data, n, n);
   
   gsl_vector_view b
   = gsl_vector_view_array(vector_data, n);
   
   gsl_permutation * p = gsl_permutation_alloc(n);
   
   gsl_linalg_LU_decomp(&m.matrix, p, &s);
   gsl_linalg_LU_solve(&m.matrix, p, &b.vector, x);
   
   gsl_permutation_free(p);
   }*/


/* factors a polynomial into a product of the form
   (x + alpha_0)^n_0 ... (x2 + beta_0 x + gamma_0)^m_0 */
static void
get_factors(const double z[], const int N,
	    double ** alpha, double ** beta, double ** gamma,
	    int ** n, int ** m,
	    int * p, int * q)
{
  int i, j, k;
  int * cl, * re;
  
  *p = *q = 0;

  cl = (int *) malloc(N * sizeof(int));
  re = (int *) malloc(N * sizeof(int));

  for(i = 0 ; i < N ; i++)
    cl[i] = 0;
  for(i = 0 ; i < N ; i++)
    {
      /* new root encountered */
      if(cl[i] == 0) {

	/* real root */
	if(near(z[2*i+1], 0))
	  {
	    cl[i] = ++(*p);
	    re[i] = 1;
	  }
	
	/* imaginary root */
	else
	  {
	    cl[i] = ++(*q);
	    re[i] = 0;
	  }

	/* look up for copies of the root and its conjugate */
	for(j = i + 1 ; j < N ; j++)
	  {
	    if( (cl[j] == 0) &&
		near(z[2*i], z[2*j]) &&
		( near(z[2*i+1], z[2*j+1]) || near(z[2*i+1], -z[2*j+1]) ) )
	      {
		cl[j] = cl[i];
		re[j] = re[i];
	      }
	  }
      }
    }

  *alpha = (double *) malloc(*p * sizeof(double));
  *beta = (double *) malloc(*q * sizeof(double));
  *gamma = (double *) malloc(*q * sizeof(double));
  *n = (int *) malloc(*p * sizeof(int));
  *m = (int *) malloc(*q * sizeof(int));

  /* count the multiplicity of the real roots +
     set coefficient alpha */
  for(k = 0 ; k < *p ; k++)
    {
      (*n)[k] = 0;
      for(i = 0 ; i < N ; i++)
	{
	  if(cl[i] == k + 1 && re[i] == 1)
	    {
	      if((*n)[k] == 0)
		(*alpha)[k] = -z[2*i];
	      (*n)[k]++;
	    }
	}
    }

  /* count the multiplicity of the imaginary roots +
     set coefficients beta and gamma */
  for(k = 0 ; k < *q ; k++)
    {
      (*m)[k] = 0;
      for(i = 0 ; i < N ; i++)
	{
	  if(cl[i] == k + 1 && re[i] == 0)
	    {
	      if((*m)[k] == 0)
		{
		  (*beta)[k] = -2*z[2*i];
		  (*gamma)[k] = z[2*i]*z[2*i] + z[2*i+1]*z[2*i+1];
		}
	      (*m)[k]++;
	    }
	}
      /* divide by two since every im. root is encountered twice */
      (*m)[k] /= 2;
    }
}

/* associate a polynomial with the list of its irreducible factors */
static foreign_t
pl_roots (term_t l,// list of coefficients
	  term_t irr, // list of irreducible polynomials
	  term_t mul // list of multiplicities
	  )
{
  int _n = 0;
  int i = 0, j;

  double * a, * z;

  double * alpha, * beta, * gamma;
  int p, q;
  int * n, * m;

  term_t head = PL_new_term_ref();
  term_t list = PL_copy_term_ref(l);
  term_t factor = PL_new_term_ref();
  term_t coeff = PL_new_term_ref();
  term_t expst = PL_new_term_ref();

  /* read the list of coefficients */
  while(PL_get_list(list, head, list))
      _n++;
  list = PL_copy_term_ref(l);
  
  a = (double *) malloc(_n * sizeof(double));
  z = (double *) malloc(2 * (_n - 1) * sizeof(double));

  while(PL_get_list(list, head, list))
    {
      double c;
      if(PL_get_float(head, &c))
	a[i] = c;
      else
	PL_fail;
      i++;
    }

  /* pathological cases */
  if(_n < 2)
    PL_fail;

  /* calculate the complex roots */
  gsl_poly_complex_workspace * w 
      = gsl_poly_complex_workspace_alloc (_n);
  gsl_poly_complex_solve (a, _n, w, z);
  gsl_poly_complex_workspace_free (w);

  /* calculate irreducible factors from the roots */
  get_factors(z, _n-1, &alpha, &beta, &gamma, &n, &m, &p, &q);

  /* export it as a list of polynomials */
  /* first-order factors */
  for(i = 0 ; i < p ; i++)
    {
      if( !PL_unify_list(irr, factor, irr) ||
	  !PL_unify_list(factor, coeff, factor) ||
	  !PL_unify_float(coeff, alpha[i]) ||
	  !PL_unify_list(factor, coeff, factor) ||
	  !PL_unify_float(coeff, 1) ||
	  !PL_unify_nil(factor) ||
	  !PL_unify_list(mul, expst, mul) ||
	  !PL_unify_integer(expst, n[i]) )
	PL_fail;
    }
  /* second-order factors */
  for(i = 0 ; i < q ; i++)
    {
      if( !PL_unify_list(irr, factor, irr) ||
	  !PL_unify_list(factor, coeff, factor) ||
	  !PL_unify_float(coeff, gamma[i]) ||
	  !PL_unify_list(factor, coeff, factor) ||
	  !PL_unify_float(coeff, beta[i]) ||
	  !PL_unify_list(factor, coeff, factor) ||
	  !PL_unify_float(coeff, 1) ||
	  !PL_unify_nil(factor) ||
	  !PL_unify_list(mul, expst, mul) ||
	  !PL_unify_integer(expst, m[i]) )
	PL_fail;
    }

  /* to be clean */
  free(a);
  free(z);

  free(alpha);
  free(beta);
  free(gamma);
  free(n);
  free(m);

  return PL_unify_nil(irr) && PL_unify_nil(mul);
}


static foreign_t
pl_display(term_t l)
{
  term_t head = PL_new_term_ref();
  term_t list = PL_copy_term_ref(l);

  while(PL_get_list(list, head, list))
    {
      double a;
      if(PL_get_float(head, &a))
	printf("%lf\n", a);
      else
	PL_fail;
    }

  return PL_get_nil(list);
}


install_t
install_roots(void)
{
  PL_register_foreign("roots", 3, (pl_function_t) pl_roots, 0);
  PL_register_foreign("display", 1, (pl_function_t) pl_display, 0);
}
