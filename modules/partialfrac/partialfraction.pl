:- module(partialfraction, [
	      partial_fraction/5
	  ]).

% :- use_foreign_library(foreign(roots)).

% A polynomial is represented by the list of its coefficients
% the fist one is the constant and the last one is the leading coefficient
% The null polynomial is represented by the empty list
% A polynomial is under its proper form if its l.c. is nonzero.

% this predicate is used instead of the test to zero
small(A) :-
    A < 0.0001, A > -0.0001.

% calculates the degree of a polynomial
% with the null polynomial being of degree -1 instead of -infinity.
degree([], -1).

degree([_|A_next], D) :-
    degree(A_next, D_next),
    D is D_next + 1.

% calculates the oppisite polynomial, i.e. -P.
opposite([], []).

opposite([A|A_next], [B|B_next]) :-
    B is -A,
    opposite(A_next, B_next).

% calculates the leading coefficient of a polynomial.
leading([], 0).

leading([A|A_next], LC) :-
    leading(A_next, X),
    ( X =:= 0 -> LC is A ; LC is X ).

% calculates the difference betwenn two polynomial, possibly not in proper form.
row_subtract(A, [], A) :- !.

row_subtract([], B, C) :-
    opposite(B, C).

row_subtract([A|A_next], [B|B_next], [C|C_next]) :-
    C is float(A - B),
    row_subtract(A_next, B_next, C_next).

% calculates the sum of two polynomials, possibly not in proper form.
row_add(A, [], A) :- !.

row_add([], B, B) :- !.

row_add([A|A_next], [B|B_next], [C|C_next]) :-
    C is float(A + B),
    row_add(A_next, B_next, C_next).

% multiply a polynomial by a constant C.
mult(0, _, []) :- !.

mult(_, [], []) :- !.

mult(C, [A0|A_next], [B0|B_next]) :-
    B0 is C * A0,
    mult(C, A_next, B_next).

% calculates the product of a polynomial by a given monomial
% whose degree is D and coefficient is C.
monomial_mult(0, C, A, B) :-
    mult(C, A, B), !.

monomial_mult(D, C, A, [0|B]) :-
    Dm is D - 1,
    monomial_mult(Dm, C, A, B).

% delete zeros at the end, i.e. calculates the proper form
simplify([], []).

simplify([N|T], []) :-
    small(N),
    simplify(T, []), !.

simplify([A|Anxt], [A|Asmpl]) :-
    simplify(Anxt, Asmpl).

% calculates the unitary polynomial associated
normalized([], []) :- !.

normalized(A, Anorm) :-
    leading(A, Lc),
    mult(float(1 rdiv Lc), A, Anorm).

% subtracts two polynomials
subtract(A, B, C) :-
    row_subtract(A, B, D),
    simplify(D, C).

% adds two polynomials
add(A, B, C) :-
    row_add(A, B, D),
    simplify(D, C).

% multiply two polynomials
prod(_, [], []) :- !.

prod(A, [B|Bnxt], C) :-
    mult(B, A, C0),
    prod(A, Bnxt, C1), !,
    add(C0, [0|C1], C).

% calculates Q and R s.t. A = BQ + R with deg(R) < deg(B).
% [in floating point arithmetic]
long_division(_, [], _, _) :-
    fail.

long_division(A, B, Q, R) :-
    degree(A, DegA),
    degree(B, DegB),
    (
	DegA < DegB
     ->
	 Q = [],
	 R = A
     ;
     leading(A, LcA),
     leading(B, LcB),
     D is DegA - DegB,
     C is float(LcA rdiv LcB),
     monomial_mult(D, C, B, B1),
     subtract(A, B1, A1),
     long_division(A1, B, Q1, R), !,
     monomial_mult(D, C, [1], Q0),
     add(Q0, Q1, Q)
    ).

% calculates a greatest common divisor D of two polynomials
% and (U, V) s.t. AU + BV = D
euclid(A, [], [1], [], A) :- !.

euclid([], A, [], [1], A) :- !.

euclid(A, B, U, V, D) :-
    degree(A, DegA),
    degree(B, DegB),
    DegA >= DegB, !,
    long_division(A, B, Q, R),

    euclid(B, R, Uprime, Vprime, D),

    U = Vprime,
    prod(Q, Vprime, T),
    subtract(Uprime, T, V).

euclid(A, B, U, V, D) :-
    euclid(B, A, V, U, D).

% reduces a rational fraction into a fraction whose
% denominator and numerator are coprimes.
simplify_fraction(A, B, Aprime, Bprime) :-
    euclid(A, B, _, _, D),
    long_division(A, D, Aprime, _),
    long_division(B, D, Bprime, _).

% calculates the only unitary gcd.
normalized_euclid(A, B, Unorm, Vnorm, Dnorm) :-
    euclid(A, B, U, V, D),
    leading(D, LcD),
    C is float(1 rdiv LcD),
    mult(C, U, Unorm),
    mult(C, V, Vnorm),
    mult(C, D, Dnorm), !.

prod_list([], [1]).

prod_list([Q|L], P) :-
    prod_list(L, R),
    prod(Q, R, P).

exp(_, 0, [1]) :- !.

exp(P, N, Q) :-
    Nprime is N - 1,
    exp(P, Nprime, R),
    prod(P, R, Q).

% builds a polynomial from a list of factors with multiplicities.
prod_factors([], [], [1]).

prod_factors([P|PT], [M|MT], Q) :-
    prod_factors(PT, MT, R),
    exp(P, M, S),
    prod(R, S, Q).

% builds a polynomial from a list of factors with multiplicities
% except for the nth polynomial of the list.
prod_special([_|PT], [_|MT], 0, Q) :-
    prod_factors(PT, MT, Q), !.

prod_special([P|PT], [M|MT], N, Q) :-
    Nprime is N - 1,
    prod_special(PT, MT, Nprime, R),
    exp(P, M, S),
    prod(R, S, Q).

% calculates the product of a list of polynomials by a given polynomial.
prod_list([], _, []).

prod_list([P|PT], Q, [R|RT]) :-
    prod(P, Q, R),
    prod_list(PT, Q, RT).

dotprod_list([], [], []).

dotprod_list([P|PT], [Q|QT], [R|RT]) :-
    prod(P, Q, R),
    dotprod_list(PT, QT, RT).

% gives bezout relation for a list of polynomials
general_bezout([A], [[1]], A) :- !.

general_bezout([P|PT], [U|QT], D) :-
    general_bezout(PT, RT, E),
    normalized_euclid(P, E, U, V, D),
    prod_list(RT, V, QT).

% returns the list of the co-polynomials of a list of polynomial
% i.e. the list [Q/Q-0, Q/Q_1, ...] where Q = Q_0 ... Q_n.
co_polynomials(QL, ML, QLco) :-
    built_co_polynomials(QL, ML, 0, QLco).

built_co_polynomials(QL, _, N, []) :-
    length(QL, N), !.

built_co_polynomials(QL, ML, N, [Qco|QTco]) :-
    prod_special(QL, ML, N, Qco),
    Nnext is N + 1,
    built_co_polynomials(QL, ML, Nnext, QTco).

% from P/Q_0...Q_n get S0/Q0^m_0 + ... + Sn/Qn^m_n
pre_partial_fraction(P, QL, ML, SL) :-
    co_polynomials(QL, ML, TL),
    general_bezout(TL, UL, _),
    prod_list(UL, P, SL).

% from S/Q^m gets A_m/Q^m + ... + A_1/Q with deg(A_i) < deg(Q)
local_partial_fraction(_, _, 0, []) :- !.

local_partial_fraction(S, Q, M, [A|AT]) :-
    long_division(S, Q, Sprime, A),
    Mprime is M - 1,
    local_partial_fraction(Sprime, Q, Mprime, AT).

list_local_partial_fraction([], [], [], []) :- !.

list_local_partial_fraction([S|ST], [Q|QT], [M|MT], [A|AT]) :-
    local_partial_fraction(S, Q, M, A),
    list_local_partial_fraction(ST, QT, MT, AT).

% calculates partial fraction decomposition from numerator
% and denominator decomposition.
partial_fraction(P, QL, ML, AL) :-
    pre_partial_fraction(P, QL, ML, SL),
    list_local_partial_fraction(SL, QL, ML, AL).

/* Inputs:
     P, Q,
   Outputs:
     A is a list of polynomials of the form A_j_i/Q_i^j (i increasing, j decreasing),
     QL is a list of irreductible unitary factors,
     ML is the list of multiplicities with respect to QL.
*/
partial_fraction(P, Q, A, QL, ML) :-
    % the following three lines ensure Q is unitary
    % without altering the fraction P/Q
    leading(Q, Lead),
    PCoef is float(1 rdiv Lead),
    mult(PCoef, P, P_normal),
    
    roots(Q, QLraw, ML),
    clean_recursive(QLraw, QL),
    partial_fraction(P_normal, QL, ML, Araw),
    clean_recursive(Araw, A).

% to compensate rounding errors and stay clean
clean_number(X, Y) :-
    Xr is round(X),
    ERR is X - Xr,
    ( small(ERR) -> Y = Xr ; Y = X).

clean_recursive(X, Y) :-
    (X = [] -> Y = []
     ;
     X = [H|T] -> clean_recursive(H, CH),
		  clean_recursive(T, CT),
		  Y = [CH|CT]
     ;
     clean_number(X, Y)
    ).

