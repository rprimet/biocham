:- module(
  simple,
  [
    simple/2
  ]
).

:- use_module('../graphviz').

simple(SourceFilename, TargetFilename) :-
  agread(SourceFilename, Graph),
  gvLayout(Graph, dot),
  gvRenderFilename(Graph, plain, TargetFilename),
  gvFreeLayout(Graph),
  agclose(Graph).
