:- module(
  sbml_utils,
  [
    member_listof/2,
    model_reaction/2,
    model_species/2,
    model_assignment_rule/2,
    model_function/2,
    reaction_reactant/2,
    reaction_modifier/2,
    reaction_product/2,
    reaction_kinetics/3,
    species_id_and_initial/3,
    species_constant_and_boundary/3,
    species_has_only_substance_units/1,
    parameter_id_and_value/3,
    rule_var_and_math/3,
    function_name_args_body/4,
    model_compartment/2,
    model_parameter/2,
    compartment_volume/3,
    get_reaction_id/2
  ]
).


member_listof(Item, ListOf) :-
  listOf_size(ListOf, N),
  M is N - 1,
  between(0, M, Index),
  listOf_get(ListOf, Index, Item).


model_reaction(Model, Reaction) :-
  model_getListOfReactions(Model, ListOfReactions),
  member_listof(Reaction, ListOfReactions).


model_species(Model, Species) :-
  model_getListOfSpecies(Model, ListOfSpecies),
  member_listof(Species, ListOfSpecies).


model_compartment(Model, Compartment) :-
  model_getListOfCompartments(Model, ListOfCompartments),
  member_listof(Compartment, ListOfCompartments).


model_parameter(Model, Parameter) :-
  model_getListOfParameters(Model, ListOfParameters),
  member_listof(Parameter, ListOfParameters).


model_assignment_rule(Model, Rule) :-
  model_getListOfRules(Model, ListOfRules),
  member_listof(Rule, ListOfRules),
  is_assignment(Rule).


model_function(Model, Function) :-
  model_getListOfFunctions(Model, ListOfFunctions),
  member_listof(Function, ListOfFunctions).


reaction_reactant(Reaction, Reactant) :-
  reaction_getListOfReactants(Reaction, ListOfReactants),
  member_listof(Reactant, ListOfReactants).


reaction_modifier(Reaction, Modifier) :-
  reaction_getListOfModifiers(Reaction, ListOfModifiers),
  member_listof(Modifier, ListOfModifiers).


reaction_product(Reaction, Product) :-
  reaction_getListOfProducts(Reaction, ListOfProducts),
  member_listof(Product, ListOfProducts).


reaction_kinetics(Reaction, Kinetics, LocalParameters) :-
  reaction_getKineticLaw(Reaction, KineticLaw),
  kineticLaw_getMath(KineticLaw, KineticsAtom),
  sbml_atom_to_term(KineticsAtom, Kinetics),
  kineticLaw_getListOfParameters(KineticLaw, ListOfParameters),
  findall(Parameter, member_listof(Parameter, ListOfParameters), LocalParameters).


compartment_volume(Compartment, Id, Volume) :-
  compartment_getId(Compartment, Id),
  compartment_getVolume(Compartment, Volume).


species_id_and_initial(Species, Id, Initial) :-
  species_getId(Species, Id),
  (
    species_getInitialConcentration(Species, Initial)
  ->
    true
  ;
    % FIXME handle compartment volume, maybe even units
    species_getInitialAmount(Species, Amount)
  ->
    species_getCompartment(Species, Compartment),
    Initial = Amount / Compartment
  ;
    Initial = _Unspecified
  ).


species_has_only_substance_units(Species) :-
  species_hasOnlySubstanceUnits(Species).


species_constant_and_boundary(Species, Constant, BoundaryCondition) :-
  species_getConstant(Species, Constant),
  species_getBoundaryCondition(Species, BoundaryCondition).


parameter_id_and_value(Parameter, Id, Value) :-
  parameter_getId(Parameter, Id),
  (
    parameter_getValue(Parameter, Value)
  ->
    true
  ;
    Value = _Unspecified
  ).


rule_var_and_math(Rule, Variable, Math) :-
  rule_getVariable(Rule, Variable),
  rule_getMath(Rule, MathAtom),
  sbml_atom_to_term(MathAtom, Math).


function_name_args_body(Function, Name, Args, Body) :-
  function_getName(Function, Name),
  function_getBody(Function, BodyAtom),
  sbml_atom_to_term(BodyAtom, Body),
  function_getNumArguments(Function, NumArgs),
  findall(
    Arg,
    (
      between(0, NumArgs, Index),
      function_getArgument(Function, Index, Arg)
    ),
    Args
  ).


sbml_atom_to_term(Atom, Term) :-
  read_term_from_atom(Atom, STerm, [variable_names(Names)]),
  name_variables_and_anonymous(STerm, Names),
  substitute([pow(X, Y)], [X^Y], STerm, Term).


get_reaction_id(Reaction, Id) :-
  reaction_getId(Reaction, Id).
