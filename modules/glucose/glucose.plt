:- use_module(library(plunit)).

:- use_module(glucose).

:- begin_tests(glucose).

test(
  sat,
  [
    forall(
      member(
        (Formula, Solutions),
        [
          (*([(A + ~(B)), (B + ~(A))]), [(0, 0), (1, 1)]),
          (+([(A * ~(B)), (B * ~(A)), *([A, ~(A)])]), [(0, 1), (1, 0)])
        ]
      )
    ),
    all((A, B) == Solutions)
    ]
) :-
  sat(Formula),
  labeling([A, B]).

:- end_tests(glucose).
