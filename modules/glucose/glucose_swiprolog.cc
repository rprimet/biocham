/* Originally based on the PL-SATsolver code by Amit Metodi
 * http://amit.metodi.me/research/plsatsolver/ implementing the ideas of the
 * article: Logic Programming with Satisfiability
 * Michael Codish, Vitaly Lagoon and Peter Stuckey
 * Theory and Practice of Logic Programming; 8 (1): 121-128, 2008 */

#include <SWI-Prolog.h>
#include <stdio.h>
#include <assert.h>

#include "Solver.h"

#define val(i) ((s->model[i] != l_Undef) ? ((s->model[i]==l_True)? i+1:-1*(i+1)):0)

#define PL_check(result) \
    if (!(result)) { \
        PL_fail; \
    }

Glucose::Solver   *s = NULL;
int               seed = 0;

static foreign_t
pl_default_seed(term_t pl_seed) {
   PL_check(PL_get_integer(pl_seed,&seed));
   PL_succeed;
}

static foreign_t
pl_new_solver() {
   if (s) {
      delete s;
      s = NULL;
   }
   s = new Glucose::Solver;
   if (seed != 0) {
      s->random_seed = seed;
   }
   PL_succeed;
}

static foreign_t
pl_delete_solver() {
   if (s) {
      delete s;
      s = NULL;
   }
   PL_succeed;
}

static inline Glucose::Lit pl2lit(term_t pl_literal)
{
   int pl_lit_int, var;
   assert(PL_get_integer(pl_literal,&pl_lit_int));
   var = abs(pl_lit_int)-1;
   while (var >= s->nVars()) s->newVar();
   return Glucose::mkLit(var,!(pl_lit_int > 0));
}


static foreign_t
pl_add_clause(term_t l) {
   term_t head = PL_new_term_ref();      /* variable for the elements */
   term_t list = PL_copy_term_ref(l);    /* copy as we need to write */

   Glucose::vec<Glucose::Lit> lits;

   while (PL_get_list(list, head, list)) {
      lits.push( pl2lit(head) );
   }

   assert(PL_get_nil(list));

   if (s->addClause(lits)) PL_succeed; else PL_fail;
}


static foreign_t pl_solve() {
   if (s->solve()) PL_succeed; else PL_fail;
}


static foreign_t
pl_get_var_assignment(term_t var, term_t res) {
   int i;

   PL_check(PL_get_integer(var,&i));
   i--;

   if (i < s->nVars()) {
      return PL_unify_integer(res,val(i));
   } else {
      PL_fail;
   }
}


static foreign_t
pl_get_model(term_t model) {
   term_t l = PL_new_term_ref();
   term_t pl_lit = PL_new_term_ref();
   int pl_lit_int;

   PL_check(PL_put_nil(l));

   int i=s->nVars();
   while (--i >= 0) {
      PL_check(PL_put_integer(pl_lit,val(i)));
      PL_check(PL_cons_list(l, pl_lit, l));
   }

   return PL_unify(model,l);
}


static foreign_t
pl_assign_model(term_t asgnTo) {
   term_t asgnList = PL_copy_term_ref(asgnTo);    /* copy as we need to write */
   term_t asgnVar = PL_new_term_ref();      /* variable for the elements */

   int indx=0;
   while (PL_get_list(asgnList, asgnVar, asgnList)) {
      if (s->model[indx]==l_True) {
         PL_check(PL_unify_integer(asgnVar,1));
      } else {
         PL_check(PL_unify_integer(asgnVar,-1));
      }
      indx++;
   }

   PL_succeed;
}


static foreign_t
pl_nvars(term_t res) {
   return PL_unify_integer(res,s->nVars());
}



PL_extension glucose_predicates[] =
{
   { "sat_new_solver",         0, (pl_function_t) pl_new_solver,         0 },
   { "sat_delete_solver",      0, (pl_function_t) pl_delete_solver,      0 },
   { "sat_add_clause",         1, (pl_function_t) pl_add_clause,         0 },
   { "sat_solve",              0, (pl_function_t) pl_solve,              0 },
   { "sat_get_var_assignment", 2, (pl_function_t) pl_get_var_assignment, 0 },
   { "sat_get_model",          1, (pl_function_t) pl_get_model,          0 },
   { "sat_assign_model",       1, (pl_function_t) pl_assign_model,       0 },
   { "sat_nvars",              1, (pl_function_t) pl_nvars,              0 },
   { "sat_default_seed",       1, (pl_function_t) pl_default_seed,       0 },
   { NULL,                     0, NULL,                   0 }
};

extern "C" install_t
install_glucose_swiprolog() {
   PL_register_extensions(glucose_predicates);
}
