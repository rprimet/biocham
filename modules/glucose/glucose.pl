:- module(
  glucose,
  [
    % Public API
    sat/1,
    labeling/1
  ]
).


:- (
  % If we are in the swipl-biocham binary with the C++ code already compiled
  % in, do nothing
  current_predicate(sat_new_solver/0)
->
  devdoc(
    'The idea is to provide access to the Glucose SAT solver through an
    interface identical with that of the library(clpb) solver
    (except that we do not provide taut/2).'
  )
;
  % If we are only running local unit-tests, load the foreign extension
  use_foreign_library(foreign(glucose_swiprolog))
).


:- dynamic(has_solver/0).


sat(C) :-
  (
    has_solver
  ->
    true
  ;
    sat_new_solver,
    nb_setval(glucose_maxindex, 1),
    assertz(has_solver)
  ),
  (
    add_cnf_clauses(C)
  ->
    true
  ;
    cleanup_and_fail
  ).


labeling(Vars) :-
  (
    sat_solve
  ->
    (
      sat_get_model(Model),
      assign_solution(Vars, Model),
      nb_setval(last_model, Model)
    ;
      nb_getval(last_model, LastModel),
      add_neg_clause(LastModel),
      labeling(Vars)
    )
  ;
    cleanup_and_fail
  ).


add_neg_clause(Model) :-
  maplist(neg, Model, NoAsgn),
  (
    sat_add_clause(NoAsgn)
  ->
    true
  ;
    cleanup_and_fail
  ).


cleanup_and_fail :-
  sat_delete_solver,
  retractall(has_solver),
  fail.


assign_solution([], _).

assign_solution([V | L], Model) :-
  get_attr(V, glucose, I),
  !,
  nth1(I, Model, M),
  (
    M > 0
  ->
    V = 1
  ;
    V = 0
  ),
  assign_solution(L, Model).

assign_solution([_ | L], Model) :-
  assign_solution(L, Model).


add_cnf_clauses(Cnf) :-
  term_variables(Cnf, FVars),
  set_indices(FVars),
  % Succeed but do not bind variables
  \+ \+ (
    % replace each var by its index
    maplist(bind_to_index, FVars),
    bool_to_cnf(Cnf, CNF),
    add_bound_cnf_clauses(CNF)
  ).


set_indices([]).

set_indices([V | L]) :-
  (
    get_attr(V, glucose, _)
  ->
    true
  ;
    nb_getval(glucose_maxindex, I),
    put_attr(V, glucose, I),
    J is I + 1,
    nb_setval(glucose_maxindex, J)
  ),
  set_indices(L).


bind_to_index(V) :-
  get_attr(V, glucose, V).


attr_unify_hook(_, _).


add_bound_cnf_clauses(L) :-
  maplist(to_minisat, L, ML),
  forall(member(C, ML), sat_add_clause(C)).

to_minisat(L, ML) :-
  maplist(is, ML, L).


neg(X, Y) :-
  Y is -(X).


% FIXME seems redundant with DNF code in qual_files.pl
bool_to_cnf(X * Y, C) :-
  !,
  bool_to_cnf(*([X, Y]), C).

bool_to_cnf(*(L), C) :-
  !,
  maplist(bool_to_cnf, L, LC),
  foldl(append, LC, [], C).

bool_to_cnf(X + Y, C) :-
  !,
  bool_to_cnf(+([X, Y]), C).

bool_to_cnf(+(L), C) :-
  !,
  maplist(bool_to_cnf, L, LC),
  findall(
    Clause,
    (
      maplist(contains, LC, Clauses),
      foldl(append, Clauses, [], Clause)
    ),
    C
  ).

bool_to_cnf(~(X * Y), C) :-
  !,
  bool_to_cnf(~(X) + ~(Y), C).

bool_to_cnf(~(*(L)), C) :-
  !,
  maplist(bool_neg, L, NL),
  bool_to_cnf(+(NL), C).

bool_to_cnf(~(X + Y), C) :-
  !,
  bool_to_cnf(~(X) * ~(Y), C).

bool_to_cnf(~(+(L)), C) :-
  !,
  maplist(bool_neg, L, NL),
  bool_to_cnf(*(NL), C).

bool_to_cnf(~(~(X)), C) :-
  !,
  bool_to_cnf(X, C).

bool_to_cnf(~(X), [[-(X)]]) :-
  !.

bool_to_cnf(X, [[X]]).


bool_neg(X, ~(X)).


contains(L, E) :-
  member(E, L).


is_taut(L) :-
  member(X, L),
  member(~(X), L),
  !.


bool_simplify(CNF, C) :-
  list_to_set(CNF, CNF2),
  exclude(is_taut, CNF2, C).
